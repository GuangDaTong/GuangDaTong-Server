package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(arr[:5])
	fmt.Println(arr[0:5])
	fmt.Println(arr[1:])
	fmt.Println(arr[:1])
}
