package main

import (
	"fmt"

	"github.com/ylywyn/jpushclient"
)

const (
	appKey = "ed6a976d18df50114238bd07"
	secret = "4f5de741acc27f725028c14e"
)

func main() {
	notice := &jpushclient.Notice{}
	notice.SetAlert("alert_test")
	//notice.SetAndroidNotice(&jpushclient.AndroidNotice{Alert: "AndroidNotice"})
	//notice.SetIOSNotice(&jpushclient.IOSNotice{Alert: "IOSNotice"})

	platform := &jpushclient.Platform{}
	platform.All()
	audience := &jpushclient.Audience{}
	audience.All()

	payload := jpushclient.NewPushPayLoad()
	payload.SetOptions(&jpushclient.Option{ApnsProduction: false})
	payload.SetPlatform(platform)
	payload.SetAudience(audience)
	payload.SetNotice(notice)
	payload.SetMessage(&jpushclient.Message{Title: "hello", Content: "aldkjflaskdj"})

	bytes, err := payload.ToBytes()
	if err != nil {
		panic(err.Error())
	}

	client := jpushclient.NewPushClient(secret, appKey+"a")
	result, err := client.Send(bytes)
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println(result)
	}
}
