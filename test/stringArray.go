package main

import (
	"errors"
	"fmt"
	"logs"
	"sync"
	"time"
	"utils/pgsql"

	"github.com/ylywyn/jpushclient"
)

type test struct {
	arr []string
}

func (t *test) appendStr(str string) {
	t.arr = append(t.arr, str)
}
func main() {
	a := &test{}
	b := &test{}
	a.appendStr("aaa")
	b.appendStr("bbb")
	fmt.Println("a:", a, "b:", b)
	Init()
	pushAdpotComment.appendID("aaa")
	pushAskInformed.appendID("bbb")
	pushCommentAsk.appendID("ccc")
	pushCommentAskInformed.appendID("ddd")
	time.Sleep(time.Second * 20)
}

type pushInfo struct {
	lock     sync.Mutex
	IDs      []string
	alert    string
	interval time.Duration //发送时间间隔
}

var (
	pushLeaveWords         = &pushInfo{alert: "您收到一条留言", interval: time.Second * 1}
	pushAdpotComment       = &pushInfo{alert: "您的回复被采纳啦", interval: time.Second * 10}
	pushCommentAsk         = &pushInfo{alert: "有人回复了您的问问", interval: time.Second * 10}
	pushCommentUser        = &pushInfo{alert: "有人@您", interval: time.Second * 10}
	pushGiveCoins          = &pushInfo{alert: "有人赠送了您喵币", interval: time.Second * 10}
	pushWantCoins          = &pushInfo{alert: "有人向您索要喵币", interval: time.Second * 2}
	pushContactAdded       = &pushInfo{alert: "有人添加您为联系人", interval: time.Second * 10}
	pushAskInformed        = &pushInfo{alert: "有人举报了您的问问", interval: time.Second * 10}
	pushUserInformed       = &pushInfo{alert: "有人举报了您", interval: time.Second * 10}
	pushCommentAskInformed = &pushInfo{alert: "有人举报了您的问问评论", interval: time.Second * 10}
)

func (p *pushInfo) appendUserID(userID string) {
	regID, err := getRegID(userID)
	if err != nil {
		logs.ErrorPrint("getRegID error:", err.Error())
		return
	}
	pushLeaveWords.appendID(regID)
}

func (p *pushInfo) appendID(id string) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.IDs = append(p.IDs, id)
	if len(p.IDs) >= 1000 {
		go Push(p.alert, p.IDs)
		p.IDs = nil
	}
}

func (p *pushInfo) push() {
	//logs.DebugPrint("push:", p.IDs, p.alert)
	time.AfterFunc(p.interval, p.push)
	p.lock.Lock()
	defer p.lock.Unlock()
	if p.IDs == nil {
		return
	}
	go Push(p.alert, p.IDs)
	p.IDs = nil
}

func Push(alert string, IDs []string) {
	logs.DebugPrint("Push:", IDs, alert)
	audience := &jpushclient.Audience{}
	if IDs != nil {
		audience.SetID(IDs)
	} else {
		audience.All()
	}

	notice := &jpushclient.Notice{}
	notice.SetAlert(alert)
	platform := &jpushclient.Platform{}
	platform.All()

	payload := jpushclient.NewPushPayLoad()
	payload.SetOptions(&jpushclient.Option{ApnsProduction: false})
	payload.SetPlatform(platform)
	payload.SetAudience(audience)
	payload.SetNotice(notice)
	//payload.SetMessage()

	bytes, err := payload.ToBytes()
	if err != nil {
		logs.ErrorPrint(err.Error())
		return
	}

	client := jpushclient.NewPushClient(secret, appKey)
	result, err := client.Send(bytes)
	if err != nil {
		logs.ErrorPrint(err.Error(), result)
		return
	}
}

//推送留言
func PushLeaveWords(userID string) {
	pushLeaveWords.appendUserID(userID)
}

//推送采纳回答
func PushAdpotComment(userID string) {
	pushAdpotComment.appendUserID(userID)
}

//推送回复问问
func PushCommentAsk(userID string) {
	pushCommentAsk.appendUserID(userID)
}

//推送在问问里评论别人
func PushCommentUser(userID string) {
	pushCommentUser.appendUserID(userID)
}

//推送送喵币
func PushGiveCoins(userID string) {
	pushGiveCoins.appendUserID(userID)
}

//推送索要喵币
func PushWantCoins(userID string) {
	pushWantCoins.appendUserID(userID)
}

//推送被添加联系人
func PushContactAdded(userID string) {
	pushContactAdded.appendUserID(userID)
}

//推送问问被举报
func PushAskInformed(userID string) {
	pushAskInformed.appendUserID(userID)
}

//推送人被举报
func PushUserInformed(userID string) {
	pushUserInformed.appendUserID(userID)
}

//推送问问评论被举报
func PushCommentAskInformed(userID string) {
	pushCommentAskInformed.appendUserID(userID)
}

func getRegID(userID string) (string, error) {
	rows, err := pgsql.Query(`select regID from userInfo where userID = $1`, userID)
	if err != nil {
		return "", err
	}
	var regID string
	if !rows.Next() {
		return "", errors.New("no rows")
	}
	err = rows.Scan(&regID)
	return regID, err
}

func PushUrgent() {

}

func InitPush() {
	logs.DebugPrint(
		pushLeaveWords,
		pushAdpotComment,
		pushCommentAsk,
		pushCommentUser,
		pushGiveCoins,
		pushWantCoins,
		pushContactAdded,
		pushAskInformed,
		pushUserInformed,
		pushCommentAskInformed,
	)
	pushAdpotComment.push()
	pushLeaveWords.push()
	pushCommentAsk.push()
	pushCommentUser.push()
	pushGiveCoins.push()
	pushWantCoins.push()
	pushContactAdded.push()
	pushAskInformed.push()
	pushUserInformed.push()
	pushCommentAskInformed.push()
	logs.DebugPrint(
		pushLeaveWords,
		pushAdpotComment,
		pushCommentAsk,
		pushCommentUser,
		pushGiveCoins,
		pushWantCoins,
		pushContactAdded,
		pushAskInformed,
		pushUserInformed,
		pushCommentAskInformed,
	)
}
