package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	appKey       = "13b58bff96976aedd5307f6c"
	secret       = "11e3fda2975dc97d154e02cf"
	url          = "https://api.jpush.cn/v3/push"
	BASE64_TABLE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	auth         = "ZWQ2YTk3NmQxOGRmNTAxMTQyMzhiZDA3OjRmNWRlNzQxYWNjMjdmNzI1MDI4YzE0ZQ=="
)

func main() {
	auth := base64.NewEncoding(BASE64_TABLE).EncodeToString([]byte(appKey + ":" + secret))
	fmt.Println(auth)
	req, err := http.NewRequest("POST", url, strings.NewReader(`
{
   "platform": "all",
	"audience" : "all",
   "notification" : {
      "alert" : "Hi, JPushaaaaaaaaaaaaaaaa!"
   },
        "message": {
        "msg_content": "Hi,JPush",
        "content_type": "text",
        "title": "msg",
        "extras": {
            "key": "value"
        }
	},
	    "sms_message":{
        "content":"sms msg content",
        "delay_time":3600
    },
    "options": {
		"sendno": 1000,
        "time_to_live": 60,
        "apns_production": true,
        "apns_collapse_id":"jiguang_test_201706011100"
    }
}
	`))
	if err != nil {
		panic(err.Error())
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Basic "+auth)
	resp, err := http.DefaultClient.Do(req)
	fmt.Println(resp.StatusCode)
	if err != nil {
		panic(err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(string(body))
	fmt.Println(resp.Header)
}
