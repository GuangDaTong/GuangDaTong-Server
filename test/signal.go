package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {

	signalChan := make(chan os.Signal)

	// 监听指定信号
	signal.Notify(
		signalChan,
		syscall.SIGHUP,
		syscall.SIGUSR2,
		syscall.SIGKILL,
	)

	// 输出当前进程的pid
	fmt.Println("pid is: ", os.Getpid())
	go func() {
		for {
			time.Sleep(time.Second * 1)
			signalChan <- nil
		}
	}()

	// 处理信号
	for {
		sig := <-signalChan
		fmt.Println("get signal: ", sig)
	}
}
