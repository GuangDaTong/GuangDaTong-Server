package main

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"sync"
	"time"
)

func main() {
	start := time.Now()
	lock := sync.RWMutex{}
	file, err := os.Create("json.txt")
	if err != nil {
		panic(err.Error())
	}
	runtime.GOMAXPROCS(runtime.NumCPU())
	for j := 0; j < 20; j++ {
		go func() {
			lock.RLock()
			defer lock.RUnlock()
			for i := 0; i < 50000; i++ {
				_, err := json.Marshal(map[string]interface{}{
					"laskdjf": "lksadjfl",
					"lksajdf": 20,
					"erlktj": map[string]interface{}{
						"aaslkdjf":       "asldkjfklasdjf",
						"lsadkjflksdjfj": 10000,
						"sdlkafjklsdj":   false,
					},
				})
				if err != nil {
					panic(err.Error())
				}
				fmt.Fprintln(file, runtime.NumGoroutine())
			}
		}()
	}
	lock.Lock()
	defer lock.Unlock()
	fmt.Println(time.Since(start))
}
