package main

import (
	"bufio"
	. "fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"unsafe"
)

func main() {
	var t interface{} = nil
	Println("tmp:",t.(string))
	testAddDataToResult()
	var a interface{}
	Println(&a)
	v := reflect.ValueOf(&a)
	Println(v.CanSet())
	Println(v.Elem())
	Println(v.Elem().CanSet())
	Println(v.Elem().Addr())
	var b interface{}
	b = map[string]interface{}{}
	v.Elem().SetPointer(unsafe.Pointer(&b))
	Println(v)
	v.Elem().SetFloat(7.1)
	Println(a)
	//v.SetFloat(7.1) // Error: will panic.
	tmp := reflect.ValueOf(&a).Elem()
	typeOfTmp := tmp.Type()
	Println("name:", typeOfTmp.Name())
	Println(typeOfTmp.Key())
	buf := bufio.NewReader(strings.NewReader("aaaaaaa\x00 a a a aaaa aaaa"))
	for {
		s, err := buf.ReadString('\x00')
		if err == io.EOF {
			Println(s)
			break
		}
		Println(string(s))
	}
	buf = bufio.NewReader(os.Stdout)
	for i := 0; i < 10; i++ {
		Fprintln(os.Stdout, "aaaaaaa\x00aaaaaaaa")
		s, err := buf.ReadString('\x00')
		println(s)
		if err == io.EOF {
			break
		}
	}
}

func testAddDataToResult() {
	var a map[string]interface{}
	Println(a, &a)
	addDataToResult(a, "aa", "bb")
	Println(a, &a)
}
func addDataToResult(result map[string]interface{}, key string, val interface{}) {
	if result == nil {
		Println("nil result:", result, &result)
		result = map[string]interface{}{key: val}
		//result[key] = val
		Println("nil result:", result, &result)
		return
	}
	result[key] = val
	Println("result:", result, &result)
}
