package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan bool, 3)
	go func() {
		for {
			a, ok := <-ch
			if !ok {
				fmt.Println("not close:", ok)
				return
			}
			fmt.Println(a)
		}
	}()
	go func() {
		for {
			if ch == nil {
				fmt.Println("chan has been close")
			}
			ch <- true
			time.Sleep(1 * time.Second)
		}
	}()
	close(ch)
	fmt.Println("close")
	time.Sleep(10 * time.Second)
}
