package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	start := time.Now()
	mu := sync.Mutex{}
	for i := 0; i < 1000000; {
		mu.Lock()
		i++
		mu.Unlock()
	}
	fmt.Println(time.Since(start))
	start = time.Now()
	ch := make(chan *bool, 1)
	for i := 0; i < 1000000; {
		ch <- nil
		i++
		<-ch
	}
	fmt.Println(time.Since(start))
}
