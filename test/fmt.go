package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	file, err := os.Create("test.log")
	if err != nil {
		panic(err.Error())
	}
	start := time.Now()
	for i := 0; i < 1000000; i++ {
		now := time.Now()
		nowStr := fmt.Sprintf("%d-%d-%d %02d:%02d:%02d", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
		fmt.Fprintln(file, nowStr, "200", "success", "", "laksjdflkjsdal")
	}
	fmt.Println("time:", time.Since(start))
}
