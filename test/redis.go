/*package main

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

func main() {
	start := time.Now()
	client := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	err := client.Ping().Err()
	if err != nil {
		panic(err.Error())
	}
	for i := 0; i < 1000000; i++ {
		now := time.Now()
		nowStr := fmt.Sprintf("%d-%d-%d %02d:%02d:%02d", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
		client.LPush("log", nowStr+" 200 success laskdjflaskjdflkjasdl")
	}
	fmt.Println(time.Since(start))
}
*/
package main

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"time"
	"utils/mewRedis"
)

func main() {
	c := mewRedis.GetClient()
	reply, err := c.Do("SET", "a", "b")
	if err != nil {
		panic(err.Error())
	}
	start := time.Now()
	for i := 0; i < 10000; i++ {
		c.Do("GET", "a")
	}
	fmt.Println(time.Since(start))
	fmt.Println(reply)
	ok, err := redis.Bool(reply, nil)
	fmt.Println(ok)
	reply, err = c.Do("GET", "a")
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(reply)
	s, err := redis.String(reply, err)
	fmt.Println(s, err)
	reply, err = c.Do("GET", "lskdjaflkj")
	fmt.Println(reply, err)
	reply, err = c.Do("DEL", "a")
	fmt.Println(reply, err)
	err = c.Send("MULTI")
	fmt.Println(err)
	err = c.Send("HMSET", "a", "aa", "bb", "cc", "dd")
	fmt.Println(err)
	err = c.Send("HGET", "a", "aa")
	fmt.Println(err)
	reply, err = c.Do("EXEC")
	fmt.Println(reply, err)
	fmt.Printf("%T\n", reply)
	for _, val := range reply.([]interface{}) {
		fmt.Printf("%T:%v\n", val, val)
	}
	strs, err := redis.Strings(reply, err)
	fmt.Println(strs, err)
	reply, err = c.Do("HGETALL", "a")
	fmt.Println(reply, err)
	sm, err := redis.StringMap(reply, err)
	fmt.Println(sm, err)
	go func() {
		c := mewRedis.GetClient()
		reply, err := c.Do("HMGET", "a", "aa", "cc")
		fmt.Println("go", reply, err)
	}()
	c.Send("MULTI")
	c.Send("DEL", "a")
	time.Sleep(1 * 1e5)
	reply, err = c.Do("EXEC")
	fmt.Println(reply, err)
	time.Sleep(time.Second)

	c.Send("HMSET", "album:1", "title", "Red", "rating", 5)
	c.Send("HMSET", "album:2", "title", "Earthbound", "rating", 1)
	c.Send("HMSET", "album:3", "title", "Beat", "rating", 4)
	c.Send("LPUSH", "albums", "1")
	c.Send("LPUSH", "albums", "2")
	c.Send("LPUSH", "albums", "3")
	values, err := redis.Values(c.Do("SORT", "albums",
		"BY", "album:*->rating",
		"GET", "album:*->title",
		"GET", "album:*->rating"))
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(values)

	var albums []struct {
		Title  string
		Rating int
	}
	if err := redis.ScanSlice(values, &albums); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%v\n", albums)
}
