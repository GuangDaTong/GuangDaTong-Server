package main

import (
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/lib/pq"
)

func openDB() *sql.DB {
	var (
		host     = "localhost"
		port     = "5432"
		user     = "mew"
		dbname   = "mew"
		sslmode  = "disable"
		password = "mewmewstudio"
	)
	postgrelConfig := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s",
		host, port, user, dbname, sslmode)
	db, err := sql.Open(`postgres`, postgrelConfig+" password="+password)
	if err != nil {
		panic(err.Error())
	}
	return db
}
func main() {
	err := deal()
	if err != nil {
		panic(err.Error())
	}
}
func deal() (err error) {
	db := openDB()
	tx, err := db.Begin()
	if err != nil {
		panic(err.Error())
	}
	defer func() {
		if err != nil {
			panic(err.Error())
		}
	}()
	defer func() {
		if err != nil {
			err = tx.Rollback()
			return
		}
		err = tx.Commit()
		return
	}()
	rows, err := tx.Query("select id from users where id > 100000103")
	if err != nil {
		return err
	}
	var IDs []string
	for rows.Next() {
		var id string
		err := rows.Scan(&id)
		if err != nil {
			return err
		}
		IDs = append(IDs, id)
	}
	rows.Close()
	fmt.Println(IDs)
	for _, val := range IDs {
		rows, err := tx.Query(`select amount from accountBook where userid = $1`, val)
		if err != nil {
			return err
		}
		sum := 0
		for rows.Next() {
			var amount int
			err = rows.Scan(&amount)
			if err != nil {
				return err
			}
			sum += amount
		}
		rows.Close()
		fmt.Println(sum, val)
		sqlResult, err := tx.Exec(`update users set coins = $1 where id = $2`, sum, val)
		if err != nil {
			return err
		}
		affect, err := sqlResult.RowsAffected()
		if err != nil {
			return err
		}
		if affect != 1 {
			return errors.New("affect not 1")
		}
	}
	return nil
}
