package main

import (
	"context"
	"fmt"
	"logs"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/qiniu/api.v7/auth/qbox"
	"github.com/qiniu/api.v7/storage"
)

var (
	accessKey = "MYqxCWi4Nrv114F4LeLaD9ekYTgnNdgvrBkyVvRS"
	secretKey = "rHRBY7WTffwK5Na064oVm33sLKyg-Efph3KllhEa"
	fileDir   = "../src/debugPublic"
)

var lock = sync.RWMutex{}

func main() {
	prefetch()
	//broweDir(fileDir)
	lock.Lock()
	lock.Unlock()
}
func saveFile() {
	mac := qbox.NewMac(accessKey, secretKey)
	cfg := storage.Config{
		// 是否使用https域名进行资源管理
		UseHTTPS: false,
		Zone:     &storage.ZoneHuanan,
	}
	// 指定空间所在的区域，如果不指定将自动探测
	// 如果没有特殊需求，默认不需要指定
	//cfg.Zone=&storage.ZoneHuabei
	bucketManager := storage.NewBucketManager(mac, &cfg)

	bucket := "public"
	//resURL := "http://devtools.qiniu.com/qiniu.png"
	resURL := "http://ou447kgyf.bkt.clouddn.com/image/default5.jpg"
	//resURL := "https://debug.guangdamiao.com/image/default4.jpg"
	//query := "?imageView2/2/w/200/h/200/format/jpg/q/1005imageslim"
	query := "?imageView2/2/w/200/h/200/format/jpg/q/100"
	query1 := "?imageView2/3/w/2000/h/2000/q/100|watermark/2/text/5bm_5aSn5Za1/font/5a6L5L2T/fontsize/600/fill/IzQ5OTBFMg==/dissolve/100/gravity/SouthEast/dx/10/dy/10|imageslim"
	//query1 := "?imageView2/0/q/100|watermark/2/text/5bm_5aSn5Za1/font/5b6u6L2v6ZuF6buR/fontsize/400/fill/IzQ5OTBFMg==/dissolve/100/gravity/SouthEast/dx/10/dy/10|imageslim"
	fetchRet, err := bucketManager.Fetch(resURL+query1, bucket, "image/default5.jpg")
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(fetchRet.String())
	// 指定保存的key
	fetchRet, err = bucketManager.Fetch(resURL+query, bucket, "thumbnailImage/default5.jpg")
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(fetchRet.String())

}
func broweDir(path string) {
	lock.RLock()
	defer lock.RUnlock()
	fmt.Println("broweDir:", path)
	dir, err := os.Open(path)
	if err != nil {
		panic(err.Error())
	}
	defer dir.Close()
	names, err := dir.Readdirnames(-1)
	if err != nil {
		panic(err.Error())
	}
	for _, name := range names {
		dirPath := path + "/" + name
		if !isDir(dirPath) {
			if updateAfter(dirPath, time.Now().Add(-time.Hour*2)) {
				fileName := strings.TrimPrefix(dirPath, fileDir+"/")
				uploadImage(dirPath, "debugpublic:"+fileName, fileName)
			}
			continue
		}
		//fmt.Println(dirPath, "is dir")
		broweDir(dirPath)
	}
}

func updateAfter(path string, t time.Time) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		panic(err.Error())
	}
	return fileInfo.ModTime().After(t)
}

func isDir(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		panic(err.Error())
	}
	return fileInfo.IsDir()
}

/*
	localFile := "qiniuyun.go"
	bucket := "public:qiniuyun.go"
	key := "qiniuyun.go"
*/
func uploadImage(localFile, bucket, key string) {

	// 自定义返回值结构体
	type MyPutRet struct {
		Key    string
		Hash   string
		Fsize  int
		Bucket string
		Name   string
	}
	// 使用 returnBody 自定义回复格式
	putPolicy := storage.PutPolicy{
		Scope:      bucket,
		ReturnBody: `{"key":"$(key)","hash":"$(etag)","fsize":$(fsize),"bucket":"$(bucket)","name":"$(x:name)"}`,
		InsertOnly: 0,
	}
	mac := qbox.NewMac(accessKey, secretKey)
	upToken := putPolicy.UploadToken(mac)
	cfg := storage.Config{}
	formUploader := storage.NewFormUploader(&cfg)
	ret := MyPutRet{}
	putExtra := storage.PutExtra{
		Params: map[string]string{
			"x:name": "github logo",
		},
	}
	err := formUploader.PutFile(context.Background(), &ret, upToken, key, localFile, &putExtra)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(ret.Bucket, ret.Key, ret.Fsize, ret.Hash, ret.Name)
}

func prefetch() {
	mac := qbox.NewMac(accessKey, secretKey)
	cfg := storage.Config{
		// 是否使用https域名进行资源管理
		UseHTTPS: true,
		Zone:     &storage.ZoneHuanan,
	}
	// 指定空间所在的区域，如果不指定将自动探测
	// 如果没有特殊需求，默认不需要指定
	//cfg.Zone=&storage.ZoneHuabei
	bucketManager := storage.NewBucketManager(mac, &cfg)
	imagePath := "icon/icon_1.jpg"
	//resURL := config.SrcHost + imagePath
	err := bucketManager.Prefetch("debugpublic", imagePath)
	if err != nil {
		logs.ErrorPrint("更新镜像缓存", imagePath, "error:", err.Error())
	}
}
