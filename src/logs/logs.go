package logs

import (
	"fmt"
	"os"
	"runtime/debug"
	"strings"
	"time"
)

var (
	test          = true
	maxBufferSize = 10000
	accessQueue   chan []interface{}
	errorQueue    chan []interface{}
	resultQueue   chan []interface{}
)

// Print 打印调用此函数的外层函数的文件和行号信息
//
// -- parent's parent func
// 		|
//		-- parent func  //打印这个函数的信息
// 			|
//			-- PrintParent
//
// PrintParent 打印调用此函数的外层函数的外层函数的文件和行号信息
//
// -- parent's parent func  //打印这个函数的信息
// 		|
//		-- parent func
// 			|
//			-- PrintParent
//
// 注意：main调用此函数，程序会崩溃！

func Init(t bool) {
	test = t
}
func init() {
	config()
}
func config() {
	fmt.Println(`logConfig: `, time.Now(), `
	test =`, test, `
	maxBufferSize =`, maxBufferSize, `
	`)
	if accessQueue == nil {
		accessQueue = make(chan []interface{}, maxBufferSize)
	}
	go log("access.log", accessQueue)
	if errorQueue == nil {
		errorQueue = make(chan []interface{}, maxBufferSize)
	}
	go log("error.log", errorQueue)
	if resultQueue == nil {
		resultQueue = make(chan []interface{}, maxBufferSize)
	}
	go log("result.log", resultQueue)
}
func log(fileName string, queue chan []interface{}) {
	var date = formatTimeToDate(time.Now())
	file, err := os.OpenFile(date+fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		panic(err.Error())
	}
	for {
		args := <-queue
		if date != formatTimeToDate(time.Now()) {
			date = formatTimeToDate(time.Now())
			file.Close()
			file, err = os.OpenFile(date+fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
			if err != nil {
				panic(err.Error())
			}
		}
		fmt.Fprintln(file, args...)
	}
}

func formatTimeToDate(t time.Time) string {
	return fmt.Sprintf("%04d%02d%02d", t.Year(), t.Month(), t.Day())
}

//输出访问记录
func AccessPrint(args ...interface{}) {
	accessQueue <- args
}

//输出code不为200的http result
func ResultPrint(args ...interface{}) {
	resultQueue <- args
}

//输出严重错误
func ErrorPrint(args ...interface{}) {

	stack := string(debug.Stack())
	stackLines := strings.Split(stack, "\n")
	debugLine := strings.SplitAfter(stackLines[6], "/")
	debugInfo := debugLine[len(debugLine)-1]
	debugInfo = debugInfo[:len(debugInfo)-6]
	a := []interface{}{debugInfo + ":", time.Now().Format("2006-01-02 15:04:05")}
	a = append(a, args...)
	errorQueue <- a
}

func ErrorPrintParent(args ...interface{}) {
	stack := string(debug.Stack())
	stackLines := strings.Split(stack, "\n")
	debugLine := strings.SplitAfter(stackLines[8], "/")
	debugInfo := debugLine[len(debugLine)-1]
	debugInfo = debugInfo[:len(debugInfo)-6]
	a := []interface{}{debugInfo + ":", time.Now().Format("2006-01-02 15:04:05")}
	a = append(a, args...)
	errorQueue <- a
}

//test模式下debug的log都输出到终端,非test模式下debug的log不输出
func DebugPrint(args ...interface{}) {
	if !test {
		return
	}

	stack := string(debug.Stack())
	stackLines := strings.Split(stack, "\n")
	debugLine := strings.SplitAfter(stackLines[6], "/")
	debugInfo := debugLine[len(debugLine)-1]
	debugInfo = debugInfo[:len(debugInfo)-6]
	a := []interface{}{debugInfo + ":", time.Now().Format("2006-01-02 15:04:05")}
	a = append(a, args...)
	fmt.Fprintln(os.Stdout, a...)
}

func DebugPrintParent(args ...interface{}) {
	if !test {
		return
	}

	stack := string(debug.Stack())
	stackLines := strings.Split(stack, "\n")
	debugLine := strings.SplitAfter(stackLines[8], "/")
	debugInfo := debugLine[len(debugLine)-1]
	debugInfo = debugInfo[:len(debugInfo)-6]
	a := []interface{}{debugInfo + ":"}
	a = append(a, args...)
	fmt.Fprintln(os.Stdout, a...)
}
