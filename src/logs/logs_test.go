package logs_test

import (
	"fmt"
	. "logs"
	"testing"
	"time"
)

func TestLogs(*testing.T) {
	Init(true)
	print()
	parent()
	for i := 0; i < 20000; i++ {
		print()
	}
	Init(false)
	print()
	parent()
	Init(true)
	print()
	parent()
	for i := 0; i < 20000; i++ {
		print()
	}
	Init(false)
	print()
	parent()

	start := time.Now()
	fmt.Print("test accessPrint:")
	for i := 0; i < 10000; i++ {
		AccessPrint("access")
	}
	fmt.Println(time.Since(start))
	start = time.Now()
	fmt.Print("test errorPrint:")
	for i := 0; i < 10000; i++ {
		ErrorPrint("skaldjflaskdj")
	}
	fmt.Println(time.Since(start))
}
func print() {
	AccessPrint("access")
	ErrorPrint("error")
	DebugPrint("debug")
}
func parent() {
	ErrorPrintParent("errorParent")
	DebugPrintParent("debugParent")
}
