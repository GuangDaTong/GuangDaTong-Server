package modal

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type RespJSON struct {
	Code     string      `json:"code"`
	Msg      string      `json:"msg"`
	DebugMsg string      `json:"debugMsg"`
	Result   interface{} `json:"result"`
}

type User struct {
	ID        string `json:"id"`
	Account   string `json:"account"`
	Nickname  string `json:"nickname"`
	Icon      string `json:"icon"`
	Password  string `json:"password"`
	Gender    string `json:"gender"`
	Coins     int    `json:"coins"`
	College   string `json:"college"`
	School    string `json:"school"`
	Exp       int    `json:"exp"`
	Level     string `json:"level"`
	Zan       string `json:"zan"`
	Grade     string `json:"grade"`
	LastVisit string `json:"lastVisit"`
}
