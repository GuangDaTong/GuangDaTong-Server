package modal

type LoginJSON struct {
	Account  string `json:"account"`
	Password string `json:"password"`
	Sign     string `json:"sign"`
}

type QQLoginJSON struct {
	UUID     string `json:"uuid"`
	Gender   string `json:"gender"`
	Nickname string `json:"nickname"`
	College  string `json:"college"`
	School   string `json:"school"`
	Sign     string `json:"sign"`
}

type IsQQRegisterJSON struct {
	UUID string `json:"uuid"`
	Sign string `json:"sign"`
}

type CheckVerifyCodeJSON struct {
	Account string `json:"phone"`
	Code    string `json:"code"`
	Sign    string `json:"sign"`
}

type CheckVersionJSON struct {
	Sign   string `json:"sign"`
	OSType string `json:"OSType"`
}
type RegisterJSON struct {
	Account  string `json:"account"`
	Password string `json:"password"`
	College  string `json:"college"`
	School   string `json:"school"`
	Sign     string `json:"sign"`
}
type IsAccountExistJSON struct {
	Account string `json:"account"`
	Sign    string `json:"sign"`
}
type ZanJSON struct {
	ID   string `json:"id"`
	Sign string `json:"sign"`
}
type ShareJSON struct {
	ObjectID string `json:"objectID"`
	Token    string `json:"token"`
	ShareWay string `json:"shareWay"`
	Sign     string `json:"sign"`
}
type ListSchoolJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	Sign      string `json:"sign"`
}

type ListCollegeJSON struct {
	School string `json:"school"`
	Sign   string `json:"sign"`
}

type SMSRespJSON struct {
	Status int    `json:"status"`
	Error  string `json:"error"`
}

type ListSchoolDataJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	Token     string `json:"token"`
	//OrderBy   string `json:"orderBy"`
	//Order     string `json:"order"`
	School string `json:"school"`
	Sign   string `json:"sign"`
}

type ListAdvertisementJSON struct {
	School string `json:"school"`
	Sign   string `json:"sign"`
}

type ResetPasswordJSON struct {
	Account  string `json:"account"`
	Password string `json:"password"`
	Sign     string `json:"sign"`
}

type ReportJSON struct {
	Description string `json:"description"`
	ObjectID    string `json:"objectID"`
	Sign        string `json:"sign"`
	Token       string `json:"token"`
}

type ListDiscoveryDataJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	Token     string `json:"token"`
	OrderBy   string `json:"orderBy"`
	Order     string `json:"order"`
	School    string `json:"school"`
	Sign      string `json:"sign"`
}

type PushInfoJSON struct {
	RegID    string `json:"regID"`
	UserID   string `json:"userID"`
	Sign     string `json:"sign"`
	OSType   string `json:"OSType"` //ios|android
	Country  string `json:"country"`
	Province string `json:"province"`
	City     string `json:"city"`
	District string `json:"district"`
	County   string `json:"county"`
	Street   string `json:"street"`
	SiteName string `json:"siteName"`
	Version  string `json:"version"`
}

type ListAskJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	OrderBy   string `json:"orderBy"`
	Order     string `json:"order"`
	Range     string `json:"range"`
	Token     string `json:"token"`
	Sign      string `json:"sign"`
}

type ListSearchAskJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	OrderBy   string `json:"orderBy"`
	Order     string `json:"order"`
	Range     string `json:"range"`
	Key       string `json:"key"`
	Type      string `json:"type"`
	Label     string `json:"label"`
	Token     string `json:"token"`
	Sign      string `json:"sign"`
}

type DataAskJSON struct {
	ID    string `json:"id"`
	Token string `json:"token"`
	Sign  string `json:"sign"`
}

type DeleteAskJSON struct {
	IDs  []string `json:"IDs"`
	Sign string   `json:"sign"`
}

type ListCommentAskJSON struct {
	ID        string `json:"id"`
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	OrderBy   string `json:"orderBy"`
	Order     string `json:"order"`
	Sign      string `json:"sign"`
}

type AdoptAskJSON struct {
	AskID     string `json:"askID"`
	CommentID string `json:"commentID"`
	Sign      string `json:"sign"`
}

type FavoriteJSON struct {
	ID   string `json:"id"`
	Sign string `json:"sign"`
}

type ListFavoriteJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	OrderBy   string `json:"orderBy"`
	Order     string `json:"order"`
	Sign      string `json:"sign"`
}

type InformJSON struct {
	ID     string `json:"id"`
	Reason string `json:"reason"`
	Sign   string `json:"sign"`
}

type AlterPasswordJSON struct {
	OldPwd string `json:"oldPwd"`
	NewPwd string `json:"newPwd"`
	Sign   string `json:"sign"`
}

type CommentAskJSON struct {
	ID                string `json:"id"`
	Content           string `json:"content"`
	CommentReceiverID string `json:"commentReceiverID"`
	Sign              string `json:"sign"`
}

type AdviseJSON struct {
	Description string `json:"description"`
	Token       string `json:"token"`
}

type BusinessJSON struct {
	Gender      string `json:"gender"`
	Name        string `json:"name"`
	Phone       string `json:"phone"`
	Description string `json:"description"`
	Email       string `json:"email"`
	QQ          string `json:"qq"`
	WeChat      string `json:"weChat"`
	Company     string `json:"company"`
	Token       string `json:"token"`
}

type AccountBookJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	OrderBy   string `json:"orderBy"`
	Order     string `json:"order"`
	Sign      string `json:"sign"`
}

type DataSelfJSON struct {
	Sign string `json:"sign"`
}

type LogoutJSON struct {
	Sign string `json:"sign"`
}

type ListMyAskJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	OrderBy   string `json:"orderBy"`
	Order     string `json:"order"`
	Sign      string `json:"sign"`
}

type DataPersonJSON struct {
	Token string `json:"token"`
	ID    string `json:"id"`
	Sign  string `json:"sign"`
}

type DeleteFavoriteJSON struct {
	IDs  []string `json:"IDs"`
	Sign string   `json:"sign"`
}

type AddFriendJSON struct {
	FriendID string `json:"friendID"`
	Sign     string `json:"sign"`
}

type SignUpJSON struct {
	ID                 string `json:"id"`
	Name               string `json:"name"`
	Phone              string `json:"phone"`
	ParticipationCount string `json:"participationCount"`
	QQ                 string `json:"qq"`
	WeChat             string `json:"weChat"`
	Sign               string `json:"sign"`
}

type ListFriendsJSON struct {
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
	Sign      string `json:"sign"`
}

type DeleteFriendsJSON struct {
	IDs  []string `json:"IDs"`
	Sign string   `json:"sign"`
}

type DeleteCommentAskJSON struct {
	ID   string `json:"id"`
	Sign string `json:"sign"`
}

type LeaveWordsJSON struct {
	ReceiverID string `json:"receiverID"`
	Words      string `json:"words"`
	Sign       string `json:"sign"`
}
type ContactServiceJSON struct {
	Words    string `json:"words"`
	Sign     string `json:"sign"`
	ObjectID string `json:"objectID"`
}

type ListMessageJSON struct {
	Sign string `json:"sign"`
}

type WantCoinsJSON struct {
	GiverID string `json:"giverID"`
	Amount  string `json:"amount"`
	Words   string `json:"words"`
	Sign    string `json:"sign"`
}

type GiveCoinsJSON struct {
	ReceiverID string `json:"receiverID"`
	Amount     string `json:"amount"`
	Words      string `json:"words"`
	Sign       string `json:"sign"`
}

type ClearMessage struct {
	Sign string `json:"sign"`
}

type CrashLogJSON struct {
	Log       string `json:"log"`
	Sign      string `json:"sign"`
	IsRelease bool   `json:"isRelease"`
	Version   string `json:"version"`
}

type GetCoinsAndRulesJSON struct {
	Sign string `json:"sign"`
}

type SetPushJSON struct {
	Sign          string `json:"sign"`
	UrgentCollege bool   `json:"urgentCollege"`
	UrgentSchool  bool   `json:"urgentSchool"`
	Comment       bool   `json:"comment"`
	LeaveWords    bool   `json:"leaveWords"`
	Activity      bool   `json:"activity"`
	System        bool   `json:"system"`
}

type ListUserDataJSON struct {
	Sign string   `json:"sign"`
	IDs  []string `json:"IDs"`
}

type LoginServiceJSON struct {
	Sign     string `json:"sign"`
	Account  string `json:"account"`
	Password string `json:"password"`
	RegID    string `json:"regID"`
	OSType   string `json:"OSType"`
}

type ListServiceMessageJSON struct {
	Sign string `json:"sign"`
}

type ServiceReplyJSON struct {
	Sign       string `json:"sign"`
	Words      string `json:"words"`
	ReceiverID string `json:"receiverID"`
}

type ListBlacklistJSON struct {
	Sign string `json:"sign"`
}

type BlacklistJSON struct {
	Sign   string `json:"sign"`
	UserID string `json:"userID"`
}

type DeleteBlacklistJSON struct {
	Sign string   `json:"sign"`
	IDs  []string `json:"IDs"`
}

type ListSearchUsersJSON struct {
	Sign      string `json:"sign"`
	Key       string `json:"key"`
	PageIndex int64  `json:"pageIndex"`
	PageSize  int64  `json:"pageSize"`
}
