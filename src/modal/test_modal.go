package modal

import (
	"bytes"
	"config"
	"encoding/json"
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
	"logs"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"
	"path/filepath"
	"strings"
	"time"
	"utils/pgsql"
)

type TestJSON struct {
	Name        string
	Method      string
	Pattern     string
	SetExample  func() []TestExample
	Test        func(*TestJSON)
	TestExample []TestExample
}

type TestExample struct {
	Request  interface{}
	Response *RespJSON
	Test     bool
	Name     string
	NotBench bool
}

var (
	Socket              = "http://127.0.0.1:7777"
	DebugExamplesPath   = "debugExamples"
	ReleaseExamplesPath = "releaseExamples"
)

func RunTestExamples(val TestExample, testJson *TestJSON, w io.Writer) {
	fmt.Fprintln(w, "\""+val.Name+"\":")
	//config.Test = val.Test
	bytes, ok := val.Request.([]byte)
	if !ok {
		panic("type assert failed")
	}
	//logs.DebugPrint(string(bytes))
	req, err := http.NewRequest(testJson.Method, Socket+testJson.Pattern, strings.NewReader(string(bytes)))
	if err != nil {
		panic(err.Error())
	}
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err.Error())
	}
	bytes, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, string(bytes))
	var res RespJSON
	err = json.Unmarshal(bytes, &res)
	if err != nil {
		panic(err.Error())
	}
	if res.Code != val.Response.Code || res.Msg != val.Response.Msg {
		fmt.Fprintln(w, res)
		fmt.Fprintln(w, val.Response)
		panic("test " + val.Name + " error")
	}
	logs.DebugPrint("test", val.Name, "ok")
}

func RunTestHtmlExamples(val TestExample, testJson *TestJSON, w io.Writer) {
	fmt.Fprintln(w, `"`+val.Name+`":`)
	uri, ok := val.Request.(string)
	if !ok {
		panic("request not string")
	}
	resp, err := http.Get(Socket + testJson.Pattern + "?id=" + uri)
	if err != nil {
		panic(err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "statusCode:", resp.StatusCode)
	fmt.Fprintln(w, string(body))
	if fmt.Sprint(resp.StatusCode) != val.Response.Code {
		panic(string(body))
	}
	logs.DebugPrint("test", val.Name, "ok")
}

type MultiPartRequest struct {
	FileName []string
	FilePath []string
	Form     map[string]string
	NotBench bool
}

type Bounds struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

func RunTestMultiPartRequest(val TestExample, testJson *TestJSON, w io.Writer) {

	example, ok := val.Request.(MultiPartRequest)
	if !ok {
		panic("MultiPartRequest type assert failed")
	}
	var filePath = example.FilePath
	var fileName = example.FileName
	var form = example.Form
	fmt.Fprintln(w, `"`+val.Name+`":`)
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	bounds := map[string]Bounds{}
	for i := 0; i < len(filePath); i++ {
		file, err := os.Open(filePath[i])
		if err != nil {
			panic(err.Error())
		}
		defer file.Close()
		//part, err := writer.CreateFormFile(fileName[i], filepath.Base(filePath[i])) //filepath.Base()返回路径的文件名
		//map[Content-Disposition:[form-data; name="image1"; filename="default1.jpg"] Content-Type:[application/octet-stream]]
		h := make(textproto.MIMEHeader)
		h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="%s"; filename="%s"`, fileName[i], filepath.Base(filePath[i])))
		var MIMEType, subfix string
		index := strings.LastIndex(filePath[i], ".")
		if index == -1 {
			panic("index == -1 ,no type")
		}
		subfix = filePath[i][index:]
		switch subfix {
		case ".png":
			MIMEType = "image/png"
		case ".gif":
			MIMEType = "image/gif"
		case ".jpg", ".jpeg":
			MIMEType = "image/jpeg"
		case ".ico":
			MIMEType = "image/x-icon"
		default:
			MIMEType = "application/octet-stream"
		}
		h.Set("Content-Type", MIMEType)

		part, err := writer.CreatePart(h)
		if err != nil {
			panic(err.Error())
		}
		_, err = io.Copy(part, file)
		if err != nil {
			panic(err.Error())
		}
		_, err = file.Seek(0, 0)
		if err != nil {
			panic(err.Error())
		}
		img, _, err := image.Decode(file)
		if err != nil {
			logs.DebugPrint(img)
			bounds[fmt.Sprintf("image%d", i+1)] = Bounds{}
		} else {
			bounds[fmt.Sprintf("image%d", i+1)] = Bounds{Width: img.Bounds().Dx(), Height: img.Bounds().Dy()}
		}
	}

	size, err := json.Marshal(bounds)
	if err != nil {
		panic(err.Error())
	}
	for key, val := range form {
		err = writer.WriteField(key, val)
		if err != nil {
			panic(err.Error())
		}
	}
	err = writer.WriteField("size", string(size))
	if err != nil {
		panic(err.Error())
	}
	err = writer.Close()
	if err != nil {
		panic(err.Error())
	}
	req, err := http.NewRequest("POST", Socket+testJson.Pattern, body)
	req.Header.Add("Content-Type", writer.FormDataContentType())
	if err != nil {
		panic(err.Error())
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err.Error())
	}
	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, string(bytes))
	var res RespJSON
	err = json.Unmarshal(bytes, &res)
	if err != nil {
		panic(err.Error())
	}
	if res.Code != val.Response.Code || res.Msg != val.Response.Msg {
		fmt.Fprintln(w, res)
		fmt.Fprintln(w, val.Response)
		panic("test " + val.Name + " error")
	}
	logs.DebugPrint("test", val.Name, "ok")
}

func ToBytes(v interface{}) []byte {
	bytes, err := json.Marshal(v)
	if err != nil {
		panic(err.Error())
	}
	return bytes
}

func RemoveExample() {
	os.RemoveAll(DebugExamplesPath)
	os.RemoveAll(ReleaseExamplesPath)
	err := os.Mkdir(DebugExamplesPath, 0777)
	if err != nil {
		panic(err.Error())
	}
	os.Mkdir(ReleaseExamplesPath, 0777)
	if err != nil {
		panic(err.Error())
	}
}

func TestRoute(InitSQL func(), examplePath *string, TestExamples []TestJSON, basePath string) {
	config.UseQiNiuYun = false
	RemoveExample()
	for i := 0; i < 2; i++ {
		config.Init(basePath + "config/mew.conf")
		if i == 0 {
			config.Test = true
			*examplePath = DebugExamplesPath
			logs.Init(true)
		} else {
			config.Test = false
			*examplePath = ReleaseExamplesPath
			logs.Init(false)
		}
		config.PublicFilePath = basePath + config.PublicFilePath
		pgsql.Init(true, basePath+"config/"+config.PgsqlConfigFile)
		InitSQL()
		for _, val := range TestExamples {
			val.TestExample = val.SetExample()
			val.Test(&val)
		}
	}
}

func TestRouteBench(InitSQL func(), examplePath *string, TestExamples []TestJSON, basePath string) {
	RemoveExample()
	config.UseJPush = false
	config.UseQiNiuYun = false
	for i := 0; i < 2; i++ {
		config.Init(basePath + "config/mew.conf")
		if i == 0 {
			config.Test = true
			*examplePath = DebugExamplesPath
			logs.Init(false)
		} else {
			config.Test = false
			*examplePath = ReleaseExamplesPath
			logs.Init(false)
		}
		config.PublicFilePath = basePath + "debugPublic/"
		pgsql.Init(true, basePath+"config/"+config.PgsqlConfigFile)
		InitSQL()
		for _, val := range TestExamples {
			start := time.Now()
			for i := 0; i < 200; i++ {
				test := val.SetExample()
				val.TestExample = nil
				for _, testVal := range test {
					if !testVal.NotBench {
						val.TestExample = append(val.TestExample, testVal)
					}
				}
				val.Test(&val)
			}
			fmt.Println(time.Since(start), val.Name)
		}
	}
}
