/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	//error config:		liteedite,Audio,imgupload,simple-ruler,uploadcare,stylesheetparser-fixed,imgur,ckeditortablecellsselection,bootstrapTable,balloonpanel,cssanim,enhancedcolorbuttton,allmedias,
	//all config:		config.extraPlugins= 'dialogui,dialog,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,button,toolbar,notification,clipboard,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,copyformatting,div,resize,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,forms,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,tableselection,undo,wsc,lineutils,widgetselection,widget,filetools,notificationaggregator,uploadwidget,uploadimage,about,maxheight,oembed,mediaembed,embedbase,tliyoutube,nbsp,noabp,tliyoutube2,numericinput,mrmonkey,onchange,embed,pastecode,pastefromexcel,pasteFromGoogleDoc,osem_googlemaps,pastebase64,page2images,lightbox,liveedit,lite,lineheight,loremipsum,locationmap,leaflet,letterspacing,basewidget,layoutmanager,markdown,mathjax,niftyimages,placeholder,placeholder_select,performx,qrc,quran,resizewithwindow,removespan,replaceTagNameByBsquochoai,embedsemantic,scribens,quicktable,selectallcontextmenu,ccmssourcedialog,Audio,html5validation,iframedialog,html5video,imgupload,fastimage,eqneditor,fixed,FMathEditor,floating-tools,format_buttons,glossary,wlpheix,gg,ckeditor-gwf-plugin,xmas,htmlbuttons,googlethisterm,googledocs,imgbrowse,imagebrowser,imageresize,imagerotate,imageCustomUploader,imageuploader,imagepaste,imgur,inlinecancel,insertpre,closebtn,internallink,textselection,inserthtml4x,symbol,mathedit,codesnippet,prism,sharedspace,showprotected,simplebutton,html5audio,simple-image-browser,SimpleLink,sketchfab,soundPlayer,SPImage,image2,imageresponsive,simpleImageUpload,simple-ruler,slideshow,smallerselection,spoiler,SimpleImage,sourcedialog,label,extraformattributes,stat,backup,strinsert,stylesheetparser,tabbedimagebrowser,ckeditortablecellsselection,syntaxhighlight,tabletoolstoolbar,tableresize,textsignature,xdsoft_translater,Text2Speech,staticspace,texttransform,stylesheetparser-fixed,toolbarswitch,uicolor,uploadfile,videoembed,uploadcare,widgetcontextmenu,wenzgmap,videosnapshot,token,tweetabletext,xml,ajax,xmltemplates,emojione,divarea,docprops,docfont,devtools,dropdownmenumanager,easykeymap,custimage,crossreference,niftytimers,ccmsconfighelper,pre,cssanim,codemirror,codeTag,confighelper,codesnippetgeshi,ckwebspeech,cleanuploader,footnotes,ckeditor_fa,chart,cavacnote,ckawesome,brclear,bootstrapTabs,bootstrapTable,pbckcode,collapsibleItem,accordionList,glyphicons,bt_table,btgrid,btbutton,btquicktable,bootstrapVisibility,balloonpanel,backgrounds,bgimage,base64image,autosave,bbcode,autocorrect,autogrow,autolink,autoembed,deselect,wordcount,youtube,enhancedcolorbuttton,videodetector,allmedias,ccmsacdc,zoom,allowsave,a11ychecker,texzilla';
	var configPlugins = ""
	configPlugins += "dialogui,dialog,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,button"
	configPlugins += ",toolbar,notification,clipboard,panelbutton,panel,floatpanel,colorbutton"
	configPlugins += ",colordialog,templates,menu,contextmenu,copyformatting,div,resize,elementspath"
	configPlugins += ",enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace"
	configPlugins += ",listblock,richcombo,font,forms,format,horizontalrule,htmlwriter,iframe"
	configPlugins += ",wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton"
	configPlugins += ",language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext"
	configPlugins += ",pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders"
	configPlugins += ",sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,tableselection"
	configPlugins += ",undo,wsc,lineutils,widgetselection,widget,filetools,notificationaggregator"
	//configPlugins += ",maxheight"		会导致刚打开编辑器时编辑器高度为0
	configPlugins += ",uploadimage,uploadwidget,about,mediaembed,embedbase,pastefromexcel"
	//tliyoutube:	Inserisci filmato YouTube
	//tliyoutube2:		嵌入 Youtube 影片
	//oembed:		Embed Media from External Sites
	//mrmonkey: Toggle Mr. Monkey && Mr. Monkey Settings
	//embed,embedsemantic: 插入媒体
	//pastecode: Paste as code
	//html5video: Insert HTML5 video 
	//lineheight: Line Height
	//mathjax: 数学公式
	//quran: insert quran
	//ccmssourcedialog: 源码弹出框
	//eqneditor: insert equation
	//FMathEditor: math editor
	//format_buttons: H1-H6按钮
	//glossary: Термин
	//wlpheix: Вставить изображение из системной галереи Pheix
	//gg: Insert a ZS Google map
	//googledocs: Document
	//htmlbuttons: A nested list && A simple table && insert items && A link to Google
	//closebtn: close(关闭编辑器)
	//insertpre: Insert code snippet	(插入代码，没有高亮没有语言)
	//inlinecancel: Cancel(一个叉，不知道干嘛的，点了没反应)
	//mathedit: insert math
	//internallink: insert internallink
	//sourcedislog:	源码(弹出对话框，有确定有取消)
	//configPlugins += ",tliyoutube,oembed,tliyoutube2,mrmonkey,embed,pastecode,ccmssourcedialog" //icon
	configPlugins += ",html5video,lineheight,mathjax,eqneditor,FMathEditor"	//icon
	configPlugins += ",nbsp,noabp,numericinput,onchange,basewidget,layoutmanager,html5validation"
	configPlugins += ",quicktable,selectallcontextmenu,iframedialog,fastimage,fixed,floating-tools"
	configPlugins += ",ckeditor-gwf-plugin"
	//configPlugins += ",pasteFromGoogleDoc,osem_googlemaps,pastebase64,page2images,lightbox,lite"
	//configPlugins += ",loremipsum,locationmap,leaflet,letterspacing,internallink,inserthtml4x"
	//configPlugins += ",markdown,niftyimages,placeholder,placeholder_select,performx,qrc"
	//configPlugins += ",resizewithwindow" 会出问题，整个编辑器变了
	//configPlugins += ",quran,removespan,replaceTagNameByBsquochoai,closebtn,insertpre,inlinecancel"
	//configPlugins += ",embedsemantic,scribens,xmas,glossary,wlpheix,gg,htmlbuttons,googledocs,mathedit"
	configPlugins += ",format_buttons"	//icon
	configPlugins += ",imgbrowse,imagebrowser,imageresize,imagerotate"
	configPlugins += ",googlethisterm"
	configPlugins += ",imageCustomUploader,imageuploader,imagepaste,prism,textselection"
	configPlugins += ",symbol,codesnippet,html5audio,simplebutton,sourcedialog" //icon
	configPlugins += ",sharedspace"
	//configPlugins += ",simple-image-browser,SimpleLink,sketchfab,soundPlayer,SPImage,spoiler,slideshow"
	configPlugins += ",image2,imageresponsive,simpleImageUpload"
	//configPlugins += ",SimpleImage,syntaxhighlight,strinsert,xdsoft_translater,Text2Speech,videoembed"
	configPlugins += ",smallerselection,label,extraformattributes"
	configPlugins += ",stat,backup,tabbedimagebrowser,stylesheetparser,textsignature"
	configPlugins += ",tabletoolstoolbar,tableresize"
	//configPlugins += ",toolbarswitch,wenzgmap,tweetabletext,token,videosnapshot,docprops,custimage"
	configPlugins += ",staticspace,texttransform,uicolor,uploadfile,widgetcontextmenu"
	configPlugins += ",xml,ajax,xmltemplates,emojione"
	//configPlugins += ",docfont,dropdownmenumanager,devtools,accordionList,bootstrapTabs,glyphicons"
	//configPlugins += ",divarea,easykeymap,pre,footnotes,chart,ckeditor_fa,cavacnote,ckawesome"
	//configPlugins += ",crossreference,niftytimers,ccmsconfighelper,codemirror,codeTag,confighelper"
	configPlugins += ",codesnippetgeshi,ckwebspeech,cleanuploader"
	configPlugins += ",brclear,pbckcode,collapsibleItem"
	configPlugins += ",bt_table,btgrid,btbutton,bootstrapVisibility,backgrounds"
	//configPlugins += ",bbcode" 只以源码查看
	//configPlugins += ",autogrow,showprotected,btquicktable"
	//configPlugins += ",youtube,videodetector,zoom,a11ychecker,ccmsacdc,texzilla"

	//configPlugins += ",autocorrect,autosave,autolink,autoembed,base64image"
	configPlugins += ",deselect,wordcount,allowsave,bgimage"
	//error
	//configPlugins += ",liteedite,simple-ruler,imgupload,Audio,uploadcare,stylesheetparser-fixed,imgur"
	//configPlugins += ",ckeditortablecellsselection,bootstrapTable,balloonpanel,cssanim,enhancedcolorbuttton,allmedias"
	//codemirror:	源码代码高亮，html补全，源码搜索等
	configPlugins += ",codemirror";
	config.extraPlugins= configPlugins;
	//config.imgurClientID
	//config.extraPlugins= 'autogrow,uploadimage,emojione,uploadfile,tabletools,uicolor,pastecode,preview,wordcount,menu,menubutton,layoutmanager,fastimage,filebrowser,image2,mathjax,maxheight,maximize,placeholder,ajax,codemirror,codesnippet,codesnippetgeshi,filebrowser,fastimage';
	config.uiColor = '#7fb2ed';
	config.uploadUrl = '/upload';
	config.skin = 'moonocolor';
	config.mathJaxLib = '//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML';
	config.autoGrow_maxHeight = 500;
	config.autoGrow_minHeight = 200;
	config.height = 300;
	config.protectedSource.push(/<div class=[\s\S]*<\/div>/g);
	config.protectedSource.push(/<style [\s\S]*<\/style>/g);
	config.protectedSource.push(/<script [\s\S]*<\/script>/g);
	config.allowedContent = true;//允许源码
	config.filebrowserBrowseUrl = '/image/',
	config.filebrowserUploadUrl = '/upload'
	//config.toolbarCanCollapse = true;	//折叠工具栏
	/*config.codeSnippet_languages = {
		c: 'c++'
	};*/
};
