"use strict";

var pageSize = 10  //全局pageSize
  ,  messageBoxNum = 0
  ,  alertBox
  ;

$(document).ready(function() {
  alertBox = $(".alert-box");
  alertBox.css("left",$("html").width()/2-alertBox.width()/2);
  //alertSuccess(screen.availWidth+" "+screen.availHeight)
  $(".menu .nav.dropdown-list li").click(function() {
    $(".menu .nav.dropdown-list li").removeClass("disabled");
    $(this).addClass("disabled");
  });
  $(".menu .nav.nav-stacked.list .item").click(function() {
    $(".menu .nav.nav-stacked.list .item .mark").removeClass("active");
    $(this).children(".mark").addClass("active");
  });
  function autosave() {
    $("#editor-modal.modal.fade.in button[name='save']").click();
  }
  setInterval(autosave,60000);
  $("[data-toggle='tooltip']").tooltip(); 
});

//共用函数
$(document).ajaxStart(function(){
  loading();
});
$(document).ajaxComplete(function(){
  loaded();
  setTimeout(function() { $("[data-toggle='tooltip']").tooltip() },100); 
});
function loading() {
  $(".loading").css("display","block");
}
function loaded() {
  $(".loading").css("display","none");
}

function showMask(obj) {
  $(obj).css("display","block");
  $(".mask.window").css("display","block");
}

function hideMask(obj) {
  $(obj).css("display","none");
  $(".mask.window").css("display","none");
}

function alertClose(name) {
  function close(){
    alertBox.children("div[name='"+name+"']").remove();
  }
  setTimeout(close,3000);
}

function alertSuccess(str) {
  var name="messageBox"+messageBoxNum;
  messageBoxNum++;
  alertBox.prepend(`
  <div class="alert alert-success" name="`+name+`">
    <a href="#" class="close" data-dismiss="alert">
        &times;
    </a>
    `+str+`
  </div>
  `);
  alertClose(name);
}

function alertWarning(str) {
  var name="messageBox"+messageBoxNum;
  messageBoxNum++;
  alertBox.prepend(`
  <div class="alert alert-warning" name="`+name+`">
    <a href="#" class="close" data-dismiss="alert">
        &times;
    </a>
    `+str+`
  </div>
  `);
  alertClose(name);
}

function alertDanger(str) {
  var name="messageBox"+messageBoxNum;
  messageBoxNum++;
  $(".alert-box").prepend(`
  <div class="alert alert-danger" name="`+name+`">
    <a href="#" class="close" data-dismiss="alert">
        &times;
    </a>
    `+str+`
  </div>
  `);
  alertClose(name);
}

function dealError(xhr,status,error) {
  alertDanger(status+":"+error);
}
function editorTableCell(obj) {
  var text=$(obj).text()
  $(obj).html('<input value="'+text+'" onblur=""></input>')
}

function tableRowChange(obj) {
  console.log("tableRowChange")
  $(obj).find('input[type="checkbox"]').attr("checked",true)
}

function tableSelectAll(obj) {
  $(obj).find("tr input[type='checkbox']").prop("checked",true);
}
function tableSelectNoOne(obj) {
  $(obj).find("tr input[type='checkbox']").prop("checked",false);
}

function toggleList(obj) {
  var id = $(obj).parent().attr('name')+"-list"
  //console.log(obj,id)
  $('#'+id).toggle()
}

function setPageIndex(index) {
  $(".footer .pageIndex").val(index);
}

function setPageSize(size) {
  $(".footer .pageSize").val(size);
}

function rebootServer() {
  $.ajax({
    type: "POST",
    url: "/rebootServer",
    contentType: "application/json",
    data: JSON.stringify({
      token: token,
    }),
    success: function(result) {
      if(result["code"]!=200) {
        alertWarning(result["msg"]+":"+result["debugMsg"]);
        return;
      }
      alertSuccess("重启成功！"+result["debugMsg"]);
    },
    error: dealError,
  })
}

function previewDealImages() {
  $("#editor-modal .phone-body img").each(function() {
    if(!$(this).attr("width") || Number($(this).attr("width"))<150) return;
    $(this).css({
      "display": "block",
      "height":"auto",
      "width":"auto",
      "margin-left":"auto",
      "margin-right": "auto",
    });
  });
}

function dataUser(id) {
  $.ajax({
    type: "POST",
    url: "/data/user",
    contentType: "application/json",
    data: JSON.stringify({
      token: token,
      id: id,
    }),
    success: function(result) {
      if(result["code"]!=200) {
        alertWarning(result["msg"]);
        return;
      }
      var res = result["result"]
      $("#data-user").html(res["html"]).modal("show");
    },
    error:dealError,
  })
}

function deleteTableIDs(name) {
  var IDs = new Array();
  $(".table tr input[type='checkbox']").each(function() {
    if(!$(this).prop("checked")) {
      return;
    }
    IDs.push($(this).parents("tr").find("td[name='id']").text());
  });
  if(IDs.length==0) {
    alertDanger("请先选择要删除的行！");
    return;
  }
  //console.log(JSON.stringify({IDs: IDs}));
  $.ajax({
    type: 'POST',
    url: '/delete/'+name,
    contentType: "application/json",
    data: JSON.stringify({
      token: token,
      IDs: IDs,
    }),
    success: function(result) {
      if(result["code"]!=200) {
        alertWarning(result["msg"]);
        return;
      }
      alertSuccess("删除成功");
      $(".table tr input[type='checkbox']").each(function() {
        if($(this).prop("checked")) {
          $(this).parents("tr").remove();
        }
      });
    },
    error: dealError,
  })
}

function uploadIcon(obj) {
  var formData = new FormData()
  ,   xhr = new XMLHttpRequest(); 
  console.log(obj.files[0],obj.files[0].name)
  xhr.open( 'PUT', "/upload/icon", true );
  formData.append( 'upload', obj.files[0], obj.files[0].name);
  formData.append("type",$("#editor-modal input[name='type']").attr("title"));
  xhr.send( formData ); // Prevented the default behavior.
  xhr.onreadystatechange = function() {
    if (xhr.readyState==4 && xhr.status==200) {
      var result = JSON.parse(xhr.responseText);
      if(result["uploaded"]!=1) {
        alertDanger(result["message"])
        return;
      }
      if(result["message"]) {
        alertWarning(result["message"]);
      }
      $(obj).siblings("img").attr("src",result["url"]);
      console.log(result);
    }
  }
}

function getPhoneBodyImages(preview) {
  preview();
  var images = new Array;
  $("#editor-modal .phone-body img").each(function() {
    var src = $(this).attr("src")
    if(src.indexOf("data:image/")==0 && src.indexOf("base64")!=-1) return;
    images.push(src);
  });
  console.log(images);
  return images;
}
//尽量将同一个局部用的函数放到一个$(document).ready(function(){})里,降低函数命名冲突
//然后在加载html后设置事件响应函数

//网页内容管理->内容管理
$(document).ready(function() {
  var index = {"guidance":1,"summary":1,"club":1,"look":1,"partTimeJob":1,"activity":1,"training":1}
    ,  maxIndex = {"guidance":1,"summary":1,"club":1,"look":1,"partTimeJob":1,"activity":1,"training":1}
    ,  tableSelect
    ,  typeSchoolData = "schoolData"
    ,  typeDiscovery = "discovery"
    ,  typeUnknown = "unknown"
    ;

  function judgeType(str) {
    switch (str)
    {
      case "guidance":
      case "summary":
      case "club":
      case "look": return typeSchoolData;
      break;
      case "partTimeJob":
      case "activity":
      case "training": return typeDiscovery;
      break;
      default: return typeUnknown;
    }
  }

  function preview() {
    loading();
    var body = $('.phone-body');
    $('.phone-header>h4[name="title"]').text($('input[name="title"]').val());
    body.html('');
    $(body).append('<h3>'+$('input[name="title"]').val()+'</h3>');
    $(body).append('<span><small>'+$('#editor-modal input[name="createTime"]').val()+'</small></span><br><br>');
    var data = editor.getData();
    if(data.indexOf("location.href") == -1) body.append(data);
    previewDealImages();
    loaded();
  }

  function alter(obj) {
    var table = tableSelect.val()
      , tr = $(obj).parents("tr")
      ;
    var id = tr.find('td[name="id"]>a').text()
      , title = tr.find('td[name="title"]').text()
      , createTime = tr.find('td[name="createTime"]').text()
      ,  status = tr.find('td[name="status"]').text()
      , fee = tr.find('td[name="fee"]').text()
      ,  position = tr.find('td[name="position"]').text()
      , description = tr.find('td[name="description"]').text()
      , serviceAccount = tr.find('td[name="serviceAccount"]').text()
      , icon = tr.find('img[name="icon"]').attr("src")
      ;
      //console.log(id+title+createTime)
    $.ajax({
      type: 'POST',
      url: "/data/"+table,
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: id,
      }),
      success: function (result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        //console.log(id,title,createTime)
        var res = result["result"]
        openEditor(id,title,createTime,status,fee,position,description,serviceAccount,icon,res["content"])
      },
      error: dealError,
    });
  }

  function openEditor(id,title,createTime,status,fee,position,description,serviceAccount,icon,editorData) {
    showMask($("#editor-modal"));
    $("#editor-modal input[name='id']").val(id);
    $("#editor-modal input[name='title']").val(title);
    $("#editor-modal input[name='createTime']").val(createTime);
    $("#editor-modal input[name='type']").attr("title",tableSelect.val());
    $("#editor-modal input[name='type']").val(tableSelect.find("option[value='"+tableSelect.val()+"']").text());
    var type = judgeType(tableSelect.val());
    if(type==typeSchoolData) {
      $("#editor-modal input[name='status']").val(status).parent().show();
      $("#editor-modal input[name='fee']").val("").parent().hide();
      $("#editor-modal input[name='position']").val("").parent().hide();
      $("#editor-modal input[name='serviceAccount']").val("").parent().hide();
      $("#editor-modal textarea[name='description']").val("").parent().hide();
      $("#editor-modal img[name='icon']").attr("src",icon).parent().hide();
    } else if (type == typeDiscovery) {
      $("#editor-modal input[name='status']").val("").parent().hide();
      $("#editor-modal input[name='fee']").val(fee).parent().show();
      $("#editor-modal input[name='position']").val(position).parent().show();
      $("#editor-modal input[name='serviceAccount']").val(serviceAccount).parent().show();
      $("#editor-modal textarea[name='description']").val(description).parent().show();
      $("#editor-modal img[name='icon']").attr("src",icon).parent().show();
    }
    editor.setData(editorData)
    preview();
  }

  function flushTable(table) {
    $.ajax({
      type: 'POST',
      url: "/list/"+table,
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: index[table]-1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        $(".table tr td>button[name='alter']").click(function() {
          alter(this);
        });
      },
      error: dealError,
    });
  }

  function getTable(table) {
    $.ajax({
      type: 'GET',
      url: "/list/"+table,
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: index[table]-1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        //console.log(result,status,xhr);
        var res = result["result"]
        $(".content").html(res["html"]);
        $("#editor-modal .header").html(res["editorHeader"]);
        maxIndex[table] = Number($(".footer span[name='maxPageIndex']").text());
        tableSelect = $("#table");
        $('.operation button[name="save"]').click(function() {
          var list=new Array();
          var type=judgeType(tableSelect.val());
          $('.table tr input[type="checkbox"]').each(function() {
            if(!$(this).prop("checked")) {
              return
            }
            //console.log(this,"push")
            var tr = $(this).parents("tr");
            if(type==typeSchoolData) {
              list.push({
                id: tr.find('td[name="id"]').text(),
                title: tr.find('td[name="title"]').text(),
                penName: tr.find('td[name="penName"]').text(),
                status: tr.find('td[name="status"]').text(),
                publishStatus: tr.find("td[name='publishStatus'] select").val(),
              });
            } else if(type==typeDiscovery) {
              list.push({
                id: tr.find('td[name="id"]').text(),
                title: tr.find('td[name="title"]').text(),
                penName: tr.find('td[name="penName"]').text(),
                fee: tr.find('td[name="fee"]').text(),
                position: tr.find('td[name="position"]').text(),
                serviceAccount: tr.find('td[name="serviceAccount"]').text(),
                description: tr.find('td[name="description"]').text(),
                publishStatus: tr.find("td[name='publishStatus'] select").val(),
              });
            }
          });
          console.log(list)
          if(list.length==0) {
            alertDanger("请先选择要保存的行！")
            return
          }
          $.ajax({
            type: 'POST',
            url: "/save/list/"+tableSelect.val(),
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              list: list,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return
              }
              alertSuccess("保存成功");
              $(".table tr input[type='checkbox']").prop("checked",false);
            },
            error: dealError,
          })
        });
        $('.operation button[name="setTop"]').click(function() {
          var IDs=new Array();
          var type=judgeType(tableSelect.val());
          $('.table tr input[type="checkbox"]').each(function() {
            if(!$(this).prop("checked")) {
              return
            }
            IDs.push($(this).parents("tr").find("td[name='id']").text());
          });
          console.log(IDs)
          if(IDs.length==0) {
            alertDanger("请先选择要置顶的行！")
            return
          }
          $.ajax({
            type: 'POST',
            url: "/setTop/"+tableSelect.val(),
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              IDs: IDs,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return
              }
              alertSuccess("置顶成功");
              $(".table tr input[type='checkbox']").prop("checked",false);
            },
            error: dealError,
          })
        });
        $('.operation button[name="refresh"]').click(function() {
          flushTable(tableSelect.val());
        });
        $('.operation button[name="delete"]').click(function() {
          deleteTableIDs(tableSelect.val());
        });
        $('.operation button[name="new"]').click(function() {
          openEditor("","",(new Date()).toISOString(),"","","","","10000000001","icon/default.ico","")
        });
        $(".footer button[name='lastPage']").click(function() {
          var table = tableSelect.val();
          if(index[table] <= 1) {
            index[table] = 1;
            setPageIndex(index[table]);
            alertWarning("没有上一页啦");
            return;
          }
          index[table]--;
          flushTable(table);
          setPageIndex(index[table]);
        });
        $(".footer button[name='nextPage']").click(function() {
          var table = tableSelect.val();
          if(index[table] >= maxIndex[table]) {
            index[table] = maxIndex[table];
            setPageIndex(index[table]);
            alertWarning("没有下一页啦");
            return;
          }
          //console.log(table)
          index[table]++;
          flushTable(table);
          setPageIndex(index[table]);
        });
        $(".footer .pageIndex").change(function() {
          index[tableSelect.val()] = Number($(this).val());
          flushTable(tableSelect.val());
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable(tableSelect.val());
        });
        $("#table option[value='"+table+"']").prop("selected",true);
        $("#table").change(function() {
          getTable($(this).val());
        });
        $("#editor-modal .header button[name='save']").click(function() {
          var title = $('#editor-modal input[name="title"]').val()
            , content = editor.getData()
            ,  table = $('#editor-modal input[name="type"]').attr("title")
            ,  id = $('#editor-modal input[name="id"]').val()
            ;
          var type = judgeType(table);
          var data;
          if(type == typeSchoolData) {
            data = {
              token: token,
              id: id,
              content: content,
              title: title,
              status: $("#editor-modal input[name='status']").val(),
              images: getPhoneBodyImages(preview),
            };
          } else if (type == typeDiscovery) {
            data = {
              token: token,
              id: id,
              content: content,
              title: title,
              fee: $("#editor-modal input[name='fee']").val(),
              position: $("#editor-modal input[name='position']").val(),
              serviceAccount: $("#editor-modal input[name='serviceAccount']").val(),
              description: $("#editor-modal textarea[name='description']").val(),
              icon: $("#editor-modal img[name='icon']").attr("src"),
              images: getPhoneBodyImages(preview),
            };
          } else {
            alertDanger("类型有误！");
            return;
          }
          $.ajax({
            type: 'POST',
            url: "/save/"+table,
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function(result,status,xhr) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              alertSuccess("保存成功");
              var res = result["result"];
              $("#editor-modal input[name='id']").val(res["id"]);
              $("#editor-modal input[name='createTime']").val(res["createTime"]);
              flushTable(table);
            },
            error: dealError,
          });
        });
        $("#editor-modal .header button[name='preview']").click(function() {
          preview();
        });
        $(".table tr td>button[name='alter']").click(function() {
          alter(this);
        });
      },
      error: dealError,
    });
  }
  $('#webPage-manage-list>li[name="content-manage"]').click(function(){ 
    getTable("guidance");
  });
});

//网页内容管理->模板管理
$(document).ready(function() {
  var pageIndex = 1
    ,  maxPageIndex
    ;
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/template",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        $(".table tr td>button[name='alter']").click(function() {
          alter(this);
        });
      },
      error: dealError,
    });
  }
  
  function preview() {
    loading();
    var body = $('.phone-body');
    $('.phone-header>h4[name="title"]').text($('input[name="name"]').val());
    body.html(editor.getData());
    previewDealImages();
    loaded();
  }

  function openEditor(id,name,url,createTime,editorData,publishStatus) {
    showMask($("#editor-modal"));
    $('#editor-modal input[name="id"]').val(id);
    $('#editor-modal input[name="name"]').val(name);
    $('#editor-modal input[name="url"]').val(url);
    $('#editor-modal input[name="createTime"]').val(createTime);
    var html="";
    if($("#editor-modal .header span[name='publishStatus']").length == 0) {
      $("#editor-modal .header").append("<span name='publishStatus'></span>");
    } 
    if(publishStatus == "") {
      html = "发布状态：<select><option>编辑中</option></select>";
    }
    else {
      html = "发布状态："+$(publishStatus).html();
    }
    $("#editor-modal .header span[name='publishStatus']").html(html);
    editor.setData(editorData);
    preview();
  }

  function alter(obj) {
    var tr = $(obj).parents("tr")
      ;
    var id = tr.find("td[name='id']").text()
      , name = tr.find("td[name='name']").text()
      , createTime = tr.find("td[name='createTime']").text()
      ,  url = tr.find("td[name='url']").text()
      , publishStatus = tr.find("td[name='publishStatus']")
      ;
    $.ajax({
      type: "POST",
      url: "/data/template",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: id
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        var res = result["result"];
        openEditor(id,name,url,createTime,res["content"],publishStatus);
      },
      error: dealError,
    });
  }

  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/template",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $("#editor-modal .header").html(res["editorHeader"]);
        $(".operation button[name='new']").click(function() {
          openEditor("","","",(new Date()).toISOString(),"","");
        })
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".operation button[name='delete']").click(function() {
          deleteTableIDs("template");
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $("#editor-modal .header button[name='save']").click(function() {
          $.ajax({
            type: "POST",
            url: "/save/template",
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              id: $("#editor-modal input[name='id']").val(),
              name: $("#editor-modal input[name='name']").val(),
              url: $("#editor-modal input[name='url']").val(),
              publishStatus: $("#editor-modal span[name='publishStatus'] select").val(),
              content: editor.getData(),
              images : getPhoneBodyImages(preview),
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              alertSuccess("保存成功");
              var res = result["result"];
              $("#editor-modal input[name='id']").val(res["id"]);
              $("#editor-modal input[name='createTime']").val(res["createTime"]);
              flushTable();
            },
            error: dealError,
          });
        });
        $("#editor-modal .header button[name='preview']").click(function() {
          preview();
        });
        $(".table tr td>button[name='alter']").click(function() {
          alter(this);
        });
      },
      error: dealError,
    });
  }
  $('#webPage-manage-list>li[name="template-manage"]').click(function() {
    getTable();
  });
});

//网页内容管理->广告管理
$(document).ready(function() {
  var pageIndex = 1
    ,  maxPageIndex
    ;
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/advertisement",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        $(".table tr td>button[name='alter']").click(function() {
          alter(this);
        });
      },
      error: dealError,
    });
  }
  
  function preview() {
    loading();
    var body = $('.phone-body');
    $('.phone-header>h4[name="title"]').text($('input[name="title"]').val());
    body.html('');
    $(body).append('<h3>'+$('input[name="title"]').val()+'</h3>');
    $(body).append('<span><small>'+$('#editor-modal input[name="createTime"]').val()+'</small></span><br><br>');
    body.append(editor.getData());
    previewDealImages();
    loaded();
  }

  function openEditor(id,position,title,createTime,link,image,editorData,publishStatus) {
    showMask($("#editor-modal"));
    $('#editor-modal input[name="id"]').val(id);
    $('#editor-modal input[name="position"]').val(position);
    $('#editor-modal input[name="title"]').val(title);
    $('#editor-modal input[name="createTime"]').val(createTime);
    $('#editor-modal input[name="link"]').val(link);
    $('#editor-modal img[name="image"]').attr("src",image);
    var html="";
    if($("#editor-modal .header span[name='publishStatus']").length == 0) {
      $("#editor-modal .header").append("<span name='publishStatus'></span>");
    } 
    if(publishStatus == "") {
      html = "发布状态：<select><option>编辑中</option></select>";
    }
    else {
      html = "发布状态："+$(publishStatus).html();
    }
    $("#editor-modal .header span[name='publishStatus']").html(html);
    editor.setData(editorData);
    preview();
  }

  function alter(obj) {
    var tr = $(obj).parents("tr")
      ;
    var id = tr.find("td[name='id']").text()
      , position = tr.find("td[name='position']").text()
      , title = tr.find("td[name='title']").text()
      , createTime = tr.find("td[name='createTime']").text()
      ,  link = tr.find("td[name='link']").text()
      , image = tr.find("td[name='image'] img").attr("src")
      , publishStatus = tr.find("td[name='publishStatus']")
      ;
    if(link=="") {
      $.ajax({
        type: "POST",
        url: "/data/advertisement",
        contentType: "application/json",
        data: JSON.stringify({
          token: token,
          id: id
        }),
        success: function(result) {
          if(result["code"]!=200) {
            alertWarning(result["msg"]);
            return;
          }
          var res = result["result"];
          openEditor(id,position,title,createTime,link,image,res["content"],publishStatus);
        },
        error: dealError,
      });
    } else {
      openEditor(id,position,title,createTime,link,image,"",publishStatus);
    }
  }

  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/advertisement",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $("#editor-modal .header").html(res["editorHeader"]);
        $(".operation button[name='new']").click(function() {
          openEditor("","","",(new Date()).toISOString(),"","icon/default.ico","","");
        })
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".operation button[name='delete']").click(function() {
          deleteTableIDs("advertisement");
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $("#editor-modal .header button[name='save']").click(function() {
          $.ajax({
            type: "POST",
            url: "/save/advertisement",
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              id: $("#editor-modal input[name='id']").val(),
              title: $("#editor-modal input[name='title']").val(),
              link: $("#editor-modal input[name='link']").val(),
              image: $("#editor-modal img[name='image']").attr("src"),
              position: Number($("#editor-modal input[name='position']").val()),
              publishStatus: $("#editor-modal span[name='publishStatus'] select").val(),
              content: editor.getData(),
              images : getPhoneBodyImages(preview),
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              alertSuccess("保存成功");
              var res = result["result"];
              $("#editor-modal input[name='id']").val(res["id"]);
              $("#editor-modal input[name='createTime']").val(res["createTime"]);
              flushTable();
            },
            error: dealError,
          });
        });
        $("#editor-modal .header button[name='preview']").click(function() {
          preview();
        });
        $(".table tr td>button[name='alter']").click(function() {
          alter(this);
        });
      },
      error: dealError,
    });
  }
  $('#webPage-manage-list>li[name="advertisement-manage"]').click(function() {
    getTable();
  });
});

//用户管理->用户管理
$(document).ready(function() {
  function forbidInform(obj) {
    $.ajax({
      type: "POST",
      url: "/forbid/inform",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: $(obj).parents("tr").find("td[name='id']").text(),
      }),
      success: function (result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        alertSuccess("禁止举报成功！该用户72小时内将不能再举报任何内容！");
        $(obj).parent().append(`<button class="btn btn-success" data-toggle="tooltip" title="取消'禁止此用户举报'" name="cancelForbidInform">取消禁止举报</button>`);
        $(obj).siblings(".tooltip.fade").remove();
        $(obj).siblings("button[name='cancelForbidInform']").click(function() {
          cancelForbid(this,"inform");
        });
        $(obj).remove();
      },
      error: dealError,
    })
  }

  function forbidLeaveWords(obj) {
    $.ajax({
      type: "POST",
      url: "/forbid/leaveWords",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: $(obj).parents("tr").find("td[name='id']").text(),
      }),
      success: function (result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        alertSuccess("禁止留言成功！该用户72小时内将不能再给其它用户留言！");
        $(obj).parent().append(`<button class="btn btn-success" data-toggle="tooltip" title="取消'禁止此用户留言'" name="cancelForbidLeaveWords">取消禁止留言</button>`);
        $(obj).siblings(".tooltip.fade").remove();
        $(obj).siblings("button[name='cancelForbidLeaveWords']").click(function() {
          cancelForbid(this,"leaveWords");
        });
        $(obj).remove();
      },
      error: dealError,
    })
  }

  function cancelForbid(obj,type) {
    $.ajax({
      type: "POST",
      url: "/cancelForbid/"+type,
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: $(obj).parents("tr").find("td[name='id']").text(),
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        switch(type) {
          case "inform":
          alertSuccess("取消禁止举报成功！该用户已经可以使用举报功能了！");
          $(obj).parent().append(`<button class="btn btn-danger" data-toggle="tooltip" title="在未来72小时内禁止此用户举报任何内容" name="forbidInform">禁止举报</button>`);
          $(obj).siblings("button[name='forbidInform']").click(function() {
            forbidInform(this);
          });
          break;
          case "leaveWords":
          alertSuccess("取消禁止留言成功！该用户已经可以使用留言功能了！");
          $(obj).parent().append(`<button class="btn btn-danger" data-toggle="tooltip" title="在未来72小时内禁止此用户向其它用户留言" name="forbidLeaveWords">禁止留言</button>`);
          $(obj).siblings("button[name='forbidLeaveWords']").click(function() {
            forbidLeaveWords(this);
          });
          break;
          case "sendAsk":
          alertSuccess("取消禁止发问成功！该用户已经可以发送问问了！");
          break;
          case "commentAsk":
          alertSuccess("取消禁止评论成功！该用户已经可以评论问问了！");
          break;
          default:
          alertDanger("type 有误："+type);
          break;
        }
        $(obj).siblings(".tooltip.fade").remove();
        $(obj).remove();
      },
      error: dealError,
    })
  }

  function setClickForbid() {
    var content = $(".content");
    content.find("tr button[name='forbidInform']").click(function() {
      forbidInform(this);
    });
    content.find("tr button[name='forbidLeaveWords']").click(function() {
      forbidLeaveWords(this);
    });
    content.find("tr button[name='cancelForbidInform']").click(function() {
      cancelForbid(this,"inform");
    });
    content.find("tr button[name='cancelForbidLeaveWords']").click(function() {
      cancelForbid(this,"leaveWords");
    });
    content.find("tr button[name='cancelForbidSendAsk']").click(function() {
      cancelForbid(this,"sendAsk");
    });
    content.find("tr button[name='cancelForbidCommentAsk']").click(function() {
      cancelForbid(this,"commentAsk");
    });
  }

  $("#createUserModal button[name='createUser']").click(function() {
    var account = $("#createUserModal input[name='account']").val();
    var partten = new RegExp("[0-9]{10}[^0-9]{0}");
    if(!account.match(partten)) {
      alertWarning("请输入10位数字");
      return;
    }
    $.ajax({
      type: "POST",
      url: "/createUser",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        account: "2"+account,
      }),
      success: function (result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return;
        }
        alertSuccess("创建成功!");
        $("#createUserModal input[name='account']").val("");
        $("#createUserModal").modal("hide");
      }
    });
  });
  var pageIndex = 1
    , maxPageIndex
    ;
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/users",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return;
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        setClickForbid();
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/users",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".operation button[name='delete']").click(function() {
          deleteTableIDs("users");
        });
        $(".operation button[name='setManager']").click(function() {
          var IDs = new Array();
          $(".table tr input[type='checkbox']").each(function() {
            if(!$(this).prop("checked")) {
              return;
            }
            IDs.push($(this).parents("tr").find("td[name='id']").text());
          });
          if(IDs.length==0) {
            alertDanger("请先选择要设置为管理员的的用户！");
            return;
          }
          //console.log(JSON.stringify({IDs: IDs}));
          $.ajax({
            type: 'POST',
            url: '/setManager',
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              IDs: IDs,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              alertSuccess("设置成功");
              $(".table tr input[type='checkbox']").prop("checked",false);
            },
            error: dealError,
          })
        });
        $(".operation button[name='send666Coins']").click(function() {
          var IDs = new Array();
          $(".table tr input[type='checkbox']").each(function() {
            if(!$(this).prop("checked")) {
              return;
            }
            IDs.push($(this).parents("tr").find("td[name='id']").text());
          });
          if(IDs.length==0) {
            alertDanger("请先选择要赠送的的用户！");
            return;
          }
          //console.log(JSON.stringify({IDs: IDs}));
          $.ajax({
            type: 'POST',
            url: '/send666Coins',
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              IDs: IDs,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              alertSuccess("赠送成功");
              $(".table tr input[type='checkbox']").prop("checked",false);
            },
            error: dealError,
          })
        });
        $(".operation button[name='new']").click(function() {
          $("#createUserModal").modal("show");
        });
        setClickForbid();
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#user-manage-list>li[name='user-manage']").click(function() {
    getTable();
  });
});

//用户管理->管理员管理
$(document).ready(function() {
  var pageIndex = 1
    , maxPageIndex
    ;
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/manager",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/manager",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".operation button[name='delete']").click(function() {
          deleteTableIDs("manager");
        });
        $('.operation button[name="save"]').click(function() {
          var list=new Array();
          $('.table tr input[type="checkbox"]').each(function() {
            if(!$(this).prop("checked")) {
              return;
            }
            var tr = $(this).parents("tr");
            list.push({
              id: tr.find('td[name="id"]').text(),
              type: tr.find('td[name="type"]').text(),
            });
          });
          //console.log(list)
          if(list.length==0) {
            alertDanger("请先选择要保存的行！")
            return;
          }
          $.ajax({
            type: 'POST',
            url: "/save/list/manager",
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              list: list,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              alertSuccess("保存成功");
              $(".table tr input[type='checkbox']").prop("checked",false);
            },
            error: dealError,
          });
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#user-manage-list>li[name='manager-manage']").click(function() {
    getTable();
  });
});

//用户管理->客服管理
$(document).ready(function() {
  $("#createServiceModal button[name='createService']").click(function() {
    var account = $("#createServiceModal input[name='account']").val();
    var partten = new RegExp("[0-9]{11}[^0-9]{0}");
    if(!account.match(partten)) {
      alertWarning("请输入11位数字");
      return;
    }
    $.ajax({
      type: "POST",
      url: "/createService",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        account: account,
      }),
      success: function (result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return;
        }
        alertSuccess("创建成功!");
        $("#createServiceModal input[name='account']").val("");
        $("#createServiceModal").modal("hide");
      },
      error: dealError,
    });
  });
  var pageIndex = 1
    , maxPageIndex
    ;
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/service",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/service",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".operation button[name='delete']").click(function() {
          deleteTableIDs("service");
        });
        $(".operation button[name='new']").click(function() {
          $("#createServiceModal").modal("show");
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#user-manage-list>li[name='service-manage']").click(function() {
    getTable();
  });
});

//校园管理->学院管理
$(document).ready(function() {
  var pageIndex = 1
    , maxPageIndex
    ;
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/college",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/college",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".operation button[name='delete']").click(function() {
          var list = new Array;
          $("table tr input[type='checkbox']").each(function() {
            if(!$(this).prop("checked")) {
              return;
            }
            var college = $(this).parents("tr").find("td[name='college']").text();
            if(college !="") {
              list.push(college);
            }
          })
          if(list.length == 0) {
            alertDanger("请选择要删除的行,或者刷新删除空行!")
            return;
          }
          $.ajax({
            type: "POST",
            url: "/delete/college",
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              list: list,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              $("table tr input[type='checkbox']").each(function() {
                if($(this).prop("checked")) {
                  $(this).parents("tr").remove();
                }
              });
            },
            error: dealError,
          });
        });
        $(".operation button[name='save']").click(function() {
          var list = new Array;
          $("table tr input[type='checkbox']").each(function() {
            if(!$(this).prop("checked")) {
              return;
            }
            var college = $(this).parents("tr").find("td[name='college']").text();
            if(college !="") {
              list.push(college);
            }
          })
          if(list.length == 0) {
            alertDanger("请选择要保存的行,或者刷新删除空行!");
            return;
          }
          $.ajax({
            type: "POST",
            url: "/save/list/college",
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              list: list,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              $("table tr input[type='checkbox']").prop("checked",false);
            },
            error: dealError,
          });
        });
        $(".operation button[name='new']").click(function() {
          $(".table tbody").prepend("<tr><td><input type=\"checkbox\" checked></td><td name=\"college\" contenteditable=\"true\"></td></tr>")
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#school-manage-list>li[name='college-manage']").click(function() {
    getTable();
  });
});

//问问管理->问问管理
$(document).ready(function() {
  var pageIndex = 1
    , maxPageIndex
    ;
  function deleteAsk(obj) {
    $.ajax({
      type: "POST",
      url: "/delete/ask",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: $(obj).parents("tr").find("td[name='id'] a").text(),
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        alertSuccess("删除成功，发布此问问的用户在此之后72小时内已被禁止发问！");
        $(obj).parents("tr").remove();
      },
      error: dealError,
    });
  }
  function flushTable() {
    $.ajax({
      type: "POST",
      url: "/list/ask",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        $(".table-content button[name='delete']").click(function() {
          deleteAsk(this);
        });
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/ask",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".table-content button[name='delete']").click(function() {
          deleteAsk(this);
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#ask-manage-list>li[name='ask-manage']").click(function() {
    getTable();
  });
});

//问问管理->评论管理
$(document).ready(function() {
  var pageIndex = 1
    , maxPageIndex
    ;
  function deleteCommentAsk(obj) {
    $.ajax({
      type: "POST",
      url: "/delete/commentAsk",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: $(obj).parents("tr").find("td[name='id']").text(),
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        alertSuccess("删除成功，发布此评论的用户在此之后72小时内已被禁止发问！");
        $(obj).parents("tr").remove();
      },
      error: dealError,
    });
  }
  function flushTable() {
    $.ajax({
      type: "POST",
      url: "/list/commentAsk",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        $(".table-content button[name='delete']").click(function() {
          deleteCommentAsk(this);
        });
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/commentAsk",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".table-content button[name='delete']").click(function() {
          deleteCommentAsk(this);
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#ask-manage-list>li[name='comment-manage']").click(function() {
    getTable();
  });
});

//服务器信息->nginx运行状态
$(document).ready(function() {
  $("#server-data-list>li[name='nginxStatus']").click(function() {
    $(".content").load("/serverStatus",function(responseTxt,statusTxt,xhr){
      if(statusTxt=="success") 
        $(".content").html('<div style="white-space: pre-wrap;">'+responseTxt+'</div>');
      else if(statusTxt=="error")
        alert("Error: "+xhr.status+": "+xhr.statusText);
    });
  });
})

//版本信息->版本设置
$(document).ready(function() {
  var pageIndex = 1
    , maxPageIndex
    , alterModal = $("#alterUpdateLog");
    ;
  $("button[name='saveUpdateLog']").click(function() {
    var OSType
      , version = ""
      ,  log = alterModal.find("textarea").val()
      ;
    alterModal.find("input[name='OSType']").each(function() {
      if($(this).prop("checked")) {
        OSType = $(this).val();
      }
    });
    if(!OSType) {
      alertDanger("请选择系统类型！");
      return;
    }
    var versionNum = alterModal.find("input[type='number']");
    for(var i=0;i<versionNum.length;i++) {
      if($(versionNum[i]).val() == "" || Number($(versionNum[i]).val())<0) {
        alertDanger("版本号有误,请检查!")
        return;
      }
      if(i!=0) version += ".";
      version += $(versionNum[i]).val();
    }
    $.ajax({
      type: "POST",
      url: "/save/updateLog",
      contentType: "application/json",
      data: JSON.stringify({
        id: alterModal.find("input[name='id']").val(),
        token: token,
        version: version,
        OSType: OSType,
        log: log,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        alterModal.modal("hide");
        flushTable();
      },
      error: dealError,
    })
  })
  function alter(id,version,OSType,log) {
    var versionNum = version.split(".");
    alterModal.find("input[name='id']").val(id);
    alterModal.find("input[name='1']").val(versionNum[0]);
    alterModal.find("input[name='2']").val(versionNum[1]);
    alterModal.find("input[name='3']").val(versionNum[2]);
    alterModal.find("input[value='"+OSType+"']").prop("checked",true);
    alterModal.find("textarea").val(log);
    alterModal.modal("show");
  }
  function setClick() {
    $(".table-content button[name='alter']").click(function() {
      var tr = $(this).parents("tr");
      alter(tr.find("td[name='id']").text(),
      tr.find("td[name='version']").text(),
      tr.find("td[name='OSType']").text(),
      tr.find("td[name='log']").text());
    });
    function setVersion(id,type) {
      $.ajax({
        type: "POST",
        url: "/setVersion",
        contentType: "application/json",
        data: JSON.stringify({
          token: token,
          id: id,
          type: type,
        }),
        success: function(result) {
          if(result["code"]!=200) {
            alertWarning(result["msg"]);
            return;
          }
          alertSuccess("设置成功！")
          flushTable();
        },
        error: dealError,
      })
    }
    $(".table-content button[name='setCur']").click(function() {
      setVersion($(this).parents("tr").find("td[name='id']").text(),"cur");
    });
    $(".table-content button[name='setMin']").click(function() {
      setVersion($(this).parents("tr").find("td[name='id']").text(),"min")
    });
  }
  function addVersion(version) {
    for(var i=0;i<version.length;i++) {
      var color,title;
      if(version[i].type == "min") {
        color = "#D44052";
        title = "最低兼容版本";
      } else {
        color = "#4990E2";
        title = "当前发布版本";
      }
      $(".table tbody").prepend(`
      <tr data-toggle="tooltip" title="`+title+`" style="background:`+color+`">
        <td>`+version[i].id+`</td>
        <td>`+version[i].publisherID+`</td>
        <td>`+version[i].version+`</td>
        <td>`+version[i].log+`</td>
        <td>`+version[i].OSType+`</td>
        <td>`+version[i].createTime+`</td>
      </tr>`);
    }
  }
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/updateLog",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return;
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        addVersion(res["version"]);
        setClick();
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/updateLog",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        addVersion(res["version"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".operation button[name='new']").click(function() {
          alter("","1.0.0","","");
        });
        setClick();
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#version-data-list>li[name='updateLog']").click(function() {
    getTable();
  });
});

//问问管理->问问管理
$(document).ready(function() {
  var pageIndex = 1
    , maxPageIndex
    ;
  function deleteAsk(obj) {
    $.ajax({
      type: "POST",
      url: "/delete/ask",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: $(obj).parents("tr").find("td[name='id']").text(),
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        alertSuccess("删除成功，发布此问问的用户在此之后72小时内已被禁止发问！");
        $(obj).parents("tr").remove();
      },
      error: dealError,
    });
  }
  function flushTable() {
    $.ajax({
      type: "POST",
      url: "/list/ask",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        $(".table-content button[name='delete']").click(function() {
          deleteAsk(this);
        });
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/ask",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".table-content button[name='delete']").click(function() {
          deleteAsk(this);
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#ask-manage-list>li[name='ask-manage']").click(function() {
    getTable();
  });
});

//问问管理->评论管理
$(document).ready(function() {
  var pageIndex = 1
    , maxPageIndex
    ;
  function deleteCommentAsk(obj) {
    $.ajax({
      type: "POST",
      url: "/delete/commentAsk",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        id: $(obj).parents("tr").find("td[name='id']").text(),
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        alertSuccess("删除成功，发布此评论的用户在此之后72小时内已被禁止发问！");
        $(obj).parents("tr").remove();
      },
      error: dealError,
    });
  }
  function flushTable() {
    $.ajax({
      type: "POST",
      url: "/list/commentAsk",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
        $(".table-content button[name='delete']").click(function() {
          deleteCommentAsk(this);
        });
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/commentAsk",
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return;
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $(".table-content button[name='delete']").click(function() {
          deleteCommentAsk(this);
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#ask-manage-list>li[name='comment-manage']").click(function() {
    getTable();
  });
});

//常用->报名表
function common(table) {
  var pageIndex = 1
    , maxPageIndex
    ;
  function flushTable() {
    $.ajax({
      type: 'POST',
      url: "/list/"+table,
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function (result,status,xhr) {
        if(result["code"]!=200) {
          alertWarning(result["msg"])
          return
        }
        alertSuccess("表格刷新成功");
        var res = result["result"]
        $(res["html"]).replaceAll(".content>.table-content");
      },
      error: dealError,
    });
  }
  function getTable() {
    $.ajax({
      type: 'GET',
      url:  "/list/"+table,
      contentType: "application/json",
      data: JSON.stringify({
        token: token,
        pageSize: pageSize,
        pageIndex: pageIndex - 1,
      }),
      success: function(result) {
        if(result["code"]!=200) {
          alertWarning(result["msg"]);
          return
        }
        var res = result["result"];
        $(".content").html(res["html"]);
        maxPageIndex = Number($(".footer span[name='maxPageIndex']").text());
        $(".operation button[name='refresh']").click(function() {
          flushTable();
        });
        $('.operation button[name="save"]').click(function() {
          var list=new Array();
          $('.table tr input[type="checkbox"]').each(function() {
            if(!$(this).prop("checked")) {
              return;
            }
            var tr = $(this).parents("tr");
            list.push({
              id: tr.find('td[name="id"]').text(),
              status: tr.find('td[name="status"] select').val(),
              award: tr.find('td[name="award"]').text(),
            });
          });
          console.log(list)
          if(list.length==0) {
            alertDanger("请先选择要保存的行！")
            return;
          }
          $.ajax({
            type: 'POST',
            url: "/save/list/"+table,
            contentType: "application/json",
            data: JSON.stringify({
              token: token,
              list: list,
            }),
            success: function(result) {
              if(result["code"]!=200) {
                alertWarning(result["msg"]);
                return;
              }
              alertSuccess("保存成功");
              $(".table tr input[type='checkbox']").prop("checked",false);
            },
            error: dealError,
          });
        });
        $(".footer .pageIndex").change(function() {
          pageIndex = Number($(this).val());
          flushTable();
        });
        $(".footer .pageSize").change(function() {
          pageSize = Number($(this).val());
          getTable();
        });
        $(".footer button[name='nextPage']").click(function() {
          if(pageIndex >= maxPageIndex) {
            pageIndex = maxPageIndex
            setPageIndex(pageIndex);
            alertWarning("没有下一页啦");
            return
          }
          pageIndex++;
          flushTable();
          setPageIndex(pageIndex);
        });
        $(".footer button[name='lastPage']").click(function() {
          if(pageIndex <= 1) {
            pageIndex = 1;
            setPageIndex(pageIndex);
            alertWarning("没有上一页啦");
            return
          }
          pageIndex--;
          flushTable();
          setPageIndex(pageIndex);
        });
      },
      error: dealError,
    });
  }
  $("#common-list>li[name='"+table+"']").click(function() {
    getTable();
  });
}
$(document).ready(function() {
  common("signUp");
  common("business");
  common("inform");
  common("advise");
  common("report");
});