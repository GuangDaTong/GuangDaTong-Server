package mewRedis

import "github.com/garyburd/redigo/redis"
import "time"

var (
	maxConnection  = 1000
	keepConnection = 100
	socket         = "127.0.0.1:6379"
	dbNum          = "0" //db编号，编号不同相当于不同的数据库
	password       = "123456"

	Pool *redis.Pool
)

func init() {
	Init(true)
}

func Init(test bool) {
	if !test {
		dbNum = "1"
		password = "mewmewstudio"
	}
	Pool = &redis.Pool{
		MaxIdle:     keepConnection,
		MaxActive:   maxConnection,
		IdleTimeout: time.Hour,
		Dial: func() (c redis.Conn, err error) {
			c, err = redis.Dial("tcp", socket)
			if err != nil {
				return nil, err
			}

			/*_, err = c.Do("AUTH", password)
			if err != nil {
				c.Close()
				return nil, err
			}*/

			_, err = c.Do("SELECT", dbNum)
			if err != nil {
				c.Close()
				return nil, err
			}
			return
		},
	}

	c := Pool.Get()
	defer c.Close()
	if c.Err() != nil {
		panic(c.Err().Error())
	}
}

func Do(command string, args ...interface{}) (reply interface{}, err error) {
	client := Pool.Get()
	defer client.Close()
	return client.Do(command, args...)
}

func GetClient() redis.Conn {
	return Pool.Get()
}

func CloseClient(client redis.Conn) {
	client.Close()
}
