package jpush

import (
	"config"
	"logs"

	"github.com/ylywyn/jpushclient"
)

func PushServiceUserID(userID string) {
	var (
		appKey = "950f0f17d23e46a8dbeedecf"
		secret = "ce2b33e35a3ca96afd1a2bef"
	)
	regID, err := getRegID(userID, `select regID "regID" from serviceToken where userID = $1`)
	if err != nil {
		logs.ErrorPrint("getRegID error:", err.Error(), userID)
		return
	}
	if regID == "" {
		return
	}
	alert := "有人咨询啦，快去回复吧"
	IDs := []string{regID}
	if !config.UseJPush {
		return
	}
	//logs.DebugPrint("send", alert, IDs)
	audience := &jpushclient.Audience{}
	if IDs != nil {
		audience.SetID(IDs)
	}

	notice := &jpushclient.Notice{}
	notice.SetAlert(alert)
	notice.SetIOSNotice(&jpushclient.IOSNotice{Alert: alert, Sound: "default", Badge: "+1"})
	notice.SetAndroidNotice(&jpushclient.AndroidNotice{Alert: alert})
	platform := &jpushclient.Platform{}
	platform.All()

	payload := jpushclient.NewPushPayLoad()
	payload.SetOptions(&jpushclient.Option{ApnsProduction: false})
	payload.SetPlatform(platform)
	payload.SetAudience(audience)
	payload.SetNotice(notice)

	bytes, err := payload.ToBytes()
	if err != nil {
		logs.ErrorPrint(err.Error())
		return
	}

	client := jpushclient.NewPushClient(secret, appKey)
	result, err := client.Send(bytes)
	if err != nil {
		logs.ErrorPrint(err.Error(), result)
		return
	}
}
