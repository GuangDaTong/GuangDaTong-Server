package jpush

import (
	"config"
	"testing"
	"time"
)

func TestPush(*testing.T) {
	go InitPush()
	PushLeaveWords("1")
	PushCommentAsk("1")
	PushAdpotComment("1")
	PushAskInformed("1")
	PushCommentAskInformed("1")
	PushCommentUser("1")
	PushContactAdded("1")
	PushGiveCoins("1")
	PushUserInformed("1")
	PushWantCoins("1")
	PushUrgent("1", "college", "学院求助", "广州大学", "计算机科学与教育软件学院")
	PushUrgent("1", "school", "学校求助", "广州大学", "")
	PushUrgent("1", "all college", "所有相同学院求助", "", "计算机科学与教育软件学院")
	PushUrgent("1", "all school", "所有学校求助", "", "")
	time.Sleep(time.Second * 20)
	config.UseJPush = false
	PushLeaveWords("1")
	PushCommentAsk("1")
	PushAdpotComment("1")
	PushAskInformed("1")
	PushCommentAskInformed("1")
	PushCommentUser("1")
	PushContactAdded("1")
	PushGiveCoins("1")
	PushUserInformed("1")
	PushWantCoins("1")
	PushUrgent("1", "college", "学院求助", "广州大学", "计算机科学与教育软件学院")
	PushUrgent("1", "school", "学校求助", "广州大学", "")
	PushUrgent("1", "all college", "所有相同学院求助", "", "计算机科学与教育软件学院")
	PushUrgent("1", "all school", "所有学校求助", "", "")
	time.Sleep(time.Second * 20)
}
