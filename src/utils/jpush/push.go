package jpush

import (
	"config"
	"logs"
	"sync"
	"time"
	"utils/pgsql"

	"github.com/ylywyn/jpushclient"
)

var (
	appKey     = "830864c3b1e485ba9117d120"
	secret     = "599e0eec00dda89950ea55ee"
	maxPushNum = 1000
)

func Init(test bool) {
	if !test {
		appKey = "13b58bff96976aedd5307f6c"
		secret = "11e3fda2975dc97d154e02cf"
	}
}

type pushInfo struct {
	lock     sync.Mutex
	IDs      []string
	alert    string
	interval time.Duration //发送时间间隔
	typee    string
}

type pushData struct {
	IDs   []string
	alert string
	extra map[string]interface{}
	badge string
}

var (
	pushLeaveWords         = &pushInfo{alert: "您收到一条留言", interval: time.Second * 1, typee: "leaveWords"}
	pushAdpotComment       = &pushInfo{alert: "您的回复被采纳啦", interval: time.Second * 5, typee: "ask"}
	pushCommentAsk         = &pushInfo{alert: "有人回复了您的问问", interval: time.Second * 5, typee: "ask"}
	pushCommentUser        = &pushInfo{alert: "有人@您", interval: time.Second * 5, typee: "ask"}
	pushGiveCoins          = &pushInfo{alert: "有人赠送了您喵币", interval: time.Second * 2, typee: "notice"}
	pushWantCoins          = &pushInfo{alert: "有人向您索要喵币", interval: time.Second * 2, typee: "notice"}
	pushContactAdded       = &pushInfo{alert: "有人添加您为联系人", interval: time.Second * 5, typee: "notice"}
	pushAskInformed        = &pushInfo{alert: "有人举报了您的问问", interval: time.Second * 3, typee: "notice"}
	pushUserInformed       = &pushInfo{alert: "有人举报了您", interval: time.Second * 3, typee: "notice"}
	pushCommentAskInformed = &pushInfo{alert: "有人举报了您的问问评论", interval: time.Second * 3, typee: "notice"}
	pushAdviseAdopted      = &pushInfo{alert: "您的建议被采纳啦", interval: time.Second * 4, typee: "notice"}
	pushAdviseRejected     = &pushInfo{alert: "抱歉，您的建议未被采纳", interval: time.Second * 4, typee: "notice"}
	pushReportAdopted      = &pushInfo{alert: "您的反馈被采纳啦", interval: time.Second * 4, typee: "notices"}
	pushReportRejected     = &pushInfo{alert: "抱歉，您的反馈未被采纳", interval: time.Second * 4, typee: "notices"}

	pushQueue = make(chan *pushData, 100000)

	//是否使用推送，并发测试时不能用
	Use = true

	fixIOSCrash = " and (OSType = 'android' or version >= '1.0.2')"
)

func (p *pushInfo) appendUserID(userID string, SQL string) {
	//logs.DebugPrint("appendUserID", p.alert, p.IDs, userID)
	regID, err := getRegID(userID, SQL)
	if err != nil {
		logs.ErrorPrint("getRegID error:", err.Error(), userID)
		return
	}
	if regID != "" {
		p.appendID(regID)
	}
}

func (p *pushInfo) appendID(id string) {
	//logs.DebugPrint("appendID", p.alert, p.IDs, id)
	p.lock.Lock()
	defer p.lock.Unlock()
	p.IDs = append(p.IDs, id)
	if len(p.IDs) >= maxPushNum {
		pushQueue <- &pushData{alert: p.alert, IDs: p.IDs, extra: map[string]interface{}{"type": p.typee}, badge: "+1"}
		p.IDs = nil
	}
}

func (p *pushInfo) push() {
	//logs.DebugPrint("push", p.alert, p.IDs)
	defer time.AfterFunc(p.interval, p.push)
	p.lock.Lock()
	defer p.lock.Unlock()
	if len(p.IDs) <= 0 {
		return
	}
	pushQueue <- &pushData{alert: p.alert, IDs: p.IDs, extra: map[string]interface{}{"type": p.typee}, badge: "+1"}
	p.IDs = nil
}

func Push(alert string, IDs []string, extra map[string]interface{}, badge string) {
	if !config.UseJPush {
		return
	}
	//logs.DebugPrint("send", alert, IDs)
	audience := &jpushclient.Audience{}
	if IDs != nil {
		audience.SetID(IDs)
	} else {
		audience.All()
	}

	notice := &jpushclient.Notice{}
	notice.SetAlert(alert)
	notice.SetIOSNotice(&jpushclient.IOSNotice{Alert: alert, Extras: extra, Sound: "default", Badge: badge})
	notice.SetAndroidNotice(&jpushclient.AndroidNotice{Alert: alert, Extras: extra})
	platform := &jpushclient.Platform{}
	platform.All()

	payload := jpushclient.NewPushPayLoad()
	payload.SetOptions(&jpushclient.Option{ApnsProduction: !config.Test})
	payload.SetPlatform(platform)
	payload.SetAudience(audience)
	payload.SetNotice(notice)
	//payload.SetMessage()

	bytes, err := payload.ToBytes()
	if err != nil {
		logs.ErrorPrint(err.Error())
		//pushQueue <- &pushData{alert: alert, IDs: IDs, extra: extra, badge: badge}
		return
	}

	client := jpushclient.NewPushClient(secret, appKey)
	result, err := client.Send(bytes)
	if err != nil {
		logs.ErrorPrint(err.Error(), result)
		return
	}
}

//推送留言
func PushLeaveWords(userID string) {
	pushLeaveWords.appendUserID(userID, `select regID from userInfo where userID = $1 and pushLeaveWords = true`)
}

//推送采纳回答
func PushAdpotComment(userID string) {
	pushAdpotComment.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

//推送回复问问
func PushCommentAsk(userID string) {
	pushCommentAsk.appendUserID(userID, `select regID from userInfo where userID = $1 and pushComment = true`)
}

//推送在问问里评论别人
func PushCommentUser(userID string) {
	pushCommentUser.appendUserID(userID, `select regID from userInfo where userID = $1 and pushComment = true`)
}

//推送送喵币
func PushGiveCoins(userID string) {
	pushGiveCoins.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

//推送索要喵币
func PushWantCoins(userID string) {
	pushWantCoins.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

//推送被添加联系人
func PushContactAdded(userID string) {
	pushContactAdded.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

//推送问问被举报
func PushAskInformed(userID string) {
	pushAskInformed.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

//推送人被举报
func PushUserInformed(userID string) {
	pushUserInformed.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

//推送问问评论被举报
func PushCommentAskInformed(userID string) {
	pushCommentAskInformed.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

func PushAdviseAdopted(userID string) {
	pushAdviseAdopted.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

func PushAdviseRejected(userID string) {
	pushAdviseRejected.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

func PushReportAdopted(userID string) {
	pushReportAdopted.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

func PushReportRejected(userID string) {
	pushReportRejected.appendUserID(userID, `select regID from userInfo where userID = $1 and pushSystem = true`)
}

func getRegID(userID string, SQL string) (string, error) {
	rows, err := pgsql.Query(SQL, userID)
	if err != nil {
		return "", err
	}
	defer rows.Close()
	var regID string
	if !rows.Next() {
		return "", nil
	}
	err = rows.Scan(&regID)
	return regID, err
}

func PushUrgent(id, rangee, title, school, college string) {
	alert := "有人向您求助：" + title
	args := []interface{}{}
	SQL := `select regID from userInfo inner join users on (userInfo.userID = users.id `
	switch rangee {
	case "college":
		SQL += "and users.school = $1 and users.college = $2) where userInfo.pushUrgentCollege = true"
		args = append(args, school, college)
	case "school":
		SQL += "and users.school = $1) where userInfo.pushUrgentSchool = true"
		args = append(args, school)
	default:
		logs.ErrorPrint("error range:", rangee)
		return
	}

	//iOS 问问推送crash
	SQL += fixIOSCrash

	rows, err := pgsql.Query(SQL, args...)
	if err != nil {
		logs.ErrorPrint(err.Error())
		return
	}
	defer rows.Close()
	var IDs []string
	for rows.Next() {
		var ID string
		err = rows.Scan(&ID)
		if err != nil {
			logs.ErrorPrint(err.Error())
			return
		}
		IDs = append(IDs, ID)
		if len(ID) > maxPushNum {
			pushQueue <- &pushData{alert: alert, IDs: IDs, extra: map[string]interface{}{"type": "urgentAsk", "id": id, "range": rangee}}
			IDs = nil
		}
	}
	if len(IDs) > 0 {
		pushQueue <- &pushData{alert: alert, IDs: IDs, extra: map[string]interface{}{"type": "urgentAsk", "id": id, "range": rangee}}
	}
}

func InitPush() {
	pushAdpotComment.push()
	pushLeaveWords.push()
	pushCommentAsk.push()
	pushCommentUser.push()
	pushGiveCoins.push()
	pushWantCoins.push()
	pushContactAdded.push()
	pushAskInformed.push()
	pushUserInformed.push()
	pushCommentAskInformed.push()
	pushAdviseAdopted.push()
	pushAdviseRejected.push()
	pushReportAdopted.push()
	pushReportRejected.push()
	for {
		data := <-pushQueue
		go Push(data.alert, data.IDs, data.extra, data.badge)
		time.Sleep(100 * time.Millisecond)
	}
}
