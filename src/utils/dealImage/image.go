package dealImage

import (
	"config"
	"context"
	"database/sql"
	"fmt"
	"io"
	"logs"
	"mewNet"
	"mime/multipart"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"

	rpc "qiniupkg.com/x/rpc.v7"

	"github.com/qiniu/api.v7/auth/qbox"
	"github.com/qiniu/api.v7/storage"
)

var (
	maxCompressSize int64 = 256 << 10 //256k
	maxImageSize    int64 = 2 << 20

	accessKey = "MYqxCWi4Nrv114F4LeLaD9ekYTgnNdgvrBkyVvRS"
	secretKey = "rHRBY7WTffwK5Na064oVm33sLKyg-Efph3KllhEa"
	bucket    = "debugpublic"
)

type Bounds struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

func InitBucket(buc string) {
	bucket = buc
}

//保存原图和缩略图	服务器压图
/*func SaveImage(imagePath string, thumbnailImagePath string, file multipart.File) (code int, msg, debugMsg string) {
	persent := 100
	size, ok := GetFileSize(file)
	if !ok {
		return j.CodeGetFileSizeFailed, j.MsgGetFileSizeFailed, "get file size error"
	}
	if size > maxCompressSize {
		persent = (int)(maxCompressSize / size)
	}
	if persent < 10 {
		persent = 10
	}
	img, _, err := image.Decode(file)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	//缩略图
	compress_img, err := os.Create(config.PublicFilePath + thumbnailImagePath)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	defer compress_img.Close()
	err = jpeg.Encode(compress_img, img, &jpeg.Options{persent})
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	//原图
	origin_img, err := os.Create(config.PublicFilePath + imagePath)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	defer origin_img.Close()
	err = jpeg.Encode(origin_img, img, &jpeg.Options{100})
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}

	return j.CodeSuccess, j.MsgSuccess, ""
}*/

func SaveImage(imagePath string, thumbnailImagePath string, file multipart.File, watermark bool, size int) (code int, msg, debugMsg string) {
	defer func() {
		if config.UseQiNiuYun {
			go fetchToBucket(imagePath, thumbnailImagePath, watermark, size)
		}
	}()
	defer file.Close()
	img, err := os.Create(config.PublicFilePath + imagePath)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	defer img.Close()

	thumbImg, err := os.Create(config.PublicFilePath + thumbnailImagePath)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	defer thumbImg.Close()

	_, err = io.Copy(img, file)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	_, err = img.Seek(0, 0)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}
	_, err = io.Copy(thumbImg, img)
	if err != nil {
		return j.CodeSaveImageError, j.MsgSaveImageError, err.Error()
	}

	return j.CodeSuccess, j.MsgSuccess, ""
}

var (
	wartermarkQuery = "?watermark/1/image/aHR0cDovL3NyYy5jb2xsZWdlbWV3LmNvbS93YXRlcm1hcmsucG5n/dissolve/80/gravity/SouthEast/dx/10/dy/10"
	imgQuery        = "?imageView2/2/w/2000/h/2000/q/100|imageslim"
	thumbImgQuery   = "?imageView2/4/w/300/h/300/q/100"
)

func fetchToBucket(imagePath, thumbnailImagePath string, wartermark bool, size int) {
	mac := qbox.NewMac(accessKey, secretKey)
	cfg := storage.Config{
		// 是否使用https域名进行资源管理
		UseHTTPS: true,
		Zone:     &storage.ZoneHuanan,
	}
	// 指定空间所在的区域，如果不指定将自动探测
	// 如果没有特殊需求，默认不需要指定
	//cfg.Zone=&storage.ZoneHuabei
	bucketManager := storage.NewBucketManager(mac, &cfg)

	resURL := config.SrcHost + imagePath
	/*err := bucketManager.Prefetch(bucket, imagePath)
	if err != nil {
		logs.ErrorPrint("更新镜像缓存", imagePath, "error:", err.Error())
	}*/
	uploadImage(config.PublicFilePath+imagePath, imagePath)
	fetchRet, err := bucketManager.Fetch(resURL+imgQuery, bucket, imagePath)
	if err != nil {
		logs.ErrorPrint("图片瘦身并压缩到2000×2000", resURL+imgQuery, "error:", err.Error())
	}
	logs.DebugPrint(fetchRet.String())
	// 指定保存的key
	thumbImgQuery = fmt.Sprintf("?imageView2/4/w/%d/h/%d/q/100", size, size)
	fetchRet, err = bucketManager.Fetch(resURL+thumbImgQuery, bucket, thumbnailImagePath)
	if err != nil {
		logs.ErrorPrint("保存缩略图300×300", resURL+thumbImgQuery, "error:", err.Error())
	}
	logs.DebugPrint(fetchRet.String())
	if wartermark {
		fetchRet, err = bucketManager.Fetch(resURL+wartermarkQuery, bucket, imagePath)
		if err != nil {
			logs.ErrorPrint("原图打水印", resURL+wartermarkQuery, "error:", err.Error())
		}
		logs.DebugPrint(fetchRet.String())
	}
}

func uploadImage(localFile, key string) {

	logs.DebugPrint("upload", localFile, key)
	/*	// 自定义返回值结构体
		type MyPutRet struct {
			Key    string `json:"key"`
			Hash   string `json:"hash"`
			Fsize  int    `json:"h"`
			Bucket string `json:"w"`
			Name   string `json:"name"`
		}*/
	// 使用 returnBody 自定义回复格式
	putPolicy := storage.PutPolicy{
		Scope: bucket + ":" + key,
		ReturnBody: `{
			\"name\":$(fname),
			\"size\":$(fsize),
			\"w\":$(imageInfo.width),
			\"h\":$(imageInfo.height),
			\"hash\":$(etag)
		}`,
		InsertOnly: 0,
	}
	logs.DebugPrint(fmt.Sprintf("%+v", putPolicy))
	mac := qbox.NewMac(accessKey, secretKey)
	upToken := putPolicy.UploadToken(mac)
	cfg := storage.Config{UseHTTPS: true, Zone: &storage.ZoneHuanan}
	formUploader := storage.NewFormUploader(&cfg)
	//ret := MyPutRet{}
	err := formUploader.PutFile(context.Background(), nil, upToken, key, localFile, nil)
	if err != nil {
		logs.ErrorPrint("upload", localFile, "to", bucket, key, "error:", err.Error())
		return
	}
	//logs.DebugPrint(ret.Bucket, ret.Key, ret.Fsize, ret.Hash, ret.Name)
}

func GetFileSize(file multipart.File) (int64, bool) {
	if statInterface, ok := file.(Stat); ok {
		fileInfo, err := statInterface.Stat()
		if err == nil {
			return fileInfo.Size(), ok
		}
	}
	if sizeInterface, ok := file.(Size); ok {
		return sizeInterface.Size(), ok
	}
	return 0, false
}

func AllowImageSize(file multipart.File) (code int, msg, debugMsg string) {
	size, ok := GetFileSize(file)
	if !ok {
		return j.CodeGetFileSizeFailed, j.MsgGetFileSizeFailed, "get file size error"
	}
	logs.DebugPrint(size, maxImageSize)
	if size > maxImageSize {
		return j.CodeImageTooLarge, j.MsgImageTooLarge, fmt.Sprintf("image too large,size should less than %vb,but size is %vb", maxImageSize, size)
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}

// 文件为大小文件的依据是ParseMultipartForm的maxMemory参数的大小
// 获取文件大小的接口，上传小文件时，文件类型为：multipart.sectionReadCloser
type Size interface {
	Size() int64
}

// 获取文件信息的接口，上传大文件时，文件类型为： *os.File
type Stat interface {
	Stat() (os.FileInfo, error)
}

func JudgeImage(header *multipart.FileHeader) (code int, msg, debugMsg, subfix string) {
	logs.DebugPrint(header.Filename, header.Header, header.Size)
	if header.Size > maxImageSize {
		return j.CodeImageTooLarge, j.MsgImageTooLarge, fmt.Sprintf("max size:%v b 	upload size:%v b", header.Size, maxImageSize), ""
	}

	switch header.Header.Get("Content-Type") {
	case "image/gif":
		subfix = ".gif"
	case "image/jpeg":
		subfix = ".jpg"
	case "image/png":
		subfix = ".png"
	case "image/x-icon":
		subfix = ".ico"
	/*
		case "image/webp":
			subfix = ".webp"
		case "image/x-ms-bmp":
			subfix = ".bmp"
		case "image/svg+xml":
			subfix = ".svg"
		case "image/vnd.wap.wbmp":
			subfix = ".wbmp"
	*/
	default:
		return j.CodeSaveImageError, j.MsgSaveImageError, "not image type", ""
	}
	return j.CodeSuccess, j.MsgSuccess, "", subfix
}

func ShouldWatermark(bounds *Bounds) bool {
	return bounds.Height >= 200 && bounds.Width >= 200
}

//pathPrefix是本地存储文件夹路径
func DeleteMapArray(pathPrefix string, data []map[string]interface{}, keys ...string) {
	var arr []string
	for _, dataVal := range data {
		for _, val := range keys {
			images, ok := dataVal[val].(string)
			if !ok {
				logs.ErrorPrint("type assert to string failed:", dataVal[val])
				return
			}
			imageArr := mewNet.StringToStringArray(images)
			arr = append(arr, imageArr...)
		}
	}
	DeleteArray(pathPrefix, arr)
}

//pathPrefix是本地存储文件夹路径
func DeleteArray(pathPrefix string, arr []string) {
	for _, val := range arr {
		err := os.Remove(pathPrefix + val)
		if err != nil {
			logs.ErrorPrint("remove image", pathPrefix+val, "error:", err.Error())
		}
	}
	for i := 0; i < len(arr); i += 1000 {
		var length = i*1000 + 1000
		if length > len(arr) {
			length = len(arr)
		}
		deleteArray(arr[i*1000 : length])
	}
}

func deleteArray(arr []string) {
	if !config.UseQiNiuYun {
		return
	}
	deleteOps := make([]string, 0, len(arr))
	for _, key := range arr {
		deleteOps = append(deleteOps, storage.URIDelete(bucket, key))
	}

	mac := qbox.NewMac(accessKey, secretKey)
	cfg := storage.Config{
		// 是否使用https域名进行资源管理
		UseHTTPS: true,
		Zone:     &storage.ZoneHuanan,
	}
	bucketManager := storage.NewBucketManager(mac, &cfg)
	rets, err := bucketManager.Batch(deleteOps)
	if err != nil {
		// 遇到错误
		if _, ok := err.(*rpc.ErrorInfo); ok {
			for _, ret := range rets {
				// 200 为成功
				logs.DebugPrint("%d\n", ret.Code)
				if ret.Code != 200 {
					logs.ErrorPrint("%s\n", ret.Data.Error)
				}
			}
		} else {
			logs.ErrorPrint("batch error:", err.Error())
		}
	} else {
		// 完全成功
		for _, ret := range rets {
			// 200 为成功
			logs.DebugPrint("%d\n", ret.Code)
		}
	}

}

//保存table类型html的image
func TxSaveImageArray(tx *sql.Tx, table, id string, images []string) (code int, msg, debugMsg string) {
	var oldImages []string
	if id == "" {
		oldImages = []string{}
	} else {
		rows, err := pgsql.Query(`select images from `+table+` where id = $1`, id)
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error()
		}
		var data map[string]string
		code, msg, debugMsg, data = mewNet.RowsToMapString(rows)
		if code != j.CodeSuccess {
			return
		}
		oldImages = mewNet.StringToStringArray(data["images"])
	}
	var imagesMap = map[string]bool{}
	var oldImagesMap = map[string]bool{}
	for _, val := range images {
		imagesMap[val] = true
	}
	for _, val := range oldImages {
		oldImagesMap[val] = true
	}
	stmt, err := tx.Prepare(`delete from ` + table + `Images where link = $1`)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error()
	}
	defer stmt.Close()
	stmt1, err := tx.Prepare(`insert into ` + table + `Images(link,createTime) values($1,$2)
	on conflict do nothing`)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error()
	}
	defer stmt1.Close()
	for _, val := range images {
		if oldImagesMap[val] {
			continue
		}
		_, err := stmt.Exec(val)
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error()
		}
	}
	for _, val := range oldImages {
		if imagesMap[val] {
			continue
		}
		_, err := stmt1.Exec(val, time.Now())
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error()
		}
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}

func TxDeleteImagesAndIcon(tx *sql.Tx, table string, data []map[string]interface{}) (code int, msg, debugMsg string) {
	stmt, err := tx.Prepare(`insert into ` + table + `Images(link,createTime) values($1,$2) 
	on conflict do nothing`)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error()
	}
	defer stmt.Close()
	var now = time.Now()
	for _, data := range data {
		images, ok := data["images"].(string)
		if !ok {
			return j.CodeTypeAssertFailed, j.MsgTypeAssertFailed, `data["images"] type assert failed`
		}
		imageArr := mewNet.StringToStringArray(images)
		for _, val := range imageArr {
			_, err = stmt.Exec(val, now)
			if err != nil {
				return j.CodeDBExecError, j.MsgDBExecError, err.Error()
			}
		}
		icon, ok := data["icon"].(string)
		if !ok {
			continue
		}
		if icon == "" || icon == mewNet.DefaultIcon {
			continue
		}
		_, err := stmt.Exec(icon, now)
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error()
		}
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}
