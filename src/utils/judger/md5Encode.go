package judger

import (
	"crypto/md5"
	"fmt"
	"io"
)

func Md5Encode(args ...string) string {
	hash := md5.New()
	for _, val := range args {
		io.WriteString(hash, val)
	}
	return fmt.Sprintf("%x", hash.Sum(nil))
}
