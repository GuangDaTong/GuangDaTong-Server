# judger
这个包里放的是判断函数，一般情况下没有错误都是返回CodeSuccess
但也有例外，如IsExistAccount，具体参考代码

返回一般都包含code,msg,debugMsg
code用于识别错误码
msg会显示到客户端
debugMsg方便开发人员调试