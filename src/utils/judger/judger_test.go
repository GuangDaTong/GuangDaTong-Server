package judger

import (
	"config"
	"fmt"
	"net/http"
	"strings"
	"testing"
	"utils/pgsql"
)

func TestJudge(*testing.T) {
	for i := 0; i < 2; i++ {
		config.Test = i == 0
		fmt.Println("test:", config.Test)
		testJudgeAccount()
		testJudgeSign()
		testJudgeGetReqJSON()
		testIsExistAccount()
		testCorrectPassword()
		testJudgePassword()
		testJudgeID()
		testJudgePageIndex()
		testJudgePageSize()
		testJudgeOrder()
		testJudgeOrderBy()
		testJudgeObjectName()
		testJudgeShareWay()
		testJudgeAskLabel()
		testJudgeAskType()
		testJudgeStringToPositiveInt()
		testJudgeContent()
		testJudgeName()
		testJudgeSchoolDataType()
		testJudgeDiscoveryType()
		testJudgeIDs()
		testJudgeGender()
		testJudgeGrade()
	}
}

var (
	testAccount  = "00000000000"
	testAccount1 = "12345678901"
	testPassword = "123456"
)

func testJudgeAccount() {
	test := []struct {
		account  string
		code     int
		msg      string
		debugMsg string
		name     string
	}{
		{
			account: "1233456",
			code:    CodeMatchAccountFailed,
			msg:     MsgMatchAccountFailed,
			name:    "match account error",
		},
		{
			code: CodeEmptyAccount,
			msg:  MsgEmptyAccount,
			name: "empty account",
		},
		{
			account: testAccount,
			code:    CodeSuccess,
			msg:     MsgSuccess,
			name:    "success",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeAccount(val.account)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}
func testJudgeSign() {
	test := []struct {
		sign     string
		args     []string
		code     int
		msg      string
		debugMsg string
		name     string
	}{
		{
			sign: Md5Encode(config.Salt, testAccount),
			args: []string{config.Salt, testAccount},
			code: CodeSuccess,
			msg:  MsgSuccess,
			name: "success",
		},
		{
			sign: Md5Encode(config.Salt, testAccount, "aaa"),
			args: []string{config.Salt, testAccount + "aaa"},
			code: CodeSuccess,
			msg:  MsgSuccess,
			name: "success",
		},
		{
			sign: Md5Encode(config.Salt, testAccount, "aaa"),
			code: CodeSignWrong,
			msg:  MsgSignWrong,
			name: "sign wrong",
		},
	}
	config.Test = true
	for _, val := range test {
		code, msg, debugMsg := JudgeSign(val.sign, val.args...)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

var URL string = "http://127.0.0.1"

func testJudgeGetReqJSON() {
	type example struct {
		code     int
		msg      string
		debugMsg string
		name     string
		req      *http.Request
		reqJson  interface{}
	}
	val := example{
		code:    CodeSuccess,
		msg:     MsgSuccess,
		name:    "success",
		reqJson: map[string]string{"aaa": "bbb"},
	}
	var err error
	val.req, err = http.NewRequest("POST", URL, strings.NewReader(`{"aaa":"bbb"}`))
	if err != nil {
		panic(err.Error())
	}
	val.req.Header.Set("Content-Type", "application/json;charset=utf-8")
	code, msg, debugMsg := GetReqJSON(val.req, val.reqJson)
	fmt.Println(code, msg, debugMsg, val.code, val.msg)
	if code != val.code || msg != val.msg {
		panic(val)
	}
	fmt.Println("test", val.name, "ok")
	val = example{
		code:    CodeRequestJsonError,
		msg:     MsgRequestJsonError,
		name:    "request json error",
		req:     &http.Request{},
		reqJson: map[string]string{"aaa": "bbb"},
	}
	val.req, err = http.NewRequest("POST", URL, strings.NewReader(`{"aaa":}`))
	if err != nil {
		panic(err.Error())
	}
	val.req.Header.Set("Content-Type", "application/json;charset=utf-8")
	code, msg, debugMsg = GetReqJSON(val.req, val.reqJson)
	fmt.Println(code, msg, debugMsg, val.code, val.msg)
	if code != val.code || msg != val.msg {
		panic(val)
	}
	fmt.Println("test", val.name, "ok")
	val = example{
		code:    CodeContentTypeNotJSON,
		msg:     MsgContentTypeNotJSON,
		name:    "Content-Type error",
		reqJson: map[string]string{"aaa": "bbb"},
	}
	val.req, err = http.NewRequest("POST", URL, strings.NewReader(`{"aaa":"bbb"}`))
	if err != nil {
		panic(err.Error())
	}
	val.req.Header.Set("Content-Type", "text/html;charset=utf-8")
	code, msg, debugMsg = GetReqJSON(val.req, val.reqJson)
	fmt.Println(code, msg, debugMsg, val.code, val.msg)
	if code != val.code || msg != val.msg {
		panic(val)
	}
	fmt.Println("test", val.name, "ok")
}
func testIsExistAccount() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		account  string
	}{
		{
			code:    CodeAccountExist,
			msg:     MsgAccountExist,
			name:    "account exist",
			account: testAccount,
		},
		{
			code:    CodeAccountNotExist,
			msg:     MsgAccountNotExist,
			name:    "account not exist",
			account: testAccount1,
		},
		{
			code:    CodeMatchAccountFailed,
			msg:     MsgMatchAccountFailed,
			name:    "match account failed",
			account: "2345678901",
		},
	}
	_, err := pgsql.Exec(`delete from users where account = $1 or account = $2;`, testAccount, testAccount1)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into users(account,nickname,icon,password,gender,coins,college,school,isInblacklist,exp,custom) values
	($1,'dy','icon/default.ico','123456','男',0,'计算机科学与教育软件学院','广州大学',false,0,'{}');`, testAccount)
	if err != nil {
		panic(err.Error())
	}
	for _, val := range test {
		code, msg, debugMsg, _ := IsExistAccount(val.account)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}
func testCorrectPassword() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		account  string
		password string
	}{
		{
			code:     CodeSuccess,
			msg:      MsgSuccess,
			name:     "success",
			account:  testAccount,
			password: testPassword,
		},
		{
			code:     CodePasswordTooShort,
			msg:      MsgPasswordTooShort,
			name:     "password too short",
			account:  testAccount,
			password: "12345", //一个中文3位
		},
		{
			code:     CodeAccountNotExist,
			msg:      MsgAccountNotExist,
			name:     "account not exist",
			password: testPassword,
		},
		{
			code:     CodePasswordWrong,
			msg:      MsgPasswordWrong,
			name:     "password wrong",
			account:  testAccount,
			password: testPassword + "a",
		},
	}
	_, err := pgsql.Exec(`delete from users where account = $1 or account = $2;`, testAccount, testAccount1)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into users(account,nickname,icon,password,gender,coins,college,school,isInblacklist,exp,custom) values
	($1,'dy','icon/default.ico',$2,'男',0,'计算机科学与教育软件学院','广州大学',false,0,'{}');`, testAccount, testPassword)
	if err != nil {
		panic(err.Error())
	}
	for _, val := range test {
		code, msg, debugMsg := CorrectPassword(val.account, val.password)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}

}
func testJudgePassword() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		password string
	}{
		{
			code:     CodeSuccess,
			msg:      MsgSuccess,
			name:     "success",
			password: "123456",
		},
		{
			code:     CodePasswordTooShort,
			msg:      MsgPasswordTooShort,
			name:     "password too short",
			password: "12345",
		},
		{
			code:     CodePasswordTooLong,
			msg:      MsgPasswordTooLong,
			name:     "password too long",
			password: "1234kasldjflaskdjfl;kasdjflkjasdlkfja;lskdjflkasjdflkajsdlkfj5",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgePassword(val.password)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeID() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		objectID string
	}{
		{
			code:     CodeSuccess,
			msg:      MsgSuccess,
			name:     "success",
			objectID: "123098230480918",
		},
		{
			code:     CodeIDError,
			msg:      MsgIDError,
			name:     "not int",
			objectID: "lasdkj",
		},
		{
			code:     CodeIDError,
			msg:      MsgIDError,
			name:     "negative",
			objectID: "-12989285793547",
		},
		{
			code:     CodeIDError,
			msg:      MsgIDError,
			name:     "not int",
			objectID: "123123a",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeID(val.objectID)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgePageIndex() {
	test := []struct {
		code      int
		msg       string
		debugMsg  string
		name      string
		pageIndex int64
	}{
		{
			code:      CodeSuccess,
			msg:       MsgSuccess,
			name:      "success",
			pageIndex: 123098230480918,
		},
		{
			code:      CodePageIndexError,
			msg:       MsgPageIndexError,
			name:      "negative",
			pageIndex: -12989285793547,
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgePageIndex(val.pageIndex)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgePageSize() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		pageSize int64
	}{
		{
			code:     CodeSuccess,
			msg:      MsgSuccess,
			name:     "success",
			pageSize: 12,
		},
		{
			code:     CodePageSizeError,
			msg:      MsgPageSizeError,
			name:     "negative",
			pageSize: -12989285793547,
		},
		{
			code:     CodePageSizeError,
			msg:      MsgPageSizeError,
			name:     "too large",
			pageSize: 10000,
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgePageSize(val.pageSize)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeOrder() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		order    string
	}{
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			order: "",
			name:  "empty success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			order: "desc",
			name:  "desc success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			order: "asc",
			name:  "asc success",
		},
		{
			code:  CodeOrderError,
			msg:   MsgOrderError,
			order: "dasc",
			name:  "order error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg, _ := JudgeOrder(val.order)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeOrderBy() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		orderBy  string
	}{
		{
			code:    CodeSuccess,
			msg:     MsgSuccess,
			orderBy: "",
			name:    "empty success",
		},
		{
			code:    CodeSuccess,
			msg:     MsgSuccess,
			orderBy: "createTime",
			name:    "createTime success",
		},
		{
			code:    CodeOrderByError,
			msg:     MsgOrderByError,
			orderBy: "icon",
			name:    "orderBy error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg, _ := JudgeOrderBy(val.orderBy)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeObjectName() {
	test := []struct {
		code       int
		msg        string
		debugMsg   string
		name       string
		objectName string
	}{
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "users",
			name:       "users success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "guidance",
			name:       "guidance success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "summary",
			name:       "summary success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "club",
			name:       "club success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "look",
			name:       "look success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "partTimeJob",
			name:       "partTimeJob success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "activity",
			name:       "activity success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "training",
			name:       "training success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "ask",
			name:       "ask success",
		},
		{
			code:       CodeSuccess,
			msg:        MsgSuccess,
			objectName: "commentAsk",
			name:       "commentAsk success",
		},
		{
			code:       CodeObjectNameError,
			msg:        MsgObjectNameError,
			objectName: "commentUser",
			name:       "objectName error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeObjectName(val.objectName)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeShareWay() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		way      string
	}{
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			way:  "QQFriend",
			name: "QQFriend success",
		},
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			way:  "QQZone",
			name: "QQZone success",
		},
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			way:  "weChatFriend",
			name: "weChatFriend success",
		},
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			way:  "weChatCircle",
			name: "weChatCircle success",
		},
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			way:  "weibo",
			name: "weibo success",
		},
		{
			code: CodeShareWayError,
			msg:  MsgShareWayError,
			way:  "facebook",
			name: "shareWay error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeShareWay(val.way)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeAskLabel() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		label    string
	}{
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "失物招领",
			name:  "失物招领 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "寻物启事",
			name:  "寻物启事 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "学习",
			name:  "学习 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "考研",
			name:  "考研 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "实习",
			name:  "实习 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "游玩",
			name:  "游玩 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "交通",
			name:  "交通 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "情感",
			name:  "情感 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "培训",
			name:  "培训 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "购物",
			name:  "购物 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "美食",
			name:  "美食 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			label: "其他",
			name:  "其他 success",
		},
		{
			code:  CodeAskLabelError,
			msg:   MsgAskLabelError,
			label: "面试",
			name:  "askLabel error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeAskLabel(val.label)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeAskType() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		typee    string
	}{
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "急问",
			name:  "急问 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "普问",
			name:  "普问 success",
		},
		{
			code:  CodeAskTypeError,
			msg:   MsgAskTypeError,
			typee: "",
			name:  "askType error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeAskType(val.typee)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeStringToPositiveInt() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		str      string
	}{
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			str:  "123",
			name: "success",
		},
		{
			code: CodeIllegalPositiveInt,
			msg:  MsgIllegalPositiveInt,
			str:  "-123",
			name: "error negative int",
		},
		{
			code: CodeIllegalPositiveInt,
			msg:  MsgIllegalPositiveInt,
			str:  "123m",
			name: "error not int",
		},
		{
			code: CodeIllegalPositiveInt,
			msg:  MsgIllegalPositiveInt,
			str:  "alksdjf",
			name: "error not int",
		},
	}
	for _, val := range test {
		code, msg, debugMsg, _ := JudgeStringToPositiveInt(val.str)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeContent() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		content  string
	}{
		{
			code:    CodeSuccess,
			msg:     MsgSuccess,
			content: "aldksjflk",
			name:    "success",
		},
		{
			code:    CodeContentTooLong,
			msg:     MsgContentTooLong,
			content: "aaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaa",
			name:    "success",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeContent(val.content)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeName() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		Name     string
	}{
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			Name: "dy",
			name: "success",
		},
		{
			code: CodeIllegalName,
			msg:  MsgIllegalName,
			Name: "dylsakjdflkasjdkljfaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaajsalkdjflk",
			name: "name too long",
		},
		{
			code: CodeIllegalName,
			msg:  MsgIllegalName,
			Name: "",
			name: "empty name",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeName(val.Name)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}
func testJudgeSchoolDataType() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		typee    string
	}{
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "guidance",
			name:  "guidance success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "summary",
			name:  "summary success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "club",
			name:  "club success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "look",
			name:  "look success",
		},
		{
			code:  CodeObjectNameError,
			msg:   MsgObjectNameError,
			typee: "",
			name:  "type error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeSchoolDataType(val.typee)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeDiscoveryType() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		typee    string
	}{
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "partTimeJob",
			name:  "partTimeJob success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "activity",
			name:  "activity success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			typee: "training",
			name:  "training success",
		},
		{
			code:  CodeObjectNameError,
			msg:   MsgObjectNameError,
			typee: "",
			name:  "type error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeDiscoveryType(val.typee)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeIDs() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		IDs      []string
	}{
		{
			code: CodeSuccess,
			msg:  MsgSuccess,
			IDs:  []string{"1", "2"},
			name: "success",
		},
		{
			code: CodeIDError,
			msg:  MsgIDError,
			IDs:  []string{},
			name: "empty IDs",
		},
		{
			code: CodeIDError,
			msg:  MsgIDError,
			IDs:  []string{"1", "2-"},
			name: "empty IDs",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeIDs(val.IDs)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeGender() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		gender   string
	}{
		{
			code:   CodeSuccess,
			msg:    MsgSuccess,
			gender: "",
			name:   "empty success",
		},
		{
			code:   CodeSuccess,
			msg:    MsgSuccess,
			gender: "男",
			name:   "男 success",
		},
		{
			code:   CodeSuccess,
			msg:    MsgSuccess,
			gender: "女",
			name:   "女 success",
		},
		{
			code:   CodeIllegalGender,
			msg:    MsgIllegalGender,
			gender: "man",
			name:   "gender error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeGender(val.gender)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}

func testJudgeGrade() {
	test := []struct {
		code     int
		msg      string
		debugMsg string
		name     string
		grade    string
	}{
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			grade: "其他",
			name:  "其他 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			grade: "2014级",
			name:  "2014级 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			grade: "2000级",
			name:  "2000级 success",
		},
		{
			code:  CodeSuccess,
			msg:   MsgSuccess,
			grade: "2017级",
			name:  "2017级 success",
		},
		{
			code:  CodeIllegalGrade,
			msg:   MsgIllegalGrade,
			grade: "2017",
			name:  "2017 success",
		},
		{
			code:  CodeIllegalGrade,
			msg:   MsgIllegalGrade,
			grade: "17级",
			name:  "grade error",
		},
	}
	for _, val := range test {
		code, msg, debugMsg := JudgeGrade(val.grade)
		fmt.Println(code, msg, debugMsg, val.code, val.msg)
		if code != val.code || msg != val.msg {
			panic(val)
		}
		fmt.Println("test", val.name, "ok")
	}
}
