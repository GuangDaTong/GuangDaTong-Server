package judger

import (
	"config"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
	"utils/pgsql"
)

func JudgeAccount(account string) (code int, msg, debugMsg string) {
	if account == "" {
		return CodeEmptyAccount, MsgEmptyAccount, "empty account,account: " + account
	}
	ok, err := regexp.MatchString("^[0-9]{11}$", account)
	if err != nil {
		return CodeMatchStringError, MsgMacthStringError, err.Error()
	}
	if !ok {
		return CodeMatchAccountFailed, MsgMatchAccountFailed, "match account failed,account: " + account
	}
	return CodeSuccess, MsgSuccess, debugMsg
}

func JudgeSign(sign string, args ...string) (code int, msg, debugMsg string) {
	defer func() {
		if config.Test {
			debugMsg = "sign wrong,sign: " + sign
		}
	}()
	sign1 := Md5Encode(args...)
	if sign != sign1 {
		return CodeSignWrong, MsgSignWrong, ""
	}
	return CodeSuccess, MsgSuccess, ""
}

//从request中获取request data struct,Content-Type须为application/json
func GetReqJSON(r *http.Request, reqJson interface{}) (code int, msg, debugMsg string) {
	if strings.Index(r.Header.Get("Content-Type"), "application/json") == -1 {
		return CodeContentTypeNotJSON, MsgContentTypeNotJSON, "Content-Type should be application/json,Content-Type: " + r.Header.Get("Content-Type")
	}
	var body []byte
	var err error
	if r.Method == "GET" {
		query, err := url.QueryUnescape(r.URL.RawQuery)
		if err != nil {
			return CodeReadRequestBodyError, MsgReadRequestBodyError, err.Error()
		}
		body = []byte(query)
	} else {
		body, err = ioutil.ReadAll(r.Body)
		if err != nil {
			return CodeReadRequestBodyError, MsgReadRequestBodyError, err.Error()
		}
	}
	err = json.Unmarshal(body, &reqJson)
	if err != nil {
		return CodeRequestJsonError, MsgRequestJsonError, err.Error()
	}
	//logs.DebugPrint(reqJson)
	return CodeSuccess, MsgSuccess, ""
}

//先判account是否合法，再判是否存在
func IsExistAccount(account string) (code int, msg, debugMsg string, userID string) {
	code, msg, debugMsg = JudgeAccount(account)
	if code != CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select id from users where account = $1;`, account)
	if err != nil {
		return CodeDBQueryError, MsgDBQueryError, err.Error(), ""
	}
	defer rows.Close()
	if !rows.Next() {
		return CodeAccountNotExist, MsgAccountNotExist, "query no rows,rows: null", ""
	}
	err = rows.Scan(&userID)
	if err != nil {
		return CodeRowsScanError, MsgRowsScanError, err.Error(), ""
	}
	if rows.Next() {
		return CodeMultiAccount, MsgMultiAccount, "query exists multi rows,rows: ...", ""
	}
	return CodeAccountExist, MsgAccountExist, "", userID
}

const passwordSalt = "aed0f655c1130e88ef2d335d7988065e"

func EncryptPassword(password string) string {
	if pgsql.Test {
		return password
	}
	return Md5Encode(Md5Encode(passwordSalt+password+passwordSalt) + passwordSalt)
}

func CorrectPassword(account string, password string) (code int, msg, debugMsg string) {
	code, msg, debugMsg = JudgePassword(password)
	if code != CodeSuccess {
		return
	}
	rows, err := pgsql.Query("select password from users where account = $1;", account)
	if err != nil {
		return CodeDBQueryError, MsgDBQueryError, err.Error()
	}
	defer rows.Close()
	if !rows.Next() {
		return CodeAccountNotExist, MsgAccountNotExist, "query no rows,rows: null"
	}
	var pwd string
	err = rows.Scan(&pwd)
	if err != nil {
		return CodeRowsScanError, MsgRowsScanError, err.Error()
	}
	if EncryptPassword(password) != pwd {
		return CodePasswordWrong, MsgPasswordWrong, "password wrong,password: " + password
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgePassword(password string) (code int, msg, debugMsg string) {
	if len(password) < 6 {
		return CodePasswordTooShort, MsgPasswordTooShort, "password too short,password: " + password
	}
	if len(password) > 20 {
		return CodePasswordTooLong, MsgPasswordTooLong, "password too long,password: " + password
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeID(id string) (code int, msg, debugMsg string) {
	ok, err := regexp.MatchString("^[0-9]+$", id)
	if err != nil {
		return CodeMatchStringError, MsgMacthStringError, err.Error()
	}
	if !ok {
		return CodeIDError, MsgIDError, "match id failed"
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgePageIndex(pageIndex int64) (code int, msg, debugMsg string) {
	if pageIndex < 0 {
		return CodePageIndexError, MsgPageIndexError, "pageIndex < 0,pageIndex: " + fmt.Sprint(pageIndex)
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgePageSize(pageSize int64) (code int, msg, debugMsg string) {
	if pageSize <= 0 {
		return CodePageSizeError, MsgPageSizeError, "pageSize < 0,pageSize: " + fmt.Sprint(pageSize)
	}
	if pageSize > 300 {
		return CodePageSizeError, MsgPageSizeError, "pageSize too large,pageSize > 300,pageSize: " + fmt.Sprint(pageSize)
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeOrder(order string) (code int, msg, debugMsg string, Order string) {
	if order == "" {
		return CodeSuccess, MsgSuccess, "", "desc"
	}
	if order != "desc" && order != "asc" {
		return CodeOrderError, MsgOrderError, "wrong order,order: " + order, ""
	}
	return CodeSuccess, MsgSuccess, "", order
}

var OrderByName = map[string]bool{
	"readCount":   true,
	"zan":         true,
	"exp":         true,
	"grade":       true,
	"reward":      true,
	"coins":       true,
	"school":      true,
	"college":     true,
	"id":          true,
	"createTime":  true,
	"userID":      true,
	"publisherID": true,
	"fee":         true,
	"position":    true,
	"amount":      true,
}

func JudgeOrderBy(orderBy string) (code int, msg, debugMsg string, OrderBy string) {
	if orderBy == "" {
		return CodeSuccess, MsgSuccess, "", "createTime"
	}
	if !OrderByName[orderBy] {
		return CodeOrderByError, MsgOrderByError, "wrong orderBy,orderBy: " + orderBy, ""
	}
	return CodeSuccess, MsgSuccess, "", orderBy
}

var objectName = map[string]bool{
	"users":       true,
	"guidance":    true,
	"summary":     true,
	"club":        true,
	"look":        true,
	"partTimeJob": true,
	"activity":    true,
	"training":    true,
	"ask":         true,
	"commentAsk":  true,
	"App":         true,
}

func JudgeObjectName(name string) (code int, msg, debugMsg string) {
	if !objectName[name] {
		return CodeObjectNameError, MsgObjectNameError, "wrong objectName,objectName: " + name
	}
	return CodeSuccess, MsgSuccess, ""
}

var shareWay = map[string]bool{
	"QQFriend":     true,
	"QQZone":       true,
	"weChatFriend": true,
	"weChatCircle": true,
	"weibo":        true,
}

func JudgeShareWay(way string) (code int, msg, debugMsg string) {
	if !shareWay[way] {
		return CodeShareWayError, MsgShareWayError, "wrong shareWay,shareWay: " + way
	}
	return CodeSuccess, MsgSuccess, ""
}

var askLabel = map[string]bool{
	"失物招领": true, "寻物启事": true,
	"学习": true, "考研": true, "实习": true, "游玩": true,
	"交通": true, "情感": true, "培训": true,
	"购物": true, "美食": true, "其他": true,
}

func JudgeAskLabel(label string) (code int, msg, debugMsg string) {
	if !askLabel[label] {
		return CodeAskLabelError, MsgAskLabelError, "ask label error,label:" + label
	}
	return CodeSuccess, MsgSuccess, ""
}

var askType = map[string]bool{
	"普问": true, "急问": true,
}

func JudgeAskType(typee string) (code int, msg, debugMsg string) {
	if !askType[typee] {
		return CodeAskTypeError, MsgAskTypeError, "ask type error,type:" + typee
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeStringToPositiveInt(str string) (code int, msg, debugMsg string, num int) {
	_, err := fmt.Sscan(str, &num)
	if err != nil {
		return CodeIllegalPositiveInt, MsgIllegalPositiveInt, err.Error(), 0
	}
	if fmt.Sprint(num) != str {
		return CodeIllegalPositiveInt, MsgIllegalPositiveInt, "fmt.Sprint(num) != str", 0
	}
	if num < 0 {
		return CodeIllegalPositiveInt, MsgIllegalPositiveInt, "negative int:" + str, 0
	}
	return CodeSuccess, MsgSuccess, "", num
}

func JudgeContent(content string) (code int, msg, debugMsg string) {
	if len(content) <= 0 {
		return CodeEmptyContent, MsgEmptyContent, "len(content) == 0"
	}
	if len([]rune(content)) > 2048 {
		return CodeContentTooLong, MsgContentTooLong, "content too long,content:" + content
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeName(name string) (code int, msg, debugMsg string) {
	if name == "" {
		return CodeIllegalName, MsgIllegalName, "empty name,name:" + name
	}
	if len(name) > 50 {
		return CodeIllegalName, MsgIllegalName, "name too long,name:" + name
	}
	return CodeSuccess, MsgSuccess, ""
}

var schoolDataType = map[string]bool{
	"guidance": true,
	"summary":  true,
	"club":     true,
	"look":     true,
}

func JudgeSchoolDataType(objectName string) (code int, msg, debugMsg string) {
	if !schoolDataType[objectName] {
		return CodeObjectNameError, MsgObjectNameError, "objectName error,objectName:" + objectName
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeSchoolDataStatus(status string) (code int, msg, debugMsg string) {
	if status != "" && status != "NEW" && status != "HOT" {
		return CodeSchoolDataStatusError, MsgSchoolDataStatusError, "schoolDataStatus error,status: " + status
	}
	return CodeSuccess, MsgSuccess, ""
}

var discoveryType = map[string]bool{
	"partTimeJob": true,
	"activity":    true,
	"training":    true,
}

func JudgeDiscoveryType(objectName string) (code int, msg, debugMsg string) {
	if !discoveryType[objectName] {
		return CodeObjectNameError, MsgObjectNameError, "objectName error,objectName:" + objectName
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeIDs(IDs []string) (code int, msg, debugMsg string) {
	empty := true
	for _, val := range IDs {
		code, msg, debugMsg = JudgeID(val)
		if code != CodeSuccess {
			return
		}
		empty = false
	}
	if empty {
		return CodeIDError, MsgIDError, "empty IDs"
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeGender(gender string) (code int, msg, debugMsg string) {
	if gender != "男" && gender != "女" && gender != "" {
		return CodeIllegalGender, MsgIllegalGender, "error gender,gender: " + gender
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeGrade(grade string) (code int, msg, debugMsg string) {
	if grade == "其他" {
		return CodeSuccess, MsgSuccess, ""
	}
	var year int
	_, err := fmt.Sscanf(grade, "%d级", &year)
	if err != nil {
		return CodeIllegalGrade, MsgIllegalGrade, err.Error()
	}
	if year < 2000 || year > time.Now().Year() {
		return CodeIllegalGrade, MsgIllegalGrade, "error grade on year,grade: " + grade
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeTitle(title string) (code int, msg, debugMsg string) {
	if title == "" {
		return CodeIllegalTitle, MsgIllegalTitle, "empty title"
	}
	if len([]rune(title)) > 15 {
		return CodeIllegalTitle, MsgIllegalTitle, "len(title)>15,is too long,title:" + title
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeWebViewTitle(title string) (code int, msg, debugMsg string) {
	if title == "" {
		return CodeIllegalTitle, MsgIllegalTitle, "empty title"
	}
	if len([]rune(title)) > 30 {
		return CodeWebViewTitleTooLong, MsgWebViewTitleTooLong, "len(title)>30,is too long,title:" + title
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeHTMLContent(content string) (code int, msg, debugMsg string) {
	if len(content) > (1 << 19) {
		return CodeHTMLContentTooLong, MsgHTMLContentTooLong, "content too long,len(content):" + fmt.Sprint(len(content))
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeManagerType(typee string) (code int, msg, debugMsg string) {
	if typee != "admin" && typee != "developer" && typee != "editor" {
		return CodeManagerTypeError, MsgManagerTypeError, "manager type error,type:" + typee
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeKey(key string) (code int, msg, debugMsg string) {
	if key == "" {
		return CodeEmptySearchKey, MsgEmptySearchKey, "key is empty"
	}
	return CodeSuccess, MsgSuccess, ""
}

//check blacklist
func CheckBlacklist(ownerID, userID string) (code int, msg, debugMsg string) {
	rows, err := pgsql.Query(`select userID "userID" from blacklist where ownerID = $1 and userID = $2`,
		ownerID, userID)
	if err != nil {
		return CodeDBQueryError, MsgDBQueryError, err.Error()
	}
	defer rows.Close()
	if rows.Next() {
		return CodeInBlacklist, MsgInBlacklist, "in blacklist"
	}
	return CodeSuccess, MsgSuccess, ""
}

//不允许editor操作
func NotPermitEditor(typee string) (code int, msg, debugMsg string) {
	if typee == "editor" {
		return CodeNoPermission, MsgNoPermission, "editor has no permission to do this"
	}
	return CodeSuccess, MsgSuccess, ""
}

//只允许admin操作
func PermitAdmin(typee string) (code int, msg, debugMsg string) {
	if typee != "admin" {
		return CodeNoPermission, MsgNoPermission, "editor and developer has no permission to do this"
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeOSType(OSType string) (code int, msg, debugMsg string) {
	if OSType != "android" && OSType != "ios" {
		return CodeOSTypeError, MsgOSTypeError, ""
	}
	return CodeSuccess, MsgSuccess, ""
}

func JudgeVersion(version string) (code int, msg, debugMsg string) {
	var a, b, c int
	n, err := fmt.Sscanf(version, "%d.%d.%d", &a, &b, &c)
	if err != nil {
		return CodeVersionError, MsgVersionError, err.Error()
	}
	if n != 3 {
		return CodeVersionError, MsgVersionError, "not version format a.b.c"
	}
	if a < 0 || b < 0 || c < 0 {
		return CodeVersionError, MsgVersionError, "version can't have negative number"
	}
	return CodeSuccess, MsgSuccess, ""
}
