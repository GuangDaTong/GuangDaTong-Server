package pgsql_test

import (
	"fmt"
	"testing"
	"utils/pgsql"
)

func TestInit(*testing.T) {
	pgsql.Init(false, "./postgresql.conf")
	db := pgsql.GetDB()
	_, err := db.Exec("insert into schools (name) values($1);", "广州大学")
	if err != nil {
		fmt.Println(err.Error())
	}
	pgsql.CloseDB(db)
	pgsql.Init(true, "./postgresql.conf")
	db = pgsql.GetDB()
	_, err = db.Exec("select 123;")
	if err != nil {
		fmt.Println(err.Error())
	}
	pgsql.CloseDB(db)
}
