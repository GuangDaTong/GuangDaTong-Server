package pgsql

import (
	"bufio"
	"database/sql"
	"fmt"
	"logs"
	"os"
	"time"

	_ "github.com/lib/pq"
)

var (
	Test            = true
	test            = true
	database        = "postgres"
	host            = "localhost"
	port            = "5432"
	user            = "test"
	password        = "mewmewstudio"
	dbname          = "test"
	sslmode         = "disable"
	maxDBConnection = 10
	dbQueue         chan *sql.DB
	configFilePath  = "null"
)

func Init(mode bool, filePath string) {

	test = mode
	configFilePath = filePath
	file, err := os.Open(configFilePath)
	if err != nil {
		panic(err.Error())
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var s, key, value, tmp string
		s = scanner.Text()
		if len(s) <= 0 || s[0] == '#' {
			continue
		}
		n, err := fmt.Sscan(s, &key, &tmp, &value)
		if err != nil {
			panic("打开配置文件出错：" + err.Error())
		}
		if n != 3 {
			panic("打开配置文件出错：a = b格式有误")
		}
		switch key {
		case "database":
			if !test {
				database = value
			}
		case "test_database":
			if test {
				database = value
			}
		case "host":
			if !test {
				host = value
			}
		case "test_host":
			if test {
				host = value
			}
		case "port":
			if !test {
				port = value
			}
		case "test_port":
			if test {
				port = value
			}
		case "user":
			if !test {
				user = value
			}
		case "test_user":
			if test {
				user = value
			}
		case "password":
			if !test {
				password = value
			}
		case "test_password":
			if test {
				password = value
			}
		case "dbname":
			if !test {
				dbname = value
			}
		case "test_dbname":
			if test {
				dbname = value
			}
		case "sslmode":
			if !test {
				sslmode = value
			}
		case "test_sslmode":
			if test {
				sslmode = value
			}
		case "maxDBConnection":
			if !test {
				fmt.Sscan(value, &maxDBConnection)
			}
		case "test_maxDBConnection":
			if test {
				fmt.Sscan(value, &maxDBConnection)
			}
		default:
			fmt.Println("error pgsql config:", key, tmp, value)
		}
	}
	config()
}
func config() {
	if test {
		password = "123456"
	} else {
		password = "mewmewstudio"
	}
	if dbQueue != nil {
		for len(dbQueue) != 0 {
			GetDB().Close()
		}
	}
	dbQueue = make(chan *sql.DB, maxDBConnection)
	postgrelConfig := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s",
		host, port, user, dbname, sslmode)
	for i := 0; i < maxDBConnection; i++ {
		db, err := sql.Open(database, postgrelConfig+" password="+password)
		if err != nil {
			panic(err.Error())
		}
		dbQueue <- db
	}
	fmt.Println(`pgsqlConfig:`, time.Now(), `
	pgsqlConfigFile`, configFilePath, `
	test:`, test, `
	database:`, database, `
	password: can't show
	maxDBConnection:`, maxDBConnection, `
	postgresqlConfig:`, postgrelConfig, `
	`)
	Test = test
}
func init() {
	config()
}

func GetDB() *sql.DB {
	return <-dbQueue
}
func CloseDB(db *sql.DB) {
	dbQueue <- db
}

func Exec(query string, args ...interface{}) (sql.Result, error) {
	db := GetDB()
	defer CloseDB(db)
	stmt, err := db.Prepare(query)
	if err != nil {
		logs.ErrorPrint(err.Error(), query, args)
		return nil, err
	}
	defer stmt.Close()
	return stmt.Exec(args...)
}

func Query(query string, args ...interface{}) (*sql.Rows, error) {
	db := GetDB()
	defer CloseDB(db)
	stmt, err := db.Prepare(query)
	if err != nil {
		logs.ErrorPrint(err.Error(), query, args)
		return nil, err
	}
	defer stmt.Close()
	return stmt.Query(args...)
}

func TxExec(tx *sql.Tx, Sql string, args ...interface{}) (sql.Result, error) {
	stmt, err := tx.Prepare(Sql)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	return stmt.Exec(args...)
}
