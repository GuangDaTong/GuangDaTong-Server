package message

import (
	"encoding/json"
	"fmt"
	"mewNet"
	"modal"
	"net/http"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

func giveCoins(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.GiveCoinsJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealGiveCoins(reqJson.Sign, reqJson.ReceiverID, reqJson.Words, reqJson.Amount)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealGiveCoins(sign, receiverID, words, amount string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(receiverID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, intAmount := j.JudgeStringToPositiveInt(amount)
	if code != j.CodeSuccess {
		return
	}
	if intAmount == 0 {
		return j.CodeAmountCanNotZero, j.MsgAmountCanNotZero, "amount is 0", result
	}
	code, msg, debugMsg = j.JudgeContent(words)
	if words != "" && code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.CheckBlacklist(receiverID, userID)
	if code != j.CodeSuccess {
		return
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}
	defer func() {
		defer func() {
			if code == j.CodeSuccess {
				jpush.PushGiveCoins(receiverID)
			}
		}()
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()
	sqlResult, err := pgsql.TxExec(tx, `update users set coins = coins - $2 where id = $1 and coins >= $2`,
		userID, amount)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return j.CodeCoinsNotEnough, j.MsgCoinsNotEnough, debugMsg, result
	}
	sqlResult, err = pgsql.TxExec(tx, `update users set coins = coins + $2 where id = $1`, receiverID, amount)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err = pgsql.TxExec(tx, `insert into accountBook(userID,createTime,amount,description) values
	($1,$2,$3,$4),($5,$2,$6,$7)`, userID, time.Now(), -intAmount, "赠送他人喵币", receiverID, intAmount, "获赠喵币")
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	affect, err := sqlResult.RowsAffected()
	if err != nil {
		return j.CodeDBExecNotEffect, j.MsgDBExecNotEffect, err.Error(), result
	}
	if affect != 2 {
		return j.CodeDBExecNotEffect, j.MsgDBExecNotEffect, fmt.Sprintf("db exec should affect 2 rows,but affect %d rows", affect), result
	}
	detail := map[string]string{"coins": amount, "words": words}
	if words != "" {
		detail["words"] = words
	}
	detailBytes, err := json.Marshal(detail)
	if err != nil {
		return j.CodeMarshalJsonError, j.MsgMarshalJsonError, err.Error(), result
	}
	sqlResult, err = pgsql.TxExec(tx, `insert into notices(senderID,receiverID,detail,createTime,typee)
	values ($1,$2,$3,$4,$5);`, userID, receiverID, detailBytes, time.Now(), "giveCoins")
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
