package message

import (
	"mewNet"
	"modal"
	"net/http"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

func leaveWords(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.LeaveWordsJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealLeaveWords(reqJson.Sign, reqJson.ReceiverID, reqJson.Words)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealLeaveWords(sign, receiverID, words string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(receiverID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(words)
	if code != j.CodeSuccess {
		return
	}
	//check forbidLeaveWords
	code, msg, debugMsg, createTime := mewNet.GetIntCreateTime(`select createTime from forbidLeaveWords
	where userID = $1`, userID)
	if code != j.CodeSuccess {
		return
	}
	if createTime+mewNet.ForbidLeaveWordDuration > time.Now().Unix() {
		return j.CodeLeaveWordsForbidden, j.MsgLeaveWordsForbidden, "", result
	}
	code, msg, debugMsg = j.CheckBlacklist(receiverID, userID)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`
	insert into leaveWords(senderID,receiverID,words,objectName,objectID,createTime)
	values($1,$2,$3,$4,$5,$6) returning id,createTime "createTime"`, userID, receiverID,
		words, nil, nil, time.Now())
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, mewNet.DealMapDoNotThing)
	if code != j.CodeSuccess {
		return
	}
	result = data

	var isService bool
	for _, val := range mewNet.ServiceIDs {
		if val == receiverID {
			isService = true
		}
	}
	if isService {
		jpush.PushServiceUserID(receiverID)
	} else {
		jpush.PushLeaveWords(receiverID)
	}

	return j.CodeSuccess, j.MsgSuccess, "", result
}
