package message

import (
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

func wantCoins(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.WantCoinsJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealWantCoins(reqJson.Sign, reqJson.GiverID, reqJson.Words, reqJson.Amount)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealWantCoins(sign, receiverID, words, amount string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(receiverID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, _ = j.JudgeStringToPositiveInt(amount)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(words)
	if words != "" && code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.CheckBlacklist(receiverID, userID)
	if code != j.CodeSuccess {
		return
	}
	detail := map[string]string{"coins": amount}
	if words != "" {
		detail["words"] = words
	}
	detailBytes, err := json.Marshal(detail)
	if err != nil {
		return j.CodeMarshalJsonError, j.MsgMarshalJsonError, err.Error(), result
	}
	sqlResult, err := pgsql.Exec(`insert into notices(senderID,receiverID,detail,createTime,typee) values
	($1,$2,$3,$4,$5);`, userID, receiverID, detailBytes, time.Now(), "wantCoins")
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	jpush.PushWantCoins(receiverID)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
