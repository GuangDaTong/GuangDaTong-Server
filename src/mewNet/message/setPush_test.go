package message

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func SetPushExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	_, err := db.Exec(
		`delete from userInfo where userID = ` + testUserID + `;
		insert into userInfo(regID,userID,osType) values('alksdjflksadjf','` + testUserID + `','ios');`)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.SetPushJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign:         j.Md5Encode(config.Salt, token),
				UrgentSchool: true,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
	}
}
func testSetPush(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
