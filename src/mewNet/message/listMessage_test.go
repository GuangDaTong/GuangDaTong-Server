package message

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func ListMessageExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken("1")
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	_, err := db.Exec(`
	insert into leaveWords(senderID,receiverID,words,createTime) values
	(` + testUserID + `,1,'words1',now()),
	(` + testUserID + `,1,'words2',now()),
	(` + testUserID + `,1,'words3',now());
	insert into askMsg(senderID,receiverID,askID,createTime,typee,detail) values
	(` + testUserID + `,1,1,now(),'zanAsk','{}'::jsonb),
	(` + testUserID + `,1,1,now(),'adoptComment','{}'::jsonb),
	(` + testUserID + `,1,1,now(),'favoriteAsk','{}'::jsonb);
	insert into notices(senderID,receiverID,createTime,typee,detail) values
	(` + testUserID + `,1,now(),'zanUser','{}'::jsonb),
	(` + testUserID + `,1,now(),'giveCoins','{}'::jsonb),
	(` + testUserID + `,1,now(),'wantCoins','{}'::jsonb);
	`)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.ListMessageJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not eixst",
		},
	}
}
func testListMessage(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
