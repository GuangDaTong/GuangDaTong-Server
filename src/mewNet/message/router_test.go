package message

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "leaveWords",
		Method:     "POST",
		Pattern:    "/leaveWords",
		SetExample: LeaveWordsExample,
		Test:       testLeaveWords,
	},
	{
		Name:       "listLeaveWords",
		Method:     "POST",
		Pattern:    "/list/leaveWords",
		SetExample: ListMessageExample,
		Test:       testListMessage,
	},
	{
		Name:       "wantCoins",
		Method:     "POST",
		Pattern:    "/wantCoins",
		SetExample: WantCoinsExample,
		Test:       testWantCoins,
	},
	{
		Name:       "giveCoins",
		Method:     "POST",
		Pattern:    "/giveCoins",
		SetExample: GiveCoinsExample,
		Test:       testGiveCoins,
	},
	{
		Name:       "listNotices",
		Method:     "POST",
		Pattern:    "/list/notices",
		SetExample: ListMessageExample,
		Test:       testListMessage,
	},
	{
		Name:       "listAskMsg",
		Method:     "POST",
		Pattern:    "/list/askMsg",
		SetExample: ListMessageExample,
		Test:       testListMessage,
	},
	{
		Name:       "setPush",
		Method:     "POST",
		Pattern:    "/setPush",
		SetExample: SetPushExample,
		Test:       testSetPush,
	},
	{
		Name:       "listUserData",
		Method:     "POST",
		Pattern:    "/list/userData",
		SetExample: ListUserDataExample,
		Test:       testListUserData,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples
var ToBytes = modal.ToBytes

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample
type RespJSON = modal.RespJSON
