package message

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func setPush(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.SetPushJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealSetPush(reqJson.Sign, reqJson.UrgentCollege, reqJson.UrgentSchool,
		reqJson.Comment, reqJson.LeaveWords, reqJson.Activity, reqJson.System)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealSetPush(sign string, urgentSchool, urgentCollege, comment, leaveWords, activity, system bool) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.Exec(`update userInfo set pushUrgentSchool = $1,pushUrgentCollege = $2,
	pushComment = $3,pushLeaveWords = $4,pushActivity = $5,pushSystem = $6 where userID = $7`,
		urgentSchool, urgentCollege, comment, leaveWords, activity, system, userID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
