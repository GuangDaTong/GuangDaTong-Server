package message

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listUserData(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListUserDataJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListUserData(reqJson.Sign, reqJson.IDs)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListUserData(sign string, IDs []string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	mp := map[string]bool{}
	for _, val := range IDs {
		mp[val] = true
		code, msg, debugMsg = j.JudgeID(val)
		if code != j.CodeSuccess {
			return
		}
	}
	if len(mp) <= 0 {
		return j.CodeIDError, j.MsgIDError, "empty IDs", result
	}

	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	stmt, err := db.Prepare(`select icon,nickname,gender from users where id = $1`)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	defer stmt.Close()
	var list []map[string]interface{}
	for key, _ := range mp {
		rows, err := stmt.Query(key)
		if err != nil {
			return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
		}
		var data map[string]interface{}
		code, msg, debugMsg, data = mewNet.RowsToMap(rows, mewNet.DealMapIcon)
		if code != j.CodeSuccess {
			return
		}
		data["id"] = key
		list = append(list, data)
	}
	mewNet.AddDataToResult(result, "list", list)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
