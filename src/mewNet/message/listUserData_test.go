package message

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func ListUserDataExample() []TestExample {
	type exampleJSON = modal.ListUserDataJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
				IDs:  []string{"1", "2"},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs: []string{"1", "2"},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
				IDs:  []string{"1", "2", "1", "2"},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
				IDs:  []string{},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "empty IDs",
		},
	}
}
func testListUserData(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
