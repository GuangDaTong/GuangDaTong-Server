package message

import (
	"mewNet"
	"modal"
)

var Routes = []Route{
	Route{
		"leaveWords",
		"POST",
		"/leaveWords",
		leaveWords,
	},
	Route{
		"listLeaveWords",
		"POST",
		"/list/leaveWords",
		listLeaveWords,
	},
	Route{
		"wantCoins",
		"POST",
		"/wantCoins",
		wantCoins,
	},
	Route{
		"giveCoins",
		"POST",
		"/giveCoins",
		giveCoins,
	},
	Route{
		"listNotices",
		"POST",
		"/list/notices",
		listNotices,
	},
	Route{
		"listAskMsg",
		"POST",
		"/list/askMsg",
		listAskMsg,
	},
	Route{
		"setPush",
		"POST",
		"/setPush",
		setPush,
	},
	Route{
		"listUserData",
		"POST",
		"/list/userData",
		listUserData,
	},
}

func init() {
	mewNet.AppendRoute(Routes)
}

type Route = modal.Route
