package message

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func LeaveWordsExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err := pgsql.Exec(`insert into blacklist(ownerID,userID,createTime) values($1,$2,now())
	on conflict do nothing`, 3, testUserID)
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, token1 := mewNet.SetToken("3")
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err = pgsql.Exec(`insert into forbidLeaveWords(userID,createTime) values($1,$2) on conflict(userID)
	do update set createTime = $2`, 3, time.Now().Add(-time.Hour*24).Unix())
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.LeaveWordsJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Words:      "aaaasalkdjflaskdjfasldkjf",
				ReceiverID: "1",
				Sign:       j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				Words:      "aaaasalkdjflaskdjfasldkjf",
				ReceiverID: "1",
				Sign:       j.Md5Encode(config.Salt, token1),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeLeaveWordsForbidden),
				Msg:  j.MsgLeaveWordsForbidden,
			},
			Name: "leaveWords forbidden",
		},
		{
			Request: ToBytes(&exampleJSON{
				Words:      "aaaasalkdjflaskdjfasldkjf",
				ReceiverID: "3",
				Sign:       j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeInBlacklist),
				Msg:  j.MsgInBlacklist,
			},
			Name: "in blacklist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Words:      "aaaasalkdjflaskdjfasldkjf",
				ReceiverID: "11",
				Sign:       j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "leaveWordsToService",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Words:      "aaaasalkdjflaskdjfasldkjf",
				ReceiverID: "1",
				Sign:       j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Words:      "aaaasalkdjflaskdjfasldkjf",
				ReceiverID: "1a",
				Sign:       j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				Words:      "aaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaa",
				ReceiverID: "1",
				Sign:       j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeContentTooLong),
				Msg:  j.MsgContentTooLong,
			},
			Name: "content too long",
		},
	}
}
func testLeaveWords(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
