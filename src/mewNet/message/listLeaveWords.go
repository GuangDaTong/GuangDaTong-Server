package message

import (
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func listLeaveWords(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListMessageJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListLeaveWords(reqJson.Sign)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListLeaveWords(sign string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	var now = time.Now()
	rows, err := pgsql.Query(`
	select 
	msg.id,
	msg.senderID "senderID",
	msg.createTime "createTime",
	msg.words,
	users.icon,
	users.gender,
	users.nickname
	from leaveWords msg
	inner join users on (msg.senderID = users.id) 
	left outer join getLeaveWords get on (get.userID = msg.receiverID)
	where msg.receiverID = $1 and 
	(get.createTime is null or msg.createTime > get.createTime)
	order by msg.createTime desc`,
		userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.Exec(`insert into getLeaveWords(userID,createTime) values($1,$2)
	on conflict(userID) do update set createTime = $2`, userID, now)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "data", data)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
