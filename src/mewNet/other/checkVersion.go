package other

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
)

func checkVersion(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.CheckVersionJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealCheckVersion(reqJson.Sign, reqJson.OSType)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealCheckVersion(sign, OSType string) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeOSType(OSType)
	if code != j.CodeSuccess {
		return
	}
	if OSType == "android" {
		mewNet.AddDataToResult(result, "curVersion", mewNet.AndroidCurVersion)
		mewNet.AddDataToResult(result, "minVersion", mewNet.AndroidMinVersion)
		mewNet.AddDataToResult(result, "updateLog", mewNet.AndroidUpdateLog)
	} else if OSType == "ios" {
		mewNet.AddDataToResult(result, "curVersion", mewNet.IOSCurVersion)
		mewNet.AddDataToResult(result, "minVersion", mewNet.IOSMinVersion)
		mewNet.AddDataToResult(result, "updateLog", mewNet.IOSUpdateLog)
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
