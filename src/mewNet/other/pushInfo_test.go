package other

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func PushInfoExample() []TestExample {
	type exampleJSON = modal.PushInfoJSON
	return []TestExample{
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				RegID: "alksdhflkasdjflkjlk",
				Sign:  j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				OSType: "io",
				RegID:  "alsdkjflkasdjfklasjda",
				Sign:   j.Md5Encode(config.Salt, "alsdkjflkasdjfklasjda"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOSTypeError),
				Msg:  j.MsgOSTypeError,
			},
			Name: "OSType error",
		},
		{
			Request: ToBytes(&exampleJSON{
				OSType: "ios",
				RegID:  "alsdkjflkasdjfklasjda",
				Sign:   j.Md5Encode(config.Salt, "alsdkjflkasdjfklasjda"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				OSType: "android",
				RegID:  "alsdkjflkasdjfklasjdb",
				Sign:   j.Md5Encode(config.Salt, "alsdkjflkasdjfklasjdb"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				OSType: "android",
				UserID: testUserID,
				RegID:  "alsdkjflkasdjlsakjdflfklasjdc",
				Sign:   j.Md5Encode(config.Salt, "alsdkjflkasdjlsakjdflfklasjdc"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				Country:  "中国",
				Province: "广东省",
				City:     "广州市",
				District: "番禺区",
				County:   "",
				Street:   "大学城环校西路",
				SiteName: "广州大学-计算机楼",
				UserID:   testUserID,
				OSType:   "ios",
				Version:  "1.0.1",
				RegID:    "alsdkjflkasdjlkkjafklasjdd",
				Sign:     j.Md5Encode(config.Salt, "alsdkjflkasdjlkkjafklasjdd"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "repeat success",
		},
	}
}
func testPushInfo(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
