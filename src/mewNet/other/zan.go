package other

import (
	"encoding/json"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"strings"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func zan(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	table := strings.TrimPrefix(r.URL.Path, "/zan/")
	code, msg, debugMsg = j.JudgeObjectName(table)
	if code != j.CodeSuccess {
		http.NotFound(w, r)
		return
	}
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ZanJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealZan(reqJson.Sign, reqJson.ID, table)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealZan(sign, objectID, objectName string) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(objectID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeObjectName(objectName)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.Exec(`insert into zan`+objectName+`(userID,objectID) values($1,$2) 
	on conflict(userID,objectID) do nothing;`, userID, objectID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return j.CodeHaveBeenZan, j.MsgHaveBeenZan, debugMsg, result
	}
	sqlResult, err = pgsql.Exec("update "+objectName+" set zan = zan+1 where id = $1", objectID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess { //zan数量出错对前端影响不大，出错不返回给前端，也不用事务
		logs.ErrorPrint("error: update "+objectName+" set zan = zan+1 where id = $1,id:", objectID)
	}
	if objectName == "ask" {
		go dealZanAskMsg(objectID, userID)
	} else if objectName == "users" {
		go dealZanUserMsg(userID, objectID)
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
func dealZanAskMsg(askID, senderID string) {
	rows, err := pgsql.Query(`select publisherID "receiverID",title "title" from ask where id = $1`, askID)
	if err != nil {
		logs.ErrorPrint("exec sql error:", askID)
		return
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		logs.ErrorPrint("code:", code, "msg:", msg, "debugMsg:", debugMsg)
		return
	}
	code, msg, debugMsg = j.CheckBlacklist(data["publisherID"], senderID)
	if code != j.CodeSuccess {
		return
	}
	detail := map[string]string{"title": data["title"]}
	detailBytes, err := json.Marshal(detail)
	if err != nil {
		logs.ErrorPrint("marshal json error:", err.Error())
		return
	}
	sqlResult, err := pgsql.Exec(`insert into askMsg(askID,senderID,receiverID,detail,createTime,typee)
	values($1,$2,$3,$4,$5,$6)`, askID, senderID, data["receiverID"], detailBytes, time.Now(), "zanAsk")
	if err != nil {
		logs.ErrorPrint("exec sql error:", err.Error(), askID, senderID, data["receiverID"], string(detailBytes), time.Now(), "zanAsk")
		return
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		logs.ErrorPrint("code:", code, "msg:", msg, "debugMsg:", debugMsg)
		return
	}
}
func dealZanUserMsg(senderID, receiverID string) {
	code, msg, debugMsg := j.CheckBlacklist(receiverID, senderID)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.Exec(`insert into notices(senderID,receiverID,detail,createTime,typee) values
	($1,$2,'{}'::jsonb,$3,$4)`, senderID, receiverID, time.Now(), "zanUser")
	if err != nil {
		logs.ErrorPrint("exec sql error:", err.Error(), senderID, receiverID, time.Now(), "zanUser")
		return
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		logs.ErrorPrint("code:", code, "msg:", msg, "debugMsg:", debugMsg)
		return
	}
}
