package other

import (
	"config"
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "checkVersion",
		Method:     "POST",
		Pattern:    "/checkVersion",
		SetExample: CheckVersionExample,
		Test:       testCheckVersion,
	},
	{
		Name:       "pushInfo",
		Method:     "POST",
		Pattern:    "/pushInfo",
		SetExample: PushInfoExample,
		Test:       testPushInfo,
	},
	{
		Name:       "advise",
		Method:     "POST",
		Pattern:    "/advise",
		SetExample: AdviseExample,
		Test:       testAdvise,
	},
	{
		Name:       "business",
		Method:     "POST",
		Pattern:    "/business",
		SetExample: BusinessExample,
		Test:       testBusiness,
	},
	{
		Name:       "informUsers",
		Method:     "POST",
		Pattern:    "/inform/users",
		SetExample: InformUsersExample,
		Test:       testInform,
	},
	{
		Name:       "informAsk",
		Method:     "POST",
		Pattern:    "/inform/ask",
		SetExample: InformAskExample,
		Test:       testInform,
	},
	{
		Name:       "informCommentAsk",
		Method:     "POST",
		Pattern:    "/inform/commentAsk",
		SetExample: InformCommentAskExample,
		Test:       testInform,
	},
	{
		Name:       "ZanUsers",
		Method:     "POST",
		Pattern:    "/zan/users",
		SetExample: ZanUsersExample,
		Test:       testZan,
	},
	{
		Name:       "ZanGuidance",
		Method:     "POST",
		Pattern:    "/zan/guidance",
		SetExample: ZanGuidanceExample,
		Test:       testZan,
	},
	{
		Name:       "ZanSummary",
		Method:     "POST",
		Pattern:    "/zan/summary",
		SetExample: ZanSummaryExample,
		Test:       testZan,
	},
	{
		Name:       "ZanClub",
		Method:     "POST",
		Pattern:    "/zan/club",
		SetExample: ZanClubExample,
		Test:       testZan,
	},
	{
		Name:       "ZanLook",
		Method:     "POST",
		Pattern:    "/zan/look",
		SetExample: ZanLookExample,
		Test:       testZan,
	},
	{
		Name:       "ZanPartTimeJob",
		Method:     "POST",
		Pattern:    "/zan/partTimeJob",
		SetExample: ZanPartTimeJobExample,
		Test:       testZan,
	},
	{
		Name:       "ZanActivity",
		Method:     "POST",
		Pattern:    "/zan/activity",
		SetExample: ZanActivityExample,
		Test:       testZan,
	},
	{
		Name:       "ZanTraining",
		Method:     "POST",
		Pattern:    "/zan/training",
		SetExample: ZanTrainingExample,
		Test:       testZan,
	},
	{
		Name:       "ZanAsk",
		Method:     "POST",
		Pattern:    "/zan/ask",
		SetExample: ZanAskExample,
		Test:       testZan,
	},
	{
		Name:       "crashLog",
		Method:     "POST",
		Pattern:    "/crashLog",
		SetExample: CrashLogExample,
		Test:       testCrashLog,
	},
	{
		Name:       "getCoinsAndRules",
		Method:     "POST",
		Pattern:    "/getCoinsAndRules",
		SetExample: GetCoinsAndRulesExample,
		Test:       testGetCoinsAndRules,
	},
	{
		Name:       "shareGuidance",
		Method:     "POST",
		Pattern:    "/share/guidance",
		SetExample: ShareGuidanceExample,
		Test:       testShare,
	},
	{
		Name:       "shareSummary",
		Method:     "POST",
		Pattern:    "/share/summary",
		SetExample: ShareSummaryExample,
		Test:       testShare,
	},
	{
		Name:       "shareClub",
		Method:     "POST",
		Pattern:    "/share/club",
		SetExample: ShareClubExample,
		Test:       testShare,
	},
	{
		Name:       "shareLook",
		Method:     "POST",
		Pattern:    "/share/look",
		SetExample: ShareLookExample,
		Test:       testShare,
	},
	{
		Name:       "sharePartTimeJob",
		Method:     "POST",
		Pattern:    "/share/partTimeJob",
		SetExample: SharePartTimeJobExample,
		Test:       testShare,
	},
	{
		Name:       "shareActivity",
		Method:     "POST",
		Pattern:    "/share/activity",
		SetExample: ShareActivityExample,
		Test:       testShare,
	},
	{
		Name:       "shareTraining",
		Method:     "POST",
		Pattern:    "/share/training",
		SetExample: ShareTrainingExample,
		Test:       testShare,
	},
	{
		Name:       "shareAsk",
		Method:     "POST",
		Pattern:    "/share/ask",
		SetExample: ShareAskExample,
		Test:       testShare,
	},
	{
		Name:       "shareApp",
		Method:     "POST",
		Pattern:    "/share/App",
		SetExample: ShareAppExample,
		Test:       testShare,
	},
	{
		Name:       "report",
		Method:     "POST",
		Pattern:    "/report",
		SetExample: ReportExample,
		Test:       testReport,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

func ToBytes(v interface{}) []byte {
	bytes, err := json.Marshal(v)
	if err != nil {
		panic(err.Error())
	}
	return bytes
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample
type RespJSON = modal.RespJSON
