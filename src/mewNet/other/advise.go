package other

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func advise(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.AdviseJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealAdvise(reqJson.Token, reqJson.Description)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealAdvise(token, description string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	var userID interface{}
	code, msg, debugMsg, userID = mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		userID = nil
	}
	code, msg, debugMsg = j.JudgeContent(description)
	if code != j.CodeSuccess {
		return
	}
	_, err := pgsql.Exec(`insert into advise(userID,description,createTime,status,award) values
	($1,$2,$3,'等待查看',0)`, userID, description, time.Now())
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
