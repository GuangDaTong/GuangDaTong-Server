package other

import (
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"strings"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

func inform(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	table := strings.TrimPrefix(r.URL.Path, "/inform/")
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.InformJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealInform(reqJson.Sign, reqJson.Reason, reqJson.ID, table)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealInform(sign, reason, objectID, objectName string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(objectID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeObjectName(objectName)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(reason)
	if code != j.CodeSuccess {
		return
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}

	code, msg, debugMsg, createTime := mewNet.GetIntCreateTime(`select createTime from forbidInform
	where userID = $1`, userID)
	if code != j.CodeSuccess {
		return
	}
	if createTime+mewNet.ForbidInformDuration > time.Now().Unix() {
		return j.CodeInformForbidden, j.MsgInformForbidden, "", result
	}

	var typee, receiverID string
	var pushFuntion func(string)

	defer func() {
		defer func() {
			if code == j.CodeSuccess {
				pushFuntion(receiverID)
			}
		}()
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()

	sqlResult, err := pgsql.TxExec(tx, `insert into inform(userID,reason,createTime,status,objectID,objectName,
	award) values($1,$2,$3,$4,$5,$6,0);`, userID, reason, time.Now(), "等待查看", objectID, objectName)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	var detail = map[string]string{}
	switch objectName {
	case "ask":
		code, msg, debugMsg, detail = mewNet.TxQueryMapString(tx, `select title "title",
		publisherID "receiverID" from ask where id = $1`, objectID)
		if code != j.CodeSuccess {
			return
		}
		detail["askID"] = objectID
		receiverID = detail["receiverID"]
		delete(detail, "receiverID")
		typee = "askInformed"
		pushFuntion = jpush.PushAskInformed
	case "users":
		typee = "userInformed"
		receiverID = objectID
		pushFuntion = jpush.PushUserInformed
	case "commentAsk":
		code, msg, debugMsg, detail = mewNet.TxQueryMapString(tx, `select commentAsk.askID "askID",
		commentAsk.content "comment",commentAsk.publisherID "receiverID",ask.title "title"
		from commentAsk inner join ask on (commentAsk.askID = ask.id)
		where commentAsk.id = $1`, objectID)
		if code != j.CodeSuccess {
			return
		}
		receiverID = detail["receiverID"]
		delete(detail, "receiverID")
		typee = "commentAskInformed"
		pushFuntion = jpush.PushCommentAskInformed
	}
	detail["reason"] = reason
	detailBytes, err := json.Marshal(detail)
	if err != nil {
		return j.CodeMarshalJsonError, j.MsgMarshalJsonError, err.Error(), result
	}
	sqlResult, err = pgsql.TxExec(tx, `insert into notices(senderID,receiverID,detail,createTime,typee) 
	values ($1,$2,$3,$4,$5)`, mewNet.SystemUserID, receiverID, detailBytes, time.Now(), typee)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
