package other

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func CrashLogExample() []TestExample {
	type exampleJSON = modal.CrashLogJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Log:       "ios crash",
				IsRelease: true,
				Version:   "1.0.0",
				Sign:      j.Md5Encode(config.Salt, "ios"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "ios success",
		},
		{
			Request: ToBytes(&exampleJSON{
				Log:       "android crash",
				IsRelease: false,
				Version:   "1.0.0",
				Sign:      j.Md5Encode(config.Salt, "android"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "android success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Log:  "android crash",
				Sign: j.Md5Encode(config.Salt, "and"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				Log:       "android crash",
				IsRelease: false,
				Version:   "1.0",
				Sign:      j.Md5Encode(config.Salt, "android"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalVersion),
				Msg:  j.MsgIllegalVersion,
			},
			Name: "illegal version",
		},
		{
			Request: ToBytes(&exampleJSON{
				Log:     "android crash",
				Version: "1.0.0",
				Sign:    j.Md5Encode(config.Salt, "android"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "android success",
		},
	}
}
func testCrashLog(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
