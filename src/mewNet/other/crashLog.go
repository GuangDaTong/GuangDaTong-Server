package other

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func crashLog(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.CrashLogJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealCrashLog(reqJson.Sign, reqJson.Log, reqJson.Version, reqJson.IsRelease)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealCrashLog(sign, log, version string, IsRelease bool) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	osType := "ios"
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, "ios")
	if code != j.CodeSuccess {
		code, msg, debugMsg = j.JudgeSign(sign, config.Salt, "android")
		if code != j.CodeSuccess {
			return
		}
		osType = "android"
	}
	_, err := (&config.Version{}).Scan(version)
	if err != nil {
		return j.CodeIllegalVersion, j.MsgIllegalVersion, err.Error(), result
	}
	sqlResult, err := pgsql.Exec(`insert into crashLog(OSType,log,createTime,version,IsRelease) 
	values($1,$2,$3,$4,$5)`, osType, log, time.Now(), version, IsRelease)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
