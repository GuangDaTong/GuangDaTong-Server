package other

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

const (
	objNameUsers       = "users"
	objNameGuidance    = "guidance"
	objNameSummary     = "summary"
	objNameClub        = "club"
	objNameLook        = "look"
	objNamePartTimeJob = "partTimeJob"
	objNameActivity    = "activity"
	objNameTraining    = "training"
	objNameAsk         = "ask"
	objNameComment     = "comment"

	debugExamplePath   = "../debugExample"
	releaseExamplePath = "../releaseExample"
)

var (
	SWQQFriend     = "QQFriend"
	SWQQZone       = "QQZone"
	SWWeChatFriend = "weChatFriend"
	SWWeChatCircle = "weChatCircle"
	SWWeibo        = "weibo"
)

func ShareGuidanceExample() []TestExample {
	return ShareExample(testGuidanceID)
}
func ShareSummaryExample() []TestExample {
	return ShareExample(testSummaryID)
}
func ShareClubExample() []TestExample {
	return ShareExample(testClubID)
}
func ShareLookExample() []TestExample {
	return ShareExample(testLookID)
}
func SharePartTimeJobExample() []TestExample {
	return ShareExample(testPartTimeJobID)
}
func ShareActivityExample() []TestExample {
	return ShareExample(testGuidanceID)
}
func ShareTrainingExample() []TestExample {
	return ShareExample(testGuidanceID)
}
func ShareAskExample() []TestExample {
	return ShareExample(testGuidanceID)
}
func ShareAppExample() []TestExample {
	return ShareExample("")
}

func ShareExample(id string) []TestExample {
	_, err := pgsql.Exec(`delete from share where userID = $1`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.ShareJSON
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + debugMsg)
	}
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: SWQQFriend,
				Sign:     j.Md5Encode(config.Salt, id),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: SWQQFriend,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: "-1",
				ShareWay: SWQQFriend,
				Sign:     j.Md5Encode(config.Salt, "-1"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "object id error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: SWQQFriend,
				Sign:     j.Md5Encode(config.Salt, id),
				Token:    token,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "token success",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: SWQQFriend,
				Sign:     j.Md5Encode(config.Salt, id),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "qq friend",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: SWQQZone,
				Sign:     j.Md5Encode(config.Salt, id),
				Token:    token,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "qq zone",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: SWWeChatFriend,
				Sign:     j.Md5Encode(config.Salt, id),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "weChat friend",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: SWWeChatCircle,
				Sign:     j.Md5Encode(config.Salt, id),
				Token:    token,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "weChat circle",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: id,
				ShareWay: "twitter",
				Sign:     j.Md5Encode(config.Salt, id),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeShareWayError),
				Msg:  j.MsgShareWayError,
			},
			Name: "twitter",
			Test: true,
		},
	}
}
func testShare(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
