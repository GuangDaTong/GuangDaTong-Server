package other

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func business(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.BusinessJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealBusiness(reqJson.Token, reqJson.Gender, reqJson.Name, reqJson.Phone, reqJson.Description,
		reqJson.Email, reqJson.QQ, reqJson.WeChat, reqJson.Company)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealBusiness(token, gender, name, phone, description, email, qq, weChat, company string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	var userID interface{}
	code, msg, debugMsg, userID = mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		userID = nil
	}
	code, msg, debugMsg = j.JudgeGender(gender)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(description)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeName(name)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAccount(phone)
	if code != j.CodeSuccess {
		return j.CodeIllegalPhone, j.MsgIllegalPhone, "illegal phone,phone:" + phone, result
	}
	sqlResult, err := pgsql.Exec(`insert into business(userID,status,createTime,name,phone,description,
	email,qq,weChat,company,gender) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);`, userID, "等待查看", time.Now(), name,
		phone, description, email, qq, weChat, company, gender)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
