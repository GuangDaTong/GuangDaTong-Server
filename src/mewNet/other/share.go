package other

import (
	"config"
	"fmt"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"strings"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func share(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	table := strings.TrimPrefix(r.URL.Path, "/share/")
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ShareJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealShare(table, reqJson.Sign, reqJson.Token, reqJson.ObjectID, reqJson.ShareWay)
	mewNet.AddReqJsonToResult(result, reqJson)
}

//未处理赠送喵币
func dealShare(objectName, sign, token, objectID, shareWay string) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, objectID)
	if code != j.CodeSuccess {
		return
	}
	var userID interface{}
	code, msg, debugMsg, userID = mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		userID = nil
	}
	var ObjectID interface{} = objectID
	code, msg, debugMsg = j.JudgeID(objectID)
	if objectID == "" {
		ObjectID = nil
	} else if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeShareWay(shareWay)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeObjectName(objectName)
	if code != j.CodeSuccess {
		return
	}
	logs.DebugPrint(userID)
	var t string
	if userID != nil {
		rows, err := pgsql.Query(`select createTime "createTime" from share where userID = $1 
		order by createTime desc limit 1 offset 0`, userID)
		if err != nil {
			return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
		}
		defer rows.Close()
		if rows.Next() {
			err = rows.Scan(&t)
			if err != nil {
				return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), result
			}
		}
	}
	sqlResult, err := pgsql.Exec(`insert into share(userID,way,createTime,objectID,objectName) values
	($1,$2,$3,$4,$5);`, userID, shareWay, time.Now(), ObjectID, objectName)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	if userID != nil && (t == "" || strings.Compare(t[:10], fmt.Sprint(time.Now())[:10]) != 0) {
		logs.DebugPrint(userID, t, fmt.Sprint(time.Now())[:10])
		code, msg, debugMsg = mewNet.RewardCoins(10, "每天首次分享", userID)
		if code != j.CodeSuccess {
			return
		}
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
