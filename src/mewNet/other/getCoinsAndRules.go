package other

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
)

func getCoinsAndRules(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.GetCoinsAndRulesJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealGetCoinsAndRules(reqJson.Sign)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealGetCoinsAndRules(sign string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, user := mewNet.GetUser(sign)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "coins", user.Coins)
	mewNet.AddDataToResult(result, "rules", mewNet.Rules)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
