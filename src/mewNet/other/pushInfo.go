package other

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func pushInfo(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.PushInfoJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealPushInfo(reqJson.Sign, reqJson.UserID, reqJson.RegID, reqJson.Version, reqJson.OSType,
		reqJson.Country, reqJson.Province, reqJson.City, reqJson.District, reqJson.County, reqJson.Street, reqJson.SiteName)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealPushInfo(sign, userID, regID, version, OSType, country, province, city, district, county, street,
	siteName string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, regID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeOSType(OSType)
	if code != j.CodeSuccess {
		return
	}
	var UserID interface{} = userID
	var err error
	if userID == "" {
		UserID = nil
	}
	if UserID != nil {
		_, err = pgsql.Exec(`delete from userInfo where userID = $1`, UserID)
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
		}
	}
	_, err = pgsql.Exec(`delete from userInfo where regID= $1`, regID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	_, err = pgsql.Exec(`insert into userInfo(regID,userID,OSType,country,province,city,
	district,county,street,siteName,version) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)
	on conflict(userID) do nothing`,
		regID, UserID, OSType, country, province, city, district, county, street, siteName, version)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
