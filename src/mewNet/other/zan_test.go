package other

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func ZanUsersExample() []TestExample {
	return ZanExample("1")
}
func ZanGuidanceExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanGuidance where objectID = $1 and userID = $2`, testGuidanceID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testGuidanceID)
}
func ZanSummaryExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanSummary where objectID = $1 and userID = $2`, testSummaryID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testSummaryID)
}
func ZanClubExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanClub where objectID = $1 and userID = $2`, testClubID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testClubID)
}
func ZanLookExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanLook where objectID = $1 and userID = $2`, testLookID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testLookID)
}
func ZanActivityExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanActivity where objectID = $1 and userID = $2`, testActivityID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testActivityID)
}
func ZanPartTimeJobExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanPartTimeJob where objectID = $1 and userID = $2`, testPartTimeJobID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testPartTimeJobID)
}
func ZanTrainingExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanTraining where objectID = $1 and userID = $2`, testTrainingID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testTrainingID)
}
func ZanAskExample() []TestExample {
	_, err := pgsql.Exec(`delete from zanAsk where objectID = $1 and userID = $2`, testAskID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return ZanExample(testAskID)
}

func ZanExample(id string) []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err := pgsql.Exec(`delete from zanUsers where userID = $1 and objectID = $2`, testUserID, id)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.ZanJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeHaveBeenZan),
				Msg:  j.MsgHaveBeenZan,
			},
			Name: "have been zan",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "object id error",
		},
	}
}
func testZan(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
