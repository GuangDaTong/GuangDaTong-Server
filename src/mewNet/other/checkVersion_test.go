package other

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func CheckVersionExample() []TestExample {
	type exampleJSON = modal.CheckVersionJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				OSType: "ios",
				Sign:   j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				OSType: "android",
				Sign:   j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOSTypeError),
				Msg:  j.MsgOSTypeError,
			},
			Name: "OSType error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign error",
			Test: true,
		},
		{
			Request: []byte("kalsdjf"),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "发送的数据不是json格式",
			Test: true,
		},
	}
}
func testCheckVersion(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
