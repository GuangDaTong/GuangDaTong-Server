package mewNet

const (
	objNameUsers       = "users"
	objNameGuidance    = "guidance"
	objNameSummary     = "summary"
	objNameClub        = "club"
	objNameLook        = "look"
	objNamePartTimeJob = "partTimeJob"
	objNameActivity    = "activity"
	objNameTraining    = "training"
	objNameAsk         = "ask"
	objNameComment     = "comment"

	debugExamplePath   = "../debugExample"
	releaseExamplePath = "../releaseExample"
)

var (
	SWQQFriend     = "QQFriend"
	SWQQZone       = "QQZone"
	SWWeChatFriend = "weChatFriend"
	SWWeChatCircle = "weChatCircle"
	SWWeibo        = "weibo"

	examplePath = "../debugExample"
)
