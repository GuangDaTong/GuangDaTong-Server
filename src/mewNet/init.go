package mewNet

import (
	"config"
	"encoding/json"
	"fmt"
	"logs"
	"modal"
	"net/http"
	"os"
	"time"
	"utils/pgsql"

	"utils/jpush"
	j "utils/judger"

	"github.com/gorilla/mux"
)

type Route = modal.Route

func NewRouter() *mux.Router {
	InitTemplate()
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}
	for _, route := range prefixRoutes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			PathPrefix(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	InitService()
	InitVersion()
	InitSystemUserID()
	go jpush.InitPush()
	go firstLoginProcess()

	return router
}

func fileServer(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, config.PublicFilePath+r.URL.Path)
}

var prefixRoutes = []Route{
	Route{
		"fileServer",
		"GET",
		"/",
		fileServer,
	},
}

var routes = []Route{
	Route{
		"index",
		"GET",
		"/",
		index,
	},
	Route{
		"getPID",
		"GET",
		"/getPID",
		getPID,
	},
}

func AppendRoute(appendRoutes []Route) {
	routes = append(routes, appendRoutes...)
}

func index(w http.ResponseWriter, r *http.Request) {
	err := tmpl.ExecuteTemplate(w, "index", &struct {
		SrcHost string
		Version string
	}{
		SrcHost: config.SrcHost,
		Version: config.CurVersion.String,
	})
	if err != nil {
		logs.ErrorPrint(err.Error())
	}
}

func getPID(w http.ResponseWriter, r *http.Request) {
	if r.Host != "127.0.0.1" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Write([]byte(fmt.Sprint(os.Getpid())))
}

type RespJSON = modal.RespJSON

func WriteRespJSON(w http.ResponseWriter, r *http.Request, code int, msg, debugMsg string, result interface{}) {
	w.Header().Set("Content-Type", "application/json;charset=utf-8") //返回数据格式是json
	res, ok := result.(map[string]interface{})
	if !ok {
		res = map[string]interface{}{}
	}
	if code != j.CodeSuccess {
		logs.ResultPrint(time.Now().Format("2006-01-02 15:04:05"), r.RequestURI, r.Method, r.RemoteAddr,
			code, msg, debugMsg, res["request"], r.Header.Get("X-Real-Ip"))
	}
	respJson := &RespJSON{
		Code:     fmt.Sprint(code),
		DebugMsg: debugMsg,
		Msg:      msg,
		Result:   res,
	}
	var data []byte
	var err error
	if config.Test {
		data, err = json.MarshalIndent(respJson, "", "  ")
	} else {
		delete(res, "request")
		data, err = json.Marshal(respJson)
	}
	if err != nil {
		logs.ErrorPrint("marshal json error:", err.Error())
		data = []byte(`
		{
			"code": "` + fmt.Sprint(j.CodeMarshalJsonError) + `",
			"msg": "` + j.MsgMarshalJsonError + `",
			"debugMsg": "` + err.Error() + ` ",
			"result": {}
		}`)
		_, err = w.Write(data)
		if err != nil {
			logs.ErrorPrint("httpWriter write data error:", err.Error())
		}
		return
	}
	_, err = w.Write(data)
	if err != nil {
		logs.ErrorPrint("httpWriter write data error:", err.Error())
	}
}

func AddReqJsonToResult(result interface{}, reqJson interface{}) {
	/*if !config.Test {
		return
	}*/
	AddDataToResult(result, "request", reqJson)
}

func AddDataToResult(result interface{}, key string, val interface{}) {
	if result == nil {
		logs.ErrorPrint("nil result")
		result = map[string]interface{}{key: val}
		return
	}
	res, ok := result.(map[string]interface{})
	if !ok {
		logs.ErrorPrint("not map[string]interface{}")
		return
	}
	res[key] = val
}

func GetIntCreateTime(SQL string, args ...interface{}) (code int, msg, debugMsg string, createTime int64) {
	rows, err := pgsql.Query(SQL, args...)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), 0
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.Scan(&createTime)
		if err != nil {
			return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), 0
		}
	}
	return j.CodeSuccess, j.MsgSuccess, "", createTime
}
