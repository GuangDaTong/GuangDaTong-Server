package account

import "modal"
import "mewNet"

type Route = modal.Route

var Routes = []Route{
	Route{
		"checkVerifyCode",
		"POST",
		"/checkVerifyCode",
		checkVerifyCode,
	},
	Route{
		"listSchool",
		"POST",
		"/list/school",
		listSchool,
	},
	Route{
		"listCollege",
		"POST",
		"/list/college",
		listCollege,
	},
	Route{
		"login",
		"POST",
		"/login",
		login,
	},
	Route{
		"register",
		"POST",
		"/register",
		register,
	},
	Route{
		"isAccountExist",
		"POST",
		"/isAccountExist",
		isAccountExist,
	},
	Route{
		"alterPassword",
		"POST",
		"/alterPassword",
		alterPassword,
	},
	Route{
		"dataSelf",
		"POST",
		"/data/self",
		dataSelf,
	},
	Route{
		"logout",
		"POST",
		"/logout",
		logout,
	},
	Route{
		"dataPerson",
		"POST",
		"/data/person",
		dataPerson,
	},
	Route{
		"addFriend",
		"POST",
		"/addFriend",
		addFriend,
	},
	Route{
		"resetPassword",
		"POST",
		"/resetPassword",
		resetPassword,
	},
	Route{
		"alterData",
		"POST",
		"/alterData",
		alterData,
	},
	Route{
		"isQQRegister",
		"POST",
		"/isQQRegister",
		isQQRegister,
	},
	Route{
		"QQLogin",
		"POST",
		"/login/qq",
		qqLogin,
	},
}

func init() {
	mewNet.AppendRoute(Routes)
}
