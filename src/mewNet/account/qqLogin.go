package account

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func qqLogin(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.QQLoginJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealQQLogin(reqJson.Sign, reqJson.UUID, reqJson.Nickname, reqJson.Gender,
		reqJson.College, reqJson.School)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealQQLogin(sign, uuid, nickname, gender, college, school string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, uuid)
	if code != j.CodeSuccess {
		return
	}
	if uuid == "" {
		return j.CodeEmptyAccount, j.MsgEmptyAccount, "uuid is empty", result
	}
	var userID string
	rows, err := pgsql.Query(`select id from users where qquuid = $1`, uuid)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	defer rows.Close()
	if !rows.Next() {
		code, msg, debugMsg, userID = qqRegister(uuid, nickname, gender, college, school)
		if code != j.CodeSuccess {
			return
		}
	} else {
		err = rows.Scan(&userID)
		if err != nil {
			return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), result
		}
	}
	return loginData(userID)
}

func qqRegister(uuid, nickname, gender, college, school string) (code int, msg, debugMsg string, userID string) {
	code, msg, debugMsg = j.JudgeName(nickname)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeGender(gender)
	if code != j.CodeSuccess || gender == "" {
		return j.CodeIllegalGender, j.MsgIllegalGender, "gender should be '男' or '女'", ""
	}
	if college == "" {
		return j.CodeEmptyCollege, j.MsgEmptyCollege, "", ""
	}
	if school == "" {
		school = mewNet.DefaultSchool
	}
	rows, err := pgsql.Query(`insert into users(qquuid,account,nickname,icon,password,coins,college,school,
	exp,custom,grade,gender) values ($1,null,$2,$3,$4,0,$5,$6,0,'{}'::jsonb,'其他',$7) returning id;`,
		uuid, nickname, mewNet.DefaultIcon, "", college, school, gender)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), ""
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = mewNet.RewardCoins(666, "参与大学喵推广活动", data["id"])
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", data["id"]
}
