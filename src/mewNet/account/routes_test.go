package account

import (
	"config"
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "login",
		Method:     "POST",
		Pattern:    "/login",
		SetExample: LoginExample,
		Test:       testLogin,
	},
	{
		Name:       "register",
		Method:     "POST",
		Pattern:    "/register",
		SetExample: RegisterExample,
		Test:       testRegister,
	},
	{
		Name:       "isAccountExist",
		Method:     "POST",
		Pattern:    "/isAccountExist",
		SetExample: IsAccountExistExample,
		Test:       testIsAccountExist,
	},
	{
		Name:       "listSchool",
		Method:     "POST",
		Pattern:    "/list/school",
		SetExample: ListSchoolExample,
		Test:       testListSchool,
	},
	{
		Name:       "listCollege",
		Method:     "POST",
		Pattern:    "/list/college",
		SetExample: ListCollegeExample,
		Test:       testListCollege,
	},
	{
		Name:       "resetPassword",
		Method:     "POST",
		Pattern:    "/resetPassword",
		SetExample: ResetPasswordExample,
		Test:       testResetPassword,
	},
	{
		Name:       "alterPassword",
		Method:     "POST",
		Pattern:    "/alterPassword",
		SetExample: AlterPasswordExample,
		Test:       testAlterPassword,
	},
	{
		Name:       "dataSelf",
		Method:     "POST",
		Pattern:    "/data/self",
		SetExample: DataSelfExample,
		Test:       testDataSelf,
	},
	{
		Name:       "logout",
		Method:     "POST",
		Pattern:    "/logout",
		SetExample: LogoutExample,
		Test:       testLogout,
	},
	{
		Name:       "dataPerson",
		Method:     "POST",
		Pattern:    "/data/person",
		SetExample: DataPersonExample,
		Test:       testDataPerson,
	},
	{
		Name:       "addFriend",
		Method:     "POST",
		Pattern:    "/addFriend",
		SetExample: AddFriendExample,
		Test:       testAddFriend,
	},
	{
		Name:       "alterData",
		Method:     "POST",
		Pattern:    "/alterData",
		SetExample: AlterDataExample,
		Test:       testAlterData,
	},
	{
		Name:       "isQQRegister",
		Method:     "POST",
		Pattern:    "/isQQRegister",
		SetExample: IsQQRegisterExample,
		Test:       testIsQQRegister,
	},
	{
		Name:       "QQLogin",
		Method:     "POST",
		Pattern:    "/login/qq",
		SetExample: QQLoginExample,
		Test:       testQQLogin,
	},
	{
		Name:       "checkVerifyCode",
		Method:     "POST",
		Pattern:    "/checkVerifyCode",
		SetExample: CheckVerifyCodeExample,
		Test:       testCheckVerifyCode,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

func ToBytes(v interface{}) []byte {
	bytes, err := json.Marshal(v)
	if err != nil {
		panic(err.Error())
	}
	return bytes
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample
type RespJSON = modal.RespJSON
