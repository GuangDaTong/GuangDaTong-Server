package account

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func login(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.LoginJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealLogin(reqJson.Sign, reqJson.Account, reqJson.Password)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealLogin(sign, account, password string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, password)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, userID := j.IsExistAccount(account)
	if code != j.CodeAccountExist {
		return
	}
	code, msg, debugMsg = j.CorrectPassword(account, password)
	if code != j.CodeSuccess {
		return
	}
	return loginData(userID)
}

func loginData(userID string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, token := mewNet.SetToken(userID)
	rows, err := pgsql.Query(`
	select 
	users.nickname,
	users.icon,
	users.coins,
	users.exp,
	users.gender,
	users.school,
	users.college,
	users.grade,
	userinfo.pushUrgentSchool "pushUrgentSchool",
	userinfo.pushUrgentCollege "pushUrgentCollege",
	userinfo.pushSystem "pushSystem",
	userinfo.pushLeaveWords "pushLeaveWords",
	userinfo.pushComment "pushComment",
	userinfo.pushActivity "pushActivity"
	from users left outer join userinfo on (users.id = userinfo.userID) 
	where users.id = $1`, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, func(mp map[string]interface{}) error {
		err := mewNet.DealMapIcon(mp)
		if err != nil {
			return err
		}
		err = mewNet.DealMapExpToLevel(mp)
		if err != nil {
			return err
		}
		keys := []string{"pushUrgentSchool", "pushUrgentCollege", "pushSystem", "pushLeaveWords", "pushComment", "pushActivity"}
		for _, val := range keys {
			mp[val] = (mp[val] == "" || mp[val] == "true")
		}
		return nil
	})
	if code != j.CodeSuccess {
		return
	}
	result = data
	rows, err = pgsql.Query(`
		select 
		u.id,
		u.icon,
		u.nickname,
		b.createTime "createTime"
		from blacklist b
		inner join users u on (b.userID = u.id)
		where b.ownerID = $1
		order by b.createTime desc`, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, blacklist := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "blacklist", blacklist)
	mewNet.AddDataToResult(result, "id", userID)
	mewNet.AddDataToResult(result, "token", token)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
