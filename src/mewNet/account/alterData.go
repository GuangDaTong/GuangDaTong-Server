package account

import (
	"config"
	"logs"
	"mewNet"
	"mime/multipart"
	"net/http"
	"strings"
	"utils/dealImage"
	j "utils/judger"
	"utils/pgsql"
)

func alterData(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		code, msg, debugMsg = j.CodeParseFormError, j.MsgParseFormError, err.Error()
		return
	}
	icon, header, err := r.FormFile("icon")
	var subfix string
	if err == nil || icon != nil {
		code, msg, debugMsg, subfix = dealImage.JudgeImage(header)
		if code != j.CodeSuccess {
			return
		}
	}
	nickname := r.FormValue("nickname")
	college := r.FormValue("college")
	gender := r.FormValue("gender")
	grade := r.FormValue("grade")
	custom := r.FormValue("custom")
	sign := r.FormValue("sign")
	code, msg, debugMsg, result = dealAlterData(sign, nickname, college, gender, grade, custom, subfix, icon)
	mewNet.AddReqJsonToResult(result, r.Form)
}
func dealAlterData(sign, nickname, college, gender, grade, custom, subfix string, icon multipart.File) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeName(nickname)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeGender(gender)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeGrade(grade)
	if code != j.CodeSuccess {
		return
	}
	if custom == "" {
		custom = "{}"
	}

	var oldIcon, oldGender string
	rows, err := pgsql.Query(`select gender,icon from users where id = $1`, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	var data map[string]string
	code, msg, debugMsg, data = mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	oldIcon = data["icon"]
	oldGender = data["gender"]
	defer func() {
		if code != j.CodeSuccess {
			return
		}
		if oldGender != "" && oldIcon != mewNet.DefaultIcon {
			return
		}
		//更改默认头像和性别则认为是完善用户资料
		if gender != "" && (icon != nil || oldIcon != mewNet.DefaultIcon) {
			code, msg, debugMsg := mewNet.RewardCoins(50, "完善用户资料", userID)
			if code != j.CodeSuccess {
				logs.ErrorPrint(code, msg, debugMsg)
			}
		}
	}()

	SQL := `update users set nickname = $1,college = $2,gender = $3,grade = $4,custom = $5`
	var args = []interface{}{nickname, college, gender, grade, custom, userID}

	var image, thumbnailImage string
	if icon != nil {
		//头像上传成功，若更新数据库失败则删除，否则删除旧头像
		defer func() {
			if code != j.CodeSuccess {
				dealImage.DeleteArray(config.PublicFilePath, []string{image, thumbnailImage})
				return
			}
			if oldIcon != mewNet.DefaultIcon {
				dealImage.DeleteArray(config.PublicFilePath, []string{oldIcon,
					strings.Replace(oldIcon, "/icon_", "/thumbnailIcon_", 1)})
			}
		}()
		timeStr := mewNet.FormatNowToInt()
		image = "icon/icon_" + userID + "_" + timeStr + subfix
		thumbnailImage = "icon/thumbnailIcon_" + userID + "_" + timeStr + subfix
		logs.DebugPrint(image, thumbnailImage)
		code, msg, debugMsg = dealImage.SaveImage(image, thumbnailImage, icon, false, 300)
		if code != j.CodeSuccess {
			return
		}
		SQL += `,icon = $7 `
		args = append(args, thumbnailImage)
	}
	SQL += ` where id = $6 returning icon`
	rows, err = pgsql.Query(SQL, args...)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data = mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	if icon == nil { //没传头像返旧地址
		thumbnailImage = data["icon"]
	}
	mewNet.AddDataToResult(result, "iconURL", config.SrcHost+thumbnailImage)
	return code, msg, debugMsg, result
}
