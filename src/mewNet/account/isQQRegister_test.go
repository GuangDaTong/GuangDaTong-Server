package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func IsQQRegisterExample() []TestExample {
	var uuid = "kaljdsflkjasldkjflkasdjflkj"
	var uuid1 = "alskdjflkasdjflkjaslkdjflkas"
	_, err := pgsql.Exec(`insert into users(qquuid,account,nickname,icon,password,coins,college,school,
	exp,custom,grade) values ($1,null,$2,$3,$4,0,$5,$6,0,'{}'::jsonb,'其他') on conflict do nothing;`,
		uuid1, "aa", mewNet.DefaultIcon, "", testCollege, mewNet.DefaultSchool)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.IsQQRegisterJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				UUID: uuid,
				Sign: j.Md5Encode(config.Salt, uuid),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				UUID: uuid1,
				Sign: j.Md5Encode(config.Salt, uuid1),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				UUID: uuid,
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptyAccount),
				Msg:  j.MsgEmptyAccount,
			},
			Name: "empty uuid",
		},
	}
}
func testIsQQRegister(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
