package account

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func CheckVerifyCodeExample() []TestExample {
	type exampleJSON = modal.CheckVerifyCodeJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Account: testAccount,
				Sign:    j.Md5Encode(config.Salt + testAccount),
				Code:    "1234",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyFailed),
				Msg:  j.MsgVerifyFailed,
			},
			Name:     "verify failed",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "15602309617",
				Sign:    j.Md5Encode(config.Salt + "15602309617"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyFailed),
				Msg:  j.MsgVerifyFailed,
			},
			Name:     "verify failed",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "15602309617",
				Sign:    j.Md5Encode(config.Salt + "15602309617"),
				Code:    "1234",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyCodeError),
				Msg:  j.MsgVerifyCodeError,
			},
			Name:     "verify code error",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "18200817543",
				Sign:    j.Md5Encode(config.Salt + "18200817543"),
				Code:    "1234",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyCodeError),
				Msg:  j.MsgVerifyCodeError,
			},
			Name:     "verify code error",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "18200817542",
				Sign:    j.Md5Encode(config.Salt + "18200817542"),
				Code:    "1234",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyCodeError),
				Msg:  j.MsgVerifyCodeError,
			},
			Name:     "verify code error",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "18200817541",
				Sign:    j.Md5Encode(config.Salt + "18200817541"),
				Code:    "1234",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyCodeError),
				Msg:  j.MsgVerifyCodeError,
			},
			Name:     "verify code error",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: testAccount,
				Code:    "1234",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name:     "sign wrong",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "1602309617",
				Code:    "1234",
				Sign:    j.Md5Encode(config.Salt, "1602309617"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeMatchAccountFailed),
				Msg:  j.MsgMatchAccountFailed,
			},
			Name:     "match string failed",
			Test:     true,
			NotBench: true,
		},
		{
			Request: []byte("{lkadjfkjkalds:kj}"),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name:     "发送的数据不是json格式",
			Test:     true,
			NotBench: true,
		},
		{
			Request: ToBytes(&exampleJSON{Sign: j.Md5Encode(config.Salt, "")}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptyAccount),
				Msg:  j.MsgEmptyAccount,
			},
			Name:     "empty account",
			Test:     true,
			NotBench: true,
		},
	}
}
func testCheckVerifyCode(testJson *TestJSON) {
	_, err := pgsql.Exec(`delete from users where account = $1;`, testAccount)
	if err != nil {
		panic(err.Error())
	}
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
