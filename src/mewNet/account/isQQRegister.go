package account

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func isQQRegister(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.IsQQRegisterJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealIsQQRegister(reqJson.Sign, reqJson.UUID)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealIsQQRegister(sign, uuid string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, uuid)
	if code != j.CodeSuccess {
		return
	}
	if uuid == "" {
		return j.CodeEmptyAccount, j.MsgEmptyAccount, "uuid is empty", result
	}
	rows, err := pgsql.Query(`select qquuid from users where qquuid = $1`, uuid)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	defer rows.Close()
	mewNet.AddDataToResult(result, "isRegister", rows.Next())
	return j.CodeSuccess, j.MsgSuccess, "", result
}
