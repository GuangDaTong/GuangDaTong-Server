package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
)

func AlterDataExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.MultiPartRequest
	return []TestExample{
		{
			Request: exampleJSON{
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "男",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: exampleJSON{
				FilePath: []string{config.PublicFilePath + "icon/default.ico"},
				FileName: []string{"icon"},
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "男",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name:     "success",
			NotBench: true,
		},
		{
			Request: exampleJSON{
				FilePath: []string{config.PublicFilePath + "icon/default2.jpg"},
				FileName: []string{"icon"},
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "男",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name:     "icon success",
			NotBench: true,
		},
		{
			Request: exampleJSON{
				FilePath: []string{"/home/ubuntu/Downloads/code_1.14.0-1499719149_amd64.deb"},
				FileName: []string{"icon"},
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "男",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeImageTooLarge),
				Msg:  j.MsgImageTooLarge,
			},
			Name:     "image too large",
			NotBench: true,
		},
		{
			Request: exampleJSON{
				FilePath: []string{"../README.md"},
				FileName: []string{"icon"},
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "男",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSaveImageError),
				Msg:  j.MsgSaveImageError,
			},
			Name:     "save image error,unsupported image type",
			NotBench: true,
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "男",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "不男不女",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalGender),
				Msg:  j.MsgIllegalGender,
			},
			Name: "illegal gender ",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "女",
					"grade":    "14级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalGrade),
				Msg:  j.MsgIllegalGrade,
			},
			Name: "illegal grade",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"nickname": "",
					"gender":   "男",
					"grade":    "2014级",
					"college":  "计算机科学与教育软件学院",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalName),
				Msg:  j.MsgIllegalName,
			},
			Name: "illegal name",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"nickname": "yyyyyyyyyyyyyyyyyyyyy",
					"gender":   "女",
					"grade":    "2014级",
					"college":  "计算机",
					"sign":     j.Md5Encode(config.Salt, token),
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeDBQueryError),
				Msg:  j.MsgDBQueryError,
			},
			Name: "illegal college",
		},
	}
}
func testAlterData(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		modal.RunTestMultiPartRequest(val, testJson, w)
	}
}
