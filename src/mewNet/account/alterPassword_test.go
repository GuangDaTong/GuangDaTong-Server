package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func AlterPasswordExample() []TestExample {
	rows, err := pgsql.Query(`insert into users(account,nickname,icon,password,coins,college,school,exp,custom,gender) values
	($1,$1,$2,$3,0,$4,$5,0,'{}'::jsonb,'女') on conflict(account) do update 
	set password = $3 returning id;`, testDataAccount, testIcon, j.EncryptPassword(testPassword), testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	ID, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, token := mewNet.SetToken(ID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.AlterPasswordJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				OldPwd: "123456",
				NewPwd: "666666",
				Sign:   j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				OldPwd: "123456",
				NewPwd: "666666",
				Sign:   j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePasswordWrong),
				Msg:  j.MsgPasswordWrong,
			},
			Name: "oldPwd wrong",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				OldPwd: "666666",
				NewPwd: "666667",
				Sign:   j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
	}
}
func testAlterPassword(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
