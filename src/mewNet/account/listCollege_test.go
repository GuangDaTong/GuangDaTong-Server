package account

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func ListCollegeExample() []TestExample {
	type exampleJSON = modal.ListCollegeJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				School: "广州大学",
				Sign:   j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				School: "广州大学",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "empty school default 广州大学",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				School: "lajksdfksldj",
				Sign:   j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "school not exist",
			Test: true,
		},
	}
}
func testListCollege(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
