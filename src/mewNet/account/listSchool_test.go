package account

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func ListSchoolExample() []TestExample {
	type exampleJSON =  modal.ListSchoolJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  10,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  10,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: -1,
				PageSize:  10,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  1000,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageSize error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  10,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
	}
}
func testListSchool(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
