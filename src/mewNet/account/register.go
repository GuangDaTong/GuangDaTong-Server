package account

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func register(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.RegisterJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealRegister(reqJson.Sign, reqJson.Account, reqJson.Password, reqJson.School, reqJson.College)
	mewNet.AddReqJsonToResult(result, reqJson)
}

//注册接口，在此之前会检测帐号是否已注册
func dealRegister(sign, account, password, school, college string) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = mewNet.CheckVerifyCodeToken(account, sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, _ = j.IsExistAccount(account)
	if code != j.CodeAccountNotExist {
		return
	}
	code, msg, debugMsg = j.JudgePassword(password)
	if code != j.CodeSuccess {
		return
	}
	if college == "" {
		return j.CodeEmptyCollege, j.MsgEmptyCollege, "", result
	}
	if school == "" {
		school = mewNet.DefaultSchool
	}
	nickname := "用户" + account[7:]
	rows, err := pgsql.Query(`insert into users(qquuid,account,nickname,icon,password,coins,college,school,
	exp,custom,grade,gender) values (null,$1,$2,$3,$4,0,$5,$6,0,'{}'::jsonb,'其他','') returning id;`,
		account, nickname, mewNet.DefaultIcon, j.EncryptPassword(password), college, school)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	_ = data
	/*code, msg, debugMsg = mewNet.RewardCoins(666, "参与大学喵推广活动", data["id"])
	if code != j.CodeSuccess {
		return
	}*/
	return j.CodeSuccess, j.MsgSuccess, "", result
}
