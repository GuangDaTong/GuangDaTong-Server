package account

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func IsAccountExistExample() []TestExample {
	type exampleJSON =  modal.IsAccountExistJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Account: testAccount,
				Sign:    j.Md5Encode(config.Salt + testAccount),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte("aklsdjflaksjdf"),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "13479823744",
				Sign:    j.Md5Encode(config.Salt + "98234kljaf"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: "1947981234",
				Sign:    j.Md5Encode(config.Salt + "1947981234"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeMatchAccountFailed),
				Msg:  j.MsgMatchAccountFailed,
			},
			Name: "match account failed",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account: testAccount1,
				Sign:    j.Md5Encode(config.Salt + testAccount1),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
	}
}
func testIsAccountExist(testJson *TestJSON) {
	_, err := pgsql.Exec("delete from users where account = $1 or account = $2;", testAccount, testAccount1)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into users(account,nickname,icon,password,gender,coins,college,school,isInblacklist,exp,custom) values
	($1,'dy','icon/default.ico','123456','男',0,'计算机科学与教育软件学院','广州大学',false,0,'{}');`, testAccount1)
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
