package account

import (
	"config"
	"encoding/json"
	"io/ioutil"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"net/url"
	"strings"
	j "utils/judger"
)

func checkVerifyCode(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.CheckVerifyCodeJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealCheckVerifyCode(reqJson.Sign, reqJson.Account, reqJson.Code)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealCheckVerifyCode(sign, account, verifyCode string) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, account)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAccount(account)
	if code != j.CodeSuccess {
		return
	}
	data := url.Values{"AppKey": {mewNet.AppKey}, "phone": {account}, "zone": {"86"}, "code": {verifyCode}}
	logs.DebugPrint("mob form:", data.Encode())
	resp, err := http.Post("https://webapi.sms.mob.com/sms/verify", "", strings.NewReader(data.Encode()))
	if err != nil {
		return j.CodeVerifyFailed, j.MsgVerifyFailed, err.Error(), result
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return j.CodeVerifyFailed, j.MsgVerifyFailed, err.Error(), result
	}
	respJson := modal.SMSRespJSON{}
	err = json.Unmarshal(body, &respJson)
	if err != nil {
		return j.CodeVerifyFailed, j.MsgVerifyFailed, err.Error(), result
	}
	if respJson.Status == 467 {
		return j.CodeVerifyFrequent, j.MsgVerifyFrequent, string(body), result
	} else if respJson.Status == 468 {
		return j.CodeVerifyCodeError, j.MsgVerifyCodeError, string(body), result
	} else if respJson.Status != 200 {
		return j.CodeVerifyFailed, j.MsgVerifyFailed, string(body), result
	}
	code, msg, debugMsg, token := mewNet.AddVerifyCodeToken(account)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "verifyToken", token)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
