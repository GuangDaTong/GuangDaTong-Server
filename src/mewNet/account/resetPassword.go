package account

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func resetPassword(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ResetPasswordJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealResetPassword(reqJson.Sign, reqJson.Account, reqJson.Password)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealResetPassword(sign, account, password string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = mewNet.CheckVerifyCodeToken(account, sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAccount(account)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePassword(password)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.Exec("update users set password = $1 where account = $2", j.EncryptPassword(password), account)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code == j.CodeDBExecNotEffect {
		return j.CodeAccountNotExist, j.MsgAccountNotExist, debugMsg, result
	} else if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
