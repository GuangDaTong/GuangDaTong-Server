package account

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
)

func isAccountExist(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.IsAccountExistJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealIsAccountExist(reqJson.Account, reqJson.Sign)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealIsAccountExist(account, sign string) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, account)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, _ = j.IsExistAccount(account)
	if code != j.CodeAccountExist && code != j.CodeAccountNotExist {
		return
	}
	exist := code == j.CodeAccountExist
	mewNet.AddDataToResult(result, "isExist", exist)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
