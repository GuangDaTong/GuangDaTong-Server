package account

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func QQLoginExample() []TestExample {
	var uuid = "alkjdsflkjaksldjfkl"
	_, err := pgsql.Exec(`delete from users where qquuid = $1`, uuid)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.QQLoginJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				UUID:     uuid,
				Gender:   "女",
				Sign:     j.Md5Encode(config.Salt, uuid),
				Nickname: "dy",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptyCollege),
				Msg:  j.MsgEmptyCollege,
			},
			Name: "empty college",
		},
		{
			Request: ToBytes(&exampleJSON{
				UUID:     uuid,
				Sign:     j.Md5Encode(config.Salt, uuid),
				Nickname: "dy",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalGender),
				Msg:  j.MsgIllegalGender,
			},
			Name: "illegal gender",
		},
		{
			Request: ToBytes(&exampleJSON{
				UUID:    uuid,
				Gender:  "男",
				Sign:    j.Md5Encode(config.Salt, uuid),
				College: testCollege,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalName),
				Msg:  j.MsgIllegalName,
			},
			Name: "empty nickname",
		},
		{
			Request: ToBytes(&exampleJSON{
				UUID:     uuid,
				Gender:   "女",
				Sign:     j.Md5Encode(config.Salt, uuid),
				College:  testCollege,
				Nickname: "dy",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:     j.Md5Encode(config.Salt),
				Gender:   "女",
				College:  testCollege,
				Nickname: "dy",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptyAccount),
				Msg:  j.MsgEmptyAccount,
			},
			Name: "uuid is empty",
		},
		{
			Request: ToBytes(&exampleJSON{
				UUID:     uuid,
				Gender:   "女",
				Sign:     j.Md5Encode(config.Salt, uuid),
				College:  testCollege,
				Nickname: "dy",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "already register success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				UUID:     uuid,
				Gender:   "女",
				Sign:     j.Md5Encode(config.Salt),
				College:  testCollege,
				Nickname: "dy",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
	}
}
func testQQLogin(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
