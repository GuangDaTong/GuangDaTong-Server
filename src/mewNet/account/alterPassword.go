package account

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func alterPassword(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.AlterPasswordJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealAlterPassword(reqJson.Sign, reqJson.OldPwd, reqJson.NewPwd)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealAlterPassword(sign, oldPwd, newPwd string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select password from users where id = $1`, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	if j.EncryptPassword(oldPwd) != data["password"] {
		return j.CodePasswordWrong, j.MsgPasswordWrong, "oldPwd wrong", result
	}
	sqlResult, err := pgsql.Exec(`update users set password = $1 where id = $2 and password = $3;`,
		j.EncryptPassword(newPwd), userID, data["password"])
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
