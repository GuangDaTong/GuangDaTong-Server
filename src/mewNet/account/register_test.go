package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func getToken(code int, msg, debugMsg, token string) string {
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	return token
}

func RegisterExample() []TestExample {
	type exampleJSON = modal.RegisterJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount,
				Password: testPassword,
				School:   testSchool,
				College:  testCollege,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken(testAccount))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte("lasjdkflaksjd"),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount,
				Password: testPassword,
				School:   testSchool,
				College:  testCollege,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyTokenInvalid),
				Msg:  j.MsgVerifyTokenInvalid,
			},
			Name: "verify token invalid",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  "1234567890a",
				Password: testPassword,
				School:   testSchool,
				College:  testCollege,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken("1234567890a"))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeMatchAccountFailed),
				Msg:  j.MsgMatchAccountFailed,
			},
			Name: "match account failed",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  "91847892345",
				Password: "klasdjflkasdjflkasjdflkjasdkljflkasdjfklasjdflksajdflkj",
				School:   testSchool,
				College:  testCollege,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken("91847892345"))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePasswordTooLong),
				Msg:  j.MsgPasswordTooLong,
			},
			Name: "password too long",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  "07132054923",
				Password: testPassword,
				School:   testSchool,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken("07132054923"))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptyCollege),
				Msg:  j.MsgEmptyCollege,
			},
			Name: "empty college",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount1,
				Password: testPassword,
				College:  testCollege,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken(testAccount1))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "empty school success",
			Test: true,
		},
	}
}
func testRegister(testJson *TestJSON) {
	_, err := pgsql.Exec(`delete from users where account = $1 or account = $2;`, testAccount, testAccount1)
	if err != nil {
		panic(err.Error())
	}
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
	testExamples(TestExample{

		Request: ToBytes(&modal.RegisterJSON{
			Account:  testAccount1,
			Password: testPassword,
			College:  testCollege,
			Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken(testAccount1))),
		}),
		Response: &RespJSON{
			Code: fmt.Sprint(j.CodeAccountExist),
			Msg:  j.MsgAccountExist,
		},
		Name: "already register",
		Test: true,
	}, testJson, w)
}
