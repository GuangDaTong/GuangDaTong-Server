package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func ResetPasswordExample() []TestExample {
	type exampleJSON = modal.ResetPasswordJSON
	getToken := func(code int, msg, debugMsg, token string) string {
		if code != j.CodeSuccess {
			panic(fmt.Sprint(code, msg, debugMsg))
		}
		return token
	}
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount,
				Password: testPassword,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken(testAccount))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount,
				Password: testPassword,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeVerifyTokenInvalid),
				Msg:  j.MsgVerifyTokenInvalid,
			},
			Name: "verify token invalid",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  "adsfj",
				Password: testPassword,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken("adsfj"))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeMatchAccountFailed),
				Msg:  j.MsgMatchAccountFailed,
			},
			Name: "match account failed",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  "19284756209",
				Password: testPassword,
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken("19284756209"))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAccountNotExist),
				Msg:  j.MsgAccountNotExist,
			},
			Name: "account not exist",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount1,
				Password: "12345",
				Sign:     j.Md5Encode(config.Salt, getToken(mewNet.AddVerifyCodeToken(testAccount1))),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePasswordTooShort),
				Msg:  j.MsgPasswordTooShort,
			},
			Name: "password too short",
			Test: true,
		},
	}
}
func testResetPassword(testJson *TestJSON) {
	_, err := pgsql.Exec(`delete from users where account = $1 or account = $2;`, testAccount, testAccount1)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into users(account,nickname,icon,password,gender,coins,college,school,exp,custom) values
	($1,'dy',$2,$3,'男',0,$4,$5,0,'{}'::jsonb);`, testAccount, mewNet.DefaultIcon, testPassword, testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into users(account,nickname,icon,password,gender,coins,college,school,exp,custom) values
	($1,'dy',$2,$3,'男',0,$4,$5,0,'{}'::jsonb);`, testAccount1, mewNet.DefaultIcon, testPassword, testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
