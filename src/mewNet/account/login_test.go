package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

var testAcc = "23324322987"

func LoginExample() []TestExample {
	_, err := pgsql.Exec(`insert into users(account,nickname,icon,password,gender,coins,college,school,exp,custom) values
	($1,'dy',$2,$3,'男',0,$4,$5,0,'{}'::jsonb) on conflict do nothing;`, testAccount, mewNet.DefaultIcon, j.EncryptPassword(testPassword), testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.LoginJSON
	return []TestExample{ //未判apple 和 android
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAcc,
				Password: testPassword,
				Sign:     j.Md5Encode(config.Salt, testPassword),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte("lkadjflkasd"),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount,
				Password: testPassword,
				Sign:     j.Md5Encode(config.Salt, testAccount1),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  "aaaaaaaaaaa",
				Password: testPassword,
				Sign:     j.Md5Encode(config.Salt, testPassword),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeMatchAccountFailed),
				Msg:  j.MsgMatchAccountFailed,
			},
			Name: "match account failed",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  testAccount,
				Password: "123lkj4567",
				Sign:     j.Md5Encode(config.Salt, "123lkj4567"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePasswordWrong),
				Msg:  j.MsgPasswordWrong,
			},
			Name: "password wrong",
			Test: true,
		},
	}
}
func testLogin(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------LoginExample-----------")
	_, err = pgsql.Exec(`delete from users where account = $1;`, testAcc)
	if err != nil {
		panic(err.Error())
	}
	testExamples(TestExample{
		Request: ToBytes(&exampleJSON{
			Account:  testAcc,
			Password: testPassword,
			Sign:     j.Md5Encode(config.Salt, testPassword),
		}),
		Response: &RespJSON{
			Code: fmt.Sprint(j.CodeAccountNotExist),
			Msg:  j.MsgAccountNotExist,
		},
		Name: "account not exist",
		Test: true,
	}, testJson, w)
	_, err = pgsql.Exec(`insert into users(account,nickname,icon,password,gender,coins,college,school,exp,custom) values
	($1,'dy',$2,$3,'男',0,$4,$5,0,'{}'::jsonb);`, testAcc, mewNet.DefaultIcon, j.EncryptPassword(testPassword), testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
