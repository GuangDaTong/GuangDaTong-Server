package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func dataPerson(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.DataPersonJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealDataPerson(reqJson.Sign, reqJson.Token, reqJson.ID)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealDataPerson(sign, token, id string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, id)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, userID := mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		userID = "-1"
	}
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select icon,exp,zan,nickname,gender,coins,school,college,grade,
	count(zan.userID) "isZan" from users 
	left outer join zanUsers zan on (zan.userID = $2 and zan.objectID = users.id) where id = $1
	group by icon,exp,zan,nickname,gender,coins,school,college,grade`, id, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	exp, ok := data["exp"].(string)
	if !ok {
		return j.CodeTypeAssertFailed, j.MsgTypeAssertFailed, `data["exp"] assert to string error`, result
	}
	var expNum int
	_, err = fmt.Sscan(exp, &expNum)
	if err != nil {
		return j.CodeTypeAssertFailed, j.MsgTypeAssertFailed, err.Error(), result
	}
	result = data
	mewNet.AddDataToResult(result, "id", id)
	mewNet.AddDataToResult(result, "level", fmt.Sprint(mewNet.GetLevel(expNum)))
	return j.CodeSuccess, j.MsgSuccess, "", result
}

func dealRowMap(mp map[string]interface{}) error {
	return nil
}
