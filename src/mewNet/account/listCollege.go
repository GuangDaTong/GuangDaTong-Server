package account

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listCollege(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListCollegeJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListCollege(reqJson.Sign, reqJson.School)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListCollege(sign, school string) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	if school == "" {
		school = mewNet.DefaultSchool
	}
	rows, err := pgsql.Query("select college from colleges where school = $1", school)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	colleges := make([]string, 0)
	for rows.Next() {
		var college string
		err = rows.Scan(&college)
		if err != nil {
			return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), result
		}
		colleges = append(colleges, college)
	}
	mewNet.AddDataToResult(result, "colleges", colleges)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
