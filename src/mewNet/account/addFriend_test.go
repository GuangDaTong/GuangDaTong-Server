package account

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func AddFriendExample() []TestExample {
	_, err := pgsql.Exec(`delete from friends where userID = $1 and friendID = $2`, testUserID, 1)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into blacklist(ownerID,userID,createTime) values($1,$2,now())
	on conflict do nothing`, 3, testUserID)
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.AddFriendJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				FriendID: "1",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				FriendID: "3",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeInBlacklist),
				Msg:  j.MsgInBlacklist,
			},
			Name: "in blacklist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				FriendID: "-1",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				FriendID: "1",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeHaveBeenFriend),
				Msg:  j.MsgHaveBeenFriend,
			},
			Name: "have been friend",
		},
		{
			Request: ToBytes(&exampleJSON{
				FriendID: "1000000000000000000",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeDBExecError),
				Msg:  j.MsgDBExecError,
			},
			Name: "db exec failed",
		},
	}
}
func testAddFriend(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
