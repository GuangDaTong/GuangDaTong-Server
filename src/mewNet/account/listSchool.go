package account

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listSchool(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListSchoolJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListSchool(reqJson.Sign, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListSchool(sign string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query("select name from schools limit $1 offset $2;", pageSize, pageIndex*pageSize)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	schools := make([]string, 0)
	for rows.Next() {
		var school string
		err = rows.Scan(&school)
		if err != nil {
			return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), result
		}
		schools = append(schools, school)
	}
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(schools))
	mewNet.AddDataToResult(result, "schools", schools)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
