package mewNet

import (
	"config"
	"logs"
	"net/http"
	"time"
)

func Logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		/*if config.Test {
			time.Sleep(time.Millisecond * 10)
		}*/
		start := time.Now()
		var remoteAddr string = r.RemoteAddr
		if r.Header.Get("X-NginX-Proxy") == "true" {
			remoteAddr = r.Header.Get("X-Real-Ip")
		}

		defer func() { //挡住panic
			if !config.Test {
				if err := recover(); err != nil {
					logs.ErrorPrint(
						"panic:",
						err,
						start.Format("2006-01-02 15:04:05"),
						time.Since(start),
						remoteAddr,
						r.Method,
						r.RequestURI,
						name,
					)
				}
			}
		}()

		//w.Header().Set("Access-Control-Allow-Origin", "*.guangdamiao.com")    //允许访问guangdamiao所有域
		//w.Header().Add("Access-Control-Allow-Headers", "Content-Type,Origin") //header的类型
		//logs.DebugPrint(r)
		inner.ServeHTTP(w, r)

		logs.AccessPrint(
			start.Format("2006-01-02 15:04:05"),
			time.Since(start),
			remoteAddr,
			r.Method,
			r.RequestURI,
			name,
		)
	})
}
