package mewNet

import (
	"config"
	"encoding/json"
	"fmt"
	"io"
	"logs"
	"modal"
	"net/http"
	"testing"
)

type response struct {
	header   http.Header
	reader   *io.PipeReader
	writer   *io.PipeWriter
	name     string
	Code     int
	Msg      string
	DebugMsg string
	Result   interface{}
}

func (resp *response) Header() http.Header {
	return resp.header
}
func (resp *response) Write(bytes []byte) (int, error) {
	return resp.writer.Write(bytes)
}
func (resp *response) Read(bytes []byte) (int, error) {
	return resp.reader.Read(bytes)
}
func (resp *response) WriteHeader(code int) {
	fmt.Println(code)
}

type TestExample = modal.TestExample

func TestWriteRespJSON(*testing.T) {
	logs.DebugPrint("test func WriteRespJSON")
	test := []TestExample{
		{
			Request: &response{
				name:     "success",
				header:   make(http.Header),
				Code:     200,
				Msg:      "aa",
				DebugMsg: "bb",
			},
			Response: &RespJSON{
				Code:     "200",
				Msg:      "aa",
				DebugMsg: "bb",
			},
			Test: false,
		},
		{
			Request: &response{
				name:     "success",
				header:   make(http.Header),
				Code:     200,
				Msg:      "aa",
				DebugMsg: "bb",
			},
			Response: &RespJSON{
				Code:     "200",
				Msg:      "aa",
				DebugMsg: "bb",
			},
			Test: true,
		},
		{
			Request: &response{
				name:     "error",
				header:   make(http.Header),
				Code:     400,
				Msg:      "cc",
				DebugMsg: "bb",
			},
			Response: &RespJSON{
				Code:     "400",
				Msg:      "cc",
				DebugMsg: "bb",
			},
			Test: true,
		},
		{
			Request: &response{
				name:     "error",
				header:   make(http.Header),
				Code:     400,
				Msg:      "cc",
				DebugMsg: "bb",
			},
			Response: &RespJSON{
				Code:     "400",
				Msg:      "cc",
				DebugMsg: "bb",
			},
			Test: false,
		},
		/*{
			Request: &response{
				name:     "error marshal json",
				header:   make(http.Header),
				Code:     400,
				Msg:      "cc",
				DebugMsg: "bb",
				Result:   WriteRespJSON,
			},
			Response: &RespJSON{
				Code:     fmt.Sprint(judger.CodeMarshalJsonError),
				Msg:      judger.MsgMarshalJsonError,
				DebugMsg: "json: unsupported type: func(http.ResponseWriter, *http.Request, int, string, string, interface {}) ",
			},
			Test: true,
		},
		{
			Request: &response{
				name:     "error marshal json",
				header:   make(http.Header),
				Code:     400,
				Msg:      "cc",
				DebugMsg: "",
				Result:   WriteRespJSON,
			},
			Response: &RespJSON{
				Code:     fmt.Sprint(judger.CodeMarshalJsonError),
				Msg:      judger.MsgMarshalJsonError,
				DebugMsg: "json: unsupported type: func(http.ResponseWriter, *http.Request, int, string, string, interface {}) ",
			},
			Test: false,
		},*/
	}
	for _, val := range test {
		config.Test = val.Test
		req, ok := val.Request.(*response)
		if !ok {
			panic("type assert failed")
		}
		req.reader, req.writer = io.Pipe()
		testWriteRespJSON(&val, req)
	}
}

func testWriteRespJSON(val *TestExample, req *response) {
	lock := make(chan bool, 1)
	bytes := make([]byte, 1000)
	len := 0
	go func() {
		var err error
		lock <- true
		len, err = req.reader.Read(bytes)
		if err == io.EOF {
			logs.DebugPrint(err.Error())
		} else if err != nil {
			panic(err.Error())
		}
		<-lock
	}()
	WriteRespJSON(req, &http.Request{}, req.Code, req.Msg, req.DebugMsg, req.Result)
	err := req.writer.Close()
	if err != nil {
		panic(err.Error())
	}
	lock <- true
	var resp RespJSON
	err = json.Unmarshal(bytes[:len], &resp)
	if err != nil {
		panic(err.Error())
	}
	if resp.Code != val.Response.Code && resp.Msg != val.Response.Msg {
		logs.DebugPrint(resp)
		logs.DebugPrint(*val.Response)
		panic("test " + req.name + " error")
	} else {
		logs.DebugPrint("test", req.name, "ok")
	}
	<-lock
}

func TestStringArrayToString(*testing.T) {
	s := []string{"icon/default.ico", "icon/default1.ico"}
	logs.DebugPrint(StringArrayToString(s))
	s = nil
	logs.DebugPrint(StringArrayToString(s))
}

func TestRouter(*testing.T) {
	basePath := "../"
	TmplPath = basePath + TmplPath
	route := NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
}
