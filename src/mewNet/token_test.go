package mewNet

import (
	"config"
	"fmt"
	"logs"
	"testing"
	"time"
	j "utils/judger"
	"utils/mewRedis"

	"github.com/garyburd/redigo/redis"
)

func TestToken(*testing.T) {
	go firstLoginProcess()
	code, msg, debugMsg, token := SetToken("1")
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	code, msg, debugMsg, userID := ExistToken("klasjdflkajsdlkfj")
	if code != j.CodeTokenNotExist {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	if userID != "" {
		panic("userID == " + userID)
	}
	code, msg, debugMsg, userID1 := ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	if userID1 != "1" {
		panic("userID == " + userID)
	}
	code, msg, debugMsg, user := GetUser(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	logs.DebugPrint(user)
	code, msg, debugMsg = DeleteToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
}

func TestToken1(*testing.T) {
	go firstLoginProcess()
	//没有createTime时setToken->每日启动APP
	token, err := redis.String(mewRedis.Do("HGET", "user:1", "token"))
	if err != nil && err != redis.ErrNil {
		panic(err.Error())
	} else if err != redis.ErrNil {
		_, err = mewRedis.Do("DEL", "token:"+token+":userID")
		if err != nil {
			panic(err.Error())
		}
	}
	_, err = mewRedis.Do("DEL", "user:1")
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, token := SetToken("1")
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	//有createTime时setToken->每日启动APP
	time.Sleep(time.Second * 1)
	_, err = mewRedis.Do("HSET", "user:1", "createTime", time.Now().Add(-time.Hour*24))
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, token = SetToken("1")
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	//有createTime时setToken->每日启动APP
	time.Sleep(time.Second * 1)
	_, err = mewRedis.Do("HSET", "user:1", "createTime", time.Now().Add(-time.Hour*24))
	if err != nil {
		panic(err.Error())
	}
	token = j.Md5Encode(config.Salt, token)
	code, msg, debugMsg, _ = ExistToken(token)
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	//删除Token
	code, msg, debugMsg = DeleteToken(token)
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	code, msg, debugMsg, _ = ExistToken(token)
	if code != j.CodeTokenNotExist {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	time.Sleep(time.Second * 1)
}

func TestVerifyToken(*testing.T) {
	code, msg, debugMsg, token := AddVerifyCodeToken("12345678901")
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	code, msg, debugMsg = CheckVerifyCodeToken("12345678901", token)
	if code != j.CodeVerifyTokenInvalid {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	token = j.Md5Encode(config.Salt, token)
	code, msg, debugMsg = CheckVerifyCodeToken("12345678902", token)
	if code != j.CodeVerifyTokenInvalid {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	code, msg, debugMsg = CheckVerifyCodeToken("12345678901", token)
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
}
