package discovery

import (
	"database/sql"
	"errors"
	"mewNet"
	"testing"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

var (
	testAccount       = "00000000000"
	testAccount1      = "12345678901"
	testPassword      = "123456"
	testSchool        = "广州大学"
	testCollege       = "计算机科学与教育软件学院"
	testDataAccount   = "00000000001"
	testIcon          = "icon/default.ico"
	testUserID        string
	testGuidanceID    string
	testSummaryID     string
	testClubID        string
	testLookID        string
	testPartTimeJobID string
	testActivityID    string
	testTrainingID    string
	testCommentID     string
	testAskID         string
)

func TestDB(*testing.T) {
	InitSQL()
}
func InitSQL() {
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	var err error
	pwd := j.EncryptPassword(testPassword)
	rows, err := pgsql.Query(`insert into users(account,nickname,icon,password,coins,college,school,exp,custom,gender) values
	($1,$1,$2,$3,0,$4,$5,0,'{"qq":"1769622057"}'::jsonb,'女') on conflict(account)
	do update set coins = 1000 returning id;`, testDataAccount, testIcon, pwd, testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	testUserID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into partTimeJob(icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
	($1,$4,'10元/小时','广州大学商业中心','','这是一个兼职测试页面',0,0,$2,$3,$5) returning id;`, testIcon, time.Now(), testSchool, mewNet.RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testPartTimeJobID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into activity(icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
	($1,$4,'200元/人','','海景别墅2日游，包接送，包吃住。另外还可以参加特别抽奖活动，说不定可以带走一部iPhone 7喔！','这是一个活动测试页面',0,0,$2,$3,$5) returning id;`, testIcon, time.Now(), testSchool, mewNet.RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testActivityID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into training(icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
	($1,$4,'6000元/人','广州大学商业南区','广州大学城高质量JAVA培训机构可以让你通过3个月的培训，迅速掌握JAVA开发技能，并且找到自己理想的工作！','这是一个培训测试页面',0,0,$2,$3,$5) returning id;`, testIcon, time.Now(), testSchool, mewNet.RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testTrainingID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
}

func GetTestID(rows *sql.Rows) (id string, err error) {
	defer rows.Close()
	if !rows.Next() {
		return "", errors.New("no rows")
	}
	err = rows.Scan(&id)
	if err != nil {
		return "", err
	}
	return id, nil
}
