package discovery

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
)

func SignUpActivityExample() []TestExample {
	return SignUpExample(testActivityID)
}
func SignUpTrainingExample() []TestExample {
	return SignUpExample(testActivityID)
}
func SignUpPartTimeJobExample() []TestExample {
	return SignUpExample(testActivityID)
}

func SignUpExample(id string) []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.SignUpJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ID:                 id,
				Name:               "dy",
				Phone:              "15602309617",
				ParticipationCount: "1",
				Sign:               j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:                 "adf",
				Name:               "dy",
				Phone:              "15602309617",
				ParticipationCount: "1",
				Sign:               j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "objectID error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:                 id,
				Phone:              "15602309617",
				ParticipationCount: "1",
				Sign:               j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalName),
				Msg:  j.MsgIllegalName,
			},
			Name: "illegal name",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:                 id,
				Name:               "dy",
				Phone:              "15602",
				ParticipationCount: "1",
				Sign:               j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalPhone),
				Msg:  j.MsgIllegalPhone,
			},
			Name: "illegal phone",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:                 id,
				Name:               "dy",
				Phone:              "15602309617",
				ParticipationCount: "-232",
				Sign:               j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalParticipationCount),
				Msg:  j.MsgIllegalParticipationCount,
			},
			Name: "illegal participationCount",
		},
	}
}
func testSignUp(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
