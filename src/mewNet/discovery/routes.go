package discovery

import (
	"mewNet"
	"modal"
)

var Routes = []Route{
	Route{
		"listPartTimeJob",
		"POST",
		"/list/partTimeJob",
		listPartTimeJob,
	},
	Route{
		"listActivity",
		"POST",
		"/list/activity",
		listActivity,
	},
	Route{
		"listTraining",
		"POST",
		"/list/training",
		listTraining,
	},
	Route{
		"dataPartTimeJob",
		"GET",
		"/data/partTimeJob",
		dataPartTimeJob,
	},
	Route{
		"dataActivity",
		"GET",
		"/data/activity",
		dataActivity,
	},
	Route{
		"dataTraining",
		"GET",
		"/data/training",
		dataTraining,
	},
	Route{
		"favoritePartTimeJob",
		"POST",
		"/favorite/partTimeJob",
		favoriteDiscovery,
	},
	Route{
		"favoriteActivity",
		"POST",
		"/favorite/activity",
		favoriteDiscovery,
	},
	Route{
		"favoriteTraining",
		"POST",
		"/favorite/training",
		favoriteDiscovery,
	},
	Route{
		"signUpPartTimeJob",
		"POST",
		"/signUp/partTimeJob",
		signUp,
	},
	Route{
		"signUpActivity",
		"POST",
		"/signUp/activity",
		signUp,
	},
	Route{
		"signUpTraining",
		"POST",
		"/signUp/training",
		signUp,
	},
}

func init() {
	mewNet.AppendRoute(Routes)
}

type Route = modal.Route
