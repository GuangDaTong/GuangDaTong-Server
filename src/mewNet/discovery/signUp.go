package discovery

import (
	"mewNet"
	"modal"
	"net/http"
	"strings"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func signUp(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	table := strings.TrimPrefix(r.URL.Path, "/signUp/")
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.SignUpJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealSignUp(reqJson.Sign, reqJson.ID, table,
		reqJson.Name, reqJson.Phone, reqJson.QQ, reqJson.WeChat, reqJson.ParticipationCount)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealSignUp(sign, objectID, objectName, name, phone, qq, weChat, participationCount string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(objectID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeDiscoveryType(objectName)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAccount(phone)
	if code != j.CodeSuccess {
		return j.CodeIllegalPhone, j.MsgIllegalPhone, debugMsg, result
	}
	code, msg, debugMsg = j.JudgeName(name)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, count := j.JudgeStringToPositiveInt(participationCount)
	if code != j.CodeSuccess {
		return j.CodeIllegalParticipationCount, j.MsgIllegalParticipationCount, debugMsg, result
	}
	sqlResult, err := pgsql.Exec(`insert into signUp(userID,objectName,objectID,status,phone,name,qq,
	weChat,participationCount,createTime) values($1,$2,$3,'等待查看',$4,$5,$6,$7,$8,$9);`, userID, objectName,
		objectID, phone, name, qq, weChat, count, time.Now())
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
