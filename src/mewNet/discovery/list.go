package discovery

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listPartTimeJob(w http.ResponseWriter, r *http.Request) {
	listDiscovery(w, r, "partTimeJob")
}

func listActivity(w http.ResponseWriter, r *http.Request) {
	listDiscovery(w, r, "activity")
}

func listTraining(w http.ResponseWriter, r *http.Request) {
	listDiscovery(w, r, "training")
}
func listDiscovery(w http.ResponseWriter, r *http.Request, table string) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListDiscoveryDataJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListDiscoveryData(table, reqJson.Sign, reqJson.Token, reqJson.School,
		reqJson.OrderBy, reqJson.Order, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealListDiscoveryData(table, sign, token, school, orderBy, order string,
	pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, userID := mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		userID = "-1"
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`
	select 
	data.id,
	data.icon,
	data.title,
	data.fee,
	data.position,
	data.description,
	data.createTime "createTime",
	zan,
	readCount "readCount",
	count(zan.userID) "isZan",
	count(favorite.userID) "isFavorite" 
	from `+table+` data 
	left outer join zan`+table+` zan on (zan.objectID = data.id and zan.userID = $1) 
	left outer join favorite`+table+` favorite on (favorite.objectID = data.id and favorite.userID = $1)
	where publishStatus = '已发布'
	group by zan.userID,data.id 
	order by data.updateTime desc limit $2 offset $3`,
		userID, pageSize, pageIndex*pageSize)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	for _, val := range data {
		val["link"] = config.Host + mewNet.DataLink + table
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
