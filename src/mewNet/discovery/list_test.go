package discovery

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func ListDiscoveryDataExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	_, err := db.Exec(`
	insert into ZanPartTimeJob(objectID,userID) values(` + testPartTimeJobID + "," + testUserID + `)
	on conflict (objectID,userID) do nothing;
	insert into ZanActivity(objectID,userID) values(` + testActivityID + "," + testUserID + `)
	on conflict (objectID,userID) do nothing;
	insert into ZanTraining(objectID,userID) values(` + testTrainingID + "," + testUserID + `)
	on conflict (objectID,userID) do nothing;
	`)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.ListDiscoveryDataJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt),
				Token:     token,
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt),
				PageIndex: 0,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "default para success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt),
				PageIndex: -1,
				PageSize:  300,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt),
				PageIndex: 1,
				PageSize:  301,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageSize error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt),
				PageIndex: 1,
				PageSize:  300,
				OrderBy:   "akdsfj",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderByError),
				Msg:  j.MsgOrderByError,
			},
			Name: "orderBy error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt),
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "esc",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderError),
				Msg:  j.MsgOrderError,
			},
			Name: "order error",
		},
	}
}
func testListDiscoveryData(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
