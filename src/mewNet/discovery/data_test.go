package discovery

import (
	"fmt"
	"os"
)

func DataPartTimeJobExample() []TestExample {
	return []TestExample{
		{
			Request: testPartTimeJobID,
			Response: &RespJSON{
				Code: "200",
			},
			Name: "success",
		},
		{
			Request: "aladlsfj",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "id error",
		},
		{
			Request: "12345325",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "no rows",
		},
	}
}
func DataActivityExample() []TestExample {
	return []TestExample{
		{
			Request: testActivityID,
			Response: &RespJSON{
				Code: "200",
			},
			Name: "success",
		},
		{
			Request: "aladlsfj",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "id error",
		},
		{
			Request: "12345325",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "no rows",
		},
	}
}
func DataTrainingExample() []TestExample {
	return []TestExample{
		{
			Request: testTrainingID,
			Response: &RespJSON{
				Code: "200",
			},
			Name: "success",
		},
		{
			Request: "aladlsfj",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "id error",
		},
		{
			Request: "12345325",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "no rows",
		},
	}
}

func testDataDiscovery(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testHtmlExamples(val, testJson, w)
	}
}
