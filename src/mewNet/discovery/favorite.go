package discovery

import (
	"mewNet"
	"modal"
	"net/http"
	"strings"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func favoriteDiscovery(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	table := strings.TrimPrefix(r.URL.Path, "/favorite/")
	code, msg, debugMsg = j.JudgeDiscoveryType(table)
	if code != j.CodeSuccess {
		http.NotFound(w, r)
		return
	}
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.FavoriteJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealFavoriteDiscovery(table, reqJson.Sign, reqJson.ID)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealFavoriteDiscovery(table, sign, id string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select icon,title,createTime "createTime",fee,position,description 
	from `+table+` where id = $1;`, id)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, mewNet.DealMapDoNotThing)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.Exec(`insert into favorite`+table+`(userID,createTime,icon,title,fee,
	position,objectCreateTime,description,objectID) values($1,$2,$3,$4,$5,$6,$7,$8,$9)
	on conflict(userID,objectID) do nothing;`, userID, time.Now(), data["icon"], data["title"],
		data["fee"], data["position"], data["createTime"], data["description"], id)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code == j.CodeDBExecNotEffect {
		return j.CodeHaveBeenFavorite, j.MsgHaveBeenFavorite, debugMsg, result
	} else if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
