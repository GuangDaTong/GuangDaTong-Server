package discovery

import (
	"mewNet"
	"net/http"
)

func dataActivity(w http.ResponseWriter, r *http.Request) {
	mewNet.DealData(w, r, "activity")
}
func dataPartTimeJob(w http.ResponseWriter, r *http.Request) {
	mewNet.DealData(w, r, "partTimeJob")
}
func dataTraining(w http.ResponseWriter, r *http.Request) {
	mewNet.DealData(w, r, "training")
}
