package discovery

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "listPartTimeJob",
		Method:     "POST",
		Pattern:    "/list/partTimeJob",
		SetExample: ListDiscoveryDataExample,
		Test:       testListDiscoveryData,
	},
	{
		Name:       "listActivity",
		Method:     "POST",
		Pattern:    "/list/activity",
		SetExample: ListDiscoveryDataExample,
		Test:       testListDiscoveryData,
	},
	{
		Name:       "listTraining",
		Method:     "POST",
		Pattern:    "/list/training",
		SetExample: ListDiscoveryDataExample,
		Test:       testListDiscoveryData,
	},
	{
		Name:       "dataPartTimeJob",
		Method:     "GET",
		Pattern:    "/data/partTimeJob",
		SetExample: DataPartTimeJobExample,
		Test:       testDataDiscovery,
	},
	{
		Name:       "dataActivity",
		Method:     "GET",
		Pattern:    "/data/activity",
		SetExample: DataActivityExample,
		Test:       testDataDiscovery,
	},
	{
		Name:       "dataTraining",
		Method:     "GET",
		Pattern:    "/data/training",
		SetExample: DataActivityExample,
		Test:       testDataDiscovery,
	},
	{
		Name:       "favoritePartTimeJob",
		Method:     "POST",
		Pattern:    "/favorite/partTimeJob",
		SetExample: FavoritePartTimeJobExample,
		Test:       testFavoriteDiscovery,
	},
	{
		Name:       "favoriteTraining",
		Method:     "POST",
		Pattern:    "/favorite/training",
		SetExample: FavoriteTrainingExample,
		Test:       testFavoriteDiscovery,
	},
	{
		Name:       "favoriteActivity",
		Method:     "POST",
		Pattern:    "/favorite/activity",
		SetExample: FavoriteActivityExample,
		Test:       testFavoriteDiscovery,
	},
	{
		Name:       "signUpPartTimeJob",
		Method:     "POST",
		Pattern:    "/signUp/partTimeJob",
		SetExample: SignUpPartTimeJobExample,
		Test:       testSignUp,
	},
	{
		Name:       "signUpActivity",
		Method:     "POST",
		Pattern:    "/signUp/activity",
		SetExample: SignUpActivityExample,
		Test:       testSignUp,
	},
	{
		Name:       "signUpTraining",
		Method:     "POST",
		Pattern:    "/signUp/training",
		SetExample: SignUpTrainingExample,
		Test:       testSignUp,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	config.UseQiNiuYun = false
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples
var ToBytes = modal.ToBytes

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample
type RespJSON = modal.RespJSON
