package discovery

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func FavoritePartTimeJobExample() []TestExample {
	_, err := pgsql.Exec(`delete from favoritePartTimeJob where objectID = $1 and userID = $2`, testPartTimeJobID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return FavoriteDiscoveryExample(testPartTimeJobID)
}
func FavoriteActivityExample() []TestExample {
	_, err := pgsql.Exec(`delete from favoriteActivity where objectID = $1 and userID = $2`, testActivityID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return FavoriteDiscoveryExample(testActivityID)
}
func FavoriteTrainingExample() []TestExample {
	_, err := pgsql.Exec(`delete from favoriteTraining where objectID = $1 and userID = $2`, testTrainingID, testUserID)
	if err != nil {
		panic(err.Error())
	}
	return FavoriteDiscoveryExample(testTrainingID)
}
func FavoriteDiscoveryExample(id string) []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.FavoriteJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   "askdjf",
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   "1000000000000000000",
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeNoRows),
				Msg:  j.MsgNoRows,
			},
			Name: "no rows",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeHaveBeenFavorite),
				Msg:  j.MsgHaveBeenFavorite,
			},
			Name: "already favorite",
		},
	}
}
func testFavoriteDiscovery(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
