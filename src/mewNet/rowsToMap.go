package mewNet

import (
	"config"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
	j "utils/judger"
)

//这个函数会close rows	rows中的字段全部用字符串处理,null会变为"",keys指要将string转为string array的key
func RowsToMapArray(rows *sql.Rows, dealRowFun func(map[string]interface{}) error) (code int, msg, debugMsg string, res []map[string]interface{}) {
	defer rows.Close()
	cols, err := rows.Columns()
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), nil
	}
	num := len(cols)
	res = []map[string]interface{}{} //避免返回null
	for rows.Next() {
		str := make([][]byte, num)
		row := map[string]interface{}{}
		var args []interface{}
		for i := 0; i < num; i++ {
			//interface{}时数据库的enum 和 array类型 Scan会乱码,但能自动识别int/string类型,
			args = append(args, &str[i])
		}
		err = rows.Scan(args...)
		if err != nil {
			return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), nil
		}
		for i := 0; i < num; i++ {
			row[cols[i]] = string(str[i]) //row[cols[i]]=args[i]得到的会是base64
		}
		err = dealRowFun(row)
		if err != nil {
			return j.CodeRowsToMapError, j.MsgRowsToMapError, err.Error(), nil
		}
		res = append(res, row)
	}
	return j.CodeSuccess, j.MsgSuccess, "", res
}

//这个函数会close rows	rows中的字段全部用字符串处理,null会变为"",keys指要将string转为string array的key
func RowsToMap(rows *sql.Rows, dealRowFun func(map[string]interface{}) error) (code int, msg, debugMsg string, res map[string]interface{}) {
	defer rows.Close()
	cols, err := rows.Columns()
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), nil
	}
	num := len(cols)
	if !rows.Next() {
		return j.CodeNoRows, j.MsgNoRows, "query no rows", nil
	}
	str := make([][]byte, num)
	res = map[string]interface{}{} //避免返回null
	var args []interface{}
	for i := 0; i < num; i++ {
		//interface{}时数据库的enum 和 array类型 Scan会乱码,但能自动识别int/string类型,
		args = append(args, &str[i])
	}
	err = rows.Scan(args...)
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), nil
	}
	for i := 0; i < num; i++ {
		res[cols[i]] = string(str[i]) //row[cols[i]]=args[i]得到的会是base64
	}
	err = dealRowFun(res)
	if err != nil {
		return j.CodeRowsToMapError, j.MsgRowsToMapError, err.Error(), nil
	}
	if rows.Next() {
		return j.CodeMultiRows, j.MsgMultiRows, "query multirows", nil
	}
	return j.CodeSuccess, j.MsgSuccess, "", res
}

func RowsToMapString(rows *sql.Rows) (code int, msg, debugMsg string, res map[string]string) {
	defer rows.Close()
	cols, err := rows.Columns()
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), nil
	}
	num := len(cols)
	if !rows.Next() {
		return j.CodeNoRows, j.MsgNoRows, "query no rows", nil
	}
	str := make([][]byte, num)
	res = map[string]string{}
	var args []interface{}
	for i := 0; i < num; i++ {
		//interface{}时数据库的enum 和 array类型 Scan会乱码,但能自动识别int/string类型,
		args = append(args, &str[i])
	}
	err = rows.Scan(args...)
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), nil
	}
	for i := 0; i < num; i++ {
		res[cols[i]] = string(str[i]) //row[cols[i]]=args[i]得到的会是base64
	}
	if rows.Next() {
		return j.CodeMultiRows, j.MsgMultiRows, "query multirows", nil
	}
	return j.CodeSuccess, j.MsgSuccess, "", res
}

var ErrAssertString = errors.New("type assert to string failed")

//image,thumbnailImage
func DealMapImageAndThumb(mp map[string]interface{}) error {
	err := DealMapImage(mp, "image")
	if err != nil {
		return err
	}
	err = DealMapImage(mp, "thumbnailImage")
	if err != nil {
		return err
	}
	return nil
}

//image,thumbnailImage,icon
func DealMapImgAndIcon(mp map[string]interface{}) error {
	err := DealMapImageAndThumb(mp)
	if err != nil {
		return err
	}
	err = DealMapIcon(mp)
	if err != nil {
		return err
	}
	return nil
}

func DealMapStringToInt64(mp map[string]interface{}, key string) (intVal int64, err error) {
	val, ok := mp[key].(string)
	if !ok {
		return 0, ErrAssertString
	}
	if val == "" {
		return 0, nil
	}
	_, err = fmt.Sscan(val, &intVal)
	if err != nil {
		return 0, err
	}
	return intVal, err
}

func DealMapExpToLevel(mp map[string]interface{}) error {
	exp, ok := mp["exp"].(string)
	if !ok {
		return ErrAssertString
	}
	var expNum int
	_, err := fmt.Sscan(exp, &expNum)
	if err != nil {
		return err
	}
	mp["level"] = fmt.Sprint(GetLevel(expNum))
	return nil
}

func DealPrefix(s string) string {
	if len(s) > 0 && s[0] == '/' {
		return s[1:]
	}
	return s
}

func DealMapImage(mp map[string]interface{}, key string) error {
	image, ok := mp[key].(string)
	if !ok {
		return ErrAssertString
	}
	imageArray := StringToStringArray(image)
	for i, _ := range imageArray {
		imageArray[i] = config.SrcHost + DealPrefix(imageArray[i])
	}
	mp[key] = imageArray
	return nil
}

func DealMapDoNotThing(_ map[string]interface{}) error {
	return nil
}
func DealMapIcon(mp map[string]interface{}) error {
	icon, ok := mp["icon"].(string)
	if !ok {
		return ErrAssertString
	}
	mp["icon"] = config.SrcHost + DealPrefix(icon)
	return nil
}

func DealMapFormatTime(mp map[string]interface{}) error {
	timeStr, ok := mp["createTime"].(string)
	if !ok {
		return ErrAssertString
	}
	formatStr, err := FormatPSQLTime(timeStr)
	if err != nil {
		return err
	}
	mp["createTime"] = formatStr
	return nil
}

func FormatPSQLTime(timeStr string) (string, error) {
	if len(timeStr) < 19 {
		return timeStr, errors.New("time format error")
	}
	return timeStr[:10] + " " + timeStr[11:19], nil
}

func FormatNowToInt() string {
	t := time.Now()
	return fmt.Sprintf("%d%d%d%d%d%d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
}

func DealMapStringIcon(mp map[string]string) {
	mp["icon"] = config.SrcHost + mp["icon"]
}

//如果某个字段的数据库类型是数组，用这个函数将string转化为[]string
func stringToStringArray(mp map[string]interface{}, key string) error {
	val, ok := mp[key].(string)
	if !ok {
		return errors.New("value type is not string")
	}
	arr := StringToStringArray(val)
	if key == "image" || key == "thumbnailImage" {
		for i, _ := range arr {
			arr[i] = config.SrcHost + arr[i]
		}
	}
	mp[key] = arr
	return nil
}

//将{a,b,c} string转化为[]string	a b c
func StringToStringArray(s string) []string {
	l := len(s)
	if l <= 2 {
		return make([]string, 0)
	}
	arr := strings.Split(s[1:l-1], ",")
	n := len(arr)
	for i := 0; i < n; i++ {
		str := arr[i]
		l := len(arr[i])
		if str[0] == '"' && l > 1 && str[l-1] == '"' {
			arr[i] = str[1 : l-1]
		}
	}
	return arr
}

//将[]string a b c转化为 string {a,b,c}
func StringArrayToString(arr []string) string {
	str := "{"
	n := len(arr)
	for i := 0; i < n; i++ {
		if i == 0 {
			str += ``
		} else {
			str += `,`
		}
		str += arr[i]
		str += ``
	}
	str += "}"
	return str
}
