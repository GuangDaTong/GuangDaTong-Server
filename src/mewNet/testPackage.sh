debugDir="/debugExamples"
releaseDir="/releaseExamples"
docDir="../../../mew_devdata/interfaceDoc/"
for path in `ls`
do
	if test -d $path
	then
		echo 'test package '$path
		cd $path
		./test.sh
		cd ..
		rm -r $docDir$path$debugDir
		rm -r $docDir$path$releaseDir
		cp -r $path$debugDir $docDir$path
		cp -r $path$releaseDir $docDir$path
		sleep 3s
	fi
done