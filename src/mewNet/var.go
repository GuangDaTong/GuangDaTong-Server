package mewNet

import (
	"fmt"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

var (
	AppKey    = "1f42acbbacf98"
	AppSecret = "01f01c923339a5f54c3a12e491e1efa9"

	DefaultIcon    = "icon/default.ico"
	DefaultSchool  = "广州大学"
	DefaultOrderBy = "createTime"
	DefaultOrder   = "desc"

	NewDuration     = time.Hour * 24 * 7
	MaxNewNum       = 3
	MaxHotNum       = 3
	DataLink        = "data/"
	ShareAwardCoins = 10
	CoinsRate       = 10 //系统回收多少分之一

	NormalCollege = 20
	NormalSchool  = 40
	UrgentCollege = 100
	UrgentSchool  = 600

	SystemUserID string //系统喵ID

	ServiceAccounts []string
	ServiceIDs      []string
	ServiceNum            = 3
	serviceAccount  int64 = 10000000001
	serviceID       int64 = 11

	ForbidSendAskDuration    int64 = 72 * 60 * 60
	ForbidLeaveWordDuration  int64 = 72 * 60 * 60
	ForbidInformDuration     int64 = 72 * 60 * 60
	ForbidCommentAskDuration int64 = 72 * 60 * 60

	AndroidMinVersion = "1.0.0"
	AndroidCurVersion = "1.0.0"
	IOSMinVersion     = "1.0.0"
	IOSCurVersion     = "1.0.0"
	AndroidUpdateLog  = ""
	IOSUpdateLog      = ""
)

//问问扣费规则
var Rules = map[string]map[string]int{
	"normal": map[string]int{
		"college": NormalCollege,
		"school":  NormalSchool,
	},
	"urgent": map[string]int{
		"college": UrgentCollege,
		"school":  UrgentSchool,
	},
}
var level = []int{-1, 8, 24, 56, 120, 248, 504, 1016, 2040, 4088, 1000000000}

func GetLevel(exp int) int {
	n := len(level)
	i := 0
	for ; i < n; i++ {
		if exp <= level[i] {
			break
		}
	}
	return i
}

func InitVersion() {
	rows, err := pgsql.Query(`select log,version,OSType "OSType",typee "type" from version`)
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, _ := RowsToMapArray(rows, func(mp map[string]interface{}) error {
		log, ok := mp["log"].(string)
		if !ok {
			return ErrAssertString
		}
		version, ok := mp["version"].(string)
		if !ok {
			return ErrAssertString
		}
		OSType, ok := mp["OSType"].(string)
		if !ok {
			return ErrAssertString
		}
		typee, ok := mp["type"].(string)
		if !ok {
			return ErrAssertString
		}
		if OSType == "android" && typee == "min" {
			AndroidMinVersion = version
		} else if OSType == "android" && typee == "cur" {
			AndroidCurVersion = version
			AndroidUpdateLog = log
		} else if OSType == "ios" && typee == "min" {
			IOSMinVersion = version
		} else if OSType == "ios" && typee == "cur" {
			IOSCurVersion = version
			IOSUpdateLog = log
		} else {
			return fmt.Errorf("error:version:%s;OSType:%s;type:%s;", version, OSType, typee)
		}
		return nil
	})
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
}

func InitService() {
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		panic(err.Error())
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			panic(err.Error())
		}
		err = tx.Commit()
		if err != nil {
			tx.Rollback()
			panic(err.Error())
		}
	}()
	for i := 0; i < ServiceNum; i++ {
		account := fmt.Sprint(serviceAccount + (int64)(i))
		ID := fmt.Sprint(serviceID + (int64)(i))
		nickname := "用户" + fmt.Sprint(i+1)
		ServiceAccounts = append(ServiceAccounts, account)
		ServiceIDs = append(ServiceIDs, ID)
		_, err = tx.Exec(`
		insert into users(id,account,nickname,icon,password,coins,college,school,exp,custom,gender) 
		values ($1,$2,$3,$4,$5,0,$6,$7,0,'{}'::jsonb,'女') 
		on conflict(id) do update 
		set account = $2,nickname = $3,icon = $4;`,
			ID, account, nickname, DefaultIcon, j.EncryptPassword("mewmewstudio"),
			"计算机科学与教育软件学院", DefaultSchool)
		if err != nil {
			return
		}
	}
}

func InitSystemUserID() {
	rows, err := pgsql.Query(`insert into users(id,account,nickname,icon,password,coins,college,school,exp,custom,gender,grade) values
	(10,'00000000100','广大通','icon/default.ico','` + j.Md5Encode("mewmewstudio") + `',0,'计算机科学与教育软件学院','广州大学',0,'{}'::jsonb,'','其他') on conflict(id)
	do update set coins = 0,nickname = '广大通' returning id;`)
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, data := RowsToMapString(rows)
	if code != j.CodeSuccess || data["id"] == "" {
		panic(fmt.Sprint(code, msg, debugMsg, data["id"]))
	}
	SystemUserID = data["id"]
}
