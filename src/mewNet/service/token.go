package service

import (
	"logs"
	"mewNet"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

var maxTokenTime time.Duration = time.Hour * 24

func existToken(token string) (code int, msg, debugMsg, userID string) {
	rows, err := pgsql.Query(`update serviceToken set createTime = $2 where token = $1 returning userID "userID"`,
		token, time.Now())
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), ""
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code == j.CodeNoRows {
		return j.CodeTokenNotExist, j.MsgTokenNotExist, debugMsg, ""
	} else if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", data["userID"]
}

var clearTimeInterval time.Duration = time.Minute * 5

func ClearToken() {
	_, err := pgsql.Exec("update serviceToken set token = null where createTime < $1;", time.Now().Add(-maxTokenTime))
	if err != nil {
		logs.ErrorPrint(err.Error())
	}
	time.AfterFunc(clearTimeInterval, ClearToken)
}
