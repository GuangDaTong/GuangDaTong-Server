package service

import (
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

func contactServicePartTimeJob(w http.ResponseWriter, r *http.Request) {
	contactService(w, r, "partTimeJob")
}
func contactServiceActivity(w http.ResponseWriter, r *http.Request) {
	contactService(w, r, "activity")
}
func contactServiceTraining(w http.ResponseWriter, r *http.Request) {
	contactService(w, r, "training")
}

func contactService(w http.ResponseWriter, r *http.Request, table string) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ContactServiceJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealContactService(reqJson.Sign, reqJson.Words, reqJson.ObjectID, table)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealContactService(sign, words, objectID, objectName string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(objectID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(words)
	if code != j.CodeSuccess {
		return
	}
	var receiverID string
	if objectName == "partTimeJob" {
		receiverID = mewNet.ServiceIDs[0]
	} else if objectName == "training" {
		receiverID = mewNet.ServiceIDs[1]
	} else if objectName == "activity" {
		receiverID = mewNet.ServiceIDs[2]
	} else {
		receiverID = mewNet.ServiceIDs[0]
		logs.ErrorPrint(objectName)
	}
	rows, err := pgsql.Query(`
	insert into leaveWords(senderID,receiverID,words,objectName,objectID,createTime)
	values($1,$2,$3,$4,$5,$6) returning id,createTime "createTime"`, userID, receiverID,
		words, objectName, objectID, time.Now())
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, mewNet.DealMapDoNotThing)
	if code != j.CodeSuccess {
		return
	}
	result = data
	jpush.PushServiceUserID(receiverID)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
