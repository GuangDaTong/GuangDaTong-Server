package service

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
)

func ContactServiceExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.ContactServiceJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: "1",
				Words:    "alksdflkasjdfkl",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: "1",
				Words:    "alksdflkasjdfkl",
				Sign:     j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: "-123123",
				Words:    "alksdflkasjdfkl",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ObjectID: "123123",
				Words:    "aaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!aaaaaaaaaaaaaaaaaaaaaaaaa",
				Sign:     j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeContentTooLong),
				Msg:  j.MsgContentTooLong,
			},
			Name: "content too long",
		},
	}
}
func testContactService(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
