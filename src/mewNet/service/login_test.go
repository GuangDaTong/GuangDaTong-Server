package service

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func LoginServiceExample() []TestExample {
	type exampleJSON = modal.LoginServiceJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign:     j.Md5Encode(config.Salt, "mewmewstudio"),
				Account:  "10000000001",
				Password: "mewmewstudio",
				RegID:    "lakdjsflksdajflkj",
				OSType:   "ios",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Account:  "10000000001",
				Password: "mewmewstudio",
				RegID:    "lakdjsflksdajflkj",
				OSType:   "ios",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:     j.Md5Encode(config.Salt, "mewmewstudio"),
				Account:  "10100000001",
				Password: "mewmewstudio",
				RegID:    "lakdjsflksdajflkj",
				OSType:   "ios",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAccountNotExist),
				Msg:  j.MsgAccountNotExist,
			},
			Name: "service account not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:     j.Md5Encode(config.Salt, "memewstudio"),
				Account:  "10000000001",
				Password: "memewstudio",
				RegID:    "lakdjsflksdajflkj",
				OSType:   "ios",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePasswordWrong),
				Msg:  j.MsgPasswordWrong,
			},
			Name: "password wrong",
		},
	}
}
func testLoginService(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
