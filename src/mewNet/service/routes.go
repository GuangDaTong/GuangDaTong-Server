package service

import (
	"mewNet"
	"modal"
)

var Routes = []Route{
	Route{
		"contactServicePartTimeJob",
		"POST",
		"/contactService/partTimeJob",
		contactServicePartTimeJob,
	},
	Route{
		"contactServiceActivity",
		"POST",
		"/contactService/activity",
		contactServiceActivity,
	},
	Route{
		"contactServiceTraining",
		"POST",
		"/contactService/training",
		contactServiceTraining,
	},
	Route{
		"loginService",
		"POST",
		"/login/service",
		loginService,
	},
	Route{
		"listServiceMessage",
		"POST",
		"/list/serviceMessage",
		listServiceMessage,
	},
	Route{
		"serviceReply",
		"POST",
		"/serviceReply",
		serviceReply,
	},
}

func init() {
	mewNet.AppendRoute(Routes)
}

type Route = modal.Route
