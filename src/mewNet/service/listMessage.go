package service

import (
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func listServiceMessage(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListServiceMessageJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListServiceMessage(reqJson.Sign)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListServiceMessage(sign string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := existToken(sign)
	if code != j.CodeSuccess {
		return
	}
	var now = time.Now()
	rows, err := pgsql.Query(`
	select 
	msg.id,
	msg.senderID "senderID",
	msg.words,
	msg.createTime "createTime",
	msg.objectName "objectName",
	msg.objectID "objectID",
	users.icon,
	users.nickname
	from leaveWords msg
	left outer join getLeaveWords getTime on (msg.receiverID = getTime.userID)
	inner join users on (msg.senderID = users.id)
	where msg.receiverID = $1 and (getTime.createTime is null or msg.createTime > getTime.createTime);
	`, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, func(mp map[string]interface{}) error {
		err := mewNet.DealMapIcon(mp)
		if err != nil {
			return err
		}
		objectName, ok := mp["objectName"].(string)
		if !ok {
			return mewNet.ErrAssertString
		}
		if objectName == "" {
			return nil
		}
		objectID, ok := mp["objectID"].(string)
		if !ok {
			return mewNet.ErrAssertString
		}
		words, ok := mp["words"].(string)
		if !ok {
			return mewNet.ErrAssertString
		}
		rows, err := pgsql.Query(`select title from `+objectName+` where id = $1`, objectID)
		if err != nil {
			return err
		}
		code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
		if code != j.CodeSuccess {
			data = map[string]string{"title": msg + ":" + debugMsg}
		}
		mp["words"] = "《" + data["title"] + "》\n" + words
		return nil
	})
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.Exec(`insert into getLeaveWords(userID,createTime) values($1,$2) 
	on conflict(userID) do update set createTime = $2`, userID, now)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "data", data)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
