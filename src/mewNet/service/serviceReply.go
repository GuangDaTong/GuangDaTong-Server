package service

import (
	"mewNet"
	"modal"
	"net/http"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

func serviceReply(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ServiceReplyJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealServiceReply(reqJson.Sign, reqJson.ReceiverID, reqJson.Words)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealServiceReply(sign, receiverID, words string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := existToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(receiverID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(words)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`insert into leaveWords(senderID,receiverID,words,createTime) values
	($1,$2,$3,$4) returning id,createTime "createTime";`, userID, receiverID, words, time.Now())
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, mewNet.DealMapDoNotThing)
	if code != j.CodeSuccess {
		return
	}
	result = data
	jpush.PushLeaveWords(receiverID)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
