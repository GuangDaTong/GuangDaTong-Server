package service

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func loginService(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.LoginServiceJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealLoginService(reqJson.Sign, reqJson.Account, reqJson.Password, reqJson.RegID, reqJson.OSType)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealLoginService(sign, account, password, regID, osType string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, password)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAccount(account)
	if code != j.CodeSuccess {
		return
	}
	//check account
	var exist bool
	for _, val := range mewNet.ServiceAccounts {
		if val == account {
			exist = true
			break
		}
	}
	if !exist {
		return j.CodeAccountNotExist, j.MsgAccountNotExist, "service account not exist:" + account, result
	}

	code, msg, debugMsg = j.JudgePassword(password)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select password,id "userID" from users where account = $1`, account)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	if data["password"] != j.EncryptPassword(password) {
		return j.CodePasswordWrong, j.MsgPasswordWrong, "password wrong:" + password, result
	}
	token := j.Md5Encode(config.Salt, fmt.Sprint(time.Now()))
	sqlResult, err := pgsql.Exec(`insert into serviceToken(userID,createTime,token,OSType,regID)
	values($1,$2,$3,$4,$5) on conflict(userID) do update 
	set createTime = $2,token = $3,OSType = $4,regID = $5`,
		data["userID"], time.Now(), j.Md5Encode(config.Salt, token), osType, regID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	rows, err = pgsql.Query(`select icon,nickname from users where id = $1`, data["userID"])
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data1 := mewNet.RowsToMap(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	data1["token"] = token
	data1["userID"] = data["userID"]
	result = data1

	return j.CodeSuccess, j.MsgSuccess, "", result
}
