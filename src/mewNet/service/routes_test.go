package service

import (
	"config"
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "contactServicePartTimeJob",
		Method:     "POST",
		Pattern:    "/contactService/partTimeJob",
		SetExample: ContactServiceExample,
		Test:       testContactService,
	},
	{
		Name:       "contactServiceActivity",
		Method:     "POST",
		Pattern:    "/contactService/activity",
		SetExample: ContactServiceExample,
		Test:       testContactService,
	},
	{
		Name:       "contactServiceTraining",
		Method:     "POST",
		Pattern:    "/contactService/training",
		SetExample: ContactServiceExample,
		Test:       testContactService,
	},
	{
		Name:       "loginService",
		Method:     "POST",
		Pattern:    "/login/service",
		SetExample: LoginServiceExample,
		Test:       testLoginService,
	},
	{
		Name:       "listServiceMessage",
		Method:     "POST",
		Pattern:    "/list/serviceMessage",
		SetExample: ListServiceMessageExample,
		Test:       testListServiceMessage,
	},
	{
		Name:       "serviceReply",
		Method:     "POST",
		Pattern:    "/serviceReply",
		SetExample: ServiceReplyExample,
		Test:       testServiceReply,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

func ToBytes(v interface{}) []byte {
	bytes, err := json.Marshal(v)
	if err != nil {
		panic(err.Error())
	}
	return bytes
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample

type RespJSON = modal.RespJSON
