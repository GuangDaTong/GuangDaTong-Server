package service

import (
	"config"
	"fmt"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func ListServiceMessageExample() []TestExample {
	token := fmt.Sprint(time.Now())
	_, err := pgsql.Exec(`insert into serviceToken(userID,createTime,token,osType,regID)
	values($1,$2,$3,$4,$5) on conflict(userID) do update set createTime = $2,token = $3`,
		11, time.Now().Add(time.Hour*24), j.Md5Encode(config.Salt, token), "ios", "")
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into getLeaveWords(userID,createTime) values($1,$2) 
	on conflict(userID) do update set createTime = $2`, 11, time.Now().Add(-time.Hour*24))
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.ListServiceMessageJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
	}
}
func testListServiceMessage(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
