package school

import (
	"fmt"
	"os"
)

func DataExample() []TestExample {
	return []TestExample{
		{
			Request: testGuidanceID,
			Response: &RespJSON{
				Code: "200",
			},
			Name: "success",
		},
		{
			Request: "aladlsfj",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "id error",
		},
		{
			Request: "12345325",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "no rows",
		},
	}
}

func testDataSchool(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testHtmlExamples(val, testJson, w)
	}
}
