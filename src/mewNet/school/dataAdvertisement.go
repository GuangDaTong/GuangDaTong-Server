package school

import (
	"logs"
	"mewNet"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func dataAdvertisement(w http.ResponseWriter, r *http.Request) {
	var (
		content    = "<h1>页面不存在或已删除</h1>出错信息: "
		title      = "页面不存在或已删除"
		createTime = ""
		penName    = ""
		OSType     = ""
	)
	defer func() {
		mewNet.WriteData(w, title, content, createTime, penName, OSType)
	}()
	err := r.ParseForm()
	if err != nil {
		content += "parseForm error: " + err.Error()
		return
	}
	id := r.FormValue("id")
	code, msg, debugMsg := j.JudgeID(id)
	if code != j.CodeSuccess {
		content += msg
		logs.ResultPrint(code, msg, debugMsg)
		return
	}
	OSType = r.FormValue("OSType")
	rows, err := pgsql.Query(`select content,createTime "createTime",title from advertisement where id = $1`, id)
	if err != nil {
		content += "db query failed: " + err.Error()
		return
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, mewNet.DealMapFormatTime)
	if code != j.CodeSuccess {
		content = msg
		logs.ResultPrint(code, msg, debugMsg)
		return
	}
	createTime = data["createTime"].(string)
	title = data["title"].(string)
	content = data["content"].(string)
}
