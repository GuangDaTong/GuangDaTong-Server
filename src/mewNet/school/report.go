package school

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func reportGuidance(w http.ResponseWriter, r *http.Request) {
	report(w, r, "guidance")
}
func reportSummary(w http.ResponseWriter, r *http.Request) {
	report(w, r, "summary")
}
func reportClub(w http.ResponseWriter, r *http.Request) {
	report(w, r, "club")
}
func reportLook(w http.ResponseWriter, r *http.Request) {
	report(w, r, "look")
}

func report(w http.ResponseWriter, r *http.Request, table string) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ReportJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealReport(reqJson.Sign, reqJson.Token, reqJson.Description, reqJson.ObjectID, table)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealReport(sign, token, description, objectID, objectName string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, objectID)
	if code != j.CodeSuccess {
		return
	}
	var UserID interface{}
	code, msg, debugMsg, userID := mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code == j.CodeSuccess {
		UserID = userID
	}
	code, msg, debugMsg = j.JudgeID(objectID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeSchoolDataType(objectName)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(description)
	if code != j.CodeSuccess {
		return
	}
	_, err := pgsql.Exec(`insert into report(userID,description,createTime,status,award,objectID,objectName) values
	($1,$2,$3,'等待查看',0,$4,$5)`, UserID, description, time.Now(), objectID, objectName)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
