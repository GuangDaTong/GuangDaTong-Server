package school

import (
	"database/sql"
	"errors"
	"mewNet"
	"testing"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

var (
	testAccount       = "00000000000"
	testAccount1      = "12345678901"
	testPassword      = "123456"
	testSchool        = "广州大学"
	testCollege       = "计算机科学与教育软件学院"
	testDataAccount   = "00000000001"
	testIcon          = "icon/default.ico"
	testUserID        string
	testGuidanceID    string
	testSummaryID     string
	testClubID        string
	testLookID        string
	testPartTimeJobID string
	testActivityID    string
	testTrainingID    string
	testCommentID     string
	testAskID         string
)

func TestDB(*testing.T) {
	InitSQL()
}
func InitSQL() {
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	var err error
	pwd := j.EncryptPassword(testPassword)
	rows, err := pgsql.Query(`insert into users(account,nickname,icon,password,coins,college,school,exp,custom,gender) values
	($1,$1,$2,$3,0,$4,$5,0,'{"qq":"1769622057"}'::jsonb,'女') on conflict(account)
	do update set coins = 1000 returning id;`, testDataAccount, testIcon, pwd, testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	testUserID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into guidance(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个校园导航测试页面',0,0,$2,$4) returning id`, time.Now(), testSchool, mewNet.RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testGuidanceID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into summary(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个四年概要测试页面',0,0,$2,$4) returning id;`, time.Now(), testSchool, mewNet.RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testSummaryID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into club(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个社团信息测试页面',0,0,$2,$4) returning id;`, time.Now(), testSchool, mewNet.RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testClubID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into look(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个校园看看测试页面',0,0,$2,$4) returning id;`, time.Now(), testSchool, mewNet.RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testLookID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
}

func GetTestID(rows *sql.Rows) (id string, err error) {
	defer rows.Close()
	if !rows.Next() {
		return "", errors.New("no rows")
	}
	err = rows.Scan(&id)
	if err != nil {
		return "", err
	}
	return id, nil
}
