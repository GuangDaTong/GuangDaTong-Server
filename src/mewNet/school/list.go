package school

import (
	"config"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func listGuidance(w http.ResponseWriter, r *http.Request) {
	listSchoolData(w, r, "guidance")
}

func listSummary(w http.ResponseWriter, r *http.Request) {
	listSchoolData(w, r, "summary")
}

func listClub(w http.ResponseWriter, r *http.Request) {
	listSchoolData(w, r, "club")
}

func listLook(w http.ResponseWriter, r *http.Request) {
	listSchoolData(w, r, "look")
}

func listSchoolData(w http.ResponseWriter, r *http.Request, table string) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListSchoolDataJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListSchoolData(table, reqJson.Sign, reqJson.Token, reqJson.School, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealListSchoolData(table, sign, token, school string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result map[string]interface{}) {
	result = make(map[string]interface{})
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, userID := mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		userID = "-1"
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	start := time.Now()
	rows, err := pgsql.Query(`select data.id,data.createTime "createTime",data.title,data.status "type",
	data.readCount "readCount", zan,count(zan.userID) "isZan",count(favorite.userID) "isFavorite" 
	from `+table+` data 
	left outer join zan`+table+` zan on (zan.objectID = data.id and zan.userID = $3) 
	left outer join favorite`+table+` favorite on (favorite.objectID = data.id and favorite.userID = $3)
	where publishStatus = '已发布'
	group by zan.userID,data.id
	order by status asc,data.updateTime desc nulls last 
	limit $1 offset $2;`, pageSize, pageIndex*pageSize, userID)
	logs.DebugPrint(time.Since(start))
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	start = time.Now()
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapDoNotThing)
	logs.DebugPrint(time.Since(start))
	if code != j.CodeSuccess {
		return code, msg, debugMsg, result
	}
	addElemToMapArray("link", config.Host+mewNet.DataLink+table, data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "data", data)
	return j.CodeSuccess, j.MsgSuccess, "", result
}

func addElemToMapArray(key string, value interface{}, data []map[string]interface{}) {
	for _, val := range data {
		val[key] = value
	}
}
