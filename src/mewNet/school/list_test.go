package school

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func ListSchoolDataExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	_, err := db.Exec(`
	insert into zanGuidance(userID,objectID) values
	(` + testUserID + `,1),
	(` + testUserID + `,2),
	(` + testUserID + `,3) on conflict (userID,objectID) do nothing;
	insert into zanSummary(userID,objectID) values
	(` + testUserID + `,1),
	(` + testUserID + `,2),
	(` + testUserID + `,3) on conflict (userID,objectID) do nothing;
	insert into zanClub(userID,objectID) values
	(` + testUserID + `,1),
	(` + testUserID + `,2),
	(` + testUserID + `,3) on conflict (userID,objectID) do nothing;
	insert into zanLook(userID,objectID) values
	(` + testUserID + `,1),
	(` + testUserID + `,2),
	(` + testUserID + `,3) on conflict (userID,objectID) do nothing;
	`)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.ListSchoolDataJSON
	return []TestExample{
		{
			Request: ToBytes(&modal.ListSchoolDataJSON{
				PageIndex: 0,
				PageSize:  10,
				Token:     token,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success pageIndex == 0,token exist",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 1,
				PageSize:  10,
				School:    mewNet.DefaultSchool,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 1,
				PageSize:  10,
				School:    mewNet.DefaultSchool,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: -1,
				PageSize:  10,
				School:    mewNet.DefaultSchool,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 1,
				PageSize:  -20,
				School:    mewNet.DefaultSchool,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageSize error",
			Test: true,
		},
	}
}
func testListSchoolData(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
