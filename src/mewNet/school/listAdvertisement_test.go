package school

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
)

func ListAdvertisementExample() []TestExample {
	type exampleJSON = modal.ListAdvertisementJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				School: mewNet.DefaultSchool,
				Sign:   j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "school null success",
			Test: true,
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
			Test: true,
		},
		{
			Request: ToBytes(&exampleJSON{
				School: mewNet.DefaultSchool,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
			Test: true,
		},
	}
}
func testListAdvertisement(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
