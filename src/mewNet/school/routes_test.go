package school

import (
	"config"
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "listGuidance",
		Method:     "POST",
		Pattern:    "/list/guidance",
		SetExample: ListSchoolDataExample,
		Test:       testListSchoolData,
	},
	{
		Name:       "listSummary",
		Method:     "POST",
		Pattern:    "/list/summary",
		SetExample: ListSchoolDataExample,
		Test:       testListSchoolData,
	},
	{
		Name:       "listClub",
		Method:     "POST",
		Pattern:    "/list/club",
		SetExample: ListSchoolDataExample,
		Test:       testListSchoolData,
	},
	{
		Name:       "listLook",
		Method:     "POST",
		Pattern:    "/list/look",
		SetExample: ListSchoolDataExample,
		Test:       testListSchoolData,
	},
	{
		Name:       "listAdvertisement",
		Method:     "POST",
		Pattern:    "/list/advertisement",
		SetExample: ListAdvertisementExample,
		Test:       testListAdvertisement,
	},
	{
		Name:       "dataGuidance",
		Method:     "GET",
		Pattern:    "/data/guidance",
		SetExample: DataExample,
		Test:       testDataSchool,
	},
	{
		Name:       "dataSummary",
		Method:     "GET",
		Pattern:    "/data/summary",
		SetExample: DataExample,
		Test:       testDataSchool,
	},
	{
		Name:       "dataClub",
		Method:     "GET",
		Pattern:    "/data/club",
		SetExample: DataExample,
		Test:       testDataSchool,
	},
	{
		Name:       "dataLook",
		Method:     "GET",
		Pattern:    "/data/look",
		SetExample: DataExample,
		Test:       testDataSchool,
	},
	{
		Name:       "dataAdvertisement",
		Method:     "GET",
		Pattern:    "/data/advertisement",
		SetExample: DataExample,
		Test:       testDataSchool,
	},
	{
		Name:       "favoriteGuidance",
		Method:     "POST",
		Pattern:    "/favorite/guidance",
		SetExample: FavoriteGuidanceExample,
		Test:       testFavoriteSchoolData,
	},
	{
		Name:       "favoriteSummary",
		Method:     "POST",
		Pattern:    "/favorite/summary",
		SetExample: FavoriteSummaryExample,
		Test:       testFavoriteSchoolData,
	},
	{
		Name:       "favoriteClub",
		Method:     "POST",
		Pattern:    "/favorite/club",
		SetExample: FavoriteClubExample,
		Test:       testFavoriteSchoolData,
	},
	{
		Name:       "favoriteLook",
		Method:     "POST",
		Pattern:    "/favorite/look",
		SetExample: FavoriteLookExample,
		Test:       testFavoriteSchoolData,
	},
	{
		Name:       "reportGuidance",
		Method:     "POST",
		Pattern:    "/report/guidance",
		SetExample: ReportExample,
		Test:       testReport,
	},
	{
		Name:       "reportSummary",
		Method:     "POST",
		Pattern:    "/report/summary",
		SetExample: ReportExample,
		Test:       testReport,
	},
	{
		Name:       "reportClub",
		Method:     "POST",
		Pattern:    "/report/club",
		SetExample: ReportExample,
		Test:       testReport,
	},
	{
		Name:       "reportLook",
		Method:     "POST",
		Pattern:    "/report/look",
		SetExample: ReportExample,
		Test:       testReport,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

func ToBytes(v interface{}) []byte {
	bytes, err := json.Marshal(v)
	if err != nil {
		panic(err.Error())
	}
	return bytes
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample

type RespJSON = modal.RespJSON
