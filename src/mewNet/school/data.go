package school

import (
	"mewNet"
	"net/http"
)

func dataGuidance(w http.ResponseWriter, r *http.Request) {
	mewNet.DealData(w, r, "guidance")
}
func dataSummary(w http.ResponseWriter, r *http.Request) {
	mewNet.DealData(w, r, "summary")
}
func dataClub(w http.ResponseWriter, r *http.Request) {
	mewNet.DealData(w, r, "club")
}
func dataLook(w http.ResponseWriter, r *http.Request) {
	mewNet.DealData(w, r, "look")
}
