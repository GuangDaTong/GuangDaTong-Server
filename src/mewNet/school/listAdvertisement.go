package school

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listAdvertisement(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListAdvertisementJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListAdvertisement(reqJson.Sign, reqJson.School)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListAdvertisement(sign, school string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	//link是完整URL,image是相对URL
	rows, err := pgsql.Query(`select id,link,image,title from advertisement 
		where publishStatus = '已发布' and position < 10 order by position asc`)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, func(mp map[string]interface{}) error {
		image, ok := mp["image"].(string)
		if !ok {
			return mewNet.ErrAssertString
		}
		mp["image"] = config.SrcHost + image
		link, ok := mp["link"].(string)
		if !ok {
			return mewNet.ErrAssertString
		}
		id, ok := mp["id"].(string)
		if !ok {
			return mewNet.ErrAssertString
		}
		if link == "" {
			mp["link"] = config.Host + mewNet.DataLink + "advertisement?id=" + id
		}
		return nil
	})
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "data", data)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
