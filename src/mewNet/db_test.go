package mewNet

import (
	"database/sql"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"os"
	"testing"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

var (
	testAccount       = "00000000000"
	testAccount1      = "12345678901"
	testPassword      = "123456"
	testSchool        = "广州大学"
	testCollege       = "计算机科学与教育软件学院"
	testDataAccount   = "00000000001"
	testIcon          = "icon/default.ico"
	testUserID        string
	testGuidanceID    string
	testSummaryID     string
	testClubID        string
	testLookID        string
	testPartTimeJobID string
	testActivityID    string
	testTrainingID    string
	testCommentID     string
	testAskID         string
)

func TestDB(*testing.T) {
	InitSQL()
}
func InitSQL() {
	//pgsql.Init(false, "../config/postgresql.conf")
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	var err error
	_, err = pgsql.Exec(`insert into schools(name) values ($1) on conflict do nothing;`, testSchool)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into colleges(school,college) values ($1,$2) on conflict do nothing;`, testSchool, testCollege)
	if err != nil {
		panic(err.Error())
	}
	pwd := j.EncryptPassword(testPassword)

	_, err = db.Exec(`
		insert into schools(name) values('广州大学') on conflict do nothing;
		--colleges
		insert into colleges(school,college) values 
		('广州大学','计算机科学与教育软件学院'),
		('广州大学','土木学院'),
		('广州大学','建筑学院'),
		('广州大学','人文学院'),
		('广州大学','外国语学院') on conflict do nothing;
		--users
		insert into users(id,account,nickname,icon,password,gender,coins,college,school,exp,custom) values
		(1,'00000000014','dy','icon/default.ico','` + pwd + `','男',0,'计算机科学与教育软件学院','广州大学',0,'{}'::jsonb),
		(2,'00000000015','xl','icon/default.ico','` + pwd + `','男',0,'计算机科学与教育软件学院','广州大学',0,'{}'::jsonb),
		(3,'00000000016','ja','icon/default.ico','` + pwd + `','男',0,'计算机科学与教育软件学院','广州大学',0,'{}'::jsonb),
		(4,'00000000017','js','icon/default.ico','` + pwd + `','男',0,'计算机科学与教育软件学院','广州大学',0,'{}'::jsonb),
		(5,'00000000002','中性','icon/default.ico','` + pwd + `','',0,'土木学院','广州大学',0,'{}'::jsonb),
		(6,'00000000003','女测试者','icon/default.ico','` + pwd + `','女',0,'建筑学院','广州大学',0,'{}'::jsonb),
		(7,'00000000004','c','icon/default.ico','` + pwd + `','女',0,'人文学院','广州大学',0,'{}'::jsonb),
		(8,'00000000005','c','icon/default.ico','` + pwd + `','女',0,'外国语学院','广州大学',0,'{}'::jsonb) on conflict do nothing;
		insert into users(account,nickname,icon,password,gender,coins,college,school,exp,custom) values
		('13800138000','13800138000','icon/default.ico','` + pwd + `','男',0,'计算机科学与教育软件学院','广州大学',0,'{}'::jsonb) on conflict do nothing;
		insert into ask(id,publisherID,reward,cost,rangee,typee,label,title,content,image,thumbnailImage,createTime,zan,school,college) values
		(1,1,4,10,'school','急问','学习','标题','这道题怎么做\n这道题怎么做\n这道题怎么做\n这道题怎么做\n这道题怎么做\n这道题怎么做\n这道题怎么做\n这道题怎么做','{"icon/default.ico"}','{"icon/default.ico"}',now(),1,'广州大学',null),
		(2,2,10,10,'school','急问','实习','很长很长很长很长很长很长的标题','xxx公司怎样\nxxx公司怎样\nxxx公司怎样\nxxx公司怎样\nxxx公司怎样','{"icon/default.ico","icon/default.ico"}','{"icon/default.ico","icon/default.ico"}',now(),2,'广州大学',null),
		(3,3,100,10,'college','急问','美食','标题','xxx好不好吃','{"icon/default.ico"}','{"icon/default.ico"}',now(),3,'广州大学','计算机科学与教育软件学院'),
		(4,4,200,10,'college','急问','其他','标题','xxx怎么去','{"icon/default.ico"}','{"icon/default.ico"}',now(),4,'广州大学','计算机科学与教育软件学院'),
		(5,3,100,10,'college','急问','美食','标题','xxx好不好吃','{}','{}',now(),5,'广州大学','人文学院'),
		(6,4,200,10,'college','急问','其他','标题','xxx怎么去','{"icon/default.ico","icon/default.ico"}','{"icon/default.ico","icon/default.ico"}',now(),6,'广州大学','人文学院') on conflict do nothing;
		insert into ask(publisherID,reward,cost,rangee,typee,label,title,content,image,thumbnailImage,createTime,zan,school,college) values
		(1,4,10,'school','急问','学习','标题','这道题怎么做
		这道题怎么做
		这道题怎么做
		这道题怎么做
		这道题怎么做
		这道题怎么做
		这道题怎么做
		这道题怎么做','{"icon/default1.jpg","icon/default2.jpg","icon/default3.jpg","icon/default4.jpg","icon/default6.jpg","icon/default5.jpg","icon/default7.jpg","icon/default8.jpg","icon/default.ico"}','{"icon/default1.jpg","icon/default2.jpg","icon/default3.jpg","icon/default4.jpg","icon/default6.jpg","icon/default5.jpg","icon/default7.jpg","icon/default8.jpg","icon/default.ico"}',now(),1,'广州大学',null),
		(1,100,10,'school','普问','学习','很长很长很长很长很长很长的标题','xxx公司怎样
		xxx公司怎样
		xxx公司怎样
		xxx公司怎样
		xxx公司怎样','{"icon/default4.jpg","icon/default2.jpg","icon/default1.jpg","icon/default6.jpg","icon/default.ico"}','{"icon/default4.jpg","icon/default2.jpg","icon/default1.jpg","icon/default6.jpg","icon/default.ico"}',now(),1,'广州大学',null),
		(1,100,10,'college','急问','失物招领','很长很长很长很长很长很长的标题','今天捡到了xxx的饭卡','{"icon/default4.jpg","icon/default2.jpg","icon/default2.jpg","icon/default7.jpg","icon/default1.jpg","icon/default6.jpg","icon/default.ico"}','{"icon/default4.jpg","icon/default2.jpg","icon/default2.jpg","icon/default2.jpg","icon/default1.jpg","icon/default6.jpg","icon/default.ico"}',now(),1,'广州大学',null),
		(5,1000,10,'school','普问','寻物启事','很长很长很长很长很长很长的标题','这道题怎么做','{"icon/default5.jpg","icon/default2.jpg"}','{"icon/default5.jpg","icon/default2.jpg"}',now(),1,'广州大学',null),
		(1,0,10,'school','普问','实习','很长很长很长很长很长很长的标题','xxx公司怎样','{"icon/default6.jpg"}','{"icon/default6.jpg"}',now(),1,'广州大学',null),
		(1,0,10,'college','普问','学习','标题','这道题怎么做','{"icon/default5.jpg"}','{"icon/default5.jpg"}',now(),1,'广州大学',null),
		(1,0,10,'school','急问','学习','标题','大物怎么混过考试','{"icon/default2.jpg"}','{"icon/default2.jpg"}',now(),1,'广州大学',null),
		(6,30,10,'college','普问','学习','标题','高数怎么混过考试','{}','{}',now(),1,'广州大学',null);
		`)
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`
		--advertisement
		insert into advertisement(id,publisherID,position,link,image,school,title) values
		(1,1,1,'https://debug.guangdamiao.com','http://src.debug.guangdamiao.com/advertisement/adImage1.png','广州大学','大学喵'),
		(2,1,2,'https://www.gzhuacm.cn','http://src.debug.guangdamiao.com/advertisement/adImage2.png','广州大学','GzhuOJ'),
		(3,1,3,'https://emb.mobi','http://src.debug.guangdamiao.com/advertisement/adImage3.png','广州大学','GGO'),
		(4,1,4,'https://debug.guangdamiao.com/mewmewstudio.html','https://debug.guangdamiao.com/favicon.ico','广州大学','大学喵'),
		(5,1,5,'https://debug.guangdamiao.com/login','https://debug.guangdamiao.com/favicon.ico','广州大学','大学喵'),
		(6,1,6,'https://debug.guangdamiao.com/listSchool','https://debug.guangdamiao.com/favicon.ico','广州大学','大学喵'),
		(7,1,7,'https://debug.guangdamiao.com/listCollege','https://debug.guangdamiao.com/favicon.ico','广州大学','大学喵'),
		(8,1,8,'https://debug.guangdamiao.com/register','https://debug.guangdamiao.com/favicon.ico','广州大学','大学喵'),
		(9,1,9,'https://debug.guangdamiao.com/canRegister','https://debug.guangdamiao.com/favicon.ico','广州大学','大学喵') on conflict do nothing;
		insert into guidance(id,createTime,title,content,readCount,zan,school,status,publisherID) values
		(1,now(),'校园导航1','这是一个校园导航测试页面',0,0,'广州大学','NEW',1),
		(2,now(),'校园导航2','这是一个校园导航测试页面',0,0,'广州大学','HOT',1),
		(4,now(),'校园导航3','这是一个校园导航测试页面',0,0,'广州大学','HOT',1),
		(5,now(),'校园导航4','这是一个校园导航测试页面',0,0,'广州大学','NEW',1),
		(3,now(),'校园导航5','这是一个校园导航测试页面',0,0,'广州大学','NEW',1),
		(6,now(),'校园导航6','这是一个校园导航测试页面',0,0,'广州大学','HOT',1)
		on conflict do nothing;
		insert into summary(id,createTime,title,content,readCount,zan,school,status,publisherID) values
		(1,now(),'四年概要1','这是一个四年概要测试页面',0,0,'广州大学','NEW',1),
		(2,now(),'四年概要2','这是一个四年概要测试页面',0,0,'广州大学','HOT',1),
		(4,now(),'四年概要3','这是一个四年概要测试页面',0,0,'广州大学','HOT',1),
		(5,now(),'四年概要4','这是一个四年概要测试页面',0,0,'广州大学','NEW',1),
		(3,now(),'四年概要5','这是一个四年概要测试页面',0,0,'广州大学','NEW',1),
		(6,now(),'四年概要6','这是一个四年概要测试页面',0,0,'广州大学','HOT',1)
		on conflict do nothing;
		insert into club(id,createTime,title,content,readCount,zan,school,status,publisherID) values
		(1,now(),'社团信息1','这是一个社团信息测试页面',0,0,'广州大学','NEW',1),
		(2,now(),'社团信息2','这是一个社团信息测试页面',0,0,'广州大学','HOT',1),
		(4,now(),'社团信息3','这是一个社团信息测试页面',0,0,'广州大学','HOT',1),
		(5,now(),'社团信息4','这是一个社团信息测试页面',0,0,'广州大学','NEW',1),
		(3,now(),'社团信息5','这是一个社团信息测试页面',0,0,'广州大学','NEW',1),
		(6,now(),'社团信息6','这是一个社团信息测试页面',0,0,'广州大学','HOT',1)
		on conflict do nothing;
		insert into look(id,createTime,title,content,readCount,zan,school,status,publisherID) values
		(1,now(),'校园看看1','这是一个校园看看测试页面',0,0,'广州大学','NEW',1),
		(2,now(),'校园看看2','这是一个校园看看测试页面',0,0,'广州大学','HOT',1),
		(4,now(),'校园看看3','这是一个校园看看测试页面',0,0,'广州大学','HOT',1),
		(5,now(),'校园看看4','这是一个校园看看测试页面',0,0,'广州大学','NEW',1),
		(3,now(),'校园看看5','这是一个校园看看测试页面',0,0,'广州大学','NEW',1),
		(6,now(),'校园看看6','这是一个校园看看测试页面',0,0,'广州大学','HOT',1)
		on conflict do nothing;
		insert into partTimeJob(id,icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
		(1,'` + DefaultIcon + `','兼职1','10元/小时','广州大学商业中心','','这是一个兼职测试页面',0,0,now(),'` + DefaultSchool + `',1),
		(2,'` + DefaultIcon + `','兼职2','10元/小时','广州大学商业中心','','这是一个兼职测试页面',0,0,now(),'` + DefaultSchool + `',1),
		(3,'` + DefaultIcon + `','兼职3','10元/小时','广州大学商业中心','','这是一个兼职测试页面',0,0,now(),'` + DefaultSchool + `',1)
		on conflict do nothing;
		insert into activity(id,icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
		(1,'` + DefaultIcon + `','活动1','200元/人','','海景别墅2日游，包接送，包吃住。另外还可以参加特别抽奖活动，说不定可以带走一部iPhone 7喔！','这是一个活动测试页面',0,0,now(),'` + DefaultSchool + `',1),
		(2,'` + DefaultIcon + `','活动2','200元/人','','海景别墅2日游，包接送，包吃住。另外还可以参加特别抽奖活动，说不定可以带走一部iPhone 7喔！','这是一个活动测试页面',0,0,now(),'` + DefaultSchool + `',1),
		(3,'` + DefaultIcon + `','活动3','200元/人','','海景别墅2日游，包接送，包吃住。另外还可以参加特别抽奖活动，说不定可以带走一部iPhone 7喔！','这是一个活动测试页面',0,0,now(),'` + DefaultSchool + `',1)
		on conflict do nothing;
		insert into training(id,icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
		(1,'` + DefaultIcon + `','培训1','6000元/人','广州大学商业南区','广州大学城高质量JAVA培训机构可以让你通过3个月的培训，迅速掌握JAVA开发技能，并且找到自己理想的工作！','这是一个培训测试页面',0,0,now(),'` + DefaultSchool + `',1),
		(2,'` + DefaultIcon + `','培训2','6000元/人','广州大学商业南区','广州大学城高质量JAVA培训机构可以让你通过3个月的培训，迅速掌握JAVA开发技能，并且找到自己理想的工作！','这是一个培训测试页面',0,0,now(),'` + DefaultSchool + `',1),
		(3,'` + DefaultIcon + `','培训3','6000元/人','广州大学商业南区','广州大学城高质量JAVA培训机构可以让你通过3个月的培训，迅速掌握JAVA开发技能，并且找到自己理想的工作！','这是一个培训测试页面',0,0,now(),'` + DefaultSchool + `',1)
		on conflict do nothing;
		`)
	if err != nil {
		panic(err.Error())
	}
	rows, err := pgsql.Query(`insert into users(account,nickname,icon,password,coins,college,school,exp,custom,gender) values
	($1,$1,$2,$3,1000,$4,$5,0,'{"qq":"1769622057"}'::jsonb,'女') on conflict(account) 
	do update set coins = 1000 returning id;`, testDataAccount, testIcon, pwd, testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	testUserID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into guidance(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个校园导航测试页面',0,0,$2,$4) returning id`, time.Now(), testSchool, RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testGuidanceID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into summary(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个四年概要测试页面',0,0,$2,$4) returning id;`, time.Now(), testSchool, RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testSummaryID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into club(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个社团信息测试页面',0,0,$2,$4) returning id;`, time.Now(), testSchool, RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testClubID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into look(createTime,title,content,readCount,zan,school,publisherID) values
	($1,$3,'这是一个校园看看测试页面',0,0,$2,$4) returning id;`, time.Now(), testSchool, RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testLookID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into partTimeJob(icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
	($1,$4,'10元/小时','广州大学商业中心','','这是一个兼职测试页面',0,0,$2,$3,$5) returning id;`, testIcon, time.Now(), testSchool, RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testPartTimeJobID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into activity(icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
	($1,$4,'200元/人','','海景别墅2日游，包接送，包吃住。另外还可以参加特别抽奖活动，说不定可以带走一部iPhone 7喔！','这是一个活动测试页面',0,0,$2,$3,$5) returning id;`, testIcon, time.Now(), testSchool, RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testActivityID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into training(icon,title,fee,position,description,content,readCount,zan,createTime,school,publisherID) values
	($1,$4,'6000元/人','广州大学商业南区','广州大学城高质量JAVA培训机构可以让你通过3个月的培训，迅速掌握JAVA开发技能，并且找到自己理想的工作！','这是一个培训测试页面',0,0,$2,$3,$5) returning id;`, testIcon, time.Now(), testSchool, RandString(10), testUserID)
	if err != nil {
		panic(err.Error())
	}
	testTrainingID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into ask(publisherID,reward,cost,rangee,typee,label,content,image,thumbnailImage,createTime,zan,school,title) values
	($1,10,10,'school','急问','学习','这道题怎么做',$2,$2,$3,0,$4,$5) returning id;`, testUserID, "{\"icon/default"+fmt.Sprint(rand.Uint32()%8+1)+".jpg\"}", time.Now(), testSchool, RandString(5))
	if err != nil {
		panic(err.Error())
	}
	testAskID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,$5,0) returning id;`, testUserID, testAskID, testUserID, time.Now(), RandString(10))
	if err != nil {
		panic(err.Error())
	}
	testCommentID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,'我也不知道
	a
	a
	a
	a
	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',0);`, testUserID, testAskID, "1", time.Now())
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,'哦，是吗？',0);`, testUserID, testAskID, "2", time.Now())
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,'系啊',0);`, testUserID, testAskID, "3", time.Now())
	if err != nil {
		panic(err.Error())
	}
	//dbPage()
}

func GetTestID(rows *sql.Rows) (id string, err error) {
	defer rows.Close()
	if !rows.Next() {
		return "", errors.New("no rows")
	}
	err = rows.Scan(&id)
	if err != nil {
		return "", err
	}
	return id, nil
}

func dbPage() {
	file, err := os.Create("./dbPage.html")
	if err != nil {
		file = os.Stdout
	}
	var w io.Writer = file
	w.Write([]byte(`
<!DOCTYPE html>
<html>
<head>
<title>数据库所有数据</title>
</head>
<body>
    `))
	defer w.Write([]byte(`
</body>
</html>
    `))
	db := pgsql.GetDB()

	Sql := []string{
		`select * from schools
        `, `select * from colleges
        `, `select * from users
        `, `select * from token
        `, `select * from guidance
        `, `select * from summary
        `, `select * from club
        `, `select * from look
        `, `select * from partTimeJob
        `, `select * from activity
        `, `select * from training
        `, `select * from ask
        `, `select * from favoriteAsk
        `, `select * from accountBook
        `, `select * from commentAsk
        `, `select * from advertisement
        `, `select * from joinUS
        `, `select * from business
        `, `select * from inform
        `, `select * from advise
        `, `select * from report
        `, `select * from blacklist
        `, `select * from allege
        `, `select * from share
		`, `select * from userInfo
        `,
	}
	for idx := 0; idx < len(Sql); idx++ {
		w.Write([]byte("<h2>" + fmt.Sprint(idx+1) + "." + Sql[idx] + "</h2>"))
		err := showData(w, db, Sql[idx])
		if err != nil {
			panic(err.Error())
		}
	}
}

func showData(w io.Writer, db *sql.DB, Sql string) error {
	Sql += " limit 100"
	rows, err := db.Query(Sql)
	if err != nil {
		w.Write([]byte("<p>" + err.Error() + "</p>"))
		return err
	}
	defer rows.Close()

	w.Write([]byte(`
        <table border="1">
            <tr>
            `))
	defer w.Write([]byte(` 
            </tr>
        </table> `))

	cols, err := rows.Columns()
	if err != nil {
		w.Write([]byte(err.Error()))
		return err
	}
	for i := 0; i < len(cols); i++ {
		w.Write([]byte("<th>" + cols[i] + "</th>"))
	}

	return htmlOutputRows(w, len(cols), rows)
}
func htmlOutputRows(w io.Writer, colNum int, rows *sql.Rows) error {
	str := make([][]byte, colNum)
	for rows.Next() {
		w.Write([]byte(` <tr>`))
		args := make([]interface{}, 0)
		for idx := 0; idx < colNum; idx++ {
			args = append(args, &str[idx])
		}
		err := rows.Scan(args...)
		if err != nil {
			w.Write([]byte(err.Error()))
			return err
		}
		for _, val := range str {
			w.Write([]byte("<td>" + string(val) + "</td>"))
		}
		w.Write([]byte("</tr>"))
	}
	return nil
}
