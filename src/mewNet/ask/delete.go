package ask

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	"utils/dealImage"
	j "utils/judger"
	"utils/pgsql"
)

func deleteAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.DeleteAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealDeleteAsk(reqJson.Sign, reqJson.IDs)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealDeleteAsk(sign string, IDs []string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeIDs(IDs)
	if code != j.CodeSuccess {
		return
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}
	var data []map[string]interface{}
	defer func() {
		if code == j.CodeSuccess {
			go dealImage.DeleteMapArray(config.PublicFilePath, data, "image", "thumbnailImage")
		}
	}()
	defer func() {
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()
	code, msg, debugMsg, data = mewNet.TxQueryMultiSQL(
		mewNet.DealMapDoNotThing, tx,
		`delete from ask where id = $1 and publisherID = $2 
		returning image,thumbnailImage "thumbnailImage"`,
		IDs, userID)
	return
}
