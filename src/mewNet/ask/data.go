package ask

import (
	"config"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func dataAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.DataAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealDataAsk(reqJson.Sign, reqJson.ID, reqJson.Token)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealDataAsk(sign, id, token string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, id)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, userID := mewNet.ExistToken(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		userID = "-1"
	}
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(` 
	select 
	ask.id "id",
	ask.publisherID "publisherID",
	users.nickname "nickname",
	users.icon "icon",
	users.gender "gender",
	ask.reward "reward",
	ask.createTime "createTime",
	ask.zan "zan",
	ask.typee "type",
	ask.label "label",
	ask.content "content",
	ask.image "image",
	ask.commentID "commentID",
	ask.title "title",
	ask.commentCount "commentCount",
	ask.thumbnailImage "thumbnailImage",
	count(zanAsk.userID) "isZan", 
	count(favoriteAsk.userID) "isFavorite"
	from ask 
	inner join users on (ask.publisherID=users.id)
	left outer join zanAsk on (zanAsk.objectID = ask.id and zanAsk.userID = $2)
	left outer join favoriteAsk on (favoriteAsk.objectID = ask.id and favoriteAsk.userID = $2)
	where ask.id = $1
	group by 
	ask.id,
	ask.publisherID,
	users.nickname,
	users.icon,
	users.gender,
	ask.reward,
	ask.createTime,
	ask.zan,
	ask.typee,
	ask.label,
	ask.content,
	ask.image,
	ask.commentID,
	ask.title,
	ask.commentCount,
	ask.thumbnailImage
	`, id, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, mewNet.DealMapImgAndIcon)
	if code == j.CodeNoRows {
		return j.CodeAskDeleted, j.MsgAskDeleted, debugMsg, result
	} else if code != j.CodeSuccess {
		return
	}
	commentID, ok := data["commentID"].(string)
	if !ok {
		return j.CodeTypeAssertFailed, j.MsgTypeAssertFailed, "type assert failed", result
	}
	delete(data, "commentID")
	if commentID != "" {
		rows, err := pgsql.Query(`
		select 
		commentAsk.id "id",
		commentAsk.publisherID "publisherID",
		commentAsk.createTime "createTime",
		users.icon "icon",
		users.nickname "nickname",
		commentAsk.content "content",
		users.gender "gender"
		from commentAsk
		inner join users on (commentAsk.id = $1 and users.id = commentAsk.publisherID)
		where commentAsk.askID = $2;
		`, commentID, id)
		if err != nil {
			return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
		}
		var adviseComment map[string]interface{}
		code, msg, debugMsg, adviseComment = mewNet.RowsToMap(rows, mewNet.DealMapIcon)
		if code == j.CodeSuccess {
			data["adviseComment"] = adviseComment
		} else {
			logs.ErrorPrint("adviseComment not exist:", code, msg, debugMsg, data, "commentID:", commentID)
		}
	}
	data["link"] = config.Host + mewNet.DataLink + "ask"
	return j.CodeSuccess, j.MsgSuccess, "", data
}

func getDataAsk(w http.ResponseWriter, r *http.Request) {
	type dataJSON struct {
		SrcHost        string
		Title          interface{}
		Label          string
		Nickname       interface{}
		Gender         interface{}
		CreateTime     interface{}
		Reward         string
		Content        interface{}
		Icon           interface{}
		ThumbnailImage []string
		Solve          bool
		Urgent         bool
	}
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		js       dataJSON
		err      error
	)
	defer func() {
		if code != j.CodeSuccess {
			js = dataJSON{
				Title:    msg,
				Nickname: msg,
				Content:  debugMsg,
				Icon:     config.SrcHost + mewNet.DefaultIcon,
				SrcHost:  config.SrcHost,
			}
			logs.ResultPrint(time.Now().Format("2006-01-02 15:04:05"), r.RequestURI, r.Method, r.RemoteAddr,
				code, msg, debugMsg, r.Form, r.Header.Get("X-Real-Ip"))
		}
		err = mewNet.GetTmpl().ExecuteTemplate(w, "data-ask", &js)
		if err != nil {
			logs.ErrorPrint(err.Error())
		}
	}()
	err = r.ParseForm()
	if err != nil {
		return
	}
	id := r.FormValue("id")
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`
	select 
	ask.title,
	ask.label,
	users.nickname,
	users.gender,
	users.icon,
	ask.commentID "commentID",
	ask.reward,
	ask.content,
	ask.thumbnailImage "thumbnailImage",
	ask.createTime "createTime",
	typee "type"
	from ask 
	inner join users on (ask.publisherID=users.id)
	where ask.id = $1
	`, id)
	if err != nil {
		code, msg, debugMsg = j.CodeDBQueryError, j.MsgDBQueryError, err.Error()
		return
	}
	code, msg, debugMsg, data := mewNet.RowsToMap(rows, func(mp map[string]interface{}) error {
		err := mewNet.DealMapFormatTime(mp)
		if err != nil {
			return err
		}
		err = mewNet.DealMapImage(mp, "thumbnailImage")
		if err != nil {
			return err
		}
		err = mewNet.DealMapIcon(mp)
		if err != nil {
			return err
		}
		return nil
	})
	if code != j.CodeSuccess {
		return
	}
	var gender = data["gender"]
	thumbnailImage, ok := data["thumbnailImage"].([]string)
	if !ok {
		thumbnailImage = nil
	}
	label, _ := data["label"].(string)
	reward, _ := data["reward"].(string)
	js = dataJSON{
		Title:          data["title"],
		Label:          label,
		Nickname:       data["nickname"],
		Gender:         gender,
		CreateTime:     data["createTime"],
		Reward:         reward,
		Content:        data["content"],
		Icon:           data["icon"],
		Solve:          data["commentID"] != "",
		Urgent:         data["type"] == "急问",
		ThumbnailImage: thumbnailImage,
		SrcHost:        config.SrcHost,
	}
	code, msg, debugMsg = j.CodeSuccess, j.MsgSuccess, ""
}
