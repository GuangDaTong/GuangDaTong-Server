package ask

import (
	"logs"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func deleteCommentAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.DeleteCommentAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealDeleteCommentAsk(reqJson.Sign, reqJson.ID)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealDeleteCommentAsk(sign, id string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`delete from commentAsk where id = $1 and publisherID = $2 
	returning askID`, id, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	defer rows.Close()
	if !rows.Next() {
		return j.CodeNoRows, j.MsgNoRows, "rows.Next() == false", result
	}
	var askID string
	err = rows.Scan(&askID)
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), result
	}
	logs.DebugPrint(askID)
	sqlResult, err := pgsql.Exec(`update ask set commentCount = commentCount - 1 where id = $1`, askID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess { //commentCount出错影响不大，不用事务，不返回给客户端错误
		logs.ErrorPrint(`update ask set commentCount = commentCount - 1 where id = $1`, `askID:`, askID, `userID:`, userID, `commentID:`, id)
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
