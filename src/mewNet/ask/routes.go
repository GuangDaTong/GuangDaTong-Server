package ask

import (
	"mewNet"
	"modal"
)

var Routes = []Route{
	Route{
		"listAsk",
		"POST",
		"/list/ask",
		listAsk,
	},
	Route{
		"listSearchAsk",
		"POST",
		"/list/search/ask",
		listSearchAsk,
	},
	Route{
		"dataAsk",
		"POST",
		"/data/ask",
		dataAsk,
	},
	Route{
		"getDataAsk",
		"GET",
		"/data/ask",
		getDataAsk,
	},
	Route{
		"deleteAsk",
		"POST",
		"/delete/ask",
		deleteAsk,
	},
	Route{
		"listCommentAsk",
		"POST",
		"/list/comment/ask",
		listCommentAsk,
	},
	Route{
		"commentAsk",
		"POST",
		"/comment/ask",
		commentAsk,
	},
	Route{
		"adoptAsk",
		"POST",
		"/adopt/ask",
		adoptAsk,
	},
	Route{
		"favoriteAsk",
		"POST",
		"/favorite/ask",
		favoriteAsk,
	},
	Route{
		"sendAsk",
		"POST",
		"/send/ask",
		sendAsk,
	},
	Route{
		"deleteCommentAsk",
		"POST",
		"/delete/commentAsk",
		deleteCommentAsk,
	},
}

func init() {
	mewNet.AppendRoute(Routes)
}

type Route = modal.Route
