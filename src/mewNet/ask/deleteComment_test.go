package ask

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func DeleteCommentAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	rows, err := pgsql.Query(`insert into commentAsk(askID,publisherID,commentReceiverID,createTime,content,
	zan) values($1,$2,$3,$4,$5,$6) returning id`, 1, testUserID, 2, time.Now(), "askdjflaskjdf阿斯顿开发了记录萨克的积分", 0)
	if err != nil {
		panic(err.Error())
	}
	id, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into commentAsk(askID,publisherID,commentReceiverID,createTime,content,
	zan) values($1,$2,$3,$4,$5,$6) returning id`, 1, testUserID, 2, time.Now(), "alksdjf卡洛斯的积分", 0)
	if err != nil {
		panic(err.Error())
	}
	commentID, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`update ask set commentID = $1 where id = $2`, commentID, 1)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.DeleteCommentAskJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   commentID,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeDBQueryError),
				Msg:  j.MsgDBQueryError,
			},
			Name: "can't delete adopted comment",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeNoRows),
				Msg:  j.MsgNoRows,
			},
			Name: "comment not exist or is not your comment",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   id,
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   "-1",
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
	}
}
func testDeleteCommentAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
