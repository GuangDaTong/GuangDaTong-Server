package ask

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"mewNet"
	"modal"
	"net/http"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

func adoptAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.AdoptAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealAdoptAsk(reqJson.Sign, reqJson.AskID, reqJson.CommentID)
	mewNet.AddReqJsonToResult(result, reqJson)
}

//采纳问问,喵币变化,避免重复采纳 --exp+1
func dealAdoptAsk(sign, askID, commentID string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(askID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(commentID)
	if code != j.CodeSuccess {
		return
	}

	rows, err := pgsql.Query(`select publisherID "publisherID" from commentAsk where id = $1`, commentID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	commentPublisherID := data["publisherID"]
	if commentPublisherID == userID {
		return j.CodeAdoptSelf, j.MsgAdpotSelf, "can't adopt self", result
	}

	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}

	defer func() {
		defer func() {
			if code == j.CodeSuccess {
				jpush.PushAdpotComment(commentPublisherID)
			}
		}()
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()

	code, msg, debugMsg, amount, title := getAskRewardAmount(tx, commentID, askID, userID)
	if code != j.CodeSuccess {
		return
	}

	sqlResult, err := pgsql.TxExec(tx, `update users set coins = coins + $1 where users.id = $2;`,
		amount, commentPublisherID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}

	sqlResult, err = pgsql.TxExec(tx, `insert into accountBook(userID,createTime,amount,description) values
	($1,$2,$3,$4);`, commentPublisherID, time.Now(), amount, "评论问问被采纳")
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}

	detail := map[string]string{"coins": fmt.Sprint(amount), "title": title}
	detailBytes, err := json.Marshal(detail)
	if err != nil {
		return j.CodeMarshalJsonError, j.MsgMarshalJsonError, err.Error(), result
	}
	sqlResult, err = pgsql.TxExec(tx, `insert into askMsg(askID,senderID,receiverID,detail,createTime,typee)
	values($1,$2,$3,$4,$5,$6)`, askID, userID, commentPublisherID, detailBytes, time.Now(), "adoptComment")
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}

func getAskRewardAmount(tx *sql.Tx, commentID, askID, userID string) (code int, msg, debugMsg string, amount int, title string) {
	stmt, err := tx.Prepare(`update ask set commentID = $1 where ask.id = $2 and ask.commentID is null
		and ask.publisherID = $3 and exists (select * from commentAsk where commentAsk.id = $1 and 
		commentAsk.askID = $2) returning ask.reward,ask.cost,ask.title;`)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), 0, ""
	}
	defer stmt.Close()
	rows, err := stmt.Query(commentID, askID, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), 0, ""
	}
	defer rows.Close()

	if !rows.Next() {
		return j.CodeAdoptAskFailed, j.MsgAdoptAskFailed, "query no rows", 0, ""
	}
	var reward, cost int
	err = rows.Scan(&reward, &cost, &title)
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), 0, ""
	}
	amount = reward + cost - cost/mewNet.CoinsRate
	return j.CodeSuccess, j.MsgSuccess, "", amount, title
}
