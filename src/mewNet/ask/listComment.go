package ask

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listCommentAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListCommentAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListCommentAsk(reqJson.Sign, reqJson.ID, reqJson.OrderBy,
		reqJson.Order, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListCommentAsk(sign, id, orderBy, order string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt, id)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select commentAsk.id "id",commentAsk.publisherID "publisherID",
	commentReceiverID "commentReceiverID",commentReceiverUser.Nickname "commentReceiverNickname",
	commentAsk.createTime "createTime",commentAsk.content "content",users.icon "icon",users.gender "gender",
	users.nickname "nickname" from ask inner join commentAsk on (ask.id = $1 and 
	ask.id = commentAsk.askID) inner join users on (commentAsk.publisherID = users.id)
	inner join users commentReceiverUser on (commentAsk.commentReceiverID = commentReceiverUser.id)
	order by "`+orderBy+`" `+order+` limit $2 offset $3;`, id, pageSize, pageIndex*pageSize)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	mewNet.AddDataToResult(result, "data", data)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
