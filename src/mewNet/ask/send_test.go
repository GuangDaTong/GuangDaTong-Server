package ask

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func SendAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	code, msg, debugMsg, token1 := mewNet.SetToken("3")
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err := pgsql.Exec(`insert into forbidSendAsk(userID,createTime) values($1,$2) on conflict(userID)
	do update set createTime = $2`, 3, time.Now().Add(-time.Hour*24).Unix())
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`update users set coins = 1000 where id = $1`, 3)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.MultiPartRequest
	return []TestExample{
		{
			Request: exampleJSON{
				FileName: []string{
					"image1",
					"image2",
				},
				FilePath: []string{
					config.PublicFilePath + "icon/default1.jpg",
					config.PublicFilePath + "image/default2.jpg",
				},
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "普问",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name:     "success",
			NotBench: true,
		},
		{
			Request: exampleJSON{
				FileName: []string{
					"image1",
					"image2",
				},
				FilePath: []string{
					config.PublicFilePath + "image/default3.jpg",
					config.PublicFilePath + "icon/default2.jpg",
				},
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "普问",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeImageTooLarge),
				Msg:  j.MsgImageTooLarge,
			},
			Name:     "image too large",
			NotBench: true,
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "普问",
					"range":   "college",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "normal success",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "急问",
					"range":   "college",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "urgent success",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "急问",
					"range":   "college",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token1),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "急问",
					"range":   "college",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSendAskForbidden),
				Msg:  j.MsgSendAskForbidden,
			},
			Name: "send ask foridden",
		},
		{
			Request: exampleJSON{
				FileName: []string{"image1", "image2"},
				FilePath: []string{"../README.md"},
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "普问",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSaveImageError),
				Msg:  j.MsgSaveImageError,
			},
			Name:     "not image type,save image error",
			NotBench: true,
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"content": mewNet.RandString(10),
					"title":   mewNet.RandString(5),
					"reward":  "0",
					"type":    "普问",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "失物招领",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAskTypeError),
				Msg:  j.MsgAskTypeError,
			},
			Name: "ask type error",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "急问",
					"range":   "school",
					"label":   "...",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAskLabelError),
				Msg:  j.MsgAskLabelError,
			},
			Name: "ask label error",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "0",
					"type":    "急问",
					"range":   "one school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRangeError),
				Msg:  j.MsgRangeError,
			},
			Name: "ask range error",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   mewNet.RandString(5),
					"content": mewNet.RandString(10),
					"reward":  "1000000000",
					"type":    "急问",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeCoinsNotEnough),
				Msg:  j.MsgCoinsNotEnough,
			},
			Name: "coins not enough",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"content": mewNet.RandString(10),
					"reward":  "10",
					"type":    "急问",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalTitle),
				Msg:  j.MsgIllegalTitle,
			},
			Name: "empty title",
		},
		{
			Request: exampleJSON{
				Form: map[string]string{
					"sign":    j.Md5Encode(config.Salt, token),
					"title":   "阿里看大家弗兰克将阿三来看待交费拉萨看到阿卡登陆时",
					"content": mewNet.RandString(10),
					"reward":  "10",
					"type":    "急问",
					"range":   "school",
					"label":   "学习",
				},
			},
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIllegalTitle),
				Msg:  j.MsgIllegalTitle,
			},
			Name: "title too long",
		},
	}
}
func testSendAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		modal.RunTestMultiPartRequest(val, testJson, w)
	}
}
