package ask

import (
	"config"
	"fmt"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listSearchAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListSearchAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListSearchAsk(reqJson.Sign, reqJson.Token, reqJson.OrderBy, reqJson.Order,
		reqJson.Range, reqJson.Key, reqJson.Type, reqJson.Label, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListSearchAsk(sign, token, orderBy, order, rangee, key, typee, label string,
	pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, user := mewNet.GetUser(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		if token != "" {
			return
		}
		user = &modal.User{
			ID:     "-1",
			School: mewNet.DefaultSchool,
		}
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAskLabel(label)
	if code != j.CodeSuccess && label != "" {
		return
	}
	code, msg, debugMsg = j.JudgeAskType(typee)
	if code != j.CodeSuccess && typee != "" {
		return
	}
	args := []interface{}{pageSize, pageIndex * pageSize, user.ID}
	SQL := `select ask.id "id",ask.createTime "createTime",ask.publisherID "publisherID",users.nickname,
	users.icon "icon",users.gender "gender",ask.reward "reward",ask.zan "zan",ask.commentID "commentID",
	ask.typee "type",ask.label "label",ask.content "content",ask.image "image",thumbnailImage "thumbnailImage",
	ask.title "title",ask.commentCount "commentCount",count(zanAsk.userID) "isZan",count(favoriteAsk.userID) "isFavorite"
	from ask inner join users on (ask.publisherID=users.id)
	left outer join zanAsk on (zanAsk.objectID = ask.id and zanAsk.userID = $3)
	left outer join favoriteAsk on (favoriteAsk.objectID = ask.id and favoriteAsk.userID = $3)
	`
	condition := []string{}
	//type
	num := len(args)
	if typee != "" {
		condition = append(condition, fmt.Sprintf(" ask.typee = $%d ", num+1))
		args = append(args, typee)
	}
	//label
	num = len(args)
	if label != "" {
		condition = append(condition, fmt.Sprintf(" ask.label = $%d ", num+1))
		args = append(args, label)
	}
	//range
	num = len(args)
	if rangee == "school" {
		condition = append(condition, fmt.Sprintf(` ask.rangee = 'school' and ask.school = $%d `, num+1))
		args = append(args, user.School)
	} else if rangee == "college" {
		if user.College == "" {
			return j.CodeEmptyCollege, j.MsgEmptyCollege, "empty college,college:", result
		}
		condition = append(condition, fmt.Sprintf(` ask.rangee = 'college' and ask.school = $%d and ask.college = $%d `, num+1, num+2))
		args = append(args, user.School)
		args = append(args, user.College)
	} else if rangee == "all college" {
		if user.College == "" {
			return j.CodeEmptyCollege, j.MsgEmptyCollege, "empty college,college:", result
		}
		condition = append(condition, fmt.Sprintf(` ask.rangee = 'all college' and ask.college = $%d `, num+1))
		args = append(args, user.College)
	} else if rangee == "all school" {
		condition = append(condition, fmt.Sprintf(` ask.rangee = 'all school' `))
	} else {
		return j.CodeRangeError, j.MsgRangeError, "ask range error,range:" + rangee, result
	}
	//key
	num = len(args)
	if key != "" {
		condition = append(condition, fmt.Sprintf(` ask.title like $%d `, num+1))
		args = append(args, "%"+key+"%")
	}

	num = len(condition)
	for i := 0; i < num; i++ {
		if i == 0 {
			SQL += " where "
		} else {
			SQL += "and"
		}
		SQL += condition[i]
	}

	SQL += ` group by ask.id,users.nickname,users.icon,users.gender
	order by "` + orderBy + `" ` + order + `
	limit $1 offset $2;`
	logs.DebugPrint(SQL)
	rows, err := pgsql.Query(SQL, args...)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapImgAndIcon)
	if code != j.CodeSuccess {
		return
	}
	for _, val := range data {
		val["link"] = config.Host + mewNet.DataLink + "ask"
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageIndex", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
