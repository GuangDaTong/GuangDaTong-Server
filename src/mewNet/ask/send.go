package ask

import (
	"config"
	"encoding/json"
	"fmt"
	"logs"
	"mewNet"
	"mime/multipart"
	"net/http"
	"time"
	"utils/dealImage"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

var (
	imagePath                = "image/ask/"
	thumbnailImagePath       = "thumbnailImage/ask/"
	nomalSchoolDescription   = "发学校范围的普问"
	nomalCollegeDescription  = "发学院范围的普问"
	urgentSchoolDescription  = "发学校范围的急问"
	urgentCollegeDescription = "发学院范围的急问"
)

func sendAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
		err      error
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	/*
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			panic(err.Error())
		}
		panic(string(body))
	*/
	err = r.ParseMultipartForm(20 << 20) //max size 20M
	if err != nil {
		code, msg, debugMsg = j.CodeParseFormError, j.MsgParseFormError, err.Error()
		return
	}
	sign := r.FormValue("sign")
	content := r.FormValue("content")
	reward := r.FormValue("reward")
	typee := r.FormValue("type")
	rangee := r.FormValue("range")
	label := r.FormValue("label")
	title := r.FormValue("title")
	size := r.FormValue("size")
	if size == "" {
		size = "{}"
	}
	var files []multipart.File
	var fileNames, imgType []string
	var bounds []dealImage.Bounds
	var sizeMap map[string]dealImage.Bounds
	err = json.Unmarshal([]byte(size), &sizeMap)
	if err != nil {
		code, msg, debugMsg = j.CodeUnmarshalJsonError, j.MsgUnmarshalJsonError, err.Error()
		return
	}
	for i := 1; i <= 9; i++ {
		name := fmt.Sprintf("image%d", i)
		file, header, err := r.FormFile(name)
		if err != nil {
			logs.DebugPrint(err.Error())
			break
		}
		var subfix string
		code, msg, debugMsg, subfix = dealImage.JudgeImage(header)
		if code != j.CodeSuccess {
			return
		}
		bounds = append(bounds, sizeMap[name])
		imgType = append(imgType, subfix)
		fileNames = append(fileNames, name)
		files = append(files, file)
	}
	code, msg, debugMsg, result = dealSendAsk(sign, title, content, reward, typee, rangee, label, bounds,
		files, fileNames, imgType)
	mewNet.AddReqJsonToResult(result, r.Form)
}
func dealSendAsk(sign, title, content, Reward, typee, rangee, label string, bounds []dealImage.Bounds,
	files []multipart.File, fileNames, imgType []string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, user := mewNet.GetUser(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAskType(typee)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeAskLabel(label)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeTitle(title)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(content)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, reward := j.JudgeStringToPositiveInt(Reward)
	if code != j.CodeSuccess {
		return j.CodeIllegalReward, j.MsgIllegalReward, debugMsg, result
	}

	var description string
	cost := 0
	if typee == "急问" && rangee == "college" {
		cost = mewNet.UrgentCollege
		description = urgentCollegeDescription
	} else if typee == "急问" && rangee == "school" {
		cost = mewNet.UrgentSchool
		description = urgentSchoolDescription
	} else if typee == "普问" && rangee == "college" {
		cost = mewNet.NormalCollege
		description = nomalCollegeDescription
	} else if typee == "普问" && rangee == "school" {
		cost = mewNet.NormalSchool
		description = nomalSchoolDescription
	} else {
		return j.CodeRangeError, j.MsgRangeError, "error ask range:" + rangee, result
	}
	if user.Coins < cost+reward {
		return j.CodeCoinsNotEnough, j.MsgCoinsNotEnough, fmt.Sprintf("cost:%d,coins:%d", cost+reward, user.Coins), result
	}

	code, msg, debugMsg, createTime := mewNet.GetIntCreateTime(`select createTime from forbidSendAsk 
	where userID = $1`, user.ID)
	if code != j.CodeSuccess {
		return
	}
	if createTime+mewNet.ForbidSendAskDuration > time.Now().Unix() {
		return j.CodeSendAskForbidden, j.MsgSendAskForbidden, "", result
	}

	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}

	var data map[string]string
	var imagePaths []string
	var thumbnailImagePaths []string

	defer func() {
		defer func() {
			if code == j.CodeSuccess && typee == "急问" {
				jpush.PushUrgent(data["id"], rangee, title, user.School, user.College)
			}
		}()
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		dealImage.DeleteArray(config.PublicFilePath, append(imagePaths, thumbnailImagePaths...))
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()

	for i, _ := range imgType {
		//时间会有空格,导致posgresql数组的元素被引号括着，所以将时间转换为整数字符串
		name := user.ID + "_" + fileNames[i] + "_" + mewNet.FormatNowToInt()
		imgPath := imagePath + name + imgType[i]
		thumbPath := thumbnailImagePath + name + imgType[i]
		imagePaths = append(imagePaths, imgPath)
		thumbnailImagePaths = append(thumbnailImagePaths, thumbPath)
		code, msg, debugMsg = dealImage.SaveImage(imgPath, thumbPath, files[i], false, 100)
		if code != j.CodeSuccess {
			return
		}
	}
	image := mewNet.StringArrayToString(imagePaths)
	thumbnailImage := mewNet.StringArrayToString(thumbnailImagePaths)

	code, msg, debugMsg, data = mewNet.TxQueryMapString(tx, `
	insert into ask(publisherID,reward,cost,rangee,typee,label,content,image,thumbnailImage,createTime,school,college,commentID,zan,title)
	values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12, null,0,$13) returning id;`,
		user.ID, reward, cost, rangee, typee, label, content, image,
		thumbnailImage, time.Now(), user.School, user.College, title)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err := pgsql.TxExec(tx, `update users set coins = coins - $1 where id = $2;`, cost+reward, user.ID)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	sqlResult, err = pgsql.TxExec(tx, `insert into accountBook(userID,createTime,amount,description) values
	($1,$2,$3,$4);`, user.ID, time.Now(), -cost-reward, description)
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
