package ask

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListAsk(reqJson.Sign, reqJson.Token,
		reqJson.Range, reqJson.OrderBy, reqJson.Order, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListAsk(sign, token, rangee, orderBy, order string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg = j.JudgeSign(sign, config.Salt)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, user := mewNet.GetUser(j.Md5Encode(config.Salt, token))
	if code != j.CodeSuccess {
		if token != "" {
			return
		}
		user = &modal.User{
			ID:     "-1",
			School: mewNet.DefaultSchool,
		}
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	args := []interface{}{pageSize, pageIndex * pageSize, user.ID}
	SQL := `
	select 
	ask.id "id",
	ask.createTime "createTime",
	ask.publisherID "publisherID",
	users.nickname,
	users.icon "icon",
	users.gender "gender",
	ask.reward "reward",
	ask.zan "zan",
	ask.commentID "commentID",
	ask.typee "type",
	ask.label "label",
	ask.content "content",
	ask.image "image",
	thumbnailImage "thumbnailImage",
	ask.title "title",
	ask.commentCount "commentCount",
	count(zanAsk.userID) "isZan",
	count(favoriteAsk.userID) "isFavorite"
	from ask inner join users on (ask.publisherID=users.id)
	left outer join zanAsk on (zanAsk.userID = $3 and zanAsk.objectID = ask.id)
	left outer join favoriteAsk on (favoriteAsk.userID = $3 and favoriteAsk.objectID = ask.id)
	`
	if rangee == "school" {
		num := len(args)
		SQL += fmt.Sprintf(` where ask.rangee = $%d and ask.school = $%d `, num+1, num+2)
		args = append(args, rangee)
		args = append(args, user.School)
	} else if rangee == "college" {
		num := len(args)
		SQL += fmt.Sprintf(` where ask.rangee = $%d and ask.school = $%d and ask.college = $%d `, num+1, num+2, num+3)
		args = append(args, rangee)
		args = append(args, user.School)
		if user.College == "" {
			return j.CodeEmptyCollege, j.MsgEmptyCollege, "empty college,college:", result
		}
		args = append(args, user.College)
	} else if rangee == "all college" {
		num := len(args)
		SQL += fmt.Sprintf(` where ask.college = $%d `, num+1)
		if user.College == "" {
			return j.CodeEmptyCollege, j.MsgEmptyCollege, "empty college,college:", result
		}
		args = append(args, user.College)
	} else if rangee == "all school" {
		SQL += ` and ask.rangee = 'all school' `
	} else {
		return j.CodeRangeError, j.MsgRangeError, "ask range error,range:" + rangee, result
	}
	SQL += ` group by ask.id,users.nickname,users.icon,users.gender
	order by "` + orderBy + `" ` + order + `
	limit $1 offset $2;`
	//logs.DebugPrint(SQL)
	rows, err := pgsql.Query(SQL, args...)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapImgAndIcon)
	if code != j.CodeSuccess {
		return
	}
	for _, val := range data {
		val["link"] = config.Host + mewNet.DataLink + "ask"
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageIndex", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
