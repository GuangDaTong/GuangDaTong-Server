package ask

import (
	"config"
	"fmt"
	"modal"
	"os"
	j "utils/judger"
)

func ListCommentAskExample() []TestExample {
	type exampleJSON = modal.ListCommentAskJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ID:        "1",
				Sign:      j.Md5Encode(config.Salt, "1"),
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:        "1",
				Sign:      j.Md5Encode(config.Salt, "2"),
				PageIndex: 0,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:        "1a",
				Sign:      j.Md5Encode(config.Salt, "1a"),
				PageIndex: 0,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:        "1",
				Sign:      j.Md5Encode(config.Salt, "1"),
				PageIndex: -1,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:        "1",
				Sign:      j.Md5Encode(config.Salt, "1"),
				PageIndex: 1,
				PageSize:  301,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageIndex error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:        "1",
				Sign:      j.Md5Encode(config.Salt, "1"),
				PageIndex: 0,
				PageSize:  300,
				OrderBy:   "createTime;drop table users;--",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderByError),
				Msg:  j.MsgOrderByError,
			},
			Name: "orderBy error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:        "1",
				Sign:      j.Md5Encode(config.Salt, "1"),
				PageIndex: 0,
				PageSize:  300,
				Order:     "desc;drop table users;--",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderError),
				Msg:  j.MsgOrderError,
			},
			Name: "order error",
		},
	}
}
func testListCommentAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
