package ask

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
)

func ListSearchAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.ListSearchAskJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "school",
				Key:       "怎么",
				Token:     token,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "range school key success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "school",
				Type:      "急问",
				Token:     token,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "range school type success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "school",
				Label:     "学习",
				Token:     token,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "range school label success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "school",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "range school success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "college",
				Token:     token,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "range college success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  10,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "all school",
				Token:     token,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "range all school success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "all college",
				Token:     token,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "range all college success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "college",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptyCollege),
				Msg:  j.MsgEmptyCollege,
			},
			Name: "range college empty college",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "all college",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptyCollege),
				Msg:  j.MsgEmptyCollege,
			},
			Name: "range all college empty college",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "college",
				Token:     "alskdjflkasdjf",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "range college token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "all college",
				Token:     "laskdjflkasdjflkj",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "range all college token not exist",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "college",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: -1,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "college",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  301,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "college",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageSize error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  300,
				OrderBy:   "time",
				Order:     "desc",
				Range:     "college",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderByError),
				Msg:  j.MsgOrderByError,
			},
			Name: "order by error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  300,
				OrderBy:   "createTime",
				Order:     "esc",
				Range:     "college",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderError),
				Msg:  j.MsgOrderError,
			},
			Name: "order error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Range:     "one school",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRangeError),
				Msg:  j.MsgRangeError,
			},
			Name: "range school error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Type:      "lakdjsf",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAskTypeError),
				Msg:  j.MsgAskTypeError,
			},
			Name: "ask type error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Label:     "lakdjsf",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAskLabelError),
				Msg:  j.MsgAskLabelError,
			},
			Name: "ask label error",
		},
	}
}
func testListSearchAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
