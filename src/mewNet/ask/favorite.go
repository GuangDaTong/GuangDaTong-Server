package ask

import (
	"encoding/json"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func favoriteAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.FavoriteJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealFavoriteAsk(reqJson.Sign, reqJson.ID)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealFavoriteAsk(sign, id string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select createTime "createTime",content,typee,publisherID "publisherID",
	title,reward,label from ask where id = $1`, id)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapString(rows)
	if code != j.CodeSuccess {
		return
	}
	logs.DebugPrint(data)
	sqlResult, err := pgsql.Exec(`insert into favoriteAsk(userID,createTime,objectID,publisherID,objectCreateTime,
	content,typee,title,reward,label) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) 
	on conflict(userID,objectID) do nothing;`, userID, time.Now(), id, data["publisherID"],
		data["createTime"], data["content"], data["typee"], data["title"], data["reward"], data["label"])
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code == j.CodeDBExecNotEffect {
		return j.CodeHaveBeenFavorite, j.MsgHaveBeenFavorite, debugMsg, result
	} else if code != j.CodeSuccess {
		return
	}
	go dealFavoriteAskMsg(data["title"], id, userID, data["publisherID"])
	return j.CodeSuccess, j.MsgSuccess, "", result
}

func dealFavoriteAskMsg(title, askID, senderID, receiverID string) {
	//在黑名单收藏成功但对方消息中心没有消息
	code, msg, debugMsg := j.CheckBlacklist(receiverID, senderID)
	if code == j.CodeInBlacklist {
		return
	}
	if code != j.CodeSuccess {
		logs.ErrorPrint(code, msg, debugMsg)
		return
	}
	detail := map[string]string{"title": title}
	detailBytes, err := json.Marshal(detail)
	if err != nil {
		logs.ErrorPrint("marshal json error:", err.Error())
		return
	}
	sqlResult, err := pgsql.Exec(`insert into askMsg(askID,senderID,receiverID,detail,createTime,typee)
	values($1,$2,$3,$4,$5,$6)`, askID, senderID, receiverID, detailBytes, time.Now(), "favoriteAsk")
	if err != nil {
		logs.ErrorPrint("db exec error:", err.Error())
		return
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		logs.ErrorPrint("exec insert not effect:", "code:", code, "msg:", msg, "debugMsg:", debugMsg)
		return
	}
}
