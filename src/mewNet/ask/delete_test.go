package ask

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func DeleteAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	rows, err := pgsql.Query(`insert into ask(publisherID,reward,cost,rangee,typee,label,content,image,thumbnailImage,createTime,zan) values
	($1,10,10,'school','急问','学习','这道题怎么做',$2,$2,$3,0) returning id;`, testUserID, "{\"image/default9.jpg\"}", time.Now())
	if err != nil {
		panic(err.Error())
	}
	askID1, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into ask(publisherID,reward,cost,rangee,typee,label,content,image,thumbnailImage,createTime,zan) values
	($1,10,10,'school','急问','学习','这道题怎么做',$2,$2,$3,0) returning id;`, testUserID, "{\"image/default10.jpg\"}", time.Now())
	if err != nil {
		panic(err.Error())
	}
	askID2, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.DeleteAskJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{askID2, askID1},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2"},
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{askID1, askID2},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeNoRows),
				Msg:  j.MsgNoRows,
			},
			Name: "db query no rows",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2", "a"},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "empty IDs",
		},
	}
}
func testDeleteAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
