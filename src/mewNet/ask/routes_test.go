package ask

import (
	"config"
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "sendAsk",
		Method:     "POST",
		Pattern:    "/send/ask",
		SetExample: SendAskExample,
		Test:       testSendAsk,
	},
	{
		Name:       "listAsk",
		Method:     "POST",
		Pattern:    "/list/ask",
		SetExample: ListAskExample,
		Test:       testListAsk,
	},
	{
		Name:       "listSearchAsk",
		Method:     "POST",
		Pattern:    "/list/search/ask",
		SetExample: ListSearchAskExample,
		Test:       testListSearchAsk,
	},
	{
		Name:       "dataAsk",
		Method:     "POST",
		Pattern:    "/data/ask",
		SetExample: DataAskExample,
		Test:       testDataAsk,
	},
	{
		Name:       "getDataAsk",
		Method:     "GET",
		Pattern:    "/data/ask",
		SetExample: GetDataAskExample,
		Test:       testGetDataAsk,
	},
	{
		Name:       "adoptAsk",
		Method:     "POST",
		Pattern:    "/adopt/ask",
		SetExample: AdoptAskExample,
		Test:       testAdoptAsk,
	},
	{
		Name:       "deleteAsk",
		Method:     "POST",
		Pattern:    "/delete/ask",
		SetExample: DeleteAskExample,
		Test:       testDeleteAsk,
	},
	{
		Name:       "listCommentAsk",
		Method:     "POST",
		Pattern:    "/list/comment/ask",
		SetExample: ListCommentAskExample,
		Test:       testListCommentAsk,
	},
	{
		Name:       "favoriteAsk",
		Method:     "POST",
		Pattern:    "/favorite/ask",
		SetExample: FavoriteAskExample,
		Test:       testFavoriteAsk,
	},
	{
		Name:       "commentAsk",
		Method:     "POST",
		Pattern:    "/comment/ask",
		SetExample: CommentAskExample,
		Test:       testCommentAsk,
	},
	{
		Name:       "deleteCommentAsk",
		Method:     "POST",
		Pattern:    "/delete/commentAsk",
		SetExample: DeleteCommentAskExample,
		Test:       testDeleteCommentAsk,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	config.UseQiNiuYun = false
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

func ToBytes(v interface{}) []byte {
	bytes, err := json.Marshal(v)
	if err != nil {
		panic(err.Error())
	}
	return bytes
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample
type RespJSON = modal.RespJSON
