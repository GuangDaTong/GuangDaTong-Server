package ask

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func FavoriteAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	rows, err := pgsql.Query(`insert into ask(publisherID,reward,cost,rangee,typee,label,content,image,createTime,zan) values
	($1,10,10,'school','急问','学习','这道题怎么做',$2,$3,0) returning id;`, testUserID, "{\""+testIcon+"\"}", time.Now())
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	testAskID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.FavoriteJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ID:   testAskID,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "ask success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   testAskID,
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   "12alk",
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "objectID error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   testAskID,
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeHaveBeenFavorite),
				Msg:  j.MsgHaveBeenFavorite,
			},
			Name: "already favorite",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   "1000000000000000000",
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeNoRows),
				Msg:  j.MsgNoRows,
			},
			Name: "ask not exist",
		},
	}
}
func testFavoriteAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
