package ask

import (
	"config"
	"fmt"
	"math/rand"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func AdoptAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	rows, err := pgsql.Query(`insert into ask(publisherID,reward,cost,rangee,typee,label,content,image,thumbnailImage,createTime,zan) values
	($1,10,10,'school','急问','学习','这道题怎么做',$2,$2,$3,0) returning id;`, testUserID, "{\"image/default"+fmt.Sprint(rand.Uint32()%8+1)+".jpg\"}", time.Now())
	if err != nil {
		panic(err.Error())
	}
	askID, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,'我也不知道',0) returning id;`, testUserID, askID, "1", time.Now())
	if err != nil {
		panic(err.Error())
	}
	commentID, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,'你再看看',0) returning id;`, testUserID, askID, testUserID, time.Now())
	if err != nil {
		panic(err.Error())
	}
	selfCommentID, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.AdoptAskJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				AskID:     askID,
				CommentID: commentID,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				AskID:     askID,
				CommentID: selfCommentID,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAdoptSelf),
				Msg:  j.MsgAdpotSelf,
			},
			Name: "adopt self",
		},
		{
			Request: ToBytes(&exampleJSON{
				AskID:     askID,
				CommentID: "1000000000000000000",
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeNoRows),
				Msg:  j.MsgNoRows,
			},
			Name: "commentID not exist",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				AskID:     askID,
				CommentID: commentID,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				AskID:     "1a",
				CommentID: commentID,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "askID error",
		},
		{
			Request: ToBytes(&exampleJSON{
				AskID:     "1",
				CommentID: "9a",
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "commentID error",
		},
		{
			Request: ToBytes(&exampleJSON{
				AskID:     askID,
				CommentID: commentID,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAdoptAskFailed),
				Msg:  j.MsgAdoptAskFailed,
			},
			Name: "adopt ask failed,already adopted",
		},
	}
}
func testAdoptAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
