package ask

import (
	"database/sql"
	"errors"
	"fmt"
	"math/rand"
	"mewNet"
	"testing"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

var (
	testAccount       = "00000000000"
	testAccount1      = "12345678901"
	testPassword      = "123456"
	testSchool        = "广州大学"
	testCollege       = "计算机科学与教育软件学院"
	testDataAccount   = "00000000001"
	testIcon          = "icon/default.ico"
	testUserID        string
	testGuidanceID    string
	testSummaryID     string
	testClubID        string
	testLookID        string
	testPartTimeJobID string
	testActivityID    string
	testTrainingID    string
	testCommentID     string
	testAskID         string
)

func TestDB(*testing.T) {
	InitSQL()
}
func InitSQL() {
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	var err error
	pwd := j.EncryptPassword(testPassword)
	rows, err := pgsql.Query(`insert into users(account,nickname,icon,password,coins,college,school,exp,custom,gender) values
	($1,$1,$2,$3,1000000,$4,$5,0,'{"qq":"1769622057"}'::jsonb,'女') on conflict(account) do update set coins = 1000000 returning id;`,
		testDataAccount, testIcon, pwd, testCollege, testSchool)
	if err != nil {
		panic(err.Error())
	}
	testUserID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into ask(publisherID,reward,cost,rangee,typee,label,content,image,thumbnailImage,createTime,zan,title) values
	($1,10,10,'school','急问','学习',$4,$2,$2,$3,0,$5) returning id;`, testUserID, "{\"image/default"+fmt.Sprint(rand.Uint32()%8+1)+".jpg\"}",
		time.Now(), mewNet.RandString(10), mewNet.RandString(5))
	if err != nil {
		panic(err.Error())
	}
	testAskID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	rows, err = pgsql.Query(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,$5,0) returning id;`, testUserID, testAskID, "1", time.Now().Add(-time.Hour*24), mewNet.RandString(12))
	if err != nil {
		panic(err.Error())
	}
	testCommentID, err = GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
}

func GetTestID(rows *sql.Rows) (id string, err error) {
	defer rows.Close()
	if !rows.Next() {
		return "", errors.New("no rows")
	}
	err = rows.Scan(&id)
	if err != nil {
		return "", err
	}
	return id, nil
}
