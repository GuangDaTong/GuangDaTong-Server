package ask

import (
	"encoding/json"
	"fmt"
	"logs"
	"mewNet"
	"modal"
	"net/http"
	"strings"
	"sync"
	"time"
	"utils/jpush"
	j "utils/judger"
	"utils/pgsql"
)

var lastComment = map[string]string{}
var lastCommentLock sync.Mutex

func getCommentCreateTime(userID string) (code int, msg, debugMsg string, createTime string) {
	lastCommentLock.Lock()
	defer lastCommentLock.Unlock()
	createTime = lastComment[userID]
	if createTime == "" {
		rows, err := pgsql.Query(`select createTime "createTime" from commentAsk where publisherID = $1
		order by createTime desc limit 1 offset 0`, userID)
		if err != nil {
			return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), ""
		}
		defer rows.Close()
		if rows.Next() {
			err := rows.Scan(&createTime)
			if err != nil {
				return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), ""
			}
		}
		logs.DebugPrint(createTime)
	}
	return j.CodeSuccess, j.MsgSuccess, "", createTime
}

func rewardFirstComment(userID, createTime string, now *time.Time) {
	today := fmt.Sprint(now)[:10]
	if createTime == "" || strings.Compare(createTime[:10], today) != 0 {
		code, msg, debugMsg := mewNet.RewardCoins(5, "每日回复问问", userID)
		if code != j.CodeSuccess {
			logs.ErrorPrint(code, msg, debugMsg)
		}
	}
	lastCommentLock.Lock()
	defer lastCommentLock.Unlock()
	lastComment[userID] = today
}

func commentAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.CommentAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealCommentAsk(reqJson.Sign, reqJson.ID, reqJson.Content, reqJson.CommentReceiverID)
	mewNet.AddReqJsonToResult(result, reqJson)
}

func dealCommentAsk(sign, id, content, commentReceiverID string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(id)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeID(commentReceiverID)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeContent(content)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.CheckBlacklist(commentReceiverID, userID)
	if code != j.CodeSuccess {
		return
	}
	//check forbidLeaveWords
	code, msg, debugMsg, createTime := mewNet.GetIntCreateTime(`select createTime from forbidCommentAsk
	where userID = $1`, userID)
	if code != j.CodeSuccess {
		return
	}
	if createTime+mewNet.ForbidCommentAskDuration > time.Now().Unix() {
		return j.CodeCommentAskForbidden, j.MsgCommentAskForbidden, "", result
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}

	var now = time.Now()
	code, msg, debugMsg, CreateTime := getCommentCreateTime(userID)
	if code != j.CodeSuccess {
		return
	}

	var pushFunction func(string)
	var askPublisherID string

	defer func() {
		defer func() {
			if code == j.CodeSuccess && !(askPublisherID == userID && userID == commentReceiverID) {
				pushFunction(commentReceiverID)
			}
			rewardFirstComment(userID, CreateTime, &now)
		}()
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()
	sqlResult, err := pgsql.TxExec(tx, `insert into commentAsk(askID,publisherID,commentReceiverID,
	createTime,content,zan) values($1,$2,$3,$4,$5,0);`, id, userID, commentReceiverID, now,
		content)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, data := mewNet.TxQueryMapString(tx, `update ask set commentCount = commentCount + 1 where id = $1 
	returning publisherID "askPublisherID",title "title"`, id)
	if code != j.CodeSuccess {
		return
	}
	askPublisherID = data["askPublisherID"]
	var typee string
	if askPublisherID == userID && userID == commentReceiverID { //评论自己的问问不用出现在消息中心和推送
		return j.CodeSuccess, j.MsgSuccess, "", result
	} else if askPublisherID == commentReceiverID {
		typee = "commentAsk"
		pushFunction = jpush.PushCommentAsk
	} else {
		typee = "commentUser"
		pushFunction = jpush.PushCommentUser
	}
	if len([]rune(content)) > 40 {
		content = content[:40] + "..."
	}
	detail := map[string]string{"title": data["title"], "comment": content}
	detailBytes, err := json.Marshal(detail)
	if err != nil {
		return j.CodeMarshalJsonError, j.MsgMarshalJsonError, err.Error(), result
	}
	sqlResult, err = pgsql.TxExec(tx, `insert into askMsg(askID,senderID,receiverID,detail,createTime,
		typee) values($1,$2,$3,$4,$5,$6)`, id, userID, commentReceiverID, detailBytes, now, typee)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
