package ask

import (
	"config"
	"fmt"
	"logs"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func DataAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.DataAskJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				ID:    testAskID,
				Token: token,
				Sign:  j.Md5Encode(config.Salt, testAskID),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:    testAskID,
				Token: token,
				Sign:  j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSignWrong),
				Msg:  j.MsgSignWrong,
			},
			Name: "sign wrong",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   "1001a",
				Sign: j.Md5Encode(config.Salt, "1001a"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:   "1001",
				Sign: j.Md5Encode(config.Salt, "1001"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeAskDeleted),
				Msg:  j.MsgAskDeleted,
			},
			Name: "no rows",
		},
		{
			Request: ToBytes(&exampleJSON{
				ID:    "1",
				Token: token,
				Sign:  j.Md5Encode(config.Salt, "1"),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
	}
}
func testDataAsk(testJson *TestJSON) {
	rows, err := pgsql.Query(`insert into commentAsk (commentReceiverID,askID,publisherID,createTime,content,zan) values
	($1,$2,$3,$4,$5,0) returning id;`, testUserID, testAskID, "1", time.Now(), mewNet.RandString(12))
	if err != nil {
		panic(err.Error())
	}
	commentID, err := GetTestID(rows)
	if err != nil {
		panic(err.Error())
	}
	logs.DebugPrint(commentID)
	_, err = pgsql.Exec(`update ask set commentID = $1 where id = $2 `, commentID, 1)
	if err != nil {
		panic(err.Error())
	}
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
	_, err = pgsql.Exec(`update ask set commentID = $1 where id = $2 `, testCommentID, testAskID)
	if err != nil {
		panic(err.Error())
	}
	testExamples(TestExample{
		Request: ToBytes(&modal.DataAskJSON{
			ID:   testAskID,
			Sign: j.Md5Encode(config.Salt, testAskID),
		}),
		Response: &RespJSON{
			Code: fmt.Sprint(j.CodeSuccess),
			Msg:  j.MsgSuccess,
		},
		Name: "success",
	}, testJson, w)
}

func GetDataAskExample() []TestExample {
	return []TestExample{
		{
			Request: testAskID,
			Response: &RespJSON{
				Code: "200",
			},
			Name: "success",
		},
		{
			Request: "aladlsfj",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "id error",
		},
		{
			Request: "12345325",
			Response: &RespJSON{
				Code: "200",
			},
			Name: "no rows",
		},
	}
}

func testGetDataAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testHtmlExamples(val, testJson, w)
	}
}
