package mewNet

import (
	"database/sql"
	"logs"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func SqlAffectOne(result sql.Result) (code int, msg, debugMsg string) {
	affect, err := result.RowsAffected()
	if err != nil {
		logs.ErrorPrint("get rows affected error:", err.Error())
		return j.CodeDBExecNotEffect, j.MsgDBExecNotEffect, err.Error()
	}
	if affect <= 0 {
		return j.CodeDBExecNotEffect, j.MsgDBExecNotEffect, "db exec affected no rows"
	}
	if affect > 1 {
		return j.CodeDBExecEffectMulti, j.MsgDBExecEffectMulti, "db exec affected multi rows"
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}

//事务执行多条类似相同SQL，$1为member的一个元素
func TxExecMultiSQL(tx *sql.Tx, SQL string, member []string, args ...interface{}) (code int, msg, debugMsg string) {
	stmt, err := tx.Prepare(SQL)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error()
	}
	defer stmt.Close()
	for _, val := range member {
		allArgs := make([]interface{}, 0)
		allArgs = append(allArgs, val)
		allArgs = append(allArgs, args...)
		result, err := stmt.Exec(allArgs...)
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error()
		}
		code, msg, debugMsg = SqlAffectOne(result)
		if code != j.CodeSuccess {
			return
		}
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}

//事务执行多条类似相同SQL，$1为member的一个元素
func TxQueryMultiSQL(dealData func(mp map[string]interface{}) error, tx *sql.Tx,
	SQL string, member []string, args ...interface{}) (code int, msg, debugMsg string, res []map[string]interface{}) {
	stmt, err := tx.Prepare(SQL)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), nil
	}
	defer stmt.Close()
	for _, val := range member {
		allArgs := make([]interface{}, 0)
		allArgs = append(allArgs, val)
		allArgs = append(allArgs, args...)
		rows, err := stmt.Query(allArgs...)
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error(), nil
		}
		var data map[string]interface{}
		code, msg, debugMsg, data = RowsToMap(rows, dealData)
		if code != j.CodeSuccess {
			return
		}
		res = append(res, data)
	}
	return j.CodeSuccess, j.MsgSuccess, "", res
}

func TxQueryMapString(tx *sql.Tx, SQL string,
	args ...interface{}) (code int, msg, debugMsg string, data map[string]string) {
	stmt, err := tx.Prepare(SQL)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), nil
	}
	defer stmt.Close()
	rows, err := stmt.Query(args...)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), nil
	}
	return RowsToMapString(rows)
}

func TxQueryMap(f func(map[string]interface{}) error, tx *sql.Tx, SQL string,
	args ...interface{}) (code int, msg, debugMsg string, data map[string]interface{}) {
	stmt, err := tx.Prepare(SQL)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), nil
	}
	defer stmt.Close()
	rows, err := stmt.Query(args...)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), nil
	}
	return RowsToMap(rows, f)
}

func RewardCoins(coins int, description string, userID interface{}) (code int, msg, debugMsg string) {
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error()
	}
	defer func() {
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()
	sqlResult, err := pgsql.TxExec(tx, `update users set coins = coins + $2 where id = $1`, userID, coins)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error()
	}
	code, msg, debugMsg = SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return j.CodeRewardCoinsError, j.MsgRewardCoinsError, debugMsg
	}
	sqlResult, err = pgsql.TxExec(tx, `insert into accountBook(userID,createTime,amount,description)
		values($1,$2,$3,$4)`, userID, time.Now(), coins, description)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error()
	}
	code, msg, debugMsg = SqlAffectOne(sqlResult)
	if code != j.CodeSuccess {
		return j.CodeRewardCoinsError, j.MsgRewardCoinsError, debugMsg
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}
