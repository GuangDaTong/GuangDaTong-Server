package my

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func DeleteFavoriteSchoolDataExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	_, err := db.Exec(`
	insert into favoriteGuidance(userID,createTime,objectCreateTime,title,objectID,id) values
	($1,now(),now(),'guidance',1,1),
	($1,now(),now(),'guidance',2,2) on conflict do nothing;
	`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`insert into favoriteSummary(userID,createTime,objectCreateTime,title,objectID,id) values
	($1,now(),now(),'summary',1,1),
	($1,now(),now(),'summary',2,2) on conflict do nothing;
	`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`insert into favoriteClub(userID,createTime,objectCreateTime,title,objectID,id) values
	($1,now(),now(),'club',1,1),
	($1,now(),now(),'club',2,2) on conflict do nothing;
	`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`insert into favoriteLook(userID,createTime,objectCreateTime,title,objectID,id) values
	($1,now(),now(),'look',1,1),
	($1,now(),now(),'look',2,2) on conflict do nothing;
	`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.DeleteFavoriteJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2"},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2"},
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2"},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeDBExecNotEffect),
				Msg:  j.MsgDBExecNotEffect,
			},
			Name: "db exec not effect",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2", "a"},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "empty IDs",
		},
	}
}
func testDeleteFavoriteSchoolData(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
