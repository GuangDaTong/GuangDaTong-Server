package my

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listMyAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListMyAskJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListMyAsk(reqJson.Sign, reqJson.OrderBy, reqJson.Order,
		reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListMyAsk(sign, orderBy, order string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(` select ask.id "id",ask.createTime "createTime",ask.publisherID "publisherID",users.nickname,
	users.icon "icon",users.gender "gender",ask.reward "reward",ask.zan "zan",ask.commentID "commentID",ask.title,
	ask.typee "type",ask.label "label",ask.content "content",ask.image "image",ask.commentCount "commentCount",
	ask.thumbnailImage "thumbnailImage",count(zanAsk.userID) "isZan",count(favoriteAsk.userID) "isFavorite"
	from ask inner join users on (ask.publisherID = $3 and ask.publisherID=users.id)
	left outer join zanAsk on (zanAsk.objectID = ask.id and zanAsk.userID = $3)
	left outer join favoriteAsk on (favoriteAsk.objectID = ask.id and favoriteAsk.userID = $3)
	group by ask.id,users.nickname,users.icon,users.gender
	order by "`+orderBy+`" `+order+` limit $1 offset $2;`, pageSize, pageIndex*pageSize, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapImgAndIcon)
	if code != j.CodeSuccess {
		return
	}
	for _, val := range data {
		val["link"] = config.Host + mewNet.DataLink + "ask"
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
