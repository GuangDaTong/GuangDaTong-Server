package my

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listFavoriteGuidance(w http.ResponseWriter, r *http.Request) {
	listFavoriteSchoolData(w, r, "guidance")
}
func listFavoriteSummary(w http.ResponseWriter, r *http.Request) {
	listFavoriteSchoolData(w, r, "summary")
}
func listFavoriteClub(w http.ResponseWriter, r *http.Request) {
	listFavoriteSchoolData(w, r, "club")
}
func listFavoriteLook(w http.ResponseWriter, r *http.Request) {
	listFavoriteSchoolData(w, r, "look")
}
func listFavoriteSchoolData(w http.ResponseWriter, r *http.Request, table string) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListFavoriteJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListFavoriteSchoolData(table, reqJson.Sign, reqJson.OrderBy, reqJson.Order,
		reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListFavoriteSchoolData(table, sign, orderBy, order string, pageIndex,
	pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select favorite.objectCreateTime "createTime",favorite.id "favorID",
	favorite.title,favorite.objectID "id",data.zan,readCount "readCount",count(zan.userID) "isZan"
	from favorite`+table+` favorite
	left outer join zan`+table+` zan on (zan.objectID = favorite.objectID and zan.userID = $3)
	left outer join `+table+` data on (favorite.objectID = data.id)
	where favorite.userID = $3
	group by favorite.createTime,favorite.objectCreateTime,favorite.title,favorite.objectID,zan,"readCount",favorite.id
	order by favorite.createTime `+order+` limit $1 offset $2;`, pageSize, pageIndex*pageSize, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapDoNotThing)
	if code != j.CodeSuccess {
		return
	}
	for _, val := range data {
		val["link"] = config.Host + mewNet.DataLink + table
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
