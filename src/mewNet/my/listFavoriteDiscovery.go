package my

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listFavoritePartTimeJob(w http.ResponseWriter, r *http.Request) {
	listFavoriteDiscovery(w, r, "partTimeJob")
}
func listFavoriteActivity(w http.ResponseWriter, r *http.Request) {
	listFavoriteDiscovery(w, r, "activity")
}
func listFavoriteTraining(w http.ResponseWriter, r *http.Request) {
	listFavoriteDiscovery(w, r, "training")
}

func listFavoriteDiscovery(w http.ResponseWriter, r *http.Request, table string) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListFavoriteJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListFavoriteDiscovery(table, reqJson.Sign, reqJson.OrderBy, reqJson.Order,
		reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListFavoriteDiscovery(table, sign, orderBy, order string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select favorite.icon,favorite.title,favorite.id "favorID",
	favorite.fee,favorite.position,favorite.objectCreateTime "createTime",
	favorite.description,favorite.objectID "id",zan,readCount "readCount",count(zan.userID) "isZan"
	from favorite`+table+` favorite	
	left outer join zan`+table+` zan on(favorite.objectID = zan.objectID and zan.userID = $3)
	left outer join `+table+` data on (favorite.objectID = data.id)
	where favorite.userID = $3
	group by favorite.createTime,favorite.icon,favorite.title,readCount,favorite.id,
	favorite.fee,favorite.position,favorite.objectCreateTime,favorite.description,favorite.objectID,zan
	order by favorite.createTime `+order+` limit $1 offset $2;`,
		pageSize, pageIndex*pageSize, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	for _, val := range data {
		val["link"] = config.Host + mewNet.DataLink + table
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
