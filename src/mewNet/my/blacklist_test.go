package my

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func BlacklistExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err := pgsql.Exec(`delete from blacklist where ownerID = $1 and userID = $2`, testUserID, "1")
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.BlacklistJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign:   j.Md5Encode(config.Salt, token),
				UserID: "1",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				UserID: "1",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:   j.Md5Encode(config.Salt, token),
				UserID: "-1",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:   j.Md5Encode(config.Salt, token),
				UserID: "1",
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeHaveBeenBlacklist),
				Msg:  j.MsgHaveBeenBlacklist,
			},
			Name: "have been blacklist",
		},
	}
}
func testBlacklist(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
