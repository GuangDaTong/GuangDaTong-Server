package my

import (
	"config"
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listFavoriteAsk(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListFavoriteJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListFavoriteAsk(reqJson.Sign, reqJson.OrderBy, reqJson.Order,
		reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListFavoriteAsk(sign, orderBy, order string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`
	select 
	favorite.objectID "id",
	objectCreateTime "createTime",
	favorite.publisherID "publisherID",
	users.nickname,
	users.icon,
	users.gender,
	favorite.typee "type",
	favorite.title "title",
	favorite.content,
	favorite.reward,
	favorite.label,
	favorite.id "favorID",
	count(zanAsk.userID) "isZan"
	from
	favoriteAsk favorite inner join users on (favorite.publisherID = users.id)
	left outer join ask on (favorite.objectID = ask.id)
	left outer join zanAsk on (zanAsk.objectID = favorite.objectID and zanAsk.userID = $3)
	where favorite.userID = $3
	group by favorite.objectID,favorite.typee,users.icon,users.nickname,favorite.publisherID,favorite.title,favorite.id,
	users.gender,favorite.content,favorite.objectCreateTime,favorite.createTime,ask.zan,favorite.reward,favorite.label
	order by favorite.createTime `+order+` limit $1 offset $2;`,
		pageSize, pageIndex*pageSize, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	for _, val := range data {
		val["link"] = config.Host + mewNet.DataLink + "ask"
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
