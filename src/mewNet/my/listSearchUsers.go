package my

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func ListSearchUsers(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListSearchUsersJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListSearchUsers(reqJson.Sign, reqJson.Key, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListSearchUsers(sign, key string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, _ = mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeKey(key)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`
	select
	id,
	nickname,
	gender,
	icon,
	college,
	school
	from users 
	where nickname like $1
	order by createTime desc
	limit $2 offset $3`, "%"+key+"%", pageSize, pageIndex*pageSize)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	return j.CodeSuccess, j.MsgSuccess, "", result
}
