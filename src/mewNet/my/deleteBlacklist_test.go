package my

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	"time"
	j "utils/judger"
	"utils/pgsql"
)

func DeleteBlacklistExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err := pgsql.Exec(`insert into blacklist(ownerID,userID,createTime) values($1,$2,$3)
	on conflict do nothing`, testUserID, "1", time.Now())
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.DeleteBlacklistJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
				IDs:  []string{"1"},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs: []string{"1"},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
				IDs:  []string{"-1"},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
				IDs:  []string{"1"},
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeNotInBlacklist),
				Msg:  j.MsgNotInBlackList,
			},
			Name: "not in blacklist",
		},
	}
}
func testDeleteBlacklist(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
