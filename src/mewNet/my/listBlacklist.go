package my

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listBlacklist(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListBlacklistJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListBlacklist(reqJson.Sign)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListBlacklist(sign string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`
		select 
		u.id,
		u.icon,
		u.nickname,
		b.createTime "createTime"
		from blacklist b
		inner join users u on (b.userID = u.id)
		where b.ownerID = $1
		order by b.createTime desc`, userID)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "data", data)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
