package my

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func deleteFavoriteGuidance(w http.ResponseWriter, r *http.Request) {
	deleteFavoriteSchoolData(w, r, "guidance")
}
func deleteFavoriteSummary(w http.ResponseWriter, r *http.Request) {
	deleteFavoriteSchoolData(w, r, "summary")
}
func deleteFavoriteClub(w http.ResponseWriter, r *http.Request) {
	deleteFavoriteSchoolData(w, r, "club")
}
func deleteFavoriteLook(w http.ResponseWriter, r *http.Request) {
	deleteFavoriteSchoolData(w, r, "look")
}
func deleteFavoriteSchoolData(w http.ResponseWriter, r *http.Request, table string) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.DeleteFavoriteJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealDeleteFavoriteSchoolData(table, reqJson.Sign, reqJson.IDs)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealDeleteFavoriteSchoolData(table, sign string, IDs []string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeIDs(IDs)
	if code != j.CodeSuccess {
		return
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}
	defer func() {
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()
	code, msg, debugMsg = mewNet.TxExecMultiSQL(tx, `delete from favorite`+table+` where 
	id = $1 and userID = $2`, IDs, userID)
	if code != j.CodeSuccess {
		return
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
