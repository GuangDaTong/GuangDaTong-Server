package my

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func ListFriendsExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err := pgsql.Exec(`insert into friends(userID,friendID,createTime) values
	($1,1,now()),($1,2,now()) on conflict do nothing`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	_, err = pgsql.Exec(`insert into blacklist(ownerID,userID,createTime) values
	($1,1,now()) on conflict do nothing`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.ListFriendsJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				PageSize:  10,
				PageIndex: 0,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageSize:  10,
				PageIndex: 0,
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageSize:  10,
				PageIndex: -1,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageSize:  1000,
				PageIndex: 1,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageSize error",
		},
	}
}
func testListFriends(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
