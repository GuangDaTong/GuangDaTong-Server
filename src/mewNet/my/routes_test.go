package my

import (
	"config"
	"encoding/json"
	"mewNet"
	"modal"
	"net/http"
	"testing"
)

var TestExamples = []TestJSON{
	{
		Name:       "accountBook",
		Method:     "POST",
		Pattern:    "/accountBook",
		SetExample: AccountBookExample,
		Test:       testAccountBook,
	},
	{
		Name:       "listMyAsk",
		Method:     "POST",
		Pattern:    "/list/myAsk",
		SetExample: ListMyAskExample,
		Test:       testListMyAsk,
	},
	{
		Name:       "listFriends",
		Method:     "POST",
		Pattern:    "/list/friends",
		SetExample: ListFriendsExample,
		Test:       testListFriends,
	},
	{
		Name:       "deleteFriends",
		Method:     "POST",
		Pattern:    "/delete/friends",
		SetExample: DeleteFriendsExample,
		Test:       testDeleteFriends,
	},
	{
		Name:       "listFavoritePartTimeJob",
		Method:     "POST",
		Pattern:    "/list/favorite/partTimeJob",
		SetExample: ListFavoriteDiscoveryExample,
		Test:       testListFavoriteDiscovery,
	},
	{
		Name:       "listFavoriteTraining",
		Method:     "POST",
		Pattern:    "/list/favorite/training",
		SetExample: ListFavoriteDiscoveryExample,
		Test:       testListFavoriteDiscovery,
	},
	{
		Name:       "listFavoriteActivity",
		Method:     "POST",
		Pattern:    "/list/favorite/activity",
		SetExample: ListFavoriteDiscoveryExample,
		Test:       testListFavoriteDiscovery,
	},
	{
		Name:       "deleteFavoritePartTimeJob",
		Method:     "POST",
		Pattern:    "/delete/favorite/partTimeJob",
		SetExample: DeleteFavoriteDiscoveryExample,
		Test:       testDeleteFavoriteDiscovery,
	},
	{
		Name:       "deleteFavoriteActivity",
		Method:     "POST",
		Pattern:    "/delete/favorite/activity",
		SetExample: DeleteFavoriteDiscoveryExample,
		Test:       testDeleteFavoriteDiscovery,
	},
	{
		Name:       "deleteFavoriteTraining",
		Method:     "POST",
		Pattern:    "/delete/favorite/training",
		SetExample: DeleteFavoriteDiscoveryExample,
		Test:       testDeleteFavoriteDiscovery,
	},
	{
		Name:       "listFavorietAsk",
		Method:     "POST",
		Pattern:    "/list/favorite/ask",
		SetExample: ListFavoriteAskExample,
		Test:       testListFavoriteAsk,
	},
	{
		Name:       "deleteFavoriteAsk",
		Method:     "POST",
		Pattern:    "/delete/favorite/ask",
		SetExample: DeleteFavoriteAskExample,
		Test:       testDeleteFavoriteAsk,
	},
	{
		Name:       "listFavoriteGuidance",
		Method:     "POST",
		Pattern:    "/list/favorite/guidance",
		SetExample: ListFavoriteSchoolDataExample,
		Test:       testListFavoriteSchoolData,
	},
	{
		Name:       "listFavoriteSummary",
		Method:     "POST",
		Pattern:    "/list/favorite/summary",
		SetExample: ListFavoriteSchoolDataExample,
		Test:       testListFavoriteSchoolData,
	},
	{
		Name:       "listFavoriteClub",
		Method:     "POST",
		Pattern:    "/list/favorite/club",
		SetExample: ListFavoriteSchoolDataExample,
		Test:       testListFavoriteSchoolData,
	},
	{
		Name:       "listFavoriteLook",
		Method:     "POST",
		Pattern:    "/list/favorite/look",
		SetExample: ListFavoriteSchoolDataExample,
		Test:       testListFavoriteSchoolData,
	},
	{
		Name:       "deleteFavoriteGuidance",
		Method:     "POST",
		Pattern:    "/delete/favorite/guidance",
		SetExample: DeleteFavoriteSchoolDataExample,
		Test:       testDeleteFavoriteSchoolData,
	},
	{
		Name:       "deleteFavoriteSummary",
		Method:     "POST",
		Pattern:    "/delete/favorite/summary",
		SetExample: DeleteFavoriteSchoolDataExample,
		Test:       testDeleteFavoriteSchoolData,
	},
	{
		Name:       "deleteFavoriteClub",
		Method:     "POST",
		Pattern:    "/delete/favorite/club",
		SetExample: DeleteFavoriteSchoolDataExample,
		Test:       testDeleteFavoriteSchoolData,
	},
	{
		Name:       "deleteFavoriteLook",
		Method:     "POST",
		Pattern:    "/delete/favorite/look",
		SetExample: DeleteFavoriteSchoolDataExample,
		Test:       testDeleteFavoriteSchoolData,
	},
	{
		Name:       "blacklist",
		Method:     "POST",
		Pattern:    "/blacklist",
		SetExample: BlacklistExample,
		Test:       testBlacklist,
	},
	{
		Name:       "listBlacklist",
		Method:     "POST",
		Pattern:    "/list/blacklist",
		SetExample: ListBlacklistExample,
		Test:       testListBlacklist,
	},
	{
		Name:       "deleteBlacklist",
		Method:     "POST",
		Pattern:    "/delete/blacklist",
		SetExample: DeleteBlacklistExample,
		Test:       testDeleteBlacklist,
	},
	{
		Name:       "ListSearchUsers",
		Method:     "POST",
		Pattern:    "/list/search/users",
		SetExample: ListSearchUsersExample,
		Test:       testListSearchUsers,
	},
}

func TestRoute(*testing.T) {
	basePath := "../../"
	mewNet.TmplPath = basePath + mewNet.TmplPath
	route := mewNet.NewRouter()
	go func() {
		err := http.ListenAndServe(config.Socket, route)
		if err != nil {
			panic(err.Error())
		}
	}()
	modal.TestRoute(InitSQL, &examplePath, TestExamples, basePath)
	//modal.TestRouteBench(InitSQL, &examplePath, TestExamples, basePath)
}

func ToBytes(v interface{}) []byte {
	bytes, err := json.Marshal(v)
	if err != nil {
		panic(err.Error())
	}
	return bytes
}

var testExamples = modal.RunTestExamples
var examplePath = "debugExamples"
var testHtmlExamples = modal.RunTestHtmlExamples

type TestJSON = modal.TestJSON
type TestExample = modal.TestExample
type RespJSON = modal.RespJSON
