package my

import "modal"
import "mewNet"

type Route = modal.Route

var Routes = []Route{
	Route{
		"accountBook",
		"POST",
		"/accountBook",
		accountBook,
	},
	Route{
		"listFriends",
		"POST",
		"/list/friends",
		listFriends,
	},
	Route{
		"deleteFriends",
		"POST",
		"/delete/friends",
		deleteFriends,
	},
	Route{
		"listMyAsk",
		"POST",
		"/list/myAsk",
		listMyAsk,
	},
	Route{
		"listFavoritePartTimeJob",
		"POST",
		"/list/favorite/partTimeJob",
		listFavoritePartTimeJob,
	},
	Route{
		"listFavoriteTraining",
		"POST",
		"/list/favorite/training",
		listFavoriteTraining,
	},
	Route{
		"listFavoriteActivity",
		"POST",
		"/list/favorite/activity",
		listFavoriteActivity,
	},
	Route{
		"deleteFavoritePartTimeJob",
		"POST",
		"/delete/favorite/partTimeJob",
		deleteFavoritePartTimeJob,
	},
	Route{
		"deleteFavoriteActivity",
		"POST",
		"/delete/favorite/activity",
		deleteFavoriteActivity,
	},
	Route{
		"deleteFavoriteTraining",
		"POST",
		"/delete/favorite/training",
		deleteFavoriteTraining,
	},
	Route{
		"listFavoriteAsk",
		"POST",
		"/list/favorite/ask",
		listFavoriteAsk,
	},
	Route{
		"deleteFavoriteAsk",
		"POST",
		"/delete/favorite/ask",
		deleteFavoriteAsk,
	},
	Route{
		"listFavoriteGuidance",
		"POST",
		"/list/favorite/guidance",
		listFavoriteGuidance,
	},
	Route{
		"listFavoriteSummary",
		"POST",
		"/list/favorite/summary",
		listFavoriteSummary,
	},
	Route{
		"listFavoriteClub",
		"POST",
		"/list/favorite/club",
		listFavoriteClub,
	},
	Route{
		"listFavoriteLook",
		"POST",
		"/list/favorite/look",
		listFavoriteLook,
	},
	Route{
		"deleteFavoriteGuidance",
		"POST",
		"/delete/favorite/guidance",
		deleteFavoriteGuidance,
	},
	Route{
		"deleteFavoriteSummary",
		"POST",
		"/delete/favorite/summary",
		deleteFavoriteSummary,
	},
	Route{
		"deleteFavoriteClub",
		"POST",
		"/delete/favorite/club",
		deleteFavoriteClub,
	},
	Route{
		"deleteFavoriteLook",
		"POST",
		"/delete/favorite/look",
		deleteFavoriteLook,
	},
	Route{
		"blacklist",
		"POST",
		"/blacklist",
		blacklist,
	},
	Route{
		"listBlacklist",
		"POST",
		"/list/blacklist",
		listBlacklist,
	},
	Route{
		"deleteBlacklist",
		"POST",
		"/delete/blacklist",
		deleteBlacklist,
	},
	Route{
		"ListSearchUsers",
		"POST",
		"/list/search/users",
		ListSearchUsers,
	},
}

func init() {
	mewNet.AppendRoute(Routes)
}
