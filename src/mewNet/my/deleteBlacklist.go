package my

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func deleteBlacklist(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.DeleteBlacklistJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealDeleteBlacklist(reqJson.Sign, reqJson.IDs)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealDeleteBlacklist(sign string, IDs []string) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, ownerID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgeIDs(IDs)
	if code != j.CodeSuccess {
		return
	}
	db := pgsql.GetDB()
	defer pgsql.CloseDB(db)
	tx, err := db.Begin()
	if err != nil {
		return j.CodeBeginTxError, j.MsgBeginTxError, err.Error(), result
	}
	defer func() {
		if code == j.CodeSuccess {
			err := tx.Commit()
			if err != nil {
				code, msg, debugMsg = j.CodeDBCommitFailed, j.MsgDBCommitFailed, err.Error()
				return
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			code, msg, debugMsg = j.CodeDBRollbackFailed, j.MsgDBRollbackFailed, err.Error()
			return
		}
	}()
	stmt, err := tx.Prepare(`delete from blacklist where ownerID = $1 and userID = $2`)
	if err != nil {
		return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
	}
	defer stmt.Close()
	for _, val := range IDs {
		sqlResult, err := stmt.Exec(ownerID, val)
		if err != nil {
			return j.CodeDBExecError, j.MsgDBExecError, err.Error(), result
		}
		code, msg, debugMsg = mewNet.SqlAffectOne(sqlResult)
		if code == j.CodeDBExecNotEffect {
			return j.CodeNotInBlacklist, j.MsgNotInBlackList, debugMsg, result
		} else if code != j.CodeSuccess {
			return
		}
	}
	return j.CodeSuccess, j.MsgSuccess, "", result
}
