package my

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func accountBook(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.AccountBookJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealAccountBook(reqJson.Sign, reqJson.OrderBy, reqJson.Order,
		reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealAccountBook(sign, orderBy, order string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, orderBy = j.JudgeOrderBy(orderBy)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, order = j.JudgeOrder(order)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`select createTime "createTime",amount,description from accountBook where
	userID = $1 order by "`+orderBy+`" `+order+` limit $2 offset $3;`, userID, pageSize, pageIndex*pageSize)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, func(mp map[string]interface{}) error {
		amount, ok := mp["amount"].(string)
		if !ok {
			return mewNet.ErrAssertString
		}
		if len(amount) > 0 && amount[0] != '-' {
			mp["amount"] = "+" + amount
		}
		return nil
	})
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	mewNet.AddDataToResult(result, "orderBy", orderBy)
	mewNet.AddDataToResult(result, "order", order)
	return j.CodeSuccess, j.MsgSuccess, "", result
}
