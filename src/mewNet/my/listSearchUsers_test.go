package my

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
)

func ListSearchUsersExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.ListSearchUsersJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt, token),
				Key:       "y",
				PageIndex: 0,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Key:       "y",
				PageIndex: 0,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt, token),
				PageIndex: 0,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeEmptySearchKey),
				Msg:  j.MsgEmptySearchKey,
			},
			Name: "key is empty",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt, token),
				Key:       "y",
				PageIndex: -1,
				PageSize:  3,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign:      j.Md5Encode(config.Salt, token),
				Key:       "y",
				PageIndex: 0,
				PageSize:  0,
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageSize error",
		},
	}
}
func testListSearchUsers(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
