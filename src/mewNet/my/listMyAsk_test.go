package my

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
)

func ListMyAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	type exampleJSON = modal.ListMyAskJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "default orderBy,order success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  3,
				OrderBy:   "createTime",
				Order:     "desc",
				Sign:      j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: -1,
				PageSize:  3,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageIndexError),
				Msg:  j.MsgPageIndexError,
			},
			Name: "pageIndex error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  301,
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodePageSizeError),
				Msg:  j.MsgPageSizeError,
			},
			Name: "pageSize error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  300,
				OrderBy:   "time",
				Order:     "desc",
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderByError),
				Msg:  j.MsgOrderByError,
			},
			Name: "order by error",
		},
		{
			Request: ToBytes(&exampleJSON{
				PageIndex: 0,
				PageSize:  300,
				OrderBy:   "createTime",
				Order:     "esc",
				Sign:      j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeOrderError),
				Msg:  j.MsgOrderError,
			},
			Name: "order error",
		},
	}
}
func testListMyAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
