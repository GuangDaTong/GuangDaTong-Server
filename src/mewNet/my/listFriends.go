package my

import (
	"mewNet"
	"modal"
	"net/http"
	j "utils/judger"
	"utils/pgsql"
)

func listFriends(w http.ResponseWriter, r *http.Request) {
	var (
		code     = j.CodeSuccess
		debugMsg = ""
		msg      = j.MsgSuccess
		result   interface{}
	)
	defer func() {
		mewNet.WriteRespJSON(w, r, code, msg, debugMsg, result)
	}()
	reqJson := &modal.ListFriendsJSON{}
	code, msg, debugMsg = j.GetReqJSON(r, reqJson)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg, result = dealListFriends(reqJson.Sign, reqJson.PageIndex, reqJson.PageSize)
	mewNet.AddReqJsonToResult(result, reqJson)
}
func dealListFriends(sign string, pageIndex, pageSize int64) (code int, msg, debugMsg string, result interface{}) {
	result = map[string]interface{}{}
	code, msg, debugMsg, userID := mewNet.ExistToken(sign)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageIndex(pageIndex)
	if code != j.CodeSuccess {
		return
	}
	code, msg, debugMsg = j.JudgePageSize(pageSize)
	if code != j.CodeSuccess {
		return
	}
	rows, err := pgsql.Query(`
	select 
	f.friendID "id",
	f.createTime "createTime",
	users.icon,
	users.nickname 
	from friends f
	inner join users on (f.friendID = users.id)
	where f.userID = $1
	limit $2 offset $3;`, userID, pageSize, pageIndex*pageSize)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), result
	}
	code, msg, debugMsg, data := mewNet.RowsToMapArray(rows, mewNet.DealMapIcon)
	if code != j.CodeSuccess {
		return
	}
	mewNet.AddDataToResult(result, "data", data)
	mewNet.AddDataToResult(result, "pageIndex", pageIndex)
	mewNet.AddDataToResult(result, "pageSize", pageSize)
	mewNet.AddDataToResult(result, "pageCount", len(data))
	return j.CodeSuccess, j.MsgSuccess, "", result
}
