package my

import (
	"config"
	"fmt"
	"mewNet"
	"modal"
	"os"
	j "utils/judger"
	"utils/pgsql"
)

func DeleteFavoriteAskExample() []TestExample {
	code, msg, debugMsg, token := mewNet.SetToken(testUserID)
	if code != j.CodeSuccess {
		panic(msg + " " + debugMsg)
	}
	_, err := pgsql.Exec(`
	insert into favoriteAsk(userID,createTime,objectID,publisherID,objectCreateTime,content,typee,label,reward,id) values
	($1,now(),1,1,now(),'`+mewNet.RandString(10)+`','急问','学习',0,1),
	($1,now(),2,1,now(),'`+mewNet.RandString(10)+`','普问','学习',0,2) on conflict do nothing;
	`, testUserID)
	if err != nil {
		panic(err.Error())
	}
	type exampleJSON = modal.DeleteFavoriteJSON
	return []TestExample{
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2"},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeSuccess),
				Msg:  j.MsgSuccess,
			},
			Name: "success",
		},
		{
			Request: []byte(""),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeRequestJsonError),
				Msg:  j.MsgRequestJsonError,
			},
			Name: "request json error",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2"},
				Sign: j.Md5Encode(config.Salt),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeTokenNotExist),
				Msg:  j.MsgTokenNotExist,
			},
			Name: "token not exist",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2"},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeDBExecNotEffect),
				Msg:  j.MsgDBExecNotEffect,
			},
			Name: "db exec not effect",
		},
		{
			Request: ToBytes(&exampleJSON{
				IDs:  []string{"1", "2", "a"},
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "id error",
		},
		{
			Request: ToBytes(&exampleJSON{
				Sign: j.Md5Encode(config.Salt, token),
			}),
			Response: &RespJSON{
				Code: fmt.Sprint(j.CodeIDError),
				Msg:  j.MsgIDError,
			},
			Name: "empty IDs",
		},
	}
}
func testDeleteFavoriteAsk(testJson *TestJSON) {
	w, err := os.Create("./" + examplePath + "/" + testJson.Name + ".txt")
	if err != nil {
		panic(err.Error())
	}
	defer w.Close()
	fmt.Fprintln(w, "----------"+testJson.Name+" example-----------")
	for _, val := range testJson.TestExample {
		testExamples(val, testJson, w)
	}
}
