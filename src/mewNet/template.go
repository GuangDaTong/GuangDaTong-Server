package mewNet

import (
	"bytes"
	"config"
	"fmt"
	"logs"
	"net/http"
	"text/template"
	j "utils/judger"
	"utils/pgsql"
)

var (
	tmpl            = &template.Template{}
	TmplPath        = "view/"
	pageServerError = "<html><body><h1>500 server error</h1></body></html>"
)

func InitTemplate() {
	var err error
	tmpl, err = tmpl.ParseGlob(TmplPath + "*.html")
	if err != nil {
		panic(err.Error())
	}
	rows, err := pgsql.Query(`select url,name,content from template 
		where url is not null and publishStatus = '已发布';`)
	if err != nil {
		panic(err.Error())
	}
	code, msg, debugMsg, data := RowsToMapArray(rows, DealMapDoNotThing)
	if code != j.CodeSuccess {
		panic(fmt.Sprint(code, msg, debugMsg))
	}
	for _, val := range data {
		name, ok := val["name"].(string)
		if !ok {
			panic(ErrAssertString.Error())
		}
		url, ok := val["url"].(string)
		if !ok {
			panic(ErrAssertString.Error())
		}
		content, ok := val["content"].(string)
		if !ok {
			panic(ErrAssertString.Error())
		}
		html := &bytes.Buffer{}
		tmpl.ExecuteTemplate(html, "template", &struct {
			Title   string
			Content string
			SrcHost string
		}{
			Title:   name,
			Content: content,
			SrcHost: config.SrcHost,
		})
		//logs.DebugPrint(html.String())
		tmpl.Parse(fmt.Sprintf("{{define %q}}\n%s\n{{end}}", url, html.String()))
		routes = append(routes, Route{
			Name:        name,
			Method:      "GET",
			Pattern:     url,
			HandlerFunc: serveTemplate,
		})
	}
}

func GetTmpl() *template.Template {
	return tmpl
}

func SetTmpl(t *template.Template) {
	tmpl = t
}

func serveTemplate(w http.ResponseWriter, r *http.Request) {
	err := tmpl.ExecuteTemplate(w, r.URL.Path, nil)
	if err != nil {
		logs.ErrorPrint(err.Error())
	}
}

func WriteData(w http.ResponseWriter, title, content, createTime, nickname, OSType string) {
	w.Header().Set("Content-Type", "text/html;charset=utf-8")
	err := tmpl.ExecuteTemplate(w, "data",
		struct {
			Title      string
			Content    string
			CreateTime string
			Nickname   string
			SrcHost    string
			OSType     string
		}{
			Title:      title,
			Content:    content,
			CreateTime: createTime,
			Nickname:   nickname,
			SrcHost:    config.SrcHost,
			OSType:     OSType,
		})
	if err != nil {
		w.Write([]byte(pageServerError))
		logs.ErrorPrint(err.Error())
		return
	}
}

func DealData(w http.ResponseWriter, r *http.Request, table string) {
	var (
		content    = "<h1>页面不存在或已删除</h1>出错信息: "
		title      = "页面不存在或已删除"
		createTime = ""
		penName    = ""
		OSType     = ""
	)
	defer func() {
		WriteData(w, title, content, createTime, penName, OSType)
	}()
	err := r.ParseForm()
	if err != nil {
		content += "parseForm error: " + err.Error()
		return
	}
	OSType = r.FormValue("OSType")
	id := r.FormValue("id")
	code, _, _ := j.JudgeID(id)
	if code != j.CodeSuccess {
		content += "id error,id: " + id
		return
	}
	rows, err := pgsql.Query(`
	update `+table+` 
	set readCount = readCount + 1 
	where id = $1
	returning content, title, createTime "createTime", penName "penName" `, id)
	if err != nil {
		content += "db query failed: " + err.Error()
		return
	}
	code, msg, debugMsg, data := RowsToMap(rows, DealMapFormatTime)
	if code != j.CodeSuccess {
		content = msg
		logs.ResultPrint(code, msg, debugMsg)
		return
	}
	createTime = data["createTime"].(string)
	title = data["title"].(string)
	content = data["content"].(string)
	penName = data["penName"].(string)
}
