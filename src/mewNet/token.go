package mewNet

import (
	"config"
	"fmt"
	"logs"
	"math/rand"
	"modal"
	"strings"
	"time"
	j "utils/judger"
	"utils/mewRedis"
	"utils/pgsql"

	"github.com/garyburd/redigo/redis"
)

var maxTokenTime = 60 * 60 * 24 * 7

/*func getCreateTimeByUserID(userID string) (code int, msg, debugMsg string, createTime string) {
	lastLoginTimeLock.Lock()
	defer lastLoginTimeLock.Unlock()
	createTime = lastLoginTime[userID]
	if createTime == "" {
		rows, err := pgsql.Query(`select createTime "createTime" from token where userID = $1`, userID)
		if err != nil {
			return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), ""
		}
		defer rows.Close()
		if rows.Next() {
			err := rows.Scan(&createTime)
			if err != nil {
				return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), ""
			}
		}
	}
	return j.CodeSuccess, j.MsgSuccess, "", createTime
}

func getCreateTimeByToken(token string) (code int, msg, debugMsg string, createTime string) {
	lastLoginTimeLock.Lock()
	defer lastLoginTimeLock.Unlock()
	createTime = lastLoginTime[token]
	if createTime == "" {
		rows, err := pgsql.Query(`select createTime "createTime" from token where token = $1`, token)
		if err != nil {
			return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), ""
		}
		defer rows.Close()
		if rows.Next() {
			err := rows.Scan(&createTime)
			if err != nil {
				return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), ""
			}
		}
	}
	return j.CodeSuccess, j.MsgSuccess, "", createTime
}

func rewardCoinsFormLogin(userID, token, createTime string, now *time.Time) {
	today := fmt.Sprint(now)[:10]
	lastLoginTimeLock.Lock()
	defer lastLoginTimeLock.Unlock()
	if lastLoginTime[userID] != "" && strings.Compare(lastLoginTime[userID][:10], today) == 0 {
		return
	}
	if createTime == "" || strings.Compare(createTime[:10], today) != 0 {
		code, msg, debugMsg := RewardCoins(5, "每日启动APP", userID)
		if code != j.CodeSuccess {
			logs.ErrorPrint(code, msg, debugMsg)
		}
	}
	lastLoginTime[userID] = today
	lastLoginTime[token] = today
}*/

//这里返回的token用作登录身份识别，可重复使用，maxTokenTime后失效
/*func SetToken(userID string) (code int, msg, debugMsg string, token string) {
	code, msg, debugMsg, createTime := getCreateTimeByUserID(userID)
	if code != j.CodeSuccess {
		return
	}
	token = j.Md5Encode(RandString(16), userID)
	now := time.Now()
	_, err := pgsql.Exec(`insert into token(token,userID,createTime) values($1,$2,$3)
	on conflict(userID) do update set token=$1,createTime=$3;`,
		j.Md5Encode(config.Salt, token), userID, now)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), ""
	}
	rewardCoinsFormLogin(userID, token, createTime, &now)
	return j.CodeSuccess, j.MsgSuccess, "", token
}*/

type firstLogin struct {
	userID     string
	token      string
	createTime time.Time
}

var firstLoginQueue = make(chan *firstLogin, 10000)

func rewardForFirstLogin(tmp *firstLogin) {
	code, msg, debugMsg := RewardCoins(5, "每日启动APP", tmp.userID)
	if code != j.CodeSuccess {
		logs.ErrorPrint(code, msg, debugMsg)
	}
}

func firstLoginProcess() {
	var tmp *firstLogin
	var err error
	var createTime string
	client := mewRedis.GetClient()
	for {
		tmp = <-firstLoginQueue
		createTime, err = redis.String(client.Do("HGET", "user:"+tmp.userID, "createTime"))
		if err != nil && err != redis.ErrNil {
			logs.ErrorPrint(err.Error())
			continue
		}
		if createTime == "" || strings.Compare(createTime[:10], fmt.Sprint(tmp.createTime)[:10]) != 0 {
			go rewardForFirstLogin(tmp)
		}
		_, err = client.Do("HSET", "user:"+tmp.userID, "createTime", tmp.createTime)
		if err != nil {
			logs.ErrorPrint(err.Error())
		}
	}
}

//这里返回的token用作登录身份识别，可重复使用，maxTokenTime后失效
func SetToken(userID string) (code int, msg, debugMsg string, token string) {
	client := mewRedis.GetClient()
	defer client.Close()
	//删除旧token,防止就token继续使用
	oldToken, err := redis.String(client.Do("HGET", "user:"+userID, "token"))
	if err != nil && err != redis.ErrNil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
	} else if err != redis.ErrNil {
		_, err = client.Do("DEL", "token:"+oldToken+":userID")
		if err != nil {
			return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
		}
	}

	//创建新token
	token = j.Md5Encode(RandString(16), userID)
	encodeToken := j.Md5Encode(config.Salt, token)
	client.Send("MULTI")
	client.Send("SET", "token:"+encodeToken+":userID", userID)
	client.Send("EXPIRE", "token:"+encodeToken+":userID", maxTokenTime)
	client.Send("HSET", "user:"+userID, "token", encodeToken)
	_, err = client.Do("EXEC")
	if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
	}
	firstLoginQueue <- &firstLogin{userID: userID, token: encodeToken, createTime: time.Now()}
	return j.CodeSuccess, j.MsgSuccess, "", token
}

//查询token是否存在，存在则返回userID
func ExistToken(token string) (code int, msg, debugMsg string, userID string) {
	userID, err := redis.String(mewRedis.Do("GET", "token:"+token+":userID"))
	if err == redis.ErrNil {
		return j.CodeTokenNotExist, j.MsgTokenNotExist, err.Error(), ""
	} else if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
	}
	firstLoginQueue <- &firstLogin{userID: userID, token: token, createTime: time.Now()}
	return j.CodeSuccess, j.MsgSuccess, "", userID
}

func GetUserByID(id string) (code int, msg, debugMsg string, user *modal.User) {
	rows, err := pgsql.Query(`
	select 
	account,
	nickname,
	icon,
	password,
	gender,
	coins,
	college,
	school,
	exp,
	zan,
	grade 
	from users
	where id = $1`, id)
	if err != nil {
		return j.CodeDBQueryError, j.MsgDBQueryError, err.Error(), nil
	}
	defer rows.Close()
	if !rows.Next() {
		return j.CodeTokenNotExist, j.MsgTokenNotExist, "query no rows,rows: no next", nil
	}
	var account, nickname, icon, password, gender, college, school, zan, grade []byte
	var exp, coins int
	err = rows.Scan(&account, &nickname, &icon, &password, &gender, &coins, &college, &school,
		&exp, &zan, &grade)
	level := GetLevel(exp)
	if err != nil {
		return j.CodeRowsScanError, j.MsgRowsScanError, err.Error(), nil
	}
	user = &modal.User{
		ID:       string(id),
		Account:  string(account),
		Nickname: string(nickname),
		Icon:     string(icon),
		Password: string(password),
		Gender:   string(gender),
		Coins:    coins,
		College:  string(college),
		School:   string(school),
		Exp:      exp,
		Level:    fmt.Sprint(level),
		Zan:      fmt.Sprint(zan),
		Grade:    string(grade),
	}
	return j.CodeSuccess, j.MsgSuccess, "", user
}

func GetUser(token string) (code int, msg, debugMsg string, user *modal.User) {
	code, msg, debugMsg, userID := ExistToken(token)
	if code != j.CodeSuccess {
		return
	}
	return GetUserByID(userID)
}

func DeleteToken(token string) (code int, msg, debugMsg string) {
	_, err := mewRedis.Do("DEL", "token:"+token+":userID")
	if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error()
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}

func RandString(n int) string {
	s := ""
	for i := 0; i < n; i++ {
		s += string((rand.Uint32() & 63) + 48)
	}
	return s
}

var verifyCodeTokenLiveTime = 60 * 5 //5min

//这里返回的token用于将短信验证码转换为服务器验证token，一次验证成功后即失效，maxVerifyCodeTokenLiveTime后也失效
func AddVerifyCodeToken(account string) (code int, msg, debugMsg string, token string) {
	token = j.Md5Encode(account, fmt.Sprint(time.Now()))
	client := mewRedis.GetClient()
	defer client.Close()
	err := client.Send("MULTI")
	if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
	}
	err = client.Send("SET", "verifyToken:"+j.Md5Encode(config.Salt, token), account)
	if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
	}
	err = client.Send("EXPIRE", "verifyToken:"+j.Md5Encode(config.Salt, token), verifyCodeTokenLiveTime)
	if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
	}
	_, err = client.Do("EXEC")
	if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error(), ""
	}
	return j.CodeSuccess, j.MsgSuccess, "", token
}

func CheckVerifyCodeToken(account, token string) (code int, msg, debugMsg string) {
	account1, err := redis.String(mewRedis.Do("GET", "verifyToken:"+token))
	if err == redis.ErrNil || account1 == "" {
		return j.CodeVerifyTokenInvalid, j.MsgVerifyTokenInvalid, "token not exist"
	} else if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error()
	}
	if account != account1 {
		return j.CodeVerifyTokenInvalid, j.MsgVerifyTokenInvalid, "account isn't the owner of this token"
	}
	_, err = mewRedis.Do("DEL", "verifyToken:"+token)
	if err != nil {
		return j.CodeRedisDoError, j.MsgRedisDoError, err.Error()
	}
	return j.CodeSuccess, j.MsgSuccess, ""
}
