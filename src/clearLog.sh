clearLog() {
	for file in `ls $1`
	do 
		#echo $1$file
		if test -d $1$file
		then
			clearLog $1$file'/' 
		elif [[ $file == *.log ]]
		then
			rm $1$file
			echo 'rm '$1$file
		fi
	done
}

clearLog
