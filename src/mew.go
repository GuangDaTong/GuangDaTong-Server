package main

import (
	"config"
	"fmt"
	"logs"
	"mewNet"
	_ "mewNet/account"
	_ "mewNet/ask"
	_ "mewNet/discovery"
	_ "mewNet/message"
	_ "mewNet/my"
	_ "mewNet/other"
	_ "mewNet/school"
	"mewNet/service"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"time"
	"utils/dealImage"
	"utils/jpush"
	"utils/pgsql"

	"github.com/tabalt/gracehttp"
)

func Init() (string, http.Handler) {

	logs.Init(config.Test) //, config.ConfigPath+config.LogsConfigFile)
	pgsql.Init(config.Test, config.ConfigPath+config.PgsqlConfigFile)
	dealImage.InitBucket(config.Bucket)
	jpush.Init(config.Test)
	service.ClearToken()
	return config.Socket, mewNet.NewRouter()
}

func main() {
	go http.ListenAndServe(":8081", nil)
	config.Init("./config/mew.conf")
	//run(Init())
	graceRun(Init())
}

func graceRun(socket string, router http.Handler) {
	fmt.Println(fmt.Sprintf("Serving %s with pid %d", socket, os.Getpid()))
	err := gracehttp.ListenAndServe(socket, router)
	if err != nil {
		logs.ErrorPrint(err.Error(), "Server stoped!")
	}
}

// tcpKeepAliveListener sets TCP keep-alive timeouts on accepted
// connections. It's used by ListenAndServe and ListenAndServeTLS so
// dead TCP connections (e.g. closing laptop mid-download) eventually
// go away.
type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	//logs.AccessPrint(tc.RemoteAddr())
	tc.SetKeepAlive(true)
	tc.SetKeepAlivePeriod(3 * time.Minute)
	return tc, nil
}

func run(socket string, handler http.Handler) {
	server := http.Server{Addr: socket, Handler: handler}
	//server := http.Server{Addr: ":8000", Handler: router}
	//server := http.Server{Addr: ":6001", Handler: router}
	ln, err := net.Listen("tcp", server.Addr)
	if err != nil {
		panic(err.Error())
	}
	err = server.Serve(tcpKeepAliveListener{ln.(*net.TCPListener)})
	//err := http.ListenAndServe(config.Socket, router)
	if err != nil {
		logs.ErrorPrint("http server serve error:", err.Error())
	}
}
