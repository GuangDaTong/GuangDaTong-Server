go build mew.go
scp -r mew view debug.collegemew.com:~/mew/
scp -r public/*.png public/*.jpg public/*.ico public/script/ public/css/ debug.collegemew.com:~/mew/public/
scp -r mew view collegemew.com:~/release/mew/
scp -r public/*.png public/*.jpg public/*.ico public/script/ public/css/ collegemew.com:~/release/mew/public/
