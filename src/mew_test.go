package main

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"
)

func TestMain(*testing.T) {
	go main()
	time.Sleep(time.Second * 1)
	outputJSON()
}
func outputJSON() {
	a := make(map[string]interface{})
	a["code"] = 200
	a["message"] = "success"
	a["result"] = make(map[string]interface{})
	res := a["result"].(map[string]interface{})
	res["request"] = make(map[string]interface{})
	req := res["request"].(map[string]interface{})
	req["sign"] = "aalkdsfjasdkjf"
	req["account"] = "13800138000"
	req["password"] = "******"
	req["college"] = "计算机科学与教育软件学院"
	req["school"] = "广州大学"
	data, err := json.MarshalIndent(a, "", "  ")
	if err != nil {
		panic(err.Error())
	}
	file, err := os.Create("json.txt")
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintln(file, string(data))
}
