package config

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

var (
	Test            = true //是否为测试模式
	Salt            string
	Socket          string
	ConfigPath      string
	LogsConfigFile  string
	PgsqlConfigFile string
	Host            string
	SrcHost         string
	CurVersion      *Version
	MinVersion      *Version
	PublicFilePath  string
	RedisHost       string
	Bucket          string
	UseQiNiuYun     = true
	UseJPush        = true
)

type Version struct {
	a, b, c int
	String  string
}

func (v *Version) Scan(s string) (int, error) {
	v.String = s
	return fmt.Sscanf(s, "%d.%d.%d", &v.a, &v.b, &v.c)
}

var settings map[string]string = make(map[string]string, 0)

func Init(mewConf string) {
	settings["mewConfig"] = mewConf
	file, err := os.Open(mewConf)
	if err != nil {
		panic(err.Error())
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var s, key, value, tmp string
		s = scanner.Text()
		if len(s) <= 0 || s[0] == '#' {
			continue
		}
		n, err := fmt.Sscan(s, &key, &tmp, &value)
		if err != nil {
			panic("打开配置文件出错：" + err.Error())
		}
		if n != 3 {
			panic("打开配置文件出错：a = b格式有误")
		}
		if key == "test" {
			Test = (value == "true")
			continue
		}
		if Test {
			if !strings.HasPrefix(key, "test_") {
				continue
			}
			key = strings.TrimPrefix(key, "test_")
			_, ok := settings[key]
			if !ok {
				fmt.Println("error config:", key, value)
				continue
			}
			settings[key] = value
			continue
		}
		if strings.HasPrefix(key, "test_") {
			continue
		}
		_, ok := settings[key]
		if !ok {
			fmt.Println("error config:", key, value)
			continue
		}
		settings[key] = value
	}
	config()
}
func init() {
	settings["path"] = "./config/"
	settings["logsConfigFile"] = "logs.conf"
	settings["pgsqlConfigFile"] = "postgresql.conf"
	settings["socket"] = "0.0.0.0:7777"
	settings["salt"] = "mewmewstudio"
	settings["mewConfig"] = "null"
	settings["version"] = "1.0.0"
	settings["minVersion"] = "1.0.0"
	settings["host"] = "https://debug.guangdamiao.com/"
	settings["srcHost"] = "https://debug.guangdamiao.com/"
	settings["publicFilePath"] = "debugPublic/"
	settings["redisHost"] = "127.0.0.1:6378"
	settings["bucket"] = "debugpublic"
	config()
}
func config() {
	Salt = settings["salt"]
	if !Test {
		Salt = ""
	}
	Socket = settings["socket"]
	ConfigPath = settings["path"]
	LogsConfigFile = settings["logsConfigFile"]
	PgsqlConfigFile = settings["pgsqlConfigFile"]
	PublicFilePath = settings["publicFilePath"]
	RedisHost = settings["redisHost"]
	SrcHost = settings["srcHost"]
	Host = settings["host"]
	Bucket = settings["bucket"]
	CurVersion = &Version{}
	n, err := CurVersion.Scan(settings["version"])
	if err != nil {
		fmt.Println("version scan error:", err.Error())
	}
	if n != 3 {
		fmt.Println("version scan error: not scan 3 intenger")
	}
	MinVersion = &Version{}
	n, err = MinVersion.Scan(settings["minVersion"])
	if err != nil {
		fmt.Println("version scan error:", err.Error())
	}
	if n != 3 {
		fmt.Println("version scan error: not scan 3 intenger")
	}
	fmt.Println("mewConfig:", time.Now())
	fmt.Println("\tTest:", Test)
	for key, val := range settings {
		if key != "salt" {
			fmt.Println("\t", key, ":", val)
		}
	}

}
